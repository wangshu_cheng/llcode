# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def isUnivalTree(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        def helper(root,prev):
            if not root:
                return True
            if prev != root.val:
                return False
            left = helper(root.left, prev)
            right = helper(root.right,prev)
            
            return left and right

        return helper(root,root.val)