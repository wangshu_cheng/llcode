class Solution(object):
    def rangeSumBST(self, root, L, R):
        """
        :type root: TreeNode
        :type L: int
        :type R: int
        :rtype: int
        """
        self.total = 0
        
        def helper(root,L,R):
            if not root:
                return
            if root.val >= L and root.val <= R:
                self.total += root.val
            if root.val >=L:
                helper(root.left,L,R)
            if root.val <=R:
                helper(root.right,L,R)
        helper(root,L,R)
        return self.total