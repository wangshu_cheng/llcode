'''
Rotate an array of n elements to the right by k steps.

For example, with n = 7 and k = 3, the array [1,2,3,4,5,6,7] is rotated to [5,6,7,1,2,3,4].

Note:
Try to come up as many solutions as you can, there are at least 3 different ways
 to solve this problem.

[show hint]

Hint:
Could you do it in-place with O(1) extra space?
Related problem: Reverse Words in a String II

开始以为这个是partition sort题，后来发现不是。
Thinking: 

1 Bubble swap, but it's O(nk), too slow, should be O(n)

2.

    1. Divide the array two parts: 1,2,3,4 and 5, 6
    2. Reverse first part: 4,3,2,1,5,6
    3. Reverse second part: 4,3,2,1,6,5
    4. Reverse the whole array: 5,6,1,2,3,4

'''

class Solution(object):
    def rotate(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: void Do not return anything, modify nums in-place instead.
        """


       #method 1 bubble (Not  AC)
        if not nums:
            return
        
        k = k % len(nums)
        
        for i in xrange(k):
            for j in xrange(len(nums)-1,0,-1):
                nums[j],nums[j-1] = nums[j-1],nums[j]


        #method2 best, 三段式:
        def reverse(nums,start,end):
            while(start < end):
                nums[start],nums[end] = nums[end],nums[start]
                start +=1
                end -= 1
                
        if not nums:
            return
        
        k = k % len(nums)
        
        reverse(nums,0,len(nums) -k -1)
        reverse(nums,len(nums) -k, len(nums) -1)
        reverse(nums,0,len(nums) -1)





        # k = k % len(nums)
        # pre = 0
        # cur = 0
        # count = 0
        # for i in range(len(nums)):
        #     if count >= len(nums):
        #         break
        #     cur = i
        #     pre=nums[i]
        #     for j in range(len(nums)):
        #         next = (cur + k) % len(nums)
        #         nums[next],pre = pre,nums[next]
        #         cur = next
        #         count += 1
        #         if cur == i:
        #             break


