class Solution(object):
    def reverseString(self, s):
        """
        :type s: str
        :rtype: str
        """
        n = len(s)
        a1 = list(s)
        
        i,j = 0, n-1
        while i < j:
            tmp = a1[j]
            a1[j] = a1[i]
            a1[i] = tmp

            i += 1
            j -= 1
        return "".join(a1)

        