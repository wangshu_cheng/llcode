'''
Given two strings s and t, determine if they are isomorphic.

Two strings are isomorphic if the characters in s can be replaced to get t.

All occurrences of a character must be replaced with another character while preserving the order of characters. No two characters may map to the same character but a character may map to itself.

Example 1:

Input: s = "egg", t = "add"
Output: true
Example 2:

Input: s = "foo", t = "bar"
Output: false
Example 3:

Input: s = "paper", t = "title"
Output: true
'''

''''
 Key: No two characters may map to the same character ;

 考点： 理解题意； map
'''

class Solution(object):
    def isIsomorphic(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: bool
        """
        if s == None or t == None:
            return False
        if s == '' and t == '':
            return True
        if len(s) != len(t):
            return False
        
        map = {}
        
        for i in range(len(s)):
            c1,c2 = s[i],t[i]
            if c1 in map:
                if map[c1]  != c2:
                    return False
            else:
                if c2 in map.values(): # this means c1 != c2 , and c2 is already been mapped, false, like:  cde -> aba; a already been mapped by a, can not beem mapped by e again.
                    return False
                map[c1] = c2
                    
        return True
        
        