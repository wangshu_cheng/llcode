'''
There are two sorted arrays nums1 and nums2 of size m and n respectively.

Find the median of the two sorted arrays. The overall run time complexity should be O(log (m+n)).

Example 1:
nums1 = [1, 3]
nums2 = [2]

The median is 2.0
Example 2:
nums1 = [1, 2]
nums2 = [3, 4]

The median is (2 + 3)/2 = 2.5


Step1: check the total length of 2 array is odd or even, if odd, we need 
set k = len/2 + 1, else k = len/2

'''

# define kth smallest first
def kthsmallest(A, B, k):
    if len(A) == 0:
        return B[k - 1]
    elif len(B) == 0:
        return A[k - 1]

    if k == 1:
    	return min(A[0],B[0])

    #this is a skill, to assign big value if currrent array is
    # less than k/2, in this case, we must drop the other array,
    # so can set the compare mid value to a big data.
    mida = A[k/2 - 1] if (k/2 -1) < len(A) else 2**31
    midb = B[k/2 - 1] if (k/2 -1) < len(B) else 2**31 # this is to say
    
    if mida < midb:# if mid a < mid b, 要往B靠 than drop the smaller half
    	return kthsmallest(A[k/2 :],B,k - k/2)
    #if mida == midb, drop first k/2 A or B is same thing.
    else: # else, drop the bigger half
    	return kthsmallest(A,B[k/2:],k - k/2)

#define median:

def findMedianSortedArrays(A,B):
    LEN = len(A) + len(B)
    if LEN % 2 == 1:
        return kthsmallest(A,B,LEN/2 + 1)
    else:
        return (kthsmallest(A,B,LEN/2) + kthsmallest(A,B,LEN/2 + 1)) / 2.0


#test case:
A,B = [1,2,3,4,5,6], [2,3,4,5]

print findMedianSortedArrays(A,B)
'''