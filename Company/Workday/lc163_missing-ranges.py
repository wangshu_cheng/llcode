'''
Given a sorted integer array nums, where the range of elements are in the inclusive range [lower, upper], return its missing ranges.

Example:

Input: nums = [0, 1, 3, 50, 75], lower = 0 and upper = 99,
Output: ["2", "4->49", "51->74", "76->99"]


'''


class Solution(object):
    def findMissingRanges(self, nums, lower, upper):
        """
        :type nums: List[int]
        :type lower: int
        :type upper: int
        :rtype: List[str]
        """
        def getRange(lower,upper):
            if upper>=lower:
                if upper!= lower:
                    return [str(lower)+'->'+str(upper)]
                else:
                    return[str(lower)]
            return []
    
    
        start=lower
        res=[]
    
        for i,e in enumerate(nums):
            if e<start:
                continue
            #this trick is to skip the continuous value
            if e==start:
                start+=1
                continue
            #at this point, the e must > start+1, the gap exist
            res+=getRange(start,nums[i]-1)
            start=nums[i]+1
        #finally,check the last value:
        if start<=upper:res+=getRange(start,upper)
    
        return res
        