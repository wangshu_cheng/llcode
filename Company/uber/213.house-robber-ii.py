class Solution(object):
    def rob(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        def rob_no_circle(nums):
            dp={};
            if len(nums)==0:return 0
            if len(nums)==1:return nums[0]
            dp[0]=nums[0]
            dp[1]=max(nums[0],nums[1])
        
            for i in range(2,len(nums)):
                dp[i] = max((dp[i-2]+nums[i]),dp[i-1])
            return dp[len(nums)-1]
        
        #keep the rob house1 solution, now we want to break the circle
        #If we don't rob house0, there is a result, if we do, there is another, just get the maximum one.
        
        #base case:
        if len(nums)==0:return 0
        if len(nums)==1:return nums[0]
        #case 1 house 1 robbed
        result1=rob_no_circle(nums[1:])
        #case 2 house 1 not robbed
        result2=rob_no_circle(nums[:-1])
        return max(result1,result2)
        
