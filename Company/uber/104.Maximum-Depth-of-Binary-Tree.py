# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def maxDepth(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        #bfs:
        if not root:
            return 0
        q = [root]
        level = 0
        while q:
            level += 1
            for i in range(len(q)):
                node = q.pop(0)
                if node.left:
                    q.append(node.left)
                if node.right:
                    q.append(node.right)
        return level
            
    #DFS -1: 
    # write your code here
    #1 divide conquer:
    def maxDepth(self, root):

        if not root:
            return 0
        # divide and conqure is bottom up, just imagine

        #below line can be omit, coz it's covered in other code, if left  and right child is 0 ofcause current node is 1
        #if root.left == None and root.right == None:
        #  return 1

        #your current node is leaf, than, it's 0+0+1 = 1
        left = self.maxDepth(root.left)
        right = self.maxDepth(root.right)
        #at leave node, left and right is 0. so 0+0 = 1 is the base value.
        return max(left,right) + 1
        


    #DFS - 2:dnd
    #recursively compare max length for left and right child:
    def maxDepth(self, root):
        if not root:
            return 0
        def helper(root,path):
            if not root:
                return 0
            if root.left == None and root.right == None:
                return 1 + path
            
            left = helper(root.left,path + 1)
            right = helper(root.right,path + 1)
            return max(left,right)
        
        return helper(root,0)


    # DFS traversal method
    def maxDepth(self,root):    
        # 2 traversal
        if not root:
            return 0
        
        self.max_depth = 0
        
        def helper(root,path):
            if not root:
                return
            if root.left == None and root.right == None:
                self.max_depth = max(self.max_depth,path)
            helper(root.left,path + 1)
            helper(root.right,path + 1)
        
        helper(root,1)
        
        return self.max_depth