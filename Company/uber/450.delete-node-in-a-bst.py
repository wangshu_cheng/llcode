# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def deleteNode(self, root, key):
        """
        :type root: TreeNode
        :type key: int
        :rtype: TreeNode
        """
        # Find - travels to the node, and keep the father pointer, 
        # Delete - delelte 可以用soft delete，就是直接改值，这样比较简单；delete 之后，要保证还是BST，所以，有好几种情况：
        #    1. target node left,right child 都为None,直接用None顶上
        #    2. target node left child 为None，直接用right node 顶上；
        #    3. target node right child 为None， 同理；
        #    4. 两个child 都不为None，考点；要想BST，target node的值必须是右边子树最小的node；所以：
        #        1） 找到最小node；
        #        2） 用同样的方法删掉最小的node；（因为不能有重复node）
        
        if not root:
            return None
                
        def findMin(node):
            while node.left:
                node = node.left
            return node 

        if key > root.val:
            root.right = self.deleteNode(root.right,key)
        elif key < root.val:
            root.left = self.deleteNode(root.left,key)
        else:#root.val == key;
            if root.left == None:
                return root.right
            elif root.right == None:
                return root.left
            else:
                smallestNode = findMin(root.right)
                root.val = smallestNode.val
                root.right = self.deleteNode(root.right,root.val)
                
        return root

        # 复杂度：每个node一遍，所以是O(n)

                    
                    
    
                    
        