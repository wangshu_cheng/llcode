'''
Implement a data structure supporting the following operations:

Inc(Key) - Inserts a new key with value 1. Or increments an existing key by 1. Key is guaranteed to be a non-empty string.
Dec(Key) - If Key's value is 1, remove it from the data structure. Otherwise decrements an existing key by 1. If the key does not exist, this function does nothing. Key is guaranteed to be a non-empty string.
GetMaxKey() - Returns one of the keys with maximal value. If no element exists, return an empty string "".
GetMinKey() - Returns one of the keys with minimal value. If no element exists, return an empty string "".
Challenge: Perform all these in O(1) time complexity.
'''

'''
Thinking: 要 O(1) 那么最重要就是 link list， 它的插入，删除都是O(1)

用一个link list ，它的node 里面存key list， node 的顺序是key出现的频率，或着说key的value；

 head<->node1<->node2<->node3<->tail

 class Node(object):
    self.next , self.prev = None,None
    self.val = set()

 这样很容易找到value 最大和最小的key
那么如何定位这些node 呢？每个node 频率是多少？ 用hash (node_freq ) {key = freq, value = node}
外加一个小hash key_count 来存储一个key 的freq。用一个hash， 通过tuple 存也可以

'''


from collections import defaultdict

class Node(object):
    def __init__(self):
        self.key_set = set([])
        self.prev,self.next = None,None
        
    def add_key(self,key):
        self.key_set.add(key)
        
    def remove_key(self,key):
        self.key_set.remove(key)
    
    def get_any_key(self):
        if self.key_set:
            result = self.key_set.pop()
            self.add_key(result)
            return result
        else:
            return None
    def count(self):
        return len(self.key_set)
    def is_empty(self):
        return len(self.key_set) == 0

class DoubleLinkedList(object):
    def __init__(self):
        self.head_node,self.tail_node=Node(),Node()
        self.head_node.next,self.tail_node.prev = self.tail_node,self.head_node
        return 
    
    def insert_after(self,x):
        node,temp = Node(),x.next
        x.next,node.prev = node,x
        node.next, temp.prev = temp,node
        return node
    
    def insert_before(self,x):
        return self.insert_after(x.prev)
    
    def remove(self,x):
        prev_node = x.prev
        prev_node.next,x.next.prev = x.next,prev_node
        return
    
    def get_head(self):
        return self.head_node.next
    
    #这里不是return tail_node,因为我们用sentinel node
    def get_tail(self):
        return self.tail_node.prev
    
    def get_sentinel_head(self):
        return self.head_node
    
    def get_sentinel_tail(self):
        return self.tail_node
    

    
class AllOne(object):

    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.dll,self.key_counter = DoubleLinkedList(),defaultdict(int)
        self.node_freq = {0:self.dll.get_sentinel_head()}
        
    def _rmv_key_pf_node(self,pf,key):
        node = self.node_freq[pf]
        node.remove_key(key)
        if node.is_empty():
            self.dll.remove(node)
            self.node_freq.pop(pf)
        return
        

    def inc(self, key):
        """
        Inserts a new key <Key> with value 1. Or increments an existing key by 1.
        :type key: str
        :rtype: void
        """
        #cf - current frequence, pf - previous frequence
        self.key_counter[key] += 1
        cf,pf = self.key_counter[key],self.key_counter[key]-1
        if cf not in self.node_freq:
            self.node_freq[cf] = self.dll.insert_after(self.node_freq[pf])
        self.node_freq[cf].add_key(key)
        if pf > 0:
            self._rmv_key_pf_node(pf,key)
    
    def dec(self, key):
        """
        Decrements an existing key by 1. If Key's value is 1, remove it from the data structure.
        :type key: str
        :rtype: void
        """
        if key in self.key_counter:
            self.key_counter[key] -= 1
            cf,pf = self.key_counter[key], self.key_counter[key] + 1
            if self.key_counter[key] == 0:
                self.key_counter.pop(key)
            if cf != 0:
                if cf not in self.node_freq:
                    self.node_freq[cf] = self.dll.insert_before(self.node_freq[pf])
                self.node_freq[cf].add_key(key)
            self._rmv_key_pf_node(pf,key)
        

    def getMaxKey(self):
        """
        Returns one of the keys with maximal value.
        :rtype: str
        """
        return self.dll.get_tail().get_any_key() if self.dll.get_tail().count() > 0 else ""

        

    def getMinKey(self):
        """
        Returns one of the keys with Minimal value.
        :rtype: str
        """
        return self.dll.get_head().get_any_key() if self.dll.get_tail().count() > 0 else ""

        


# Your AllOne object will be instantiated and called as such:
# obj = AllOne()
# obj.inc(key)
# obj.dec(key)
# param_3 = obj.getMaxKey()
# param_4 = obj.getMinKey()