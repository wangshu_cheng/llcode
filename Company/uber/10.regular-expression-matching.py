'''
'.' Matches any single character.
'*' Matches zero or more of the preceding element.
The matching should cover the entire input string (not partial).


The function prototype should be:
bool isMatch(string s, string p)


Thinking:
和sudoku 很像，都是 validate+递归
就是当前这一步validate 成功，而且递归步骤也能成功；那么就算该步成功，再往下走；

s 的每一点都有可能和 p的每一点 match， 所以是2维数组；2维计算。

这个题有2种情况要validate， 一个是 '.' 的情况； 一个是 '*' 的情况；

‘.’ 的情况好办，就是1，1 对应匹配； 
‘*’ 的情况有点复杂分开几种情况：
   1. 避开当前的p点，干脆跳到p 目前的点的2个点后面；
   2. 考虑到p点的重复值； 用s的下一个点和当前P match

复杂度：
Cn1 * Cm1 = n*m, because pick any from S and any from P.

'''

class Solution(object):
    def isMatch(self, s, p):
        """
        :type s: str
        :type p: str
        :rtype: bool
        """
    
        def charMatch(sChar, pChar):
            return sChar == pChar or pChar == '.'

        def isEmpty( p, pIndex):
            for i in range(pIndex,len(p),2):  #    (int i = pIndex; i < p.length(); i += 2) {
                if i + 1 >= len(p) or p[i + 1] != '*':
                    return False;
            return True

        def helper(s,sIndex,p,pIndex, memo, visited):
            if pIndex == len(p):
                return sIndex == len(s)
            if sIndex == len(s):
                return isEmpty(p,pIndex)
            
            if visited[sIndex][pIndex]:
                return memo[sIndex][pIndex]

            sChar = s[sIndex]
            pChar = p[pIndex]

            match = False
            if (pIndex + 1 < len(p) and p[pIndex+1] == '*'):

                # skip current p and it's * 
                match = helper(s, sIndex, p, pIndex + 2, memo, visited) or 
                # or match currect s and p and also match s+1 to current p,because p is duplicated
                (charMatch(sChar, pChar) and helper(s, sIndex + 1, p, pIndex, memo, visited))
            else:
                match = charMatch(sChar, pChar) and helper(s, sIndex + 1, p, pIndex + 1, memo, visited)

            visited[sIndex][pIndex] = True;
            memo[sIndex][pIndex] = match;
            return match;
    
        if  s==None or  p == None:
            return False
        memo = [[False for _ in range(len(p))] for _ in range(len(s))]
        visited = [[False for _ in range(len(p))] for _ in range(len(s))]

        return helper(s,0,p,0,memo,visited)
        
        
        
        