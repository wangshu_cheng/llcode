'''
Given a pattern and a string str, find if str follows the same pattern.

Here follow means a full match, such that there is a bijection between a letter in pattern and a non-empty substring in str.

Example 1:

Input: pattern = "abab", str = "redblueredblue"
Output: true
Example 2:

Input: pattern = pattern = "aaaa", str = "asdasdasdasd"
Output: true
Example 3:

Input: pattern = "aabb", str = "xyzabcxzyabc"
Output: false

Thinking:
DFS,Backtrack, 复杂的string 问题，都是一步步分割成substring 处理


'''
class Solution(object):
    def wordPatternMatch(self, pattern, str):
        """
        :type pattern: str
        :type str: str
        :rtype: bool
        """
        def match(p,s,hash,hashset):
            if len(p) == 0:
                return len(s) == 0
            
            c = p[0]
            #Case1: this pattern already exist
            if c in hash:
                #if current string not start w/ it should start, then fail
                if not s.startswith(hash[c]):
                    return False
                #start back tracking, direct jump to next pattern:
                return match(p[1:],s[len(hash[c]):],hash,hashset)
            
            #Case2: this pattern is new patten:
            for i in range(len(s)):
                word = s[:i+1]
                #because this is new pattern, so must use new word:
                if word in hashset:
                    continue

                #put this new pattern and new word to storage    
                hash[c] = word
                hashset.add(word)
                #start back tracking:
                if match(p[1:],s[i+1:],hash,hashset):
                    return True
                #if fail, go back.
                hashset.remove(word)
                del hash[c]
                
            return False
        
        hash = {} # this is to store the patterns

        # this is to store the words with match the patters
        # reason is, if the word already exist, then it belongs to a old pattern.
        hashset = set() 
        return match(pattern,str,hash,hashset)
    