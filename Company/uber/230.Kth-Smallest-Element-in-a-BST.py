# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def kthSmallest(self, root, k):
        """
        :type root: TreeNode
        :type k: int
        :rtype: int
        """
        # Method1:
        #USE BST 的排序特点， 所以由第几小就能确定他的位置
        #思路是：如果k <= 左树的nodes，那么k 在左树，如果k= 左nodes count + 1 那么就是root，如果K> 左count + 1 那么在右边
        
        def countNodes(root):
            if not root:
                return 0
            #if not root.left and not root.right:
                #return 1
            left = countNodes(root.left)
            right = countNodes(root.right)
            return 1 + left + right
        
        count = countNodes(root.left)
        if k<= count:
            return self.kthSmallest(root.left,k)
        elif k> count+1:
            return self.kthSmallest(root.right,k-count-1)
        return root.val
    
        #Method2, in order travels:
        self.count = 0
        self.res = 0
        def helper(root,k):
            if not root:
                return
            helper(root.left,k)
            self.count += 1
            if k==self.count:
                self.res = root.val
                return
            helper(root.right,k)
            
        helper(root,k)
        
        return self.res
        