class Solution(object):
    def romanToInt(self, s):
        """
        If small number comes before big number, than substrack
        If small number comes after big number, than add
        
        like: IV = -1+5=4
              VII = 5+1+1 = 7
        
        """
       
        roman = {'M': 1000,'D': 500 ,'C': 100,'L': 50,'X': 10,'V': 5,'I': 1}
        result = 0
        
        for i in range(0,len(s) - 1):
            if roman[s[i]] < roman[s[i+1]]:
                result -= roman[s[i]]
            else:
                result += roman[s[i]]
        
        result += roman[s[-1]]
        return result