'''
注意 visited， 不然无限循环；注意仔细看题目条件，和原来color相同的才变色，不要套老题目

'''

class Solution(object):
    def floodFill(self, image, sr, sc, newColor):
        """
        :type image: List[List[int]]
        :type sr: int
        :type sc: int
        :type newColor: int
        :rtype: List[List[int]]
        """
        
        #BFS
#         if not image:
#             return []
        
#         MAX_X = len(image)
#         MAX_Y = len(image[0])
        
#         newImage = [[0 for i in range(MAX_Y)] for j in range(MAX_X)]
#         for i in range(MAX_X):
#             for j in range(MAX_Y):
#                 newImage[i][j] = image[i][j]
        
#         visited = [[0 for i in range(MAX_Y)] for j in range(MAX_X)]
#         q = [(sr,sc)]
#         dir = [(-1,0),(1,0),(0,1),(0,-1)]
#         while q:
#             c = q.pop(0)
#             newImage[c[0]][c[1]] = newColor
#             visited [c[0]][c[1]] = 1
#             for i in range(4):
#                 x1 = c[0] + dir[i][0]
#                 y1 = c[1] + dir[i][1]
#                 if x1>=0 and x1< MAX_X and y1>=0 and y1<MAX_Y and image[x1][y1] == image[c[0]][c[1]] and visited [x1][y1] != 1:
#                     q.append((x1,y1))
#         return newImage

        #DFS:
            
        MAX_X = len(image)
        MAX_Y = len(image[0])
        
        newImage = [[0 for i in range(MAX_Y)] for j in range(MAX_X)]
        for i in range(MAX_X):
            for j in range(MAX_Y):
                newImage[i][j] = image[i][j]
        
        visited = [[0 for i in range(MAX_Y)] for j in range(MAX_X)]
        q = [(sr,sc)]
        dir = [(-1,0),(1,0),(0,1),(0,-1)]
        
        def dfs(image,x,y,color):
            newImage[x][y] = color             
            visited [x][y] = 1
            for i in range(4):
                x1 = x + dir[i][0]
                y1 = y + dir[i][1]
                if x1>=0 and x1< MAX_X and y1>=0 and y1<MAX_Y and image[x1][y1] == image[x][y] and visited [x1][y1] != 1:
                    dfs(image,x1,y1,color)
        

        dfs(image,sr,sc,newColor)    
            
        return newImage
        

            
        
    