class Solution(object):
    def replaceWords(self, dict, sentence):
        """
        :type dict: List[str]
        :type sentence: str
        :rtype: str
        """
        
        dict.sort(lambda x,y : cmp(len(x),len(y)))
        
        words = sentence.split(" ")
        
        for i in range(len(words)):
            for root in dict:
                if words[i].startswith(root):
                    words[i] = root
        
        return " ".join(words)