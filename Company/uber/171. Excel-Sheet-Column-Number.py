class Solution(object):
    def titleToNumber(self, s):
        """
        :type s: str
        :rtype: int
        """
        total = 0
        
        l = len(s) - 1
        for i in s:
            total += (ord(i) - 64) * 26**l
            l -= 1
        return total