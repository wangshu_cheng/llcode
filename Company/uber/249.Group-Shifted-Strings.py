'''
Given a string, we can "shift" each of its letter to its successive letter, for example: "abc" -> "bcd". We can keep "shifting" which forms the sequence:

"abc" -> "bcd" -> ... -> "xyz"
Given a list of strings which contains only lowercase alphabets, group all strings that belong to the same shifting sequence.

Example:

Input: ["abc", "bcd", "acef", "xyz", "az", "ba", "a", "z"],
Output: 
[
  ["abc","bcd","xyz"],
  ["az","ba"],
  ["acef"],
  ["a","z"]
]
'''
class Solution(object):
    def groupStrings(self, strings):
        """
        :type strings: List[str]
        :rtype: List[List[str]]
        """
        '''
        This question is a bit difficult to understand.
        The shifting rule is actually every alphbit shift from right to left, with same gap,like:
        az=za=ab=ba
        So the grouping rule is: the same group must has:
        same gap of 2nd to 1st alph, same gap of 3rd to 2nd gap...etc.
        if the gap is negative, it should be +26, or use %.
        we can make hash table to store them
        '''

        
        dic={}
        res=[]
        if not strings:return []
        for s in strings:
            first=ord(s[0])-97
            #can not use list as key, cause it's not hashable
            #key=tuple((ord(ch)-first)%26 for ch in s)
           # key = ''
            key ="".join([str((ord(ch)-first)%26) for ch in s])
            dic[key]=dic[key]+[s] if key in dic else [s]
        for key in dic:
            dic[key].sort()
            res.append(dic[key])
        return res
            
        
        