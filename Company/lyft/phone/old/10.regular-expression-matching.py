# 三哥上来简单介绍了下自己，然后让我简单介绍了一下，开始做题。Regex match: 包括 . ? * +. 写code 实现，并现场编译测试。

# 这是leetcode 的加强版，三哥先写几个例子，问我例子是怎么work的， 然后我和他讨论算法，我用的递归（现在想想，可能dp 好写一些）。
# 然后让我先在那些例子上run 我的算法。

# 这个时候才开始写code，然而时间只剩30分钟了。写完code, fix 完编译错误，就剩10分钟了，然而结果不对，又花了几分钟找出了错误。
#最后，过了他的4个test case, 还剩一些没过，实在没有时间了fix了，就结束了。
# 果然，第二天就安静的挂了。哎，很想去的公司，就这么挂了，不过也尽力了


# basic, only support . and *

class Solution(object):
    def isMatch(self, s, p):
        """
        :type s: str
        :type p: str
        :rtype: bool
        """
        if not p:
            return not s
        
        first_match = bool(s) and p[0] in {s[0],'.'}
        
        if len(p) >= 2 and p[1] == '*':
            return self.isMatch(s,p[2:]) or (first_match and self.isMatch(s[1:],p) )
        else:
            return first_match and self.isMatch(s[1:],p[1:])


 # Also support ? and + 
 # ? zeor or 1,  + one or more, * zero or more

class Solution(object):
    def isMatch(self, s, p):
        """
        :type s: str
        :type p: str
        :rtype: bool
        """
        if not p:
            return not s
        
        first_match = bool(s) and p[0] in {s[0],'.'}
        
        if len(p) >= 2 and p[1] == '*':
            return self.isMatch(s,p[2:]) or (first_match and self.isMatch(s[1:],p) )
        # ?

        elif len(p) >= 2 and p[1] == '?':
            return self.isMatch(s,p[2:]) or (first_match and self.isMatch(s[1:],p[2:]) )

        elif len(p) >= 2 and p[1] == '+':
            return (first_match and self.isMatch(s[1:],p[2:]) ) or (first_match and self.isMatch(s[1:],p) )

        else:
            return first_match and self.isMatch(s[1:],p[1:])

