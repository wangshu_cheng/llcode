
# 注意和原题的区别：

# 给定了 Asteroid class ：
# class Asteroid {
#   public final int mass;
#   public final int direction;
#   public Asteroid(int mass, int direction) {
#     this.mass = mass;
#     this.direction = direction;
#   }

#   public int getMass() {
#     return this.mass;
#   }

#   public int getDirection() {
#     return this.direction;
#   }

#   public String toString() {
#     return "Asteroid(" + mass + ", " + direction + ")";
#   }
# }



lyft 电面，asteroids经典变种，只需要输出个数，linear time 和constant space.

美国小哥问了高频的行星碰撞 (利口其三无)
不过和利口的不完全一样。。。
思路也是用个栈撸一下


有意思的Followup: 如果慧星有不同的速度，之间有不同的距离，结果有什么不同，如何设计算法？
好像没有好的办法，只能一步步更新。先算每个慧星碰撞到另外的时间，得到最小的碰撞时间，更新慧星的距离或坐标；重复直到没有慧星或只有反方向的慧星。

'''
We are given an array asteroids of integers representing asteroids in a row.

For each asteroid, the absolute value represents its size, and the sign represents its direction (positive meaning right, negative meaning left). Each asteroid moves at the same speed.

Find out the state of the asteroids after all collisions. If two asteroids meet, the smaller one will explode. If both are the same size, both will explode. Two asteroids moving in the same direction will never meet.

Example 1:
Input: 
asteroids = [5, 10, -5]
Output: [5, 10]
Explanation: 
The 10 and -5 collide resulting in 10.  The 5 and 10 never collide.
Example 2:
Input: 
asteroids = [8, -8]
Output: []
Explanation: 
The 8 and -8 collide exploding each other.
Example 3:
Input: 
asteroids = [10, 2, -5]
Output: [10]
Explanation: 
The 2 and -5 collide resulting in -5.  The 10 and -5 collide resulting in 10.
Example 4:
Input: 
asteroids = [-2, -1, 1, 2]
Output: [-2, -1, 1, 2]
Explanation: 
The -2 and -1 are moving left, while the 1 and 2 are moving right.
Asteroids moving the same direction never meet, so no asteroids will meet each other.
'''
#
#只有左边正，右边负才会相撞
class Solution(object):
    def asteroidCollision(self, asteroids):
        """
        :type asteroids: List[int]
        :rtype: List[int]
        """
        
        # 凡是这种配对题都是stack，比如找阔号，回文等
        # 正的压入stack，遇到负的弹出来比较
        # 1. 负的大 => 弹出来，一直弹
        # 2. 正的大 => 不弹
        # 3. 一样大 => 弹出来
        # 4. 如果只有负的了，负的压战
        stack = []
        res = []
        for  a in asteroids:
            if a > 0:
                stack.append(a)
            else:
                while len(stack) != 0 and stack[-1] > 0 and stack[-1] < - a:
                    stack.pop()
                if len(stack) != 0 and stack[-1] == - a:
                    stack.pop()
                elif len(stack) == 0:# or stack[-1] < 0:
                    stack.append(a)
                elif stack[-1] < 0:
                    stack.append(a)
        return stack
                        

                
        
