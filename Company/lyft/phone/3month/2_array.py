#   先问Given two arrays of integers, print elements that appear in both. 口头说算法和复杂度，并没让写。ok

#   让写的是一个CommonIterator class, constructor的输入是两个iterators that return sorted integers,
#    主要是写hasNext() 和 next(). 可以用它们print the intersection of two arrays in sorted order。比如

# i1 = iter([1, 2, 3, 4, 5, 6])
# i2 = iter([2, 3, 6, 7])

#   while CI.hasNext():
#    print CI.next()

# Output:
# iter = CommonIterator(i1, i2)
# iter.next() -> 2
# iter.next() -> 3
# iter.next() -> 6



# 第一轮从一个基本问题开始。
# 给两个sorted integer array
# int[] a = {1,2,3,4,4,5,5}
# int[] b = {1,1,2,2,3}

# 要求返回其中重叠的部分 {1,2,3}。

# follow up 是写一个iterator， 有两个方法 hasnext(), 和 next().
# 调用 next()， 依次返回 1-2-3.

# 第二次电面考了leetcode经典小岛问题，问多少个小岛。follow up 讨论了一下iterative and recursive 方法的tradeoff. 1p

class Solution(object):
    def __init__(self,i1,i2):
      self.i1 =i1
      self.i2 = i2
      self.common=[]
      self.getCommon()
      self.idx = 0

    def hasNext(self):  
        return  True if (len(self.common) > 0 and self.idx<len(self.common)) else False



    def next(self):
        if self.idx < len(self.common):
            res = self.common[self.idx]
            self.idx += 1
            return res
        else:
            return 0


    def getCommon(self):
        nums1 = self.i1
        nums2 =self.i2
        res = set()
        dic = {}
        
        if not nums1 and nums2:
            return []
        if not nums1 or not nums2:
            return []
        
        for i in nums1:
            dic[i] = 1
        
        for j in nums2:
            if j in dic:
                res.add(j)
        self.common = sorted(list(res))

        