# Given an array nums of n integers where n > 1,  return an array output such that output[i] is equal to
# the product of all the elements of nums except nums[i].

# Example:

# Input:  [1,2,3,4]
# Output: [24,12,8,6]

class Solution(object):
    def productExceptSelf(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        
        #1 build array1
        m=len(nums)
        array1=[1 for i in xrange(m)]
        
        # left half
        for i in xrange(1,m):
            array1[i]=array1[i-1]*nums[i-1]

        # right half 
        array2=[1 for i in xrange(m)]
        for i in xrange(m-2,-1,-1):
            array2[i] = array2[i+1]*nums[i+1]
        
        
        for i in range(m):
            array1[i]*=array2[i]
            
        return array1