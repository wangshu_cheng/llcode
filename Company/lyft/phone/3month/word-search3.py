# /* Word Search
# * 
# * Write some code to help find out if a word is in a 2D grid 
# * of letters. The word cannot wrap, and must appear in a 
# * straight line (up, down, left, right, or diagonal).
# */ 


class Solution(object):
    def test(self,board, word):
      # write your code here
        if not board or not board[0] or not word:
            return False
        m,n = len(board),len(board[0])

        def search(board,m,n,i,j,word,pos,direction):
            if pos == len(word):
                return True
                
            if i >= m or i < 0 or j >= n or j < 0 or board[i][j] != word[pos]:
               return False
            
            dr = [[-1,0],[1,0],[0,1],[0,-1],[1,1],[1,-1],[-1,1],[-1,-1]]
            rst = False

         #   board[i][j] = '#'
            #for k in range(4):
            rst = rst or search(board,m,n,i+dr[direction][0],j+dr[direction][1],word,pos+1,direction)
           # board[i][j] = word[pos]
            return rst
            
        res = False
        for i in range(m):
            for j in range(n):
                if board[i][j] == word[0]:
                    # left
                    res = search(board,m,n,i,j,word,0,0)
                    # right
                    res |= search(board,m,n,i,j,word,0,1)
                    # # up
                    res |= search(board,m,n,i,j,word,0,2)
                    # # down
                    res |= search(board,m,n,i,j,word,0,3)
                    # # right down
                    res |= search(board,m,n,i,j,word,0,4)
                    # # right up
                    res |= search(board,m,n,i,j,word,0,5)
                    # # left down
                    res |= search(board,m,n,i,j,word,0,6)
                    # # left up
                    res |= search(board,m,n,i,j,word,0,7)
                    if res == True:
                        return True
        return res
