
'''
Given two words (start and end), and a dictionary, find the length of
 shortest transformation sequence from start to end, such that:

Only one letter can be changed at a time
Each intermediate word must exist in the dictionary

Thinking:
2 way BFS,remove the word from dic when searched, to avoid loop

'''
class Solution:
    # @param start, a string
    # @param end, a string
    # @param dict, a set of string
    # @return an integer
    def ladderLength(self, start, end, dict):
        # write your code here
        if not start or not dict or not end:
            return None
        dict.add(end)
        q = [[start,1]]
        while q:
            cur,cnt = q.pop(0)
            if cur == end:
                return cnt
            for i in range(len(cur)):
                for j in "abcdefghijklmnopqrstuvwxyz":
                    if j == cur[i]:
                        continue
                    nw = cur[:i] + j + cur[i+1:]
                    if nw in dict:
                        q.append([nw,cnt+1])
                        dict.remove(nw)
        return 0




# class Solution:
#     # @param start, a string
#     # @param end, a string
#     # @param dict, a set of string
#     # @return an integer
#     def ladderLength(self, start, end, dict):
#         # write your code here
        
#         def getNext(word,used):
#             result = []
#             #every letter of word has 26 options,so totally
#             #26 * len(word) possiable change
#             for i in range(len(word)):
#                 for j in range(26):
#                     tmp = list(word)
#                     tmp[i] = chr(ord('a')+j)
#                     tmp = ''.join(tmp)
#                     if tmp != word and tmp not in used:
#                         # print "tmp",tmp
#                         result.append(tmp)
#             return result
                
        
#         #BFS:
#         length = 0
#         q = [start]
#         used  = {}
#         while q:
#             current = q.pop()

#             length += 1
#             if current  not in used:
#                 used[current] = 1
#             for i in getNext(current,used):
#                 if i == end:
#                     return length + 1
#                 if i in dict:
#                     q.append(i)
                    
        
            
                    
# test = Solution()
# start = "hit"
# end = "cog"
# dict = ["hot","dot","dog","lot","log"]


# print test.ladderLength(start,end,dict)    

#             