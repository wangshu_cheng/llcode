# 1 基本， 双stack， 问题， popMax O(n), 改为 double link list + hash, 可以降到 o(log(n))

class MaxStack(object):

    def __init__(self):
        """
        initialize your data structure here.
        """
        self.stack = []
        self.maxStack = []
        

    def push(self, x):
        """
        :type x: int
        :rtype: None
        """
        maxVal = self.maxStack[-1] if self.maxStack else x
        self.maxStack.append(maxVal if maxVal > x else x)
        self.stack.append(x);
        

    def pop(self):
        """
        :rtype: int
        """
        self.maxStack.pop()
        return self.stack.pop()

    def top(self):
        """
        :rtype: int
        """
        return self.stack[-1]

    def peekMax(self):
        """
        :rtype: int
        """
        return self.maxStack[-1]
        

    def popMax(self):
        """
        :rtype: int
        """
        maxVal = self.peekMax()
        buffer = []
        while self.top() != maxVal:
            buffer.append(self.pop())
        self.pop()
        while buffer:
            self.push(buffer.pop())
        return maxVal
        

# 2 linklist:


# Your MaxStack object will be instantiated and called as such:
# obj = MaxStack()
# obj.push(x)
# param_2 = obj.pop()
# param_3 = obj.top()
# param_4 = obj.peekMax()
# param_5 = obj.popMax()