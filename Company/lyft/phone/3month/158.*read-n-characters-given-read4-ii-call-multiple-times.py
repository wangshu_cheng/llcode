# Given an API to fetch page (10 results on each page):

# MAX_RESULTS = 103
# def fetch_page(page):
#     page = page or 0 # start from page 0 if not specified
#     return {
#         "page": page + 1,
#         "results": [range(10 * page, min(10 * (page + 1), MAX_RESULTS))]
#     } 

# Implement a ResultFetcher class with a fetch method to return required number of results:
# class  ResultRecher:

#     def fetch_page(num_results):

# 和利口 要五八(read4) 一个思路。Follow up: 
# 1. what if the fetch_page has a 30% possibility to fail? -> add retry, implement retry with a limit 
# 2. what if sometimes the server may be overloaded, retry immediately will make it even worse? no need to consider auto scale -> add wait time between retries




# 给一个第三方api, 比如 List<Integer> get(int page). 面试的时候要问面试官这个api到底是返回什么，我说返回List<Integer>, 他说可以。 这个api每次返回10个结果。
# 使用方式就是这样啊 get(1), get(2), get(3)..., 不能go back, 每次只能get 下一个 page的结果。而且这个api的implementation 也要自己写，我随便瞎写的都行。
# 然后，你要实现这样一个函数 List<Integer> fetch(int numOfItems); 
# 比如 get(1) = [1, 2, ... 10], get(2) = [11, 12, ...20], ..
# 那么 fetch(5) = [1, 2, 3, 4, 5]; fetch(1) = [6]; fetch(2) = [7, 8].
# Follow up 就是 如果这个 api是个http call, 不太稳定，有时会fail。 你怎么办。 
# 我说。 1. Guava retry 2. buffer更多的结果. 因为我实现这个fetch这个函数的时候用了一个queue 去buffer一些结果。
# 他自说可以cache 结果啥的。不知道他满不满意。

# 题目是有一个 paginated API 参数是 pageId， 每次返回该pageid的elements，现实fetchN.

# e.g. 3 pages, [ [0,1,2,3,4], [5,6,7,8,9], [10,11,12] ]
# paginated(0) = [0,1,2,3,4],

# paginated(1) =  [5,6,7,8,9],


# fetchN(2) = [0,1]
# fetchN(2) = [2,3]
# fetchN(2) = [4,5]
# fetchN(100) = [6,7,8,9,10,11,12]

#Thinking:
'''
Use global buffer  to store last time unread data.

Use buffPtr/buffCnt as 2 small pointer to the small buffer
user ptr/n as the big pointer 

'''

class Solution:

    # @param {char[]} buf destination buffer
    # @param {int} n maximum number of characters to read
    # @return {int} the number of characters read
    def __init__(self):
        self.buff = [""]*4
        self.buffPtr = 0
        self.buffCnt = 0

    
    def read(self, buf, n):
        ptr = 0
        while ptr < n:

            # if local point != 0, means there is some unconsumed data from last time read.
            if self.buffPtr == 0: 
                self.buffCnt = Reader.read4(self.buff);
            
            # buffCnt == 0 means there is no more data in API
            if self.buffCnt == 0:
                break
            
            # 
            while (ptr < n and self.buffPtr < self.buffCnt):
                buf[ptr] = self.buff[self.buffPtr]
                ptr += 1
                self.buffPtr += 1
            
            # 小buffer 指针 不能超过小buffer 大小； 如果超过了，说明小buff已经读完了，要再call 下一个了
            if (self.buffPtr >= self.buffCnt):
                self.buffPtr = 0
        
        return ptr

