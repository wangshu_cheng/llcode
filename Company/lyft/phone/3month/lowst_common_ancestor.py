'''
Questions：

Important, it's not a BST.
Given the root and two nodes in a Binary Tree. Find the lowest common ancestor(LCA) of the two nodes.
The lowest common ancestor is the node with largest depth which is the ancestor of both nodes.

Thinking：
对于BST来说， LCA 一定是大小位于这两个点之间的node，而且只有一个。
对于Non BST，就不是这样了。
1 题目要求找 LCA， LCA可能是A和B的父亲，也可能就是A或B自己，无论找父亲还是找target，都可以归结为找target
找target 其实就是traversal，所以用traveral 做容易。
'''


"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""
class Solution:
    """
    @param root: The root of the binary search tree.
    @param A and B: two nodes in a Binary.
    @return: Return the least common ancestor(LCA) of the two nodes.
    
    it's all about finding the A and B in subtrees. 不是BST，不能用value 判断
    """ 
    def lowestCommonAncestor(self, root, A, B):
        def helper(root,A,B):
            #Base情况，当root栽倒None 或 A 或 B时，返回,因为再往下，不可能是父亲了， 而是儿子了 haha:) 
            if root == None or root == A or root == B:
                return root
            #两边同时往下栽
            left = helper(root.left,A,B)
            right = helper(root.right,A,B)
            #如果左儿子是A 同时右儿子又是B（或者左儿子是B，右儿子是A），那么答案就是root
            if left and right:
                return root
            #如果左儿子是A或B，右儿子None,那么B在A下面，或反之，那么答案就是左儿子
            if left:
                return left
            #同理
            if right:
                return right

        return helper(root,A,B)

    #below code works for BST only，是BST 才可以用value 判断
    def lowestCommonAncestor_bst(self, root, A, B):
        # write your code here
        if not root:
            return
        while root:
            if root.val >= A.val and root.val <=B.val:
                break
            if root.val < A.val:
                root = root.right
            if root.val > B.val:
                root = root.left
        return root
        