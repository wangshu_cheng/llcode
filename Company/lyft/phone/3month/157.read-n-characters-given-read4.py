# LC157

# 题目是有一个 paginated API 参数是 pageId， 每次返回该pageid的elements，现实fetchN.

# e.g. 3 pages, [ [0,1,2,3,4], [5,6,7,8,9], [10,11,12] ]. check 1point3acres for more.
# paginated(0) = [0,1,2,3,4],

# paginated(1) =  [5,6,7,8,9],


# fetchN(2) = [0,1]
# fetchN(2) = [2,3]
# fetchN(2) = [4,5]
# fetchN(100) = [6,7,8,9,10,11,12]

class Solution(object):
    def fetchN(self, buf, n):
    	



157 origin question:
class Solution(object):
    def read(self, buf, n):
        """
        :type buf: Destination buffer (List[str])
        :type n: Number of characters to read (int)
        :rtype: The number of actual characters read (int)
        """
        idx = 0
        eof = False
        while idx<n and not eof:
            buf4=[""]*4
            l = read4(buf4)
            if l < 4:
                eof = True
            length = min(l,n-idx)
            for i in range(length):
                buf[idx+i] = buf4[i]
            idx += length
            
        return idx