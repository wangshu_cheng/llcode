class Solution(object):
    def myAtoi(self, str):
        """
        :type str: str
        :rtype: int
        """
        int_max = 2147483647
        int_min = -2147483648
        
        flag = 1
        
        res = 0
        
        str = str.strip()
        
        if not str:
            return 0
        
        if str[0] == "-":
            flag = -1
            str = str[1:]
        elif str[0] == "+":
            str = str[1:]
            
        if not str or not str[0].isdigit():
            return 0
        
        for i in str:
           
            if ord('0')<= ord(i) <= ord('9'):
                res = res*10 + int(i)
                if flag*res >= int_max:
                    return int_max
                elif flag*res <= int_min:
                    return int_min
                    
            else:
                break
            
                
        
        return flag*res
            