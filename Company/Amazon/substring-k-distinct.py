'''. substringKDistinct, input 一个string可一个长度k，
输出一个list包含所有长度为k且没有重复字符的substring，
'''
#1 
def test(s,k):

    res = []
    if not s or not k:
        return res
    
    tmp = ""
    for i in range(len(s) - k + 1):
        dic = {}
        tmp = ""
        for j in range(k):
            if s[i+j] not in dic:
                tmp += s[i+j]
                dic[s[i+j]] = 1
            else:
                break
        if len(tmp) == k and tmp not in res:
            res.append(tmp)

    return res


# 或者：改为有一个字符重复
#2 
def test1(s,k):

    res = []
    if not s or not k:
        return res
    
    tmp = ""
    for i in range(len(s) - k + 1):
        dic = {}
        tmp = ""
        for j in range(k):
            if dic.get(s[i+j],0) <= 1:
                tmp += s[i+j]
                dic[s[i+j]] = dic.get(s[i+j],0) + 1
            else:
                break
        if len(tmp) == k and tmp not in res:
            res.append(tmp)

    return res