'''
第二道是给一个targetList，一个availableList，选最短subsequence，要求包含所有targetList里的String，
如果有不止一个，那么返回比较小的startIndex的那个subsequence。
not like  727 , subsequnce 里面的元素可以不按照target list里面的order





does this make sense??
target list先存入hashmap<String, Integer>  前面是字符串，后面是该字符串出现次数，初始一律是1
然后用two pointers扫 available tag list。
遇到在hashmap里的就更新map，次数减一。直到找完，记录length，start，end
找完后，就移动头指针，遇到在hashmap里的，次数要加1，移动中，如果也找完了（命中次数和map的size相等）
，就对比length，如果length短，再次记录。

'''

def tags(alist,tlist):
    if not alist or not tlist:
        return []

    res = []

    thash = {}
    shash = {}

    cnt = len(tlist)

    for e in tlist:
        if e.lower() not in thash:
            thash[e] = 1

    start,end = 0,0

    len_a = len(alist)
    len_t = len(tlist)

    start,end = 0,2**32

    for i in range(len_a - len_t + 1):
        shash = {}
        count = cnt
        for j in range(i,len_a):
            if alist[j] not in thash:
                continue
            elif alist[j] not in shash:
                count -= 1
            shash[alist[j]] = shash.get(alist[j],0) + 1

            if count == 0:
                if j - i + 1 < end - start + 1:
                    end = j
                    start = i

    if end == 2**32:
        return []
    return [start,end]
                
tlist = ["made","in","spain"]
alist = ["made","weather","forcast","says","that","made","rain","in","spain","stays"]

print tags(alist,tlist)



// public static List < Integer > subSequenceTags(List < String > targetList, List < String > availableTagList) {
//     List < Integer > ret = new ArrayList < > ();
//     Map < String, Integer > map = new HashMap < > ();
//     for (String tag: targetList) {
//         String tg = tag.toLowerCase();
//         map.put(tg, map.getOrDefault(tg, 0) + 1);
//     }
//     int start = 0, end = 0, counter = targetList.size(), retStart = 0, retEnd = availableTagList.size();
//     while (end < availableTagList.size()) {
//         String cur = availableTagList.get(end++).toLowerCase();
//         map.put(cur, map.getOrDefault(cur, 0) - 1);
//         if (map.getOrDefault(cur, 0) >= 0) counter--;
//         while (counter == 0) {
//             if (end - start < retEnd - retStart + 1) {
//                 retStart = start;
//                 retEnd = end - 1;
//             }
//             cur = availableTagList.get(start++).toLowerCase();
//             map.put(cur, map.getOrDefault(cur, 0) + 1);
//             if (map.get(cur) > 0) counter++;
//         }
//     }
//     ret.add(retStart);
//     ret.add(retEnd);
//     return ret;
// }