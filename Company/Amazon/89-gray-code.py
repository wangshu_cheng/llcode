class Solution(object):
    def grayCode(self, n):
        """
        :type n: int
        :rtype: List[int]
        """
        dic={}
        #value is the item, value is index
        start=0
        for i in xrange(2**n):#total n bit
            tmp=1
            while tmp<2**n:
                if start not in dic.values():
                    print start
                    dic[i]=start
                    break
                else:
                    #revert the bit change:
                    tmp2=tmp>>1
                    start^=tmp2
                start^=tmp
                tmp<<=1
            #last item:
            if start not in dic:
                print start
                dic[i]=start
    
        return dic.values()