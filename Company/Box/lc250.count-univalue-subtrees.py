# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None


# follow up n-ary tree?
class Solution(object):
    def countUnivalSubtrees(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        self.total=0
        def helper(root):
            if not root:
                return True
            
            left = helper(root.left)
            right = helper(root.right)
            
            if left and right:
                if root.left and root.left.val != root.val:
                    return False
                if root.right and root.right.val != root.val:
                    return False
                self.total += 1
                return True
            return False
        helper(root)
           
        return self.total
        