

'''
A binary tree is univalued if every node in the tree has the same value.

Return true if and only if the given tree is univalued.



'''

#method1, pre order travers:

class Solution(object):
    def isUnivalTree(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        def helper(root,prev):
            if not root:
                return True
            if prev != root.val:
                return False
            left = helper(root.left, prev)
            right = helper(root.right,prev)
            
            return left and right

        return helper(root,root.val)

#method2: bottom up:

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def isUnivalTree(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        def helper(root):
            if not root:
                return True

            left = helper(root.left)
            right = helper(root.right)
            
            if left and right:
                if root.left and root.left.val != root.val:
                    return False
                if root.right and root.right.val != root.val:
                    return False
                return True
            
            return False

        return helper(root)