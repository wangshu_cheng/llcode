''' medium
109. Convert Sorted List to Binary Search Tree
DescriptionHintsSubmissionsDiscussSolution
Given a singly linked list where elements are sorted in ascending order, convert it to a height balanced BST.

For this problem, a height-balanced binary tree is defined as a binary tree in which the depth of the two subtrees of every node never differ by more than 1.

Example:

Given the sorted linked list: [-10,-3,0,5,9],

One possible answer is: [0,-3,9,-10,null,5], which represents the following height balanced BST:

      0
     / \
   -3   9
   /   /
 -10  5
'''



# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def sortedListToBST(self, head):
        """
        :type head: ListNode
        :rtype: TreeNode
        """
        def getLen(head):
            l = 0
            while head:
                l += 1
                head = head.next
            return l 
        
        def helper(start,end):
            if start > end:
                return
            mid = (start + end)/2
            left = helper(start,mid - 1)
            root = TreeNode(self.h.val)
            self.h=self.h.next
            right = helper(mid + 1,end)
            root.left = left
            root.right = right
            return root
            
        l =getLen(head)
        self.h = head
        return helper(0,l-1)
            