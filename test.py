
import collections
import numpy as np
class Solution(object):
    def __init__(self):
        self.data = {}
        self.log = collections.defaultdict(list)
        self.transaction = False
        self.transactionStack = []
        self.transactionId=1


    def set(self,key,val):
        self.data[key] = val
        if self.transaction:
            self.log[self.transactionId].append((key,val)) 
            np.save('log.npy',self.log)

    def delete(self,key):
        del self.data[key]

    def get(self,key):
        if key in self.data:
            return self.data[key]

    def count(self,val):
        cnt = 0
        for k in self.data:
            if self.data[k] == val:
                cnt += 1
        return cnt

    def begin(self):

        self.transaction = True
        self.transactionId += 1
       # self.log = {}

    def commit(self):
        self.transaction = False
        del self.log[self.transactionId]
        self.transactionId = max(self.transactionId - 1,0)
        np.save('log.npy',self.log)

    def rollback(self):

        # load log:
        self.log = np.load('log.npy').item()

        print ("rollback ----")
        print self.log

        # pop 
        for key,val in self.log[self.transactionId ]:
            del self.data[key]
        del self.log[self.transactionId]
        self.transactionId = max(self.transactionId - 1,0)

        self.transaction = False
        np.save('log.npy',self.log)

        print ("after rollback ----")
        print self.log


t = Solution()

t.set(1,2)
t.set(1,'a');
t.set(2,'b');
t.set(3,'c');
print t.get(3); #--> 'c'
t.set(1,'a');
t.set(2,'b');
t.set(3,'c');
print t.get(3); #--> 'c'
t.set(4,'c');
print t.count('c'); #--> 2
t.delete(2);
print t.get(2); #--> null
t.begin()
t.set(2, 'd');
t.get(2); #--> 'd'
t.commit()
print t.get(2); #--> 'd'
t.begin()
t.set(5,'e')
t.set(6,'e')
t.begin()
t.set(7,'e')
print t.count('e'); #--> 3
t.rollback();
print t.count('e'); #--> 2
t.rollback();
print t.count('e'); #--> 0













