'''
Given an array with n objects colored red, white or blue, sort them so that objects of the same color are adjacent, 
with the colors in the order red, white and blue.

Here, we will use the integers 0, 1, and 2 to represent the color red, white, and blue respectively.

'''

class Solution:
    """
    @param nums: A list of integer which is 0, 1 or 2 
    @return: nothing
    """
    def sortColors(self, nums):
        # write your code here
        # This is simple count version:
        if not nums:
            return []
        
        # use similar to quick sort method, continues swap (only doable to 3 color, not doable in 4 color and more)
        # idea is, if it's 0, swap with left most , if it's 1 don't swap, if it's 2 swap with right most
        
        l,r = 0,len(nums) - 1
        
        i = 0
        
        while i <= r:
            if nums[i] == 0 :
                nums[l],nums[i] = nums[i],nums[l]
                i += 1
                l += 1
            elif nums[i] == 1:
                i += 1
            else:
                 nums[r],nums[i] = nums[i],nums[r]
                 r -= 1
                 '''
                  here we can not do i += 1
                  since after swap with the right number,
                  the current number maybe 0, so need to swap left.

                 '''

           
               
            
                    
                
                    
        
                
        
                
        
        
            