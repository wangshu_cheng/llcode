import operator
def merge(left,right,compare):
    result=[]
    i,j=0,0
    while i<len(left) and j<len(right):
        if compare(left[i],right[j]):
            result.append(left[i])
            i+=1
        else:
            result.append(right[j])
            j+=1
    while (i<len(left)):
        result.append(left[i])
        i+=1
    while (j<len(right)):
        result.append(right[j])
        j+=1
    return result
    
def mergeSort(L,compare=operator.lt):
    if len(L)<2:
        return L[:]
    else:
        middle =int(len(L)/2)
        left=mergeSort(L[:middle], compare)
        right=mergeSort(L[middle:], compare)

        return merge(left,right,compare)


'''
Link list version:
'''
in python:
class ListNode(object):
    def __init__(self, val, next=None):
        self.val = val
        self.next = next
def findMid(head):
    slow,fast = head,head.next
    while fast and fast.next:
        slow = slow.next
        fast = fast.next.next
    return slow

def merge(h1,h2):
    dumy = ListNode(0)
    head = dumy
    while h1 and h2:
        if h1.val < h2.val:
            head.next = h1
            h1 = h1.next
        else:
            head.next = h2
            h2 = h2.next
        head = head.next

    if h1:
        head.next = h1
    elif h2:
        head.next = h2
    return dumy.next

def mergeSort(head):
    if not head or not head.next:
        return head

    mid = findMid(head)
    right = mergeSort(mid.next)
    mid.next = None
    left = mergeSort(head)

    return merge(left,right)