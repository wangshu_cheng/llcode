'''
和上题目的区别是它是K different color
Given an array of n objects with k different colors (numbered from 1 to k), 
sort them so that objects of the same color are adjacent, with the colors in the order 1, 2, ... k.

'''

class Solution:
    """
    @param colors: A list of integer
    @param k: An integer
    @return: nothing
    """
    def sortColors2(self, colors, k):
        # write your code here
        #same as sortcolor1, this time, we do the sortcolor1 first
        #then we sorted the smallest and the bigest color, than we start
        #from new left and rigtht, and contnuesly sort other colors
        #the 2nd smallest number and 2nd biggest number.
        nums = colors
        if not nums:
            return []
        
        # use similar to quick sort method, continues swap (only doable to 3 color, not doable in 4 color and more)
        # idea is, if it's 0, swap with left most , if it's 1 don't swap, if it's 2 swap with right most
        
        start,end = 0,len(nums) - 1
        cur = 0
        count = 0
                
        while count < k:
            l,r = start,end
            cur = l
            #find the biggest and smallest color this round:
            if len(nums[start : end + 1]) > 0:
                big = max(nums[start : end + 1])
                small = min(nums[start : end + 1])
            
            while cur <= r:
                if nums[cur] == small :
                    nums[l],nums[cur] = nums[cur],nums[l]
                    cur += 1
                    l += 1
                elif nums[cur] < big and nums[cur] > small:
                    cur += 1
                else:# nums[i] == big
                     nums[r],nums[cur] = nums[cur],nums[r]
                     r -= 1
            count += 2
            start,end = l,r