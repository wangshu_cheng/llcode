'''
quick sort:  先整体有序，再局部有序
#取最大点位pivot，程序比较好写；最大点作为一个存储点，不参与交换，直到最后再交换


'''


def partition(numbers,l,r):
    if l == r:
        return l
    pivot = numbers[r]
    index = l
    for i in range(l,r):
        if numbers[i] >= pivot: #降序；这里换成小于就是升序排序；
            numbers[i],numbers[index] = numbers[index],numbers[i]
            index +=1
    numbers[r] , numbers[index] = numbers[index],numbers[r]
    return index

#sort 用快排
def sort(numbers,l,r):
    if l >= r:
        return
    p = partition(numbers,l,r)
    sort(numbers,l,p-1)
    sort(numbers,p+1,r)

sort(numbers,0,len(numbers)-1)
print "after sort",numbers