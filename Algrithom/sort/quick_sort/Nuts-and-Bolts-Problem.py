'''
Given a set of n nuts of different sizes and n bolts of different sizes. There is a one-one mapping between nuts and bolts. 
Comparison of a nut to another nut or a bolt to another bolt is not allowed. 
It means nut can only be compared with bolt and bolt can only be compared with nut to see which one is bigger/smaller.

We will give you a compare function to compare nut with bolt.

Have you met this question in a real interview? Yes
Example
Given nuts = ['ab','bc','dd','gg'], bolts = ['AB','GG', 'DD', 'BC'].

Your code should find the matching bolts and nuts.

one of the possible return:

nuts = ['ab','bc','dd','gg'], bolts = ['AB','BC','DD','GG'].

we will tell you the match compare function. If we give you another compare function.

the possible return is the following:

nuts = ['ab','bc','dd','gg'], bolts = ['BC','AA','DD','GG'].

So you must use the compare function that we give to do the sorting.

The order of the nuts or bolts does not matter. You just need to find the matching bolt for each nut.


Thinking:
这是双partition。
先从nut里选一个做pivot，按此pivot与bolts array进行比较并且进行partition; 而后返回pivot element的index，代表对应原先选择的nut的bolt，
并且以此元素回到nuts array里面进行partition； 最后递归处理左右两边。



'''

# class Comparator:
#     def cmp(self, a, b)
# You can use Compare.cmp(a, b) to compare nuts "a" and bolts "b",
# if "a" is bigger than "b", it will return 1, else if they are equal,
# it will return 0, else if "a" is smaller than "b", it will return -1.
# When "a" is not a nut or "b" is not a bolt, it will return 2, which is not valid.
class Solution:
    # @param nuts: a list of integers
    # @param bolts: a list of integers
    # @param compare: a instance of Comparator
    # @return: nothing
    def sortNutsAndBolts(self, nuts, bolts, compare):
        # write your code here
        if nuts == [] or bolts == [] or compare is None:
            return 
        self.compare = compare
        self.quickSort(nuts, bolts, 0, len(nuts) - 1)

    def quickSort(self, nuts, bolts, left, right):
        if left >= right:
            return 
        #recursively partiion nuts array.
        split_nut_pos = self.partition(nuts, bolts[right], left, right)
        #this line is very important, it's recursively partition the bolts array.
        split_bolt_pos = self.partition(bolts, nuts[split_nut_pos], left, right)
        self.quickSort(nuts, bolts, left, split_nut_pos - 1)
        self.quickSort(nuts, bolts, split_nut_pos + 1, right)

    def partition(self, items, pivot, left, right):
        if items == [] or pivot == None:
            return

        #move piovt to right most(find the corresponding 'pivot partner' and swap to right)
        for i in range(left, right + 1):
            if self.compare.cmp(pivot, items[i]) == 0 \
                or self.compare.cmp(items[i], pivot) == 0:
                items[right], items[i] = items[i], items[right]
                break

        # pivot_partner = items[right]
        #use pivot to partition. small one in left of pivot partner, big on on right side of pivot partner
        index = left
        for i in range(left,right):
            if self.compare.cmp(items[i],pivot) == -1 or  self.compare.cmp(pivot,items[i]) == 1:
                items[index],items[i] = items[i],items[index]
                index += 1
        items[index],items[right] = items[right],items[index]

        return index


