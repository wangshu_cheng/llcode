'''
This is exactly quick sort.
'''

'''
Given a string which contains only letters. 
Sort it by lower case first and upper case second.
'''
class Solution:
    """
    @param chars: The letters array you should sort.
    """
    def sortLetters(self, chars):
        # write your code here
        #quick sort
        
        if not chars:
            return
        
        def partition(chars,l,r):
            if l == r:
                return l
            pivot = chars[r]
            idx = l
            while l < r:
                if chars[l] >= pivot:
                    chars[l],chars[idx] = chars[idx],chars[l]
                    idx += 1
                    l += 1
                else:
                    l += 1
            chars[idx],chars[r] = chars[r],chars[idx]
            return idx
        
        
        def sort(chars,l,r):
            if l >= r:
                return
            p = partition(chars,l,r)
            sort(chars,l,p - 1)
            sort(chars,p + 1,r)
            
        sort(chars,0,len(chars) - 1)
            
        
                
