'''
input是一个list of char, 要求把这个list分成几个尽可能小的sublist，
使得每个字符只出现在一个sublist中，return每个sublist的长度
example：
1. input [a,a,a], return 3, 因为你要让所有的a都在同一个sublist中
2. input [a,b,c,a,c,b,d,d,e,f,d, m,n,m], return [6,5,3], 
因为前6个组成的sublist中只含有abc，index from 6-10含邮def，最后3个含有mnm

Thinking:
1 先记录每个字符的最后一个index；
2 从左往右，找到目前的substring的最大的长度， 从而所有的元素的last index 都在里面了
就是一边move 一边比较，知道 substring 的右边界等于最大的last index

'''

def partitionLabels(self, S):
    """
    :type S: str
    :rtype: List[int]
    """
    if not S:
	    return []

    res = []

    mapping = [0 for i in range(26)]

    for i in range(len(S)):
        mapping[ord(S[i]) - ord('a')] = i

    start = 0
    last = 0
    for j in range(len(S)):
        last = max(last,mapping[ord(S[j]) - ord('a')])
        if last == j:
            res.append(j-start + 1)
            start = j + 1
    return res

