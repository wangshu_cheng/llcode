
var Header = React.createClass({
	render: function(){

		var headerStyle = {
			// backgroundColor:'#DCDCDC',
			backgroundColor:'#084B8A',
			height:'50px',
  		};
  		var numberStyle = {
  			marginLeft:'20px',
  			marginTop:'20px',
  			padding:'15px',
  			color:'white',
  			fontSize: '20px',
  			display:'inline'
  		};
  		 var titleStyle = {
  			margin:'5px',
  			padding:'5px',
  			color:'white',
  			fontSize: '20px',
  			display:'inline'

  		};

  		var backButton = {
  			float:'right',
  			backgroundColor:'inherit',
  			top:'-5px',
  			display:'inline'
  		};
  		var linkStyle = {
  			color:'white'
  		}

		return(
			<div style={headerStyle}>
				<button style={backButton}><a style={linkStyle} href="default.html">Back to issue list</a></button>
				<div style={numberStyle}>{this.props.data.number}</div>
				<div style={titleStyle}>{this.props.data.title}
				</div>
			</div>
			)
	}
})

var Detail = React.createClass({
    render: function() {
        var summary = this.props.data.body.toString();
        var divStyle = {
        	margin:"10px",
        	padding:"10px"
        };
        var textAreaStyle = {
        	backgroundColor:'#f5f5f5',
        	fontSize:'15px'
        }

        return ( 
        	<div  style={divStyle}>
            	<h3> Details </h3>
            	<textarea style={textAreaStyle} rows = "30" cols = "120" readOnly value = { summary } >
            	</textarea> 
            </div>
        )
    }
});


var Comment = React.createClass({
	componentDidMount: function(){
		var commentbody = $('div.commentbody');
		 //Try to replace @username to link ,but not success.
		 // commentbody.html(commentbody.html().replace(/@[a-zA-Z0-9]+\s/g,'<a href="https://github.com"'));
	},

  render: function() {
    var divStyle = {
    	margin:"10px",
    	padding:"10px",
    	color:'black'
    }
    var authorStyle = {
    	marginRight:"15px",
    	color:"blue"
    }
    var commentStyle = {
    	margin:"10px"
    }
    var content = this.props.body;

    return (
      <div className="comment">
        <span style={authorStyle} className="commentAuthor">
          {this.props.author}
        </span>
        <span >Added a comments at:   {this.props.created_at}</span>
        <div className="commentbody" style={commentStyle}>{content}</div>
        <hr/>
        
      </div>
    );
  }
});
var CommentList = React.createClass({
  render: function() {
  	
  	var commentNodes = [];
  	if (this.props.data){
	    commentNodes = this.props.data.map(function(comment) {
	      return (
	        <Comment author={comment.user.login} created_at={comment.created_at} body={comment.body} key={comment.id}>
	          {comment.body}
	        </Comment>
	      );
	    });
  		
  	}
    return (
      <div className="commentList">
        {commentNodes}
      </div>
    );
  }
});
var CommentBox = React.createClass({
	  getInitialState: function() {
        return { data: [{
        	"id": 171004058,
			    "number": "0",
			    "title": "",
			    "user": {
			        "login": "",
			    	},
			    "body":"",
			    "created_at":"",
			    "updated_at":"",
	        	} ]
	    	};
    },

    componentDidMount: function() {
    	console.log(" commentsbox mount")

        // this.setState({ data: commentsData });
    },
		render: function() {
			var divStyle = {
	        	margin:"10px",
	        	padding:"10px"
	        };
			return (
			  <div style={divStyle} className="commentBox">
			    <h1>Comments</h1>
			    <CommentList data={this.props.data}/>
			  </div>
			);
		}
	});


var People = React.createClass({
    render: function() {
        var divStyle = {
        	margin:"10px",
        	padding:"10px",
        	color:'black'
        }
        return ( 
        	<div className = "people" style={divStyle}>
	            <h3> People </h3> 
	            <table>
		            <tbody>
		            	<tr>
		            		<td>Reporter:</td>
		            		<td>{this.props.data.login}</td>
		            	</tr> 
		            	<tr>
		            		<td>Gravatar:</td>
		            		<td>{this.props.data.gravatar_id}</td>
		            	</tr> 
		            	
		            </tbody>
	            </table>
            </div>
        )
    }
})

var IssueDetail = React.createClass({
    getInitialState: function() {
        return { data: {
        	"id": 171004058,
			    "number": "0",
			    "title": "",
			    "user": {
			        "login": "",
			        "id": 1,
			        "avatar_url": "",
			        "gravatar_id": ""
			    	},
			    "body":"",
			    "comments_url":""
	        	}
	    	}
    },
    loadDataFromServer: function(id) {

			$.ajax({
	      url: this.props.url+id,
	      dataType: 'json',
	      cache: false,
	      success: function(data) {
	        this.setState({data: data});
		      return data;
	      }.bind(this),
	      error: function(xhr, status, err) {
	        console.error(this.props.url, status, err.toString());
	      }.bind(this)
	    });

			$.ajax({
	      url: this.props.url+id+'/comments',
	      dataType: 'json',
	      cache: false,
	      success: function(data) {
	        this.setState({comments: data});
		  		console.log("loaded comments",data)
		      return data;
	      }.bind(this),
	      error: function(xhr, status, err) {
	        console.error(this.props.url, status, err.toString());
	      }.bind(this)
	    });


    },
    componentDidMount: function() {
        var issueNumber = decodeURIComponent(location.search.substring("?number=".length));
        console.log("did", issueNumber)
        if (isNaN(issueNumber)) {
            return
        }
        this.loadDataFromServer(issueNumber);
        console.log("detail issue number", issueNumber);
    },
    render: function() {

        return ( 
        	<div className="detailBox" >
        	    <Header data = {this.state.data} />
	            <People data = {this.state.data.user}/>
	            <Detail data = {this.state.data}/>
	            <CommentBox data = {this.state.comments}/>
          </div>
        )
    }
});
ReactDOM.render(
    <IssueDetail url="https://api.github.com/repos/rails/rails/issues/"/> ,
    document.getElementById('detail')
);
