var Label = React.createClass({

	render: function(){
		return (
			<span>{this.props.name},</span>
		)
	}

});

var Issue = React.createClass({
  rawMarkup: function() {
    var md = new Remarkable();
    var rawMarkup = md.render(this.props.children);
    return { __html: rawMarkup };
  },
	clickHandler: function(e){
		console.log("click in issue",$(e.target).text());
		var issueNumber = $(e.target).text();
		document.location.href = "detail.html?number="+issueNumber;

	},
  render: function() {
  	var labels = this.props.labels.map(function(label){
  		return (
  			<Label name={label.name} key={label.name}></Label>
  		);

  	});
  	var spanStyle = {
			cursor:'pointer',
			color:'blue',
			textDecoration: 'underline'
  	};
    return (
	     <tr>
	        <td onClick={this.clickHandler}><span style={spanStyle}>{this.props.number}</span></td>
	        <td>{this.props.title}</td>
	        <td>{this.props.user.login}</td>
	        <td>{this.props.user.gravatar_id}</td>
	        <td>{this.props.summary}</td>
	        <td> {labels}</td>
	     </tr>
    );
  }
});



var IssuesList = React.createClass({
	clickHandler: function(e){
		console.log("click in issue list",event.target)

	},
  render: function() {
    var issues = this.props.data.map(function(issue) {
    	// cut 140 chars without break word:
    	var summary = issue.body.substring(0,145).replace(/^(.{140}[^\s]*).*/, "$1");
      return (
        <Issue number={issue.number} title={issue.title} summary={summary} user={issue.user} labels={issue.labels} key={issue.id} >
        </Issue>
      );
    });
    return (

			<table>
	        <thead>
	          <tr>
	            <th>id</th>
	            <th>Title</th>
	            <th>Reporter</th>
	            <th>Gravata</th>
	            <th>Summary</th>
	            <th>Labels</th>
	          </tr>
	        </thead>
	        <tbody >{issues}</tbody>
		   </table>
	 
    );
  }
});

var Pagination = React.createClass({
  getInitialState: function() {
    return {data: []};
  },
	handleChange: function() {
		var pageNumber = Number(this.refs.pageNumberInput.value);
		pageNumber = isNaN(pageNumber) ? 0 : pageNumber;
		pageNumber = pageNumber <0 ? 0 : pageNumber;
		this.props.onUserInput(
		  pageNumber
	)},
	clickHandler: function(e){
		if (e.target.name == "next"){
			if (parseInt(this.refs.pageNumberInput.value) >1){
				$('button[name=prev]').prop("disabled",false);
			}
      this.props.onUserInput(parseInt(this.refs.pageNumberInput.value) + 1);
		}
		else if(e.target.name == "prev"){
			if (parseInt(this.refs.pageNumberInput.value) <=1){
				$('button[name=prev]').prop("disabled",true);
			}
      this.props.onUserInput(parseInt(this.refs.pageNumberInput.value) - 1);

		}
	},
  render: function() {
    return (
      <div className="pagination">
        <span>Page:</span>
         <button className="pagination" name="prev" onClick={ this.clickHandler }>prev</button>

		     <input
          type="text"
          value={this.props.pageNumber}
          ref="pageNumberInput" 
          onChange={this.handleChange}
         />     
         <button className="pagination" name = "next" onClick={ this.clickHandler }>next</button>
      </div>
    );
  }
});

var IssueGrid = React.createClass({
  getInitialState: function() {
    return {
			pageNumber: '1',
    	data: []
    };
  },
  loadDataFromServer: function(pageNumber){

		$.ajax({
      url: this.props.url+'?'+'page='+pageNumber+'&per_page=25',
      dataType: 'json',
      cache: false,
      success: function(data) {
        this.setState({data: data});
	  		console.log("loaded",data)
	      return data;
      }.bind(this),
      error: function(xhr, status, err) {
        console.error(this.props.url, status, err.toString());
      }.bind(this)
    });

  },
	componentDidMount: function() {
		$('button[name=prev]').prop("disabled",true);
		this.loadDataFromServer(1);
	},
  handleUserInput: function(pageNumber) {
  	console.log("page number change",pageNumber)
    this.setState({
      pageNumber: pageNumber,
    });
    this.loadDataFromServer(pageNumber);
  },
  render: function() {
  	console.log('grid rendered',this.state.data)
    return (
      <div className="commentBox">
        <Pagination pageNumber={this.state.pageNumber} onUserInput={this.handleUserInput}/>
        <h1>Issue List:</h1>
        <IssuesList data={this.state.data} />
      
      </div>
    );
  }
});
ReactDOM.render(
  <IssueGrid  url="https://api.github.com/repos/rails/rails/issues" />,	
  document.getElementById('root')
);


