// define global objects
var chai = require('chai'), chaiAsPromised = require('chai-as-promised');
chai.use(chaiAsPromised);
var assert = chai.assert, expect = chai.expect, should = chai.should();

// define page objects
var ThisPage = require('./default.po.js');
var thisPage = new ThisPage();

module.exports = function() {

  /**
   * This step is used for starting from Provision page
   */
  this.Given(/^I am at default page$/, function(callback) {
    var that = this;

      thisPage.get().then(function() {
        callback();
      });

  });


  this.Given(/^I am at Provision Page with "([^"]*)" testing condition$/, function(condition, callback) {
    var that = this;

    browser.driver.sleep(200);

    this.clearStatusQ().then(function() {
      that.bookStatusCodes(statusCodesMap[condition],thisQ).then(function() {
        thisPage.get().then(function() {
          callback();
        });
      });
    });
  });


  /**
   * checking elements exist
   * @param {string} key of the element
   */
  this.Then(/^I should see "([^"]*)" on Provision Page$/, function(elem, callback) {
    var that = this;
    var timeout = 3000;

    thisPage.getJustElement(elem, timeout).then(function(result) {
      that.assert(function() {
        result.should.exist;
      }, callback);
    });
  });

  /**
   * checking elements not exist
   * @param {string} key of the element
   */
  this.Then(/^I should not see "([^"]*)" on Provision Page$/, function(elem, callback) {
    var that = this;
    browser.driver.sleep(3000);

    thisPage.getJustElement(elem,1).then(function(result) {
      that.assert(function() {
        expect(result).be.null;
        // result.should.equal({});
      }, callback);
    });

  });

  /**
   * checking elementw with expected value
   * @param  {string} elem          key of the element
   * @param  {string} checkingValue expected value
   */
  this.Then(/^I should see "([^"]*)" having "([^"]*)" on Provision Page$/, function(elem, checkingValue, callback) {
    var that = this;
    var timeout = 500;

    thisPage.getElement(elem).then(function(result) {
      that.assert(function() {
        result.should.equal(checkingValue);
      }, callback);
    });

  });

  /**
   * click an element
   * @param {string} key of the element
   */
  this.When(/^I click "([^"]*)" on Provision Page$/, function(elem, callback) {
    browser.driver.sleep(500);
    thisPage.clickElement(elem).then(function(result) {
      callback();
    });

  });

  /**
   * key in data on specific field
   */
  this.When(/^I input data "([^"]*)" into "([^"]*)" on Provision page$/, function(data, elem, callback) {
    thisPage.inputData(elem, data).then(function(result) {
      callback();
    });
  });

  /**
   * select combobox on specific field
   */
  this.When(/^I set data "([^"]*)" into "([^"]*)" on Provision page$/, function(data, elem, callback) {
    // thisPage.clickElement(elem).then(function(result) {
    thisPage.setData(elem, data).then(function(result) {
      callback();
    });
  });

  /**
   * checking count of specific elements
   * @param  {string} count     expected number
   * @param  {string} elem      key of the elements
   */
  this.Then(/^I should see total "([^"]*)" of "([^"]*)" on Provision page$/, function(count, elem, callback) {
    var that = this;

    thisPage.getNumOfElements(elem).then(function(cnt) {
      cnt.should.have.length(Number(count)); callback();
      // cnt.should.equal(Number(count)); callback();
    });

  });

};

