Feature: Detail Page Test

    Background:
      Given I am at the detail page

    Scenario: check element
      Then I should see "issue number" on detail page
      Then I should see "issue title" on detail page
      Then I should see "back button" on default page
      Then I should see "people section" on default page
      Then I should see "detail section" on default page
      Then I should see "comments section" on default page

    Scenario: check header
      Then I should see issue number is "111" on detail page
      Then I should see issue title is "Missing URL helper for namespaced `root` route" on detail page

    Scenario: check detail
      Then I should see detail contains "While upgrading our app to Rails 5.0" on detail page
    Scenario: check comments
      Then I should see comments "0" author is "abc" on detail page
      Then I should see comments "0" content contains "I tried to fix the predicate builder" on detail page




