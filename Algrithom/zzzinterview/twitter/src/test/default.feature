Feature: Default Page Test

    Background:
      Given I am at the default page

    Scenario: check element
      Then I should see "prev button" on default page
      Then I should see "next button" on default page
      Then I should see "page number input" on default page
      Then I should see "Issue grid" on default page

    Scenario: pagination check
      When I input "2" into pagination input box
      Then I should see "2" in the input box
      When I input "abc" into pagination input box
      Then I should see "0" in the input box
      When I input "-1" into pagination input box
      Then I should see "0" in the input box

    Scenario: check issue list grid
      Then I should see totally "5" issue on issue grid




