(function(){

  /*Answer to Question 1:*/
  var sampleJSONString = '{"ID":1,"Label":"A","children":[{"ID":2,"Label":"B","children":[{"ID":5,"Label":"E"},{"ID":6,"Label":"F"},{"ID":7,"Label":"G"}]},{"ID":3,"Label":"C"},{"ID":4,"Label":"D","children":[{"ID":8,"Label":"H"},{"ID":9,"Label":"I"}]}]}';

  /*Answer to Question 2:*/  //Using JQuery:  
  var sampleJSONObj = {};
  /* WE ARE NOT GOING TO MAKE REAL AJAX CALL, SO COMMENT OUT BELOW ANSWER TO AVOID ERROR.
  $.ajax({
        url:"/dumpServer/simpleJSONResource",
        dataType : "json"   
      })
      .done(function(result) {
         sampleJSONObj = result;
      })
      .fail(function (request, status, error) {
         console.error(request.responseText);
      });
  */
   // After the ajax call, we should get the json obj like this:
  sampleJSONObj = JSON.parse(sampleJSONString);
 
  /*Answer to Question 3:*/
  var findLabel = function(obj,id){
    var result = [''];
    var helper = function(obj,id,result){
      if (!obj ){
        return;
      }
      if (obj.ID == id){
        result[0] = obj.Label;
        return;
      }

      if (obj.children){
        for (var i = 0; i < obj.children.length; i++){
          helper(obj.children[i],id,result);
        }  
      }
    }
    helper(obj,id,result);
    return result[0];
    
  }
  
  /*Test cases*/
  //Test1, Null object test:
  console.log('test result ->',findLabel(null,1)); // output: test result -> null
  //Test2, first level node test:
  console.log('ID 1, Label is ->',findLabel(sampleJSONObj,1)); // output: A
  //Test3, 2nd level node test:
  console.log('ID 2, Label is ->',findLabel(sampleJSONObj,2)); // output: B
  console.log('ID 3, Label is ->',findLabel(sampleJSONObj,3)); // output: C
  console.log('ID 4, Label is ->',findLabel(sampleJSONObj,4)); // output: D
  //Test4, 3nd level node test:
  console.log('ID 5, Label is ->',findLabel(sampleJSONObj,5)); // output: E
  console.log('ID 6, Label is ->',findLabel(sampleJSONObj,6)); // output: F
  console.log('ID 7, Label is ->',findLabel(sampleJSONObj,7)); // output: G
  console.log('ID 8, Label is ->',findLabel(sampleJSONObj,8)); // output: H
  console.log('ID 9, Label is ->',findLabel(sampleJSONObj,9)); // output: I
  //Test 5, Non exist node test:
  console.log('ID 15, Label is ->',findLabel(sampleJSONObj,15)); // output: null
  //Test 6, negtive node test:
  console.log('ID -1, Label is ->',findLabel(sampleJSONObj,-1)); // output: null


/*Answer to Question 4:*/
  //Define a validtion service:
  var ValidateService = function(){
    var regObj = {
      alpha : /^[a-z]+$/i,
      alphanum : /^[a-z0-9]+$/i,
      num : /^[0-9]+$/i
     };
     return {
       validateOnReg : function(reg1,data){
         if (!reg1.test(data)){
           return false;
         }
         return true;
       },
       validateAlpha : function(data){
         return this.validateOnReg(regObj.alpha,data);
       },
       validateAlphanum : function(data){
         return this.validateOnReg(regObj.alphanum,data);
       },
       validateNum : function(data){
         return this.validateOnReg(regObj.num,data);
       }
     }

  }

  //Instanciate a validate service
  var vs = new ValidateService();

  //Click ok button to validte the form
  $('input[name=ok]').click( function(e) {
    //Validate name field:
    var inputval = $('input[name=name]').val();
    if (!vs.validateAlpha(inputval) || inputval.length > 100 ){
      $('input[name=name]').addClass("error");
      $('span[name=name]').removeClass("invisible");
    }
    else{
      $('input[name=name]').removeClass("error");
      $('span[name=name]').addClass("invisible");
    }

    //Validate Address1 field:
    inputval = $('input[name=address1]').val();
    if (!vs.validateAlphanum(inputval) || inputval.length > 100 ){
      $('input[name=address1]').addClass("error");
      $('span[name=address1]').removeClass("invisible");
    }
    else{
      $('input[name=address1]').removeClass("error");
      $('span[name=address1]').addClass("invisible");
    }

    //Validate Address2 field:
    inputval = $('input[name=address2]').val();  
    if (inputval.length >= 1 && !vs.validateAlphanum(inputval) || inputval.length > 100 ){
      $('input[name=address2]').addClass("error");
      $('span[name=address2]').removeClass("invisible");
    }
    else{
      $('input[name=address2]').removeClass("error");
      $('span[name=address2]').addClass("invisible");
    }

    //Validate City field:
    inputval = $('input[name=city]').val();  
    if (!vs.validateAlphanum(inputval) || inputval.length > 50 ){
      $('input[name=city]').addClass("error");
      $('span[name=city]').removeClass("invisible");
    }
    else{
      $('input[name=city]').removeClass("error");
      $('span[name=city]').addClass("invisible");
    }

    //Validate States field:
    inputval = $('select[name=states]').val();  
    if (!vs.validateAlpha(inputval) || inputval.length != 2){
      $('select[name=states]').addClass("error");
      $('span[name=states]').removeClass("invisible");
    }
    else{
      $('select[name=states]').removeClass("error");
      $('span[name=states]').addClass("invisible");
    }
    //Validate zip code field:
    inputval = $('input[name=zipcode]').val();  
    if (!vs.validateAlphanum(inputval) || inputval.length != 5 ){
      $('input[name=zipcode]').addClass("error");
      $('span[name=zipcode]').removeClass("invisible");
    }
    else{
      $('input[name=zipcode]').removeClass("error");
      $('span[name=zipcode]').addClass("invisible");
    }
  });

  //Cancel to reset form
  $('input[name=cancel]').click( function(e) {
    $('input[type=text]').val('');
    $('input[type=text]').removeClass('error');
    $('select').removeClass('error');
    $('span').addClass('invisible');
    $('select[name=states]').val("Not Applicable");  
  })



})("docReady", window)
