def dragon(nums):
    if not nums:
        return 0
    if len(nums) == 1:
        return 0
    n = len(nums)

    start,end = 0,0

    jumps = 0

    res = []
    idx = 0
    while end < n:
        distance = 0
        jumps += 1
        for i in range(start,end + 1):
            if i + nums[i] > distance:
                idx = i
                distance = i + nums[i]
            if distance > n - 1:
                break 

        res.append(idx)
        start = end + 1
        end = distance

        if start > end:
            break

    return res

# nums = [5,6,0,4,2,4,1,0,0,4]
nums=[]
print dragon(nums)
# 0 5 9
