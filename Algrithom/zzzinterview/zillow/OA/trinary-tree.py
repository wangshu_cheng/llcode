# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.mid = None
        self.right = None


class Solution:

	def main(self):
		
		t = self.insert(5,None);
		print("Insert 7,8,6,3,2,2")
		print("------------")
		self.insert(7,t);
		self.insert(9,t);
		self.insert(6,t);
		self.insert(3,t);
		self.insert(2,t);
		self.insert(2,t);
		
		self.show(t);
		print("Now delete 3")
		print("------------")
		self.delete(3,t);
		self.show(t);
		print("Now delete 7")
		print("------------")
		self.delete(7,t);
		self.show(t);
		print("Now delete 6")
		print("------------")
		self.delete(6,t);
		self.show(t);
		print("Now delete 2,2")
		print("------------")
		self.delete(2,t);
		self.delete(2,t);
		self.show(t);

		'''
			test result:
			
			Insert 7,8,6,3,2,2
			------------
			node:2
			node:2
			node:3
			node:5
			node:6
			node:7
			node:9
			Now delete 3
			------------
			node:2
			node:2
			node:5
			node:6
			node:7
			node:9
			Now delete 7
			------------
			node:2
			node:2
			node:5
			node:6
			node:9
			Now delete 6
			------------
			node:2
			node:2
			node:5
			node:9
			Now delete 2,2
			------------
			node:5
			node:9


		'''

	def insert(self,value,root):
		if root == None:
			return TreeNode(value)

		if value < root.val:
			if root.left:
				self.insert(value,root.left)
			else:
				root.left = TreeNode(value)
		elif value > root.val:
			if root.right:
				self.insert(value,root.right)
			else:
				root.right = TreeNode(value)
		else:
			if root.mid:
				self.insert(value,root.mid)
			else:
				root.mid = TreeNode(value)
		return root

	def delete(self,value,root):
		if not root:
			return
		if value < root.val:
			root.left = self.delete(value,root.left)
		elif value > root.val:
			root.right = self.delete(value,root.right)
		else:
			if root.mid:
				root.mid = self.delete(value,root.mid)
			elif root.right:
				minNode = self.treeMin(root.right)
				root.val = minNode.val
				root.right = self.delete(minNode.val,root.right)
			else:
				root = root.left
		return root

	def treeMin(self,root):
		if not root:
			return
		while root.left:
			return treeMin(root.left)
		return root

	def show(self,root): #inorder
		if root:
			self.show(root.left)
			print("node:" + str(root.val))
			self.show(root.mid)
			self.show(root.right)
test = Solution()
test.main()





