
import sys
class Solution:

	def main(self):
		overflowTest = "923840981238974365921734"
		negOverflowTest = "-923840981238974365921734"
		emptyTest = ""
		spaceTest = "    9377498   "
		negspaceTest = "    -9377498"
		pSpaceTest = "    +9377498"
		invalid1 = "+937+-7498"
		invalid2 = "asdfasdfsadf"
		invalid3 = "1asdfasdfsadf"
		invalid4 = "a2sdfasdfsadf"


		print overflowTest +'---->'+ str(self.string2Long(overflowTest)) #should return 
		print "Empty String" +'---->'+ str(self.string2Long(emptyTest))
		print spaceTest +'---->'+str(self.string2Long(spaceTest))
		
		print negspaceTest +'---->'+str(self.string2Long(negspaceTest))
		print pSpaceTest +'---->'+str(self.string2Long(pSpaceTest))
		print invalid1 +'---->'+str(self.string2Long(invalid1))
		print invalid2 +'---->'+str(self.string2Long(invalid2))
		print invalid3 +'---->'+str(self.string2Long(invalid3))
		print invalid4 +'---->'+str(self.string2Long(invalid4))

		'''
				Test result:
				923840981238974365921734---->9223372036854775807
				Empty String---->0
				    9377498   ---->9377498
				    -9377498---->-9377498
				    +9377498---->9377498
				+937+-7498---->937
				asdfasdfsadf---->0
				1asdfasdfsadf---->1
				a2sdfasdfsadf---->0
		'''

	def string2Long(self,s):
		if not s:
			return 0
		
		s = s.strip()

		LEN = len(s)

		if LEN == 0:
			return 0

		left = 0
		nFlag = False
		res = 0

		if s[left] == '+':
			left += 1
		elif s[left] == '-':
			left += 1
			nFlag = True

		if left == LEN:
			return 0

		for i in range(left,LEN):
			if not s[i].isdigit():
				return res
			cal = int(s[i])

			if self.isOverflow(nFlag,res,cal):
				return -sys.maxint if nFlag else sys.maxint
			else:
				res = res * 10 + cal if not nFlag else res * 10 - cal
		return res

	def isOverflow(self,nFlag,res,cal):
		if not nFlag:
			return res > (sys.maxint - cal) / 10
		else:
			return res < (-sys.maxint + cal) / 10

test = Solution()
test.main()







