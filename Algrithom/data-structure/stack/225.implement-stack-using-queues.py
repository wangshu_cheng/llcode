'''
Implement the following operations of a stack using queues.

push(x) -- Push element x onto stack.
pop() -- Removes the element on top of the stack.
top() -- Get the top element.
empty() -- Return whether the stack is empty.
Example:

MyStack stack = new MyStack();

stack.push(1);
stack.push(2);  
stack.top();   // returns 2
stack.pop();   // returns 2
stack.empty(); // returns false


Thinking: 2 个queue来回倒腾，来模拟后进先出
'''


class MyStack(object):

    # 2 个queue来回倒腾，来模拟后进先出
    def __init__(self):
        """
        Initialize your data structure here.
        """
        self.queue1 = []
        self.queue2 = []

    def push(self, x):
        """
        Push element x onto stack.
        :type x: int
        :rtype: void
        """
        if self.empty():
            self.queue1.append(x)
        else:
            if self.queue1:
                self.queue2.append(x)
                while self.queue1:
                    self.queue2.append(self.queue1.pop(0))
                    
            elif self.queue2:
                self.queue1.append(x)
                while self.queue2:
                    self.queue1.append(self.queue2.pop(0))
        

    def pop(self):
        """
        Removes the element on top of the stack and returns that element.
        :rtype: int
        """
        if self.queue1:
            return self.queue1.pop(0)
        elif self.queue2:
            return self.queue2.pop(0)
            

    def top(self):
        """
        Get the top element.
        :rtype: int
        """
        if self.queue1:
            return self.queue1[0]
        elif self.queue2:
            return self.queue2[0]
        return 0
        

    def empty(self):
        """
        Returns whether the stack is empty.
        :rtype: bool
        """
        return len(self.queue1)==0 and len(self.queue2) == 0


# Your MyStack object will be instantiated and called as such:
# obj = MyStack()
# obj.push(x)
# param_2 = obj.pop()
# param_3 = obj.top()
# param_4 = obj.empty()