'''
Implement a stack with min() function, which will return the smallest number in the stack.
It should support push, pop and min operation all in O(1) cost.

注意，该题目并不要求pop 每次都pop出最小的值

Thinking:
Key thing is create a minstack, which doesn't need to store all the stack data,
 it only need to store all the values smaller than stack bottom, like, 

if stack is:

    1
    5
    2

then min stack is need to be:
    1
    2

and 5 is no need to be stored, since 2 is bottom, we only need to store it and any value smaller than it.

kao dian:
 know how to use a small stack to store the decreasing value start from bottom value.


'''



class MinStack(object):

    def __init__(self):
        # do some intialize if necessary
        self.stack = []
        self.minstack = []

    def push(self, number):
        # write yout code here
        #store the bottom value and any value smaller then it.
        if len(self.minstack) == 0 or number <= self.minstack[-1]: #must use <=
            self.minstack.append(number)
        self.stack.append(number)

    def pop(self):
        # pop and return the top item in stack
        if not self.stack: return
        if self.stack[-1] == self.minstack[-1]:
            self.minstack.pop()
        return self.stack.pop()

    def min(self):
        # return the minimum number in stack
        return self.minstack[-1]

#testcase:
[1,5,2],[],[1,1,1]

