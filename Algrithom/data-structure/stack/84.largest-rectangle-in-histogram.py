"""
Given n non-negative integers representing the histogram's 
bar height where the width of each bar is 1, find the area
 of largest rectangle in the histogram.
"""

class Solution:
    """
    @param height: A list of integer
    @return: The area of largest rectangle in the histogram
    """
    def largestRectangleArea(self, height):
        # write your code here
        stack=[]; i=0; area=0
        while i<len(height):
            if stack==[] or height[i]>height[stack[len(stack)-1]]:
                stack.append(i)
            else:
                curr=stack.pop()
                width=i if stack==[] else i-stack[len(stack)-1]-1
                area=max(area,width*height[curr])
                i-=1
            i+=1
        while stack!=[]:
            curr=stack.pop()
            width=i if stack==[] else len(height)-stack[len(stack)-1]-1
            area=max(area,width*height[curr])
        return area

# brute force: time out:
# 每个点往左往右找到以该点为中心的最大长方形
class Solution(object):
    def largestRectangleArea(self, height):
        # write your code here

        if not height:
            return 0


        def getArea(i,height):
            #move i left and right to get largest area possible:
            N = len(height)
            bar = height[i]
            area = bar
            j = i - 1
            while j >=0:
                if height[j] >= bar:
                    area += bar
                    j -= 1
                else:
                    break
            j = i + 1
            while j < N:
                if height[j] >= bar:
                    area += bar
                    j += 1
                else:
                    break
            return area 


        area = 0
        for i in range(len(height)):
            area = max(area,getArea(i,height))

        return area