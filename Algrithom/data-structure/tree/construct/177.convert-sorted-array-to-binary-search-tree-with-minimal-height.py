'''
Question:
Given a sorted (increasing order) array, Convert it to create a binary tree with minimal height.

Notice
There may exist multiple valid solutions, return any of them.

Example
Given [1,2,3,4,5,6,7], return

     4
   /   \
  2     6
 / \    / \
1   3  5   7

Thinking:
Minium height means height balanced, since the array is sorted, so it should be a BST.

The key thing is find the root.
the root should be the middle number which lean to left (or right, which is same)
if lean to left,

This is different with convert sorted link list to BST, since in array case, we can easily 
get mid element so we can use pre order; but in link list case, we can only use in order.

'''

class Solution:
    """
    @param A: a list of integer
    @return: a tree node
    """
    def sortedArrayToBST(self, A):
        # write your code here
        
        if not A:
            return
        def build(arr,start,end):
            if start > end:
                return None
                
            mid = (start + end) / 2
            root = TreeNode(arr[mid])
            
            root.left = build(arr,start,mid - 1)
            root.right = build(arr,mid + 1,end)
            
            return root
        if len(A) == 0: return None
        return build(A,0,len(A)-1)