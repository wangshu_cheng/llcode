'''
Question:
Given inorder and postorder traversal of a tree, construct the binary tree.

Notice

You may assume that duplicates do not exist in the tree.

Example
Given inorder [1,2,3] and postorder [1,3,2], return a tree:

  2
 / \
1   3


Thinking:
To construct a tree, we need to find the root and left,right half.
and then do it recursively on each half.

We can get root from Postorder easily (the last node)
And we can use root to get the position(index) from the inorder, so to divide the
tree into half.

'''
"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""


class Solution:
    """
    @param inorder : A list of integers that inorder traversal of a tree
    @param postorder : A list of integers that postorder traversal of a tree
    @return : Root of a tree
    """
    def buildTree(self, inorder, postorder):
        # write your code here
        if not inorder or not postorder:
            return None
        #get the root:which is last one
        root=TreeNode(postorder.pop())
        #get the position of root in postorder:
        pos=inorder.index(root.val)
        #recursively set left and right child:
        #must do right first, because in post order,right sub tree come first
        # like:   .......left subtree root,right subtree root,root
        root.right=self.buildTree(inorder[pos+1:],postorder)
        root.left=self.buildTree(inorder[:pos],postorder)
        
        return root