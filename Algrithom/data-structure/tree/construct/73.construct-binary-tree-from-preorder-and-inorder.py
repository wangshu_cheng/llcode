"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""


class Solution:
    """
    @param preorder : A list of integers that preorder traversal of a tree
    @param inorder : A list of integers that inorder traversal of a tree
    @return : Root of a tree
    """
    def buildTree(self, preorder, inorder):
        # write your code here

        #Border case:,keep slice inorder and pop preorder, if anyone of it is empty, then this sub tree is ended.
        if not inorder or not preorder:return None
        #get the root:
        root=TreeNode(preorder.pop(0))
        #get the position of root in inorder:
        pos=inorder.index(root.val)
        #recursively set left and right child:
        #must do left first, because of preorder,left sub tree come first
        # like:   root,subtree root,right subtree root,.....
        root.left=self.buildTree(preorder,inorder[:pos])
        root.right=self.buildTree(preorder,inorder[pos+1:])
        return root