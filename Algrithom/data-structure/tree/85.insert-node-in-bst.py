'''
Question:
Insert a node in bst, after insert it's still a bst


Thinking:
注意，这种题目并不是要break目前的树结构，而是把新node挂到某个空节点上。

'''

"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""
class Solution:
    """
    @param root: The root of the binary search tree.
    @param node: insert this node into the binary search tree.
    @return: The root of the new binary search tree.
    """
    def insertNode(self, root, node):
        # write your code here
        if not root:
            return node
        
        def helper(root,node):
            if not node:
                return
            if node.val > root.val:
                if root.right == None:
                    root.right = node
                    return
                helper(root.right,node)
            else:
                if root.left == None:
                    root.left = node
                    return
                helper(root.left,node)
                
        helper(root,node)
        return root