# 366. Find Leaves of Binary TreeGiven a binary tree, collect a tree's nodes as if 
# you were doing this: Collect and remove all leaves, repeat until the tree is empty.

# Example:
# Given binary tree 
#           1
#          / \
#         2   3
#        / \     
#       4   5    
# Returns [4, 5, 3], [2], [1].

# Explanation:
# 1. Removing the leaves [4, 5, 3] would result in this tree:

#           1
#          / 
#         2          
# 2. Now removing the leaf [2] would result in this tree:

#           1          
# 3. Now removing the leaf [1] would result in the empty tree:

#           []         
# Returns [4, 5, 3], [2], [1].

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def findLeaves(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        self.res = []     
        
        def helper(root,ar):
            if not root:
                return
            if root.left == None and root.right == None:
                return True
            left = helper(root.left,ar)
            right = helper(root.right,ar)
            # if left is leave, cut left, if right is leave cut right
            if left:
                ar.append(root.left.val)
                root.left = None
            if right:
                ar.append(root.right.val)
                root.right = None
                
        if not root:
            return []
        
        while root.left or root.right:
            self.line=[]
            helper(root,self.line)
            self.res.append(self.line)
            
        self.res.append([root.val])
        
        return self.res
            
            
                
                