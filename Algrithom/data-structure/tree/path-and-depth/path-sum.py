'''
Question:
Given a binary tree and a sum, determine if the tree has a root-to-leaf path such that adding up 
all the values along the path equals the given sum.

For example:
Given the below binary tree and sum = 22,
              5
             / \
            4   8
           /   / \
          11  13  4
         /  \      \
        7    2      1
return true, as there exist a root-to-leaf path 5->4->11->2 which sum is 22.

Thinking:
we need to search for ALL the path to get answer , but we don't need get the subset.
we can use subset template, but don't have to.
Use simple divide and conquer, for each child node, 我们减去当前node的值

'''

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

#Method1: BFS

class Solution(object):
    def hasPathSum(self, root, sum):
        """
        :type root: TreeNode
        :type sum: int
        :rtype: bool
        """
        if not root:
            return False
        q = [(root,root.val)]
        
        while q:
            cur,curval = q.pop()
            if cur.left == None and cur.right==None and curval == sum:
                return True
            
            if cur.left:
                q.append([cur.left,curval + cur.left.val])
            if cur.right:
                q.append([cur.right,curval+cur.right.val])
                
        return False


#Method2: recursive
class Solution(object):
    def hasPathSum(self, root, sum):
        """
        :type root: TreeNode
        :type sum: int
        :rtype: bool
        """
        def helper(root,sum):
            if not root:
                #当node 为none（已经出树了）依然不等于true，那么return false
                return False
            if root.left == None and root.right == None:
                return sum == root.val
            #只要有一边有解即可
            return helper(root.left,sum - root.val) or helper(root.right,sum  - root.val)
                    
        return helper(root,sum)
