'''
Question:
Given a binary tree, return all root-to-leaf paths.

For example, given the following binary tree:

   1
 /   \
2     3
 \
  5
All root-to-leaf paths are:

["1->2->5", "1->3"]

Thinking:
Tree path是从root到leaf，不需要用 subset 模板的原因是，那些不从root开始，不到leaf结束的 path 你都不需要
'''

# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):

    #Method1, 如果我们不需要拿到所有具体的所有路径（包括不从root开始的，和不到leaf结束的） 我们不一定要用subset模板。
    #直接用简单的divide conquer 做法，每一级加上string 的node
    def binaryTreePaths(self, root):
        """
        :type root: TreeNode
        :rtype: List[str]
        """
       
        def helper(root,path):
            if not root:
                return
            if root.left == None and root.right == None: 
                ans.append(path)
                return
            if root.left:
                helper(root.left,path +"->"+ str(root.left.val))
            if root.right:
                helper(root.right,path + "->"+ str(root.right.val))
        
        if not root:
            return []
        ans = []
        helper(root,str(root.val))
        return ans



    #2: strict use 插分插弟旦 模板, looks no need，因为我们不需要中间path
    def binaryTreePaths(self, root): 
        def convert2string(arr):
            if not arr:
                return ''
            result = []
            for path in arr:
                result.append('->'.join(path))
            return result
        
        def helper(root,path):
            if not root:
                return
            if root.left == None and root.right == None:
                ans.append(list(path))
                
            if root.left:
                path.append(str(root.left.val))
                helper(root.left,path)
                path.pop()
            if root.right:
                path.append(str(root.right.val))
                helper(root.right,path)
                path.pop()
        
        if not root:
            return []
        ans = []
        path = [str(root.val)]
        helper(root,path)
        return convert2string(ans)

s = Solution()
t1 = TreeNode(1)
t2 = TreeNode(2)
t3 = TreeNode(3)
t5 = TreeNode(5)

t1.left = t2
t1.right = t3
t2.right = t5

print s.binaryTreePaths(t1)
