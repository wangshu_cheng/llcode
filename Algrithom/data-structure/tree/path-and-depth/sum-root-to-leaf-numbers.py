'''
Question:

Given a binary tree containing digits from 0-9 only, each root-to-leaf path could represent a number.
An example is the root-to-leaf path 1->2->3 which represents the number 123.
Find the total sum of all root-to-leaf numbers.

Thinking:

'''
# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def sumNumbers(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        def helper(node,s):
        	# base case:
            if node == None:
            	return 0
            # d and d is bottom up
            if node.left == None and node.right == None:
               return s*10+node.val
            # divide
            left = helper(node.left,s*10+node.val)
            right = helper(node.right,s*10+node.val)
            #return result
            return  left + right
        return helper(root,0)
