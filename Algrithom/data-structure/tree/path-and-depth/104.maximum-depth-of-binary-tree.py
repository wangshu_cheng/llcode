'''
Question:
Given a binary tree, find its maximum depth.

The maximum depth is the number of nodes along the longest path from the root node down to the
 farthest leaf node.

Example
Given a binary tree as follow:

  1
 / \ 
2   3
   / \
  4   5
The maximum depth is 3.


Thinking:
就最大深度，可以 divde and conque 或者 traversal， 一般情况用 dnd.

'''



"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""
class Solution:
    """
    @param root: The root of binary tree.
    @return: An integer
    """ 
    # DND method:
    def maxDepth(self, root):
        # write your code here
        #1 divide conquer:
        if not root:
            return 0
        # divide and conqure is bottom up, just imagine

        #below line can be omit, coz it's covered in other code, if left  and right child is 0 ofcause current node is 1
        #if root.left == None and root.right == None:
        #  return 1

        #your current node is leaf, than, it's 0+0+1 = 1
        left = self.maxDepth(root.left)
        right = self.maxDepth(root.right)
        #at leave node, left and right is 0. so 0+0 = 1 is the base value.
        return max(left,right) + 1
        
    #  traversal method
    def maxDepth(self,root):    
        # 2 traversal
        if not root:
            return 0
        
        self.max_depth = 0
        
        def helper(root,path):
            if not root:
                return
            if root.left == None and root.right == None:
                self.max_depth = max(self.max_depth,path)
            helper(root.left,path + 1)
            helper(root.right,path + 1)
        
        helper(root,1)
        
        return self.max_depth