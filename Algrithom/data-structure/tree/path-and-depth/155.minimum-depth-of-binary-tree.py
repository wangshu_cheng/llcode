'''
Question:
Given a binary tree, find its minimum depth.

The minimum depth is the number of nodes along the shortest path from the root node down to the nearest leaf node.

Thinking:
Simillar as the max depth question, still can use divde conqure some difference:
1 To get the minimal depth, the None(比如只有左儿子没用右儿子) route must be set to a very large value so it will lose when compare.

'''

class Solution:
    """
    @param root: The root of binary tree.
    @return: An integer
    """ 
    

    # bottom up - divide conque
    def minDepth_dnd(self, root):
        def helper(root):
        #this is to make sure when 1 child is None, we must make sure we use the other path (which child is not None)
        '''
        这里一点要注意，能到达leaf 的地方才能统计path， 中间的None 不算数；所以，遇到None，就把它设为最大，
        这样我们可以往右边走， base case 在于root.left == None(2**32) and root.right == None(2**32)
        '''
            if not root:
                return 2**31
            
            left = helper(root.left)
            right = helper(root.right)
            
            return 1 if min(left,right) == 2**31 else min(left,right) + 1
            
        if not root:
            return 0
        return helper(root)
        

    # top down - traversal, easy to think
    
    def minDepth_traversal(self, root):
        # write your code here
        self.min_path = 2**31
        
        def dfs(root,path):
            if not root:
                return 
            '''
                pre order traversal, 一个猛子载到底，就是path
            '''
            if root.left == None and root.right == None:
                self.min_path = min(self.min_path,path)
            
            dfs(root.left,path + 1)
            dfs(root.right,path + 1)
        
        if not root:
            return 0
        
        dfs(root,1)
        return self.min_path
        
        
    
