'''
Question:
Given a binary tree, find the maximum path sum.

The path may start and end at any node in the tree(not necessarily from root to bottom).

Thinking:
我们可以很方便的得到从root到leaf的最长path - single path，但是题目要求 any node 到 any node 的最长path - multiple path
如何从 single path 得到 multiple path 呢？
由于multiple可以看做是 2 段 single path 的组合，所以我们可以这样考虑：

首先，最长的path 一定是从一个leaf 到另一个leaf，除非有负数节点（可以和 0 比较 排除负节点)，也就是说，只有在有负节点的情况下，
最长path才有可能不是leaf to leaf

所以，就变得简单了，
1 Single path：我们可以很方便的求出每个node 到leaf的最大path sum（排除负的）；
2 Muliple path:求出对经过该节点的leaf to leaf 的最大multiple path， 就是 2条经过该节点的最大single path 加上 该节点的值


Thinking :
Divide conque,
1 到每一级的max single path
  single path = max(left[0],right[0]) + root.val （当前节点到leaf的最大一条可能路径（不是左右路径之和））
2 当前node的 multi path 就是 left child和 right child的 single path 之和加上当前node的值
  multi path = left[0] + right[0] + node.val

由于需要存储single path 和 multi path的值，所以可以定义一个结构， 2 元数组[maxSinglePath,maxMultiPath]

'''

"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""
class Solution:
    """
    @param root: The root of binary tree.
    @return: An integer
    """
    '''
    Define a return structure (max single path, max multi path)


    '''

    def maxPathSum(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        self.max_sum = -2**32
        
        def helper(root):
            if not root:
                return 0
            
            left = helper(root.left)
            right = helper(root.right)
            
            #current means the bigest path sum from this node to leaf,not go thru the root,
            current = max(root.val,max(left + root.val,right + root.val))
            
            #max(current,left + root.val+right)  means the biggest value of a end to end path which
            #pass current node
            self.max_sum = max(self.max_sum,max(current,left + root.val+right))
            return current
        
        
        
        helper(root)
        return self.max_sum
        
        


    
    def maxPathSum(self, root):
        MIN_VALUE = -2**31
        def helper(root):
            if not root:
                return (0,MIN_VALUE)#single path init value is 0, since it must include root
            left = helper(root.left)
            right = helper(root.right)
            
            #current Single path is the single path of left or right child plus current node
            maxSinglePath = max(left[0],right[0]) + root.val#single path must contain root, even it's negative
            maxSinglePath = max(maxSinglePath,0)  # if root.val is negative,cause single path less than 0,we will chose 0
            maxMultiPath = max(left[1],right[1])
            #current multi path is the sum of single path of both left and right child plus current node
            maxMultiPath = max(maxMultiPath, left[0] + right[0] + root.val)
            
            return (maxSinglePath,maxMultiPath)
            
        return helper(root)[1]
        
        