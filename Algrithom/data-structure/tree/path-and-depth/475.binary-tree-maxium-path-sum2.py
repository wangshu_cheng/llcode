'''
Question:
Given a binary tree, find the maximum path sum from root.

The path may end at any node in the tree and contain at least one node in it.

Thinking:
这题是求最大single path我们知道single path 就是 任何一个child的single path 加上当前node
的值；
还是用divide conqure 求解

'''


"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        this.val = val
        this.left, this.right = None, None
"""
class Solution:
    """
    @param root the root of binary tree.
    @return an integer
    """
    def maxPathSum2(self, root):
        # Write your code here
        #divide and conqure:
        if not root:
            # When node is none, can return any thing <=0, same thing.
            return -2**31
        if root.left == None and root.right == None:
            return root.val
        left = self.maxPathSum2(root.left)
        right = self.maxPathSum2(root.right)
        # we need max(0,max(left,right)) is because
        # if child path is negative, I will skip it.
        return max(0,max(left,right)) + root.val
        