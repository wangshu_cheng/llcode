class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Solution(object):


    def binaryTreePaths(self, root):
        """
        :type root: TreeNode
        :rtype: List[str]
        """
       
        def helper(root,path):
            if not root:
                return
            if root.left == None and root.right == None: 
                ans.append(path)
                return
            if root.left:
                helper(root.left,path +"->"+ str(root.left.val))
            if root.right:
                helper(root.right,path + "->"+ str(root.right.val))
        
        if not root:
            return []

        ans = []
        helper(root,str(root.val))
        return ans



    # def binaryTreePaths(self, root): 
    #     def convert2string(arr):
    #         if not arr:
    #             return ''
    #         result = []
    #         for path in arr:
    #             result.append('->'.join(path))
    #         return result
        
    #     def helper(root,path):
    #         if not root:
    #             return
    #         if root.left == None and root.right == None:
    #             ans.append(list(path))
                
    #         if root.left:
    #             path.append(str(root.left.val))
    #             helper(root.left,path)
    #             path.pop()
    #         if root.right:
    #             path.append(str(root.right.val))
    #             helper(root.right,path)
    #             path.pop()
        
    #     if not root:
    #         return []
    #     ans = []
    #     path = [str(root.val)]
    #     helper(root,path)
    #     return convert2string(ans)

s = Solution()
t1 = TreeNode(1)
t2 = TreeNode(2)
t3 = TreeNode(3)
t5 = TreeNode(5)

t1.left = t2
t1.right = t3
t2.right = t5

print s.binaryTreePaths(t1)