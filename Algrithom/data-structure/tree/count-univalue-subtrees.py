'''
iven a binary tree, count the number of uni-value subtrees.

A Uni-value subtree means all nodes of the subtree have the same value.

For example:
Given binary tree,
              5
             / \
            1   5
           / \   \
          5   5   5
return 4.

Notes:
Uni-value tree means all value in this tree is same, except the child is null, 
like:

   1        1        1        1
 /  \      /          \       
1    1  , 1     ,      1,  
Totally 4 cases,
So in the example, the uni-value tree is the bottom 3rd level 3 node(5), and the 2nd level one node(5).
Tree problem is often populer to use recursive,

Thinking:
Since we need make sure all children(till leaf) must be same, so we need to move bottom up.

'''

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

'''
Divide conque looks similar as post order traversal.
'''

class Solution(object):
    
    def countUnivalSubtrees(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        #必须用 bottom up，因为只有所有的下一级child node都是 uni－value 才谈的上一级。
        #Must use object, instead of a normal varable, since only object is passed by reference,
        #and can keep value change
        #in recursive function!

        '''
          Divide conque (or post order)
        '''

        self.total=0
        def helper(root):
            #base case:
            if not root:
                return True
            #dnd
            left = helper(root.left)
            right = helper(root.right)
            #only when left and right child is true, this node can possibliy be true
            if left and right:
                if root.left!=None and root.left.val!=root.val:
                    return False
                if root.right!=None and root.right.val!=root.val:
                    return False
                # if left and right is not none, than the value must be same as root.
                self.total+=1
                return True
            return False
            
        helper(root)
        return self.total

'''non recursive ??'''
    def countUnivalSubtrees(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
private class MyNode{
    int val;//assign before postorder traversal
    boolean isUni;//update after postorder traversal
    private MyNode(int val){
        this.val = val;
    }
}

public int countUnivalSubtrees(TreeNode root) {
    //boundary case
    if(root == null) return 0;

    //we need an inner class to record the parent's val and check status from its subtrees
    HashMap<TreeNode, MyNode> hs = new HashMap<TreeNode, MyNode>();
    //we don't have parent of root of main tree, we just care about its two subtrees
    //so we set an arbritary value here
    hs.put(root, new MyNode(-1));

    Stack<TreeNode> stack = new Stack<TreeNode>();
    stack.push(root);

    int count = 0;

    while(!stack.isEmpty()){
        TreeNode curr = stack.pop();
        //if left and right subtrees are both visited, then we can look at current Node
        //boundary case is leaf node, right and left is null,then isUni set to true 
        if(  (curr.left == null || hs.containsKey(curr.left)) && ((curr.right == null) || hs.containsKey(curr.right))){
            //check if all nodes in two subtrees have same value with curr node
            boolean left = curr.left == null? true : hs.get(curr.left).isUni;
            boolean right = curr.right == null? true : hs.get(curr.right).isUni;

            if(!left || !right){
                //if any of its subtree has node that got a different value, then set curr node's isUni = false
                hs.get(curr).isUni = false;//not necessary, as default value is false, but I put it here to make logic more clear
                continue;
            }

            //tree rooted at curr is a UniValue tree, we add it to count
            count ++;
            hs.get(curr).isUni = curr.val == hs.get(curr).val;//update isUni in curr Node to see if we can further expand the tree
        }else{
            //we have not done the postorder traversal on the tree rooted at curr
            stack.push(curr);
            if(curr.left != null && !hs.containsKey(curr.left)){
                //we need explore the left subtree
                hs.put(curr.left, new MyNode(curr.val));
                stack.push(curr.left);
            }else{
                //we need explore the right subtree
                hs.put(curr.right, new MyNode(curr.val));
                stack.push(curr.right);
            }
        }
    }

    return count;
}

        
        
        