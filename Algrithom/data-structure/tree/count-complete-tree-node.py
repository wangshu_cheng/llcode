'''
Question: 
Count all nodes in a complete tree

Thinking:
元芳：大人，此题目不能用常规traverse，不然耗时太多；
因此，应该要用相关tree的特性，
比如，如果要求full binary tree的nodes，那么total nodes ＝ 2**height－1
那么complete tree呢？ 
我们知道，complete tree在倒数第二层及以上都是full tree，那么是否可以部分用到full tree的公式呢？

complete tree的最后一级是向左靠拢；但是不知道最后一级有几个node
但是一定有且只有一个sub tree不是full tree，只有左child没有右child

所以，可以从root开始，如果发现最左和最右的长度不一样，那么分解为求左sub tree nodes和右sub tree nodes ；
当发现左边height大于右边时，右边必须是full tree
这样的好处就是，一旦一个subtree是full的，马上就知道它的nodes数；不用逐级计算


'''


# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def countNodes(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        def helper(root):
            #base case:
            ''' 当我们divide到那个最后一级只有left child 的node的时候， 
                  5
                 /
                2       
                就像这样，那么right child （none）的node数为0，总node = left + right + 1
                
            '''
            if not root:
                return 0
            # before recursvie, or by pass condition
            l,r=root,root
            hl,hr=0,0#height of left and right
            #Below code and fast get tree length
            while l:
                hl+=1
                l=l.left
            while r:
                hr+=1
                r=r.right
            if hl==hr:#haha this tree（subtree） is perfect  tree 
                return 2**hl-1
                
            else:
                # oh, sad, it's not, we need check the sub trees :(
                #divide and conqure
                left = helper(root.left)
                right = helper(root.right)
                
                #after recursive
                return 1 + left + right
                
        return helper(root)
        
                
           
           
            
            
                
                
        
        
        