'''
Tree knowledge:
balanced Tree - any path depth not more than 1
full Tree - every node have 2 children （但子树高度可能不一样，不一定是perfect tree）
complete tree - like a full tree, except the longest depth one must be as left as posible.
means for any subtree right of the longest, must be full tree. 
perfect Tree - all leaves are at the same level, and every parent has two children).
*perfect tree 是最全的树

tree traversal 综述：
traversal 本质就是找到一种可循环迭代的方法来遍历root和children，
有2种方法，一种是用递归函数，每次都把自己的child带入函数
一种是 用一个数据结构存储下一次要处理的节点。

第一种方法利用程序空间，第二种是数据空间

关于tree的 non recusive 解法：
由于你无法用for loop，因为你不知道tree有多深，对于tree来说，你每次只知道下一个子树，因此我们可以用一个数据结构存储下一次要处理的节点。
通过查询该数据是否为空为判断循环是否结束

bst - sorted tree

path 问题没法用bottom up，bottom up必须是同时考虑2个child的情况

'''


'''
Template1: Basic divide and conque.

'''

def question(root):

     '''
     The target can be path sum,or a target value or anything, which is a parameter pass top down.

     remember:
     The value in parameter is pass down.
     The code after recursive is bottom up.

     '''
	def helper(node,target): # The helper funcion is dfs,the input can be one or more
        '''
        base case:
        '''
        if not root:
            return 0

        ''' *some time need speical handling for leaf node: '''
        
        if root.left == None and root.right == None:
            return target 

        '''
        before deep dive, or by pass condition
        '''
        #do sth here:
       
        
        '''
        !Important for recursive, think the recursive is another function which just same name and defination!
        '''

        left = helper(root.left， target + root.val)
        right = helper(root.right, target + root.val)
        '''
        after deep dive, the code is doing bottom up calculation.
        
        In divide and conque case, we must have a return,becasue it's bottom up.The upper recursive call is
        rely on bottom recursive call's return value.

        '''
        return left + right + sth
                
    result = helper(root)
    return result



'''
Template 2: subset template, get all path (not sum of all path), result is 2d array.
'''

'''
Question:
Given a binary tree, find all paths that sum of the nodes in the path equals to a given number target.

A valid path is from root node to any of the leaf nodes.

Thinking
注意，“find all path”，一定是用 subset 模板, append, recursive and pop.

subset template: (插分插递弹)
/Users/jamescheng/Documents/llcode/subset_and_permutation(dfs)/subsets.py
'''
class Solution:
    # @param {TreeNode} root the root of binary tree
    # @param {int} target an integer
    # @return {int[][]} all valid paths
    def binaryTreePathSum(self, root, target):
        # Write your code here
        
        def dfs(node,path,target):
            if not node:
                return
            if node.left == None and node.right == None:#this is leaf node
                '''结束条件'''
                if target == node.val:
                    result.append(list(path)) #插
                return
            
            if node.left: #分
                path.append(node.left.val) #插                
                dfs(node.left,path,target - node.val) #递
                path.pop() #弹
                
            if node.right:
                path.append(node.right.val)
                dfs(node.right,path,target - node.val)
                path.pop()
            

        result = []
        
        if not root:
            return []
            
        dfs(root,[root.val],target)
        return result