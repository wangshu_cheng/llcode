'''
Question:
Given a non-empty binary search tree and a target value, find the value in the BST that is closest to the target.
'''

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
'''

Thinking:
这题和2分法模板何其相似！！， 区别就在于由于是树，所以start，end不太好定，
没法用 start + 1 < end 作为循环条件，但是我们有天然循环条件 root != None.
由于树没有index， 所以  需要用2个变量 来保存比target大的和小的边界, 就是 start , end.
最后比较start, end 哪个最靠近target

'''


class Solution(object):
    def closestValue(self, root, target):
        """
        :type root: TreeNode
        :type target: float
        :rtype: int
        """
        if not root:
            return 0
        start = end = root.val
        while root:
            # root is the mid by nature.
            if root.val < target: # if mid < target
                start = root.val # start = mid
                root = root.right # get next mid , by moving right
            else:
                end = root.val
                root = root.left

        if abs(start-target)<abs(end-target):
            result=start
        else:
            result=end
        
        return result