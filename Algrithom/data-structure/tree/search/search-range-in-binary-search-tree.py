"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""
'''
Thinking: normal In order traversal plus some condition for performance, 

'''

class Solution:
    
    # @param root: The root of the binary search tree.
    # @param k1 and k2: range k1 to k2.
    # @return: Return all keys that k1<=key<=k2 in ascending order.
 

    '''
     method 1, normal inorder traversal DFS, add some condition:
    '''
    def searchRange(self, root, k1, k2):
        # write your code here
        
        result = []
        
        def dfs(root,k1,k2):
            if not root:
                return
            #if root.val <= k1, we don't need to go left, other wise it's too small
            if root.val > k1:
               dfs(root.left,k1,k2)

            if k1 <= root.val <= k2:
                result.append(root.val)
                
            #same here
            if root.val < k2:
               dfs(root.right,k1,k2)
               
        dfs(root,k1,k2)
        return sorted(result)

    '''
     method 2, normal BFS
    '''
    def searchRange(self, root, k1, k2):
        # write your code here
        res = []

        if not root:
            return []
        
        q = [root]
        
        while q:
            node = q.pop()
            if k1 <= node.val <= k2:
                res.append(node.val)
            if node.right:
                q.append(node.right)
            if node.left:
                q.append(node.left)
        
        return sorted(res)
