class Solution(object):
    def verifyPreorder(self, preorder):
        """
        :type preorder: List[int]
        :rtype: bool
        """
        '''
        Tree travels:
           A
        B     C
        pre order:A-B-C 
        in order: B-A-C
        post order: B-C-A
        * this need recursively done all the children.
        
        BST is sorted tree.
        
        记住： 
        一旦往右边走了，以后所有的值都必须比前一个node大，比如
            11
            / \  
           8   17
          /  \
         5   10
         \   /
          7 9   
         ／
        6
       
      如果走到了7，那么后面所有的值，都在5的右边，所以不管是往下走的6还是往右走的10，17.。。都必须大于5
      所以，解法就是用stack 记住上一个node（如例子中的5），然后保证往右走的时候新值都要大于它。
      
      当一路往左走的时候，可以一直压栈，等第一个向右走的值出现（value > current top stack）就要把上一个pop出来，
      检查以后的值都要大于它，循环。。。
        
        '''
        if not preorder:
          return True
        stack=[]
        low=-2**31
        for n in preorder:
           #找到最小值每一步都要比最小值大，否则出错
           #最小值就是当你开始往右走的时候，stack 最后pop出来的值，刚刚比你小
            if n < low:
              return False
            while stack and n>stack[-1]: #if n>stack[-1] means it's a right value!
                low=stack.pop()
            stack.append(n)
        return True
        
        
        
        
        