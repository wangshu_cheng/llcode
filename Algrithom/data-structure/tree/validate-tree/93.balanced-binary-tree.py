'''
Given a binary tree, determine if it is height-balanced.

For this problem, a height-balanced binary tree is defined as a binary tree 
in which the depth of the two subtrees of every node never differ by more than 1.

Thinking:
用最大 path 的 code 可以得到每一级subtree的最大深度；
当前subtree 是否是balance要取决于当前subtree 左右最大子树的深度差不大于1 而且 所有子树都是
balanced！ 所以需要把子树的 banlance状态存下来，最简单的方法就是用一个2 元array。
[当前节点是否balance，当前节点到leaf深度(leaf节点为1)]



'''

"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""
class Solution:
    """
    @param root: The root of binary tree.
    @return: True if this Binary tree is Balanced, or false.
    """
      def isBalanced(self, root):
        # write your code here
        
        def helper(root):
            if not root:
                return [True,0]
            left = helper(root.left)
            right = helper(root.right)
            #注意，本级 为True的条件为：
            # 2个子树都是True 并且 高度差小于1
            #
            return [(left[0] and right[0]) and  abs(left[1] - right[1]) <= 1,max(left[1],right[1]) + 1]
            
        return helper(root)[0]
            