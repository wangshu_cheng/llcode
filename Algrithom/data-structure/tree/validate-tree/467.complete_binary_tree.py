'''
Question:
Check a binary tree is completed or not. A complete binary tree is a binary tree that every level is 
completed filled except the deepest level. In the deepest level, all nodes must be as left as possible. 
See more definition

Have you met this question in a real interview? Yes
Example
    1
   / \
  2   3
 /
4
is a complete binary.

    1
   / \
  2   3
   \
    4
is not a complete binary.

Thinking:
Complete tree is a like a full tree but last level is as left as possiable.


if use divide conque, 
there is 2 case to be true: 
1 left, right same height, than left must be full,right can be complete or full(height is the max height of the subtree)
2 left height is greater than right by 1, then right must be full,left must be complete
other case all false.

So we need to maintain 3 states for each subtree,(height, is full, is complete )
'''

class Solution:
    """
    @param root, the root of binary tree.
    @return true if it is a complete binary tree, or false.
    """
    def isComplete(self, root):
        
        def dfs(root):
            #base case:
            if not root:
                #(height, is full, is complete )
                return (0,True,True)
            
            #d and d
            left = dfs(root.left)
            right = dfs(root.right) # path is in return value no need pass to parameter
            
            # return values.  
            if not left[2]:
                return (-1,False,False)
            
            #height 相等
            if left[0] == right[0]:
                #那么左边必须是full，右边必须complete
                if not left[1] or not right[2]:
                    return (-1,False,False)
                return (left[0] + 1,right[1],True) # if right is full, than whole tree is full, otherwise not
                
            #左边高1
            if left[0] == right[0] + 1:
                #那么右边必须是full tree
                if not right[1]:
                    return (-1,False,False)
                #当前node为root的subtree 不是full tree
                return (left[0] + 1,False,True)
            
            #other case all fail
            return (-1,False,False)
        

        return dfs(root)[2]