"""
Question:
Validate bst

Thinking:
What is bst? how to validate?
BST 是有序树，

1 left and rigth subtree must be bst; 
2 对于每一个left subtree,所有的node都必须小于root，对于right subtree，所有的node都必须大于root

method1:
从root开始递归判断是否left tree所有值小于 root，right tree 大于root。

method2:
inorder traversal, 看是否有下一个node 值小于等于 前一个node，如果有，就False

method3:
BFS, 搞一个结构，[root,min_val,max_val]

"""

# Definition of TreeNode:
# class TreeNode:
#     def __init__(self, val):
#         self.val = val
#         self.left, self.right = None, None

#Method1

class Solution(object):
    def isValidBST(self, root):
        """
        :type root: TreeNode
        :rtype: bool
        """
        
        def helper(root,min_val,max_val):
            if not root:
                return True
            if root.val <= min_val or root.val>= max_val:
                return False
            
            return helper(root.left,min_val,root.val) and helper(root.right,root.val,max_val)
        
        min_value,max_value = -2**32,2**32
        
        return helper(root,min_value,max_value)


#Method2
        # traversal:
        # inorder traversal? check the next > the previous? but it has first node problem.
    def isValidBST2(self, root):
        self.firstNode = True
        self.preValue = -2**31
        def helper(root):
            if not root:
                return True #empty tree is true
            #left
            if helper(root.left) == False:
                return False
            #mid if it's not first node, we do normal check, if yes, bypass
            if (self.firstNode == False and root.val <= self.preValue):
                return False
            self.firstNode = False
            self.preValue = root.val
            #right
            if helper(root.right) == False:
                return False
            return True
        return helper(root)
                

    # import sys
    # def isValidBST(self, root):
    #     # write your code here
        
    #     def helper(root):
            
    #         if not root:
    #             #[isvalid,max value of this subtree, min value of this tree]
    #             return [True,-sys.maxint,sys.maxint]
            
    #         left = helper(root.left)
    #         right = helper(root.right)
                
    #         isValid = left[0] and right[0]
    #         if (root.val <= left[1] or root.val >=right[2]):
    #             isValid = False
            
    #         return [isValid,max(right[1],root.val),min(left[2],root.val)]
    #     return helper(root)[0]





    # def isValidBST(self, root):
    #     # write your code here
    #     def helper(root):
    #         MIN_VALUE,MAX_VALUE = -2**31,2**31

    #         #base case / stop sign:
    #         #set the is bst to tree, max value to small value and min value to big value

    #         if not root:
    #             return (True,MIN_VALUE,MAX_VALUE)

    #         #recursive    
    #         left = helper(root.left)
    #         right = helper(root.right)
            
    #         #logic and return value:
    #         #if left or right is not bst, fasle
    #         if left[0] == False or right[0] == False:
    #             return [False,0,0]
            
    #         #current tree, the root node must > max value of left tree and < min value of right tree.
    #         if (root.left!=None and root.val <= left[1] or root.right!=None and root.val >=right[2]):
    #             return [False,0,0]
            
    #         return (True,max(right[1],root.val),min(left[2],root.val))
        
    #     return helper(root)[0]
            

    

            
            
            