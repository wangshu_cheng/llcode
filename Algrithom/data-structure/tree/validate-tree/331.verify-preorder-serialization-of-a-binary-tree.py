'''
One way to serialize a binary tree is to use pre-order traversal. When we encounter a non-null node, we record the node's value. If it is a null node, we record using a sentinel value such as #.

     _9_
    /   \
   3     2
  / \   / \
 4   1  #  6
/ \ / \   / \
# # # #   # #
For example, the above binary tree can be serialized to the string "9,3,4,#,#,1,#,#,2,#,6,#,#", where # represents a null node.

Given a string of comma separated values, verify whether it is a correct preorder traversal serialization of a binary tree. Find an algorithm without reconstructing the tree.

Each comma separated value in the string must be either an integer or a character '#' representing null pointer.

You may assume that the input format is always valid, for example it could never contain two consecutive commas such as "1,,3".

Example 1:

Input: "9,3,4,#,#,1,#,#,2,#,6,#,#"
Output: true
Example 2:

Input: "1,#"
Output: false
Example 3:

Input: "9,#,#,1"
Output: false
'''

class Solution(object):
    def isValidSerialization(self, preorder):
        """
        :type preorder: str
        :rtype: bool
        """
        arr = preorder.split(",")
        stack = []
        for i in range(len(arr)):
            stack.append(arr[i])
            #这个题目关键 就是 检察每个 子树（root,left,right） 都必须是 element,#,#
            #或者 element,element,# 或者，elemnt,elemt,element. 后面2种情况都可以转换为第一种情况；
            # 绝对不能出现 (#,element,element) 的情况。
            while len(stack) >= 3 and stack[len(stack)-1] == '#' and stack[len(stack)-2] == '#' and not stack[len(stack)-3] == '#':
                stack.pop()
                stack.pop()
                stack.pop()
                stack.append("#")
        if len(stack) == 1 and stack[0] == "#":
            return True
        else:
            return False
                