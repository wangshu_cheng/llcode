# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

'''
Thinking: 

Serialize:  convert tree to string (can convert to array then join to string)
Deserialize: convert string to tree.

Method1 DFS, use traversal, like preorder, to convert tree to string; for deserialze, use same order to convert back.

Method2 BFS:

'''


class Codec:

#BFS:
    def serialize(self, root):
        if not root:return
        res = []
        q = [root]
        while (q):
            node = q.pop(0)
            if node == None:
                res.append('#')
            else:
                res.append(str(node.val))
                q.append(node.left)
                q.append(node.right)
        return ','.join(res)

    def deserialize(self, data):
        if not data:return
        data = data.split(',')
        root = TreeNode(data.pop(0))
        nodelist = [root]
        idx = 0
        while data:
            node = data.pop(0)
            if node == '#':
                nodelist[idx].left = None
            else:
                mynode = TreeNode(node)
                nodelist[idx].left = mynode
                nodelist.append(mynode)
            
            node = data.pop(0)
            if node == '#':
                nodelist[idx].right = None
            else:
                mynode = TreeNode(node)
                nodelist[idx].right = mynode
                nodelist.append(mynode)
            idx += 1
        return root

# DFS:

    def serialize(self, root):
        """Encodes a tree to a single string.
        :type root: TreeNode
        :rtype: str
        
        #Let's to pre order, use dfs recursive
        """
        if not root:return
        res=['']
        def helper(root,res):
            if not root:
               res[0]+='#,'
               return

            res[0]+=str(root.val)+','
            helper(root.left,res)
            helper(root.right,res)

            return res[0][:-1]
        return helper(root,res)
        

    def deserialize(self, data):
        """Decodes your encoded data to tree.
        
        :type data: str
        :rtype: TreeNode
        """
        if not data:
           return
        arr=data.split(',')
        def helper(arr):
            n=arr.pop(0)
            if n=='#':
                return None
            root=TreeNode(int(n))
            root.left=helper(arr)
            root.right=helper(arr)
            return root
        return helper(arr)
        

# Your Codec object will be instantiated and called as such:
# codec = Codec()
# codec.deserialize(codec.serialize(root))