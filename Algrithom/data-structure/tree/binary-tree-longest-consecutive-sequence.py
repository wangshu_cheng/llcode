'''
Quesiton:
Given a binary tree, find the length of the longest consecutive sequence path.

The path refers to any sequence of nodes from some starting node to any node in the tree 
along the parent-child connections. The longest consecutive path need to be from parent to child
 (cannot be the reverse).

For example,
   1
    \
     3
    / \
   2   4
        \
         5
Longest consecutive sequence path is 3-4-5, so return 3.
   2
    \
     3
    / 
   2    
  / 
 1
Longest consecutive sequence path is 2-3,not3-2-1, so return 2.

'''

# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None
#1 top down,traversal
class Solution(object):
    def longestConsecutive(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        '''
        Skill
        pre order traversal all node,if node = parent's value + 1, add path
        otherwise, path = 1
        '''
        self.maxlen = 0
        def helper(root,pre,length):
            if not root:
                return
            if pre and root.val == pre.val + 1:
                length += 1
            else:
                length = 1
            self.maxlen = max(self.maxlen,length)
            helper(root.left,root,length)
            helper(root.right,root,length)
        helper(root,None,0)
        return self.maxlen
        
#2 Non recursive:
   def longestConsecutive(self, root):
        """
        :type root: TreeNode
        :rtype: int
        """
        if not root:
            return 0
        stack = [(root,1)]
        ret = 0
        while stack:
            node,cnt = stack.pop()
            ret = max(ret,cnt)
            if node.right:
                stack.append((node.right,cnt + 1 if node.right.val == node.val + 1 else 1))
            if node.left:
                stack.append((node.left,cnt + 1 if node.left.val == node.val + 1 else 1))
        return ret


#3 Below bottom up Solution will fail some case
# class Solution(object):
#     def longestConsecutive(self, root):
#         """
#         :type root: TreeNode
#         :rtype: int
#         """
#         self.maxlen = 1
#         def helper(node):
#             if not node:
#                 return [0,-2**31]
#             if node.left == None and node.right == None:
#                 return [1,node.val]
 
#             left = helper(node.left)
#             right = helper(node.right)
#             print node.val,left,right
#             if node.val + 1 == left[1]:
#                 left[0] += 1
#             if node.val + 1 == right[1]:
#                 right[0] += 1
#             self.maxlen = max(self.maxlen,max(left[0],right[0]))
#             return [max(left[0],right[0]),node.val]
 
#         if not root:
#             return 0
#         helper(root)
#         return self.maxlen

n1 = TreeNode(1)
n2 = TreeNode(2)
n3 = TreeNode(3)
n22 = TreeNode(2)
root = n2
# root.right = n3
# n3.left = n22
# n22.left = n1


test = Solution()
print test.longestConsecutive(root)

        
            