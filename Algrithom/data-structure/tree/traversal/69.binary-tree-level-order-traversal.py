'''
This is BFS template!!

'''


class Solution:
    """
    @param root: The root of binary tree.
    @return: Level order in a list of lists of integers
    """
    def levelOrder(self, root):
        # write your code here
        if not root:
            return []

        result = []
        
        q = [root]
        
        while q:
            '''
            define level here if need
            '''
            level = list() #can not use level = []
            '''
            loop current level, can use while loop as well:
            '''
            for i in range(len(q)):
                '''
                loop current level, don't forget here is pop(0) not pop, because it's queue, not stack!!
                if use stack that is DFS. Check the non-recur solution of preorder traversal
                '''
                node = q.pop(0)
                level.append(node.val)
                if node.left:
                    q.append(node.left)
                if node.right:
                    q.append(node.right)
            '''
            if we need reverse traveral, from buttom to up, just insert to front like:
                result = [level] + result
                or:
                result.insert(0,level)
            '''
            result.append(level)
        return result