'''
Question:
Given a binary tree, return the preorder traversal of its nodes' values.
Example
Given:

    1
   / \
  2   3
 / \
4   5
return [1,2,4,5,3].

Thinking:

traversal 本质就是找到一种可循环迭代的方法来遍历root和children，
有2种方法，一种是用递归函数，每次都把自己的child带入函数
一种是 用一个数据结构存储下一次要处理的节点。

第一种方法利用程序空间，第二种是数据空间

'''


"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""


class Solution:
    """
    @param root: The root of binary tree.
    @return: Preorder in ArrayList which contains node values.
    """
      
    #1 recursive - traversal most simple
    def preorderTraversal1(self,root):
        res = []
        def helper(root,res):
            if not root:
                return
            res.append(root.val)
            helper(root.left,res)
            helper(root.right,res)
        
        helper(root,res)
        return res
        

    #3 non-recursive
    def preorderTraversal(self, root):
        # write your code here
        # Iterative:
        res = []

        #由于你无法用for loop，因为你不知道tree有多深，对于tree来说，你每次只知道下一个子树，
        #因此我们可以用一个数据结构存储下一次要处理的节点。
        #通过查询该数据是否为空为判断循环是否结束
        #用 stack 是应为我们要做DFS，BFS用 queue
        stack = []
        '''DON'T Forget!!'''
        if not root:
            return []
            
        stack.append(root)
        
        while stack:
            node = stack.pop()
            res.append(node.val)
            # !!! here must push right node first, left node later, 
            #becasue left part should be traval first!!!
            if node.right:
                stack.append(node.right)
            if node.left:
                stack.append(node.left)
        return res


            #2 recursive - d&q
    '''
    Divide and conque is bottom up solution.
    '''
    def preorderTraversal2(self,root):
        '''If use bottom up, the res must be local, cause we are all relying on return value of each recur function'''
        res = []
        if not root:
            return []
        # 一头栽倒左边底部
        left = self.preorderTraversal(root.left)
        right = self.preorderTraversal(root.right)
        '''
        In base case the 'root' now is leaf node, root.left and root.right are all none.
        So the result is current root value and plus left and right child(empty), this is the bottom case.

        Then 我们以这个为base 往上走，对每一级来说，都是他的root值append上左右children值，到最高一级，就是 root的value 加上
        第一级左右子数的值

        '''
        res.append(root.val)
        res += left
        res += right
        return res

  