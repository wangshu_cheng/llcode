'''
Given a binary tree, return the zigzag level order traversal of its nodes' values. (ie, from left to right, then right to left for the next level and alternate between).

For example:
Given binary tree {3,9,20,#,#,15,7},
    3
   / \
  9  20
    /  \
   15   7
return its zigzag level order traversal as:
[
  [3],
  [20,9],
  [15,7]
]
'''

'''
This is BFS template!!

'''


class Solution:
    """
    @param root: The root of binary tree.
    @return: Level order in a list of lists of integers
    """
    def levelOrder(self, root):
        # write your code here
        if not root:
            return []
        result = []
        
        q = [root]
        flag = 1 #flag 1 means this level traversal from left to right, -1 means from right to left
        
        while q:
            '''
            define level here if need
            '''
            level = list()
            '''
            loop current level, can use while loop as well:
            '''
            for i in range(len(q)):
                '''
                loop current level, don't forget here is pop(0) not pop, because it's queue, not stack!!
                '''
                node = q.pop(0)
                if flag == 1:
                    level.append(node.val)
                else:
                    level.insert(0,node.val)
                if node.left:
                    q.append(node.left)
                if node.right:
                    q.append(node.right)
            '''
            if we need reverse traveral, from buttom to up, just insert to front like:
                result = [level] + result
                or:
                result.insert(0,level)
            '''
            result.append(level)
            flag *= -1
        return result