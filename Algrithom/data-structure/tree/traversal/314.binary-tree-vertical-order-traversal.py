# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

'''
Given a binary tree, return the vertical order traversal of its nodes' values. (ie, from top to bottom, column by column).

If two nodes are in the same row and column, the order should be from left to right.
'''

class Solution(object):
    def verticalOrder(self, root):
        """
        :type root: TreeNode
        :rtype: List[List[int]]
        """
        '''
        Basic idea:
        how to define columns??
         - Remember, tree travers is always from root.So can think root is at column 0, 
          so this left and right child is column -1 and 1, and so on
          
        we need a dictionary to store the column information (store all the nodes in a certain column)
        also need a queue for BFS traveling.
        '''
        if not root:return []
        dic={}
        q=[]
        q.append((root,0))#append tupple, first value is node, 2nd value is column
        while q:
            cur,i = q.pop(0)
   
            if i not in dic.keys():
                dic[i]=[cur.val]
            else:dic[i].append(cur.val)
            if cur.left:
                q.append((cur.left,i-1))
            if cur.right:
                q.append((cur.right,i+1))
        #now we get the dictionary of column of nodes, turn it to list:
     
        return [dic[i] for i in sorted(dic)]
            
        
        
        
        
        