"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""


class Solution:
    """
    @param root: The root of binary tree.
    @return: Postorder in ArrayList which contains node values.
    """
    def postorderTraversal(self, root):
        # write your code here
        
        def helper(root):
            if not root:
                return
            helper(root.left)
            helper(root.right)
            result.append(root.val)
            
        result = []
        
        helper(root)
        return  result

# non- recursive: (wiki solution,more consistent with in order solution)

    def postorderTraversal(self, node):
        result = []
        stack = []

        lastVisit = None
        peekNode = None

        while stack or node:#node is none means we already reached left most.
            #if node still exist, then go left till the left most.
            #this part is same as in order traversal
            if node:
                stack.append(node)
                node = node.left
            else:
                #At first,the peak node is the left most node
                peekNode = stack[-1]
                #if right child exists and traversing node
                # from left child, then move right
                if (peekNode.right and lastVisit != peekNode.right):
                    node = peekNode.right
                else:
                    result.append(peekNode.val)
                    lastVisit = stack.pop()
        return result
# non- recursive: (jiuzhang solution,)
    def postorderTraversal(self, root):
        # write your code here
        #non recursive must store the value in some data structure
        
        result = []
        stack = []
        prev = None
        cur = root

        if not root:
            return result
        stack.append(root)

        while stack:
            cur = stack[-1]
            if not prev or prev.left == cur or prev.right == cur:
                if cur.left:
                    stack.append(cur.left)
                elif cur.right:
                    stack.append(cur.right)
            elif cur.left == prev:
                if cur.right:
                    stack.append(cur.right)
            else:
                result.append(cur.val)
                stack.pop()
            prev = cur
        return result


