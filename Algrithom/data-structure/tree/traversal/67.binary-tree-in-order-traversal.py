# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):

# method 1 and 2 are similar difference is the if else usage.

'''
一句话，一直往左移到底，不行了，弹出来，再往右走。

'''
    def inorderTraversal_non_recursive_1(self, root):
        # write your code here
         # Iterative:
        
        if not root:
            return []
        
        res = []
        stack = []
        cur = root
        while cur or stack:
            #when cur has value, I will move to left most
            while cur:
                stack.append(cur)
                cur = cur.left
            
            cur = stack.pop()
            res.append(cur.val)
            
            cur = cur.right
           
        return res
  

    # below is wiki solution,
    def inorderTraversal_non_recursive_2(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        res=[]
        stack=[]
        node=root
        while stack or node: #node is none means we already reached left most.
            #if node still exist, then go left till the left most.
            if node:
                stack.append(node)
                node=node.left

            #if already in the left most, we start to pop (the first value is the most left child)
            # and add to result, then move right,if no right, then right is none, start another iteration.
            else:
                node=stack.pop()
                res.append(node.val)
                node=node.right
        return res
        
    # recursive:
    def inorderTraversal_recursive
        res = []
        
        def helper(root):
            if not root:
                return
            helper(root.left)
            res.append(root.val)
            helper(root.right)
            
        helper(root)
        return res
        
            