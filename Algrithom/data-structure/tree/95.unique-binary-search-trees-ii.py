'''
Given an integer n, generate all structurally unique BST's (binary search trees) that store values 1 ... n.

Example:

Input: 3
Output:
[
  [1,null,3,2],
  [3,2,null,1],
  [3,1,null,null,2],
  [2,1,3],
  [1,null,2,null,3]
]
Explanation:
The above output corresponds to the 5 unique BST's shown below:

   1         3     3      2      1
    \       /     /      / \      \
     3     2     1      1   3      2
    /     /       \                 \
   2     1         2                 3


'''

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def generateTrees(self, n):
        """
        :type n: int
        :rtype: List[TreeNode]
        """
        '''
        Base on:
        F(i,n) = F(i-1,n)*F(n-i,n)
        
        我们从左到右，以每个i为root，那么我们可以遍历所有可能的左值，和右值；这是一级；如果变成recursive 就是所有的级别
        
        '''
        if n==0:return []
        def helper(start,end):
            arr=[]

            #this line is must. 什么时候会出现左边比右边大？
            if (start > end):
                arr.append(None);
            #以i 为node，所以左边的node一定是  start->i, 右边一定是 i+1->end
            for i in xrange(start,end+1):
                leftList=helper(start,i-1)
                rightList=helper(i+1,end)
                for left in leftList:
                    for right in rightList:
                        root=TreeNode(i)
                        root.left=left
                        root.right=right
                        arr.append(root)
                
            return arr
        return helper(1,n)
            
            

            