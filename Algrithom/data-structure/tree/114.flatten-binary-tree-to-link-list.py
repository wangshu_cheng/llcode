'''
Given a binary tree, flatten it to a linked list in-place.

For example,
Given

         1
        / \
       2   5
      / \   \
     3   4   6
The flattened tree should look like:
   1
    \
     2
      \
       3
        \
         4
          \
           5
            \
             6
Hints:
If you notice carefully in the flattened tree, each node's right child points
 to the next node of a pre-order traversal.

Thinking:
This is not ask you to create a link list, but use the tree structure to move everything to right.
 Method1:
 recur:
 special post order traversal (right->left->root), make everyting to right side.
 Method2:
 non-recur:
 先把右边的放入stack，然后一直往左走，走到尽了，再弹出stack，类似于preorder traversal



'''
# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None



class Solution(object):
    def flatten(self, root):
        """
        :type root: TreeNode
        :rtype: void Do not return anything, modify root in-place instead.
        """
        
        stack = []
        
        while root or stack:
            if root.right:
                stack.append(root.right)
            if root.left:
                root.right = root.left
                root.left = None
            elif stack:
                root.right = stack.pop()
            root= root.right
        

class Solution(object):
    
    def flatten(self, root):
        """
        :type root: TreeNode
        :rtype: void Do not return anything, modify root in-place instead.
        """
        '''
        It looks like use pre order traversal, how ever, if use pre order, for each node, 
        we don't know the next node yet
        to point to. So post order can solve this problem.

        '''
        self.pre=None
        def helper(root):
            if not root:return
            # we need get the most righest value first
            helper(root.right)
            helper(root.left)
            root.left=None
            root.right=self.pre
            self.pre=root
        helper(root)
        
        