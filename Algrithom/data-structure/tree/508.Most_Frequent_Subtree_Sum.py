# Given the root of a tree, you are asked to find the most frequent subtree sum. The subtree sum of a node is defined as the sum of all the node values formed by the subtree rooted at that node (including the node itself). So what is the most frequent subtree sum value? If there is a tie, return all the values with the highest frequency in any order.

# Examples 1
# Input:

#   5
#  /  \
# 2   -3
# return [2, -3, 4], since all the values happen only once, return all of them in any order.
# Examples 2
# Input:

#   5
#  /  \
# 2   -5
# return [2], since 2 happens twice, however -5 only occur once.
# Note: You may assume the sum of values in any subtree is in the range of 32-bit signed integer.

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def findFrequentTreeSum(self, root):
        """
        :type root: TreeNode
        :rtype: List[int]
        """
        self.count = {}
        
        def helper(root):
            if not root:
                return 0
#             if root.left == None and root.right == None:
#                 if root.val not in self.count:
#                     self.count[root.val] = 1
#                 else:
#                     self.count[root.val] += 1
                    
#                 return root.val
            
            left = helper(root.left)
            right = helper(root.right)        
            subtree_sum = root.val + left + right
            
            if subtree_sum not in self.count:
                self.count[subtree_sum] = 1
            else:
                self.count[subtree_sum] += 1
            return subtree_sum

                
        if not root:
            return []
        
        helper(root)
        
        res = []
        min_cnt = 0 
        
        for key in self.count:
            if self.count[key] > min_cnt:
                min_cnt = self.count[key]
                
        for key in self.count:
            if self.count[key] == min_cnt:
                res.append(key)
                
        return res
            
            
            