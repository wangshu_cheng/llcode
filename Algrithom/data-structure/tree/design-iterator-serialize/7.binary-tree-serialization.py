'''
Question:
Design an algorithm and write code to serialize and deserialize a binary tree. Writing the tree to a file is called 
'serialization' and reading back from the file to reconstruct the exact same binary tree is 'deserialization'.

There is no limit of how you deserialize or serialize a binary tree, you only need to make sure you can 
serialize a binary tree to a string and deserialize this string to the original structure.

Have you met this question in a real interview? Yes
Example
An example of testdata: Binary tree {3,9,20,#,#,15,7}, denote the following structure:

  3
 / \
9  20
  /  \
 15   7

 In the sample case, our seriaize output will be:
 3,9,#,#,20,15,#,#,7,#,#

Thinking, use traversal(dfs or bfs) to convert tree to string (array), than use build method to build back
注意，空值也需要处理；应为树中可能有很多空值；

DFS比较简单；BFS需要判断当前是否为左儿子

'''


# Definition for a binary tree node.
class TreeNode(object):
    def __init__(self, x):
        self.val = x
        self.left = None
        self.right = None

class Codec:

    def serialize(self, root):
        """Encodes a tree to a single string.
        :type root: TreeNode
        :rtype: str
        
        #Let's to pre order, use dfs recursive
        """
        if not root:return
        res = []
        def helper(root,res):
            if not root:
                res.append("#")
                return
            res.append(str(root.val))
            helper(root.left,res)
            helper(root.right,res)
            return res
        return ",".join(helper(root,res))
        

    def deserialize(self, data):
        """Decodes your encoded data to tree.
        
        :type data: str
        :rtype: TreeNode
        """
        if not data:
           return
        arr = data.split(',')
        def helper(arr):
            n = arr.pop(0)
            if n == '#':
                return None
            root = TreeNode(int(n))
            root.left = helper(arr)
            root.right = helper(arr)
            return root
        return helper(arr)
        

#BFS, 由于是 从 队列到树，所以deseraization 的关键就是找到父亲和儿子的关系，用 isleft 变量:
  def serialize(self, root):
        #BFS:
        
        if not root:
            return ""
            
        q = [root]
        res = []
        while q:
            node = q.pop(0)
            if not node:
                res.append("#")
            else:
                res.append(str(node.val))
                q.append(node.left)
                q.append(node.right)
        return ",".join(res)
        
    def deserialize(self, data):
        # write your code here
       
        #BFS:
        if not data:
            return None
        
        vals = data.split(",")
        
        root = TreeNode(vals[0])
        q = [root]
        
        isLeft = True
        index = 0
        # 0 is root, so we start from 1 which is left child of root
        for i in range(1,len(vals)):
            #we don't build node for none
            if vals[i] != "#":
                node = TreeNode(str(vals[i]))
                if isLeft :
                    q[index].left = node
                else:
                    q[index].right = node
                q.append(node)

            # 简单说，index代表父亲，没隔2 个值才走一个，因为一个父亲有两个孩子
            if not isLeft:
                index += 1
            
            isLeft = not isLeft
    
    
        return root      





# Your Codec object will be instantiated and called as such:
codec = Codec()
#   3
#  / \
# 9  20
#   /  \
#  15   7
root = TreeNode(3)
l1l = TreeNode(9)
l1r = TreeNode(20)
l2l = TreeNode(15)
l2r = TreeNode(7)

root.left = l1l
root.right = l1r
l1r.left = l2l
l1r.right = l2r

print codec.serialize(root)
# codec.deserialize(codec.serialize(root))