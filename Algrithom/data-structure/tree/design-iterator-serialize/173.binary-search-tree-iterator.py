'''
Question:
Design an iterator over a binary search tree with the following rules:

Elements are visited in ascending order (i.e. an in-order traversal)
next() and hasNext() queries run in O(1) time in average.

'''

"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None

Example of iterate a tree:
iterator = BSTIterator(root)
while iterator.hasNext():
    node = iterator.next()
    do something for node 


Thinking: 关键点是，先把所有的左树压入stack，然后每pop出一个值(next 操作)，就把next.right 压入stack    
"""

class BSTIterator(object):
    def __init__(self, root):
        """
        :type root: TreeNode
        """
        self.stack=[]
        self.pushAll(root)
        

    def hasNext(self):
        """
        :rtype: bool
        """
        return self.stack

    def next(self):
        """
        :rtype: int
        """
        tmp = self.stack.pop()
        self.pushAll(tmp.right)
        return tmp.val
        
    def pushAll(self,node):
        while node:
            self.stack.append(node)
            node=node.left