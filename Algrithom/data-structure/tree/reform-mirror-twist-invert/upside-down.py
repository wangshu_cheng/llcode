'''
Question:
Given a binary tree where all the right nodes are either leaf nodes with a sibling
 (a left node that shares the same parent node) or empty, flip it upside down and turn it
  into a tree where the original right nodes turned into left leaf nodes. Return the new root.

For example:
Given a binary tree {1,2,3,4,5},
    1
   / \
  2   3
 / \
4   5
return the root of the binary tree [4,5,2,#,#,3,1].
   4
  / \
 5   2
    / \
   3   1  

Thinking:
        就是从leaf 开始，recursively 做,就是root,left,right都顺时针转一下
             node                left
            /    \      ＝>     /     \
          left   right        right   node

'''

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def upsideDownBinaryTree(self, root):
        """
        :type root: TreeNode
        :rtype: TreeNode
        """
        '''
        就是从leaf 开始往上，recursively 做
             node                left
            /    \      ＝>     /     \
          left   right        right   node
        
        '''
        def helper(root):
            #base case(最后一级循环)，当前node不存在或为leaf
            if not root or not root.left: #leaf
                return root
                
            left = root.left
            right = root.right
            '''
             因为右边是leaf，所以不需要recursive右边，左边走到底就可以了

             new root其实就是left most，也就是变换后的新 root
            '''
            new_root = helper(root.left)
            
            left.right = root
            left.left = right
            root.left = None
            root.right = None

            
            return new_root

        return helper(root)
        


     # non -recur:
     def upsideDownBinaryTree2(self, root):
        """
        :type root: TreeNode
        :rtype: TreeNode
        """
        if not root:
            return None
        l = root.left
        r = root.right
        root.left=None
        root.right=None
        
        while l:
           next_l=l.left
           next_r=l.right
           
           l.left=r
           l.right=root
           root=l

           l=next_l
           r=next_r
        return root

        
            
       