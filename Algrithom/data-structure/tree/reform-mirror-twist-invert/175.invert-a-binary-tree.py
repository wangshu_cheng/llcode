

# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

'''
Question:
Invert a binary tree.

     4
   /   \
  2     7
 / \   / \
1   3 6   9
to
     4
   /   \
  7     2
 / \   / \
9   6 3   1

Thinking:

Buttom up (dnd) or top down(traversal)?

Top down is to do top first then bottom, bottom up is reverse.

Bottom up is offen used to calucate something which rely on the bottom case result, when
we must get some return value and it eventually rely on bottom case, then need it, like get path depth(it can do top down as well).

Top down also can pass value in paramter, but don't rely on bottom case return value.
Most often used to pure traversal, no return value questions.


'''


'''1 Top down'''
'''
像绞衣服一样，从头往脚绞
'''
   def invertTree(self, root):
        """
        :type root: TreeNode
        :rtype: TreeNode
        """
        if not root:
            return
        
        root.left,root.right = root.right,root.left
        
        self.invertTree(root.left)
        self.invertTree(root.right)
        
        return root
        
'''2 bottom up'''

   def invertTree(self, root):

        if not root:
            return
        
        # if root.left == None and root.right == None:
        #     return root
            
        self.invertTree(root.left)
        self.invertTree(root.right)
        
        root.left,root.right = root.right,root.left
        
        return root
        



        