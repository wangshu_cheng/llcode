'''
Question:
Given a binary tree, check whether it is a mirror of itself (ie, symmetric around its center).

For example, this binary tree is symmetric:

    1
   / \
  2   2
 / \ / \
3  4 4  3
  56 65
But the following is not:
    1
   / \
  2   2
   \   \
   3    3



'''

"""
Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        this.val = val
        this.left, this.right = None, None
"""
class Solution:
    """
    @param root, the root of binary tree.
    @return true if it is a mirror of itself, or false.
    """

    '''
        symmetric actually means, 
          A equals B, and A.left equals B.right  and A.right equals B.left.
    '''

    def isSymmetric(self, root):
        # Write your code here
        
        def dfs(node1,node2):
            if node1 == None and node2 == None:
                return True
            if node1 == None or node2 == None:
                return False
            
            if node1.val != node2.val:
                return False
                
            #注意，这里是node1 left and node2 right compare

            left = dfs(node1.left,node2.right) 
            right = dfs(node1.right,node2.left)
            return left and right
            
        if not root:
            return True
        return dfs(root.left,root.right)