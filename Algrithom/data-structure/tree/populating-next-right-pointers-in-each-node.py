'''
Given a binary tree
    struct TreeLinkNode {
      TreeLinkNode *left;
      TreeLinkNode *right;
      TreeLinkNode *next;
    }
Populate each next pointer to point to its next right node. 
If there is no next right node, the next pointer should be set to NULL.

Initially, all next pointers are set to NULL.

Note:

You may only use constant extra space.
You may assume that it is a perfect binary tree 
(ie, all leaves are at the same level, and every parent has two children).
For example,
Given the following perfect binary tree,
         1
       /  \
      2    3
     / \  / \
    4  5  6  7
After calling your function, the tree should look like:
         1 -> NULL
       /  \
      2 -> 3 -> NULL
     / \  / \
    4->5->6->7 -> NULL

Thinking:
3个变量，parent,level,cur
关键是cur 在从左向右移动；parent和level则是辅助变量；
'''


# Definition for binary tree with next pointer.
# class TreeLinkNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None
#         self.next = None
#method1 - best, no queue
    def connect(self, root):
        """
        :type root: TreeLinkNode
        :rtype: nothing
        """
        if not root:
            return
        parent = root
        level = parent.left
        while parent and level:
            cur = None
            while parent:
                if not cur:
                    cur = level
                else:
                    cur.next = parent.left
                    cur = cur.next
                cur.next = parent.right
                cur = cur.next
                parent = parent.next
            parent = level
            level = parent.left



    def connect(self, root):
        """
        :type root: TreeLinkNode
        :rtype: nothing
        """
        '''
        BFS, until cur == None, pre.right=cur
        '''
        if not root:return
        q = [root]
        while q:
            # level=list()
            pre=None
            for i in range(len(q)):
                node = q.pop(0)
                if not pre:
                    pre=node
                else:
                    pre.next=node
                    pre=node
                # level.append(node)
                if node.left:
                    q.append(node.left)
                if node.right:
                    q.append(node.right)

#method2
class Solution(object):
    def connect(self, root):
        """
        :type root: TreeLinkNode
        :rtype: nothing
        """
        '''
        BFS, until cur == None, pre.right=cur
        '''
        if not root:return
        q = [root]
        while q:
            # level=list()
            pre=None
            for i in range(len(q)):
                node = q.pop(0)
                if not pre:
                    node.next = None
                    pre = node
                else:
                    node.next = pre
                    pre=node
                # level.append(node)
                if node.right:
                    q.append(node.right)
                if node.left:
                    q.append(node.left)
            
        