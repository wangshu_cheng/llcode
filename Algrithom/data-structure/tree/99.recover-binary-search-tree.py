'''
Two elements of a binary search tree (BST) are swapped by mistake.

Recover the tree without changing its structure.

Example 1:

Input: [1,3,null,null,2]

   1
  /
 3
  \
   2

Output: [3,1,null,null,2]

   3
  /
 1
  \
   2
Example 2:

Input: [3,1,4,null,null,2]

  3
 / \
1   4
   /
  2

Output: [2,1,4,null,null,3]

  2
 / \
1   4
   /
  3
  
'''


# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def recoverTree(self, root):
        """
        :type root: TreeNode
        :rtype: void Do not return anything, modify root in-place instead.
        """
        self.first = None
        self.second = None
        self.pre = None
        
        #This function is to find out 2 node which swapped.
        def inorder(root):
            if not root:
                return
            inorder(root.left)
                
            if self.pre == None:
                self.pre = root
            else:
                if root.val < self.pre.val:
                    if self.first == None:
                        self.first = self.pre
                    self.second = root
                self.pre = root
                
            inorder(root.right)
        
        inorder(root)
        if self.first != None and self.second != None:
            #swap value:
            tmp = self.second.val
            self.second.val = self.first.val
            self.first.val = tmp
            
        