'''
Question:
Given a binary search tree (See Definition) and a node in it, find the in-order successor of 
that node in the BST.

Notice

If the given node has no in-order successor in the tree, return null.

Example
Given tree = [2,1] and node = 1: node为左儿子

  2
 /
1
return node 2.

Given tree = [2,1,3] and node = 2: node为右儿子，或父亲

  2
 / \
1   3
return node 3.


Thinking:
Inorder successor is the next bigger elem, so it will be the next "right" hand side node,
firstly, it can be the target's right child(or grandchild),or if not, it is the right father(or grand father.)

考虑到如example所示的两种情况，我们可以这么考虑，
1 首先所有tree 的search问题都是从root开始。
2 根据root 和 target比较的大小，抛弃一半子树，缩小范围
'''
# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    """
    @param root <TreeNode>: The root of the BST.
    @param p <TreeNode>: You need find the successor node of p.
    @return <TreeNode>: Successor of p.
    """


    #BEST non - recursive:
    def inorderSuccessor(self, root, p):
        # write your code here
        
        if not p:
            return root
        
        pre = None
        #先移动到P的位置再说，同时保留P的右父亲（左父亲比他小，不保留），因为右父亲可能是P的 inorder successor
        while root and root.val != p.val:
            if root.val > p.val:#root is right father of p
                pre = root
                root = root.left
            else:
                root = root.right
            
        #现在我们到P了， 有几种情况：

        #1 P 没有右子树，那么它父亲就是答案
        if root.right == None:
            return pre
        
        #1 P有右子树，那么答案就是右子树的最左边那个儿子。
        root = root.right
        while root.left:
            root = root.left
        return root


            
   #1 recursive traversal (most Straight fwd inorder)
   #just inorder traversal, and remember the previous, if latest prev is p, than current root is result.
    def inorderSuccessor(self, root, p):
        """
        :type root: TreeNode
        :type p: TreeNode
        :rtype: TreeNode
        """
        self.pre=None
        self.result=None
        def helper(root,p):
            if not root:
                return
            helper(root.left,p)
            if self.pre and self.pre.val == p.val:
                self.result=root
                # if we put return here, the problem is the next line won't excute so the self.pre always be the P value.
                # return 
            self.pre = root
            helper(root.right,p)
            
        if not root:return root
        helper(root,p)
        return self.result


    #3 recursive,  divide and conquer, divide by root is on left or right of p.
    def inorderSuccessor(self, root, p):
        def helper(root,p):
            # recursive 的结束值，也就是base value 就是leaf node情况，也就是child is none
            if not root:
                return
            #P 在左树，左分支：
            if root.val > p.val:
                suc = helper(root.left,p)
                # in this case, the result is the root， 也就是
                return suc if suc else root
            
            #P在右树，右分支
            else:
                return helper(root.right,p)

        return helper(root,p)
   


