'''
Question:

The differece with previous question is this time the node has a father attribute.

Given the root and two nodes in a Binary Tree. Find the lowest common ancestor(LCA) of the two nodes.
The lowest common ancestor is the node with largest depth which is the ancestor of both nodes.
The node has an extra attribute parent which point to the father of itself. The root's parent is null.

Thinking:
The original purpose is to make the question much easy by providing u a additional parent attribute.

So you can use hash table method, just put A and A's father to hash all the way to the root.
And then check if B or B's any father was in it.

But you don't need that attribute, you still can use question 1's method to get result.

'''

class Solution:
    """
    @param root: The root of the tree
    @param A and B: Two node in the tree
    @return: The lowest common ancestor of A and B
    """ 
    def lowestCommonAncestorII(self, root, A, B):
        # Write your code here
        dict = {}
        while A is not None:
            dict[A] = True
            A = A.parent

        while B is not None:
            if B in dict:
                return B
            B = B.parent

        return root