import heapq
class Solution:
    # @param {int[][]} arrays k sorted integer arrays
    # @return {int[]} a sorted array

    #thinking:

    '''
    1 push all the first element in to heap, remember they may not be the top 3 smallest!!
    2 Pop the smallest, then fill in the next element after it in his row.

    '''

    def mergekSortedArrays(self, arrays):
        # Write your code here
        if not arrays:
            return
        
        h = []
        res = []
        k = len(arrays)
        
        for i in range(k):
            if arrays[i]:
                heapq.heappush(h,(arrays[i][0],i,0))
        
        while h:
            now,i,j = heapq.heappop(h)
            res.append(now)
            if j < len(arrays[i]) - 1:
                heapq.heappush(h,(arrays[i][j+1],i,j+1))
        return res
        