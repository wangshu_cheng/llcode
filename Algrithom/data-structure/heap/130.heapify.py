'''
Given an integer array, heapify it into a min-heap array.

For a heap array A, A[0] is the root of heap, and for each A[i], A[i * 2 + 1]
 is the left child of A[i] and A[i * 2 + 2] is the right child of A[i].

'''

class Solution:
    # @param A: Given an integer array
    # @return: void
    def heapify(self, A):
        # write your code here
        def shiftup(A,k):
            while k:
                father = (k-1)/2
                if A[k] > A[father]:
                    break
                A[k],A[father] = A[father],A[k]
                
                k = father
        
        for i in range(len(A)):
            shiftup(A,i)
