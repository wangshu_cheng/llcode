'''
Firt put head of each linklist to heap,
than we can pop, after each pop, we can push the next node of that node to heap.
by doing so, we only need maintain a heap depth of length of the array.

'''

import heapq
class Solution:
    """
    @param lists: a list of ListNode
    @return: The head of one sorted list.
    """
    def mergeKLists(self, lists):
        # write your code here
        
        if not lists:
            return
        
        h = []
        dumy = ListNode(0)
        head = dumy
        
        for i in range(len(lists)):
            if lists[i]:
                #the reason put a tuple just for heap to compare value.
                heapq.heappush(h,(lists[i].val,lists[i]))
            
        while h:
            value,node = heapq.heappop(h)
            # node = ListNode(now)
            head.next = node
            head = head.next
            if node.next:
                heapq.heappush(h,(node.next.val,node.next))
                
        return dumy.next

