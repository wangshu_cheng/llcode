'''
Median is the middle value in an ordered integer list. If the size of the list is even, there is no middle value. So the median is the mean of the two middle value.

For example,
[2,3,4], the median is 3

[2,3], the median is (2 + 3) / 2 = 2.5

Design a data structure that supports the following two operations:

void addNum(int num) - Add a integer number from the data stream to the data structure.
double findMedian() - Return the median of all elements so far.
Example:

addNum(1)
addNum(2)
findMedian() -> 1.5
addNum(3) 
findMedian() -> 2
'''
import heapq
class MedianFinder(object):

    def __init__(self):
        """
        initialize your data structure here.
        """
        self.maxheap = [] #lower half,store small data
        self.minheap = [] #higher half, store big data

    def addNum(self, num):
        """
        :type num: int
        :rtype: void
        """
        #维持左边lower half 要大一些；
        #每次进来一个值，先塞到左边，然后左边弹出一个塞到右边，保证右边拿到目前左边的最大值；
        heapq.heappush(self.maxheap,-num)
        heapq.heappush(self.minheap,-heapq.heappop(self.maxheap))
        
        #如果左边比右边小了，右边弹一个出来到左边
        if len(self.maxheap) < len(self.minheap):
            heapq.heappush(self.maxheap,-heapq.heappop(self.minheap))
        

    def findMedian(self):
        """
        :rtype: float
        """
        if len(self.maxheap) == len(self.minheap):
            return (-self.maxheap[0] + self.minheap[0])/2.0
        else:
            return -self.maxheap[0]
        


# Your MedianFinder object will be instantiated and called as such:
# obj = MedianFinder()
# obj.addNum(num)
# param_2 = obj.findMedian()