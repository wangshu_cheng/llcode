'''
Implement a data structure, provide two interfaces:

add(number). Add a new number in the data structure.
topk(). Return the top k largest numbers in this data structure. 
k is given when we create the data structure.
'''
import heapq
class Solution:

    # @param {int} k an integer
    def __init__(self, k):
        # initialize your data structure here.
        self.h = []
        self.k = k
        # heapq.heapify(self.h)

        
    # @param {int} num an integer
    def add(self, num):
        # Write your code here
        #only maintain k element in the heap, we don't need k + 1 elements!!

        #if we current don't have k element in heap, we gonna add it anyway
        if len(self.h) < k:
        	heapq.heappush(self.h.num)
        	return
       	#or we already have k elements, but the new one is larger than smallest
       	#then we need pop out the smallest, since he is no longer the kth largest one.
        if self.h[0] < num:
        	heapq.heappop(self.h)
        	heapq.heappush(self.h,num)

    # @return {int[]} the top k largest numbers
    def topk(self):
    	#because the default heap is min heap, we need reverse
        return sorted(self.h,reverse = true)

s = Solution(3);
s.add(3)
s.add(10)
s.add(1000)
# print s.topk()


















class Solution:

    # @param {int} k an integer
    def __init__(self, k):
        self.k = k
        self.nums = []
        heapq.heapify(self.nums)
        
    # @param {int} num an integer
    def add(self, num):
        if len(self.nums) < self.k:
            heapq.heappush(self.nums, num)
        elif num > self.nums[0]:
            heapq.heappop(self.nums)
            heapq.heappush(self.nums, num)

    # @return {int[]} the top k largest numbers in array
    def topk(self):
        return sorted(self.nums, reverse=True)


