class Solution:
    '''
    @param {int[]} nums an integer array
    @param {int} k an integer
    @return {int[]} the top k largest numbers in array
    '''
    #method1 use heap

    def topk(self, nums, k):
        # Write your code here
        
        h = []
        res = []
        for i in range(len(nums)):
            #because heapq is min heap, so to acchieve max heap, need push invert value.
            heapq.heappush(h,-nums[i])
            
        for j in range(k):
            res.append(-heapq.heappop(h))
        return res
    

    #method2 use quick sort. #same as the kth largest number in array
    def topk(self, nums, k):
        # 必须背下！
        def partition(nums,l,r):
            #初始化
            if l == r:
                return l
            #pivot
            pivot = nums[r]
            index = l
            #循环
            for i in range(l,r): #the last element don't use
                if nums[i] >= pivot: #put large data in the front
                    nums[i],nums[index] = nums[index],nums[i]
                    index += 1
            nums[index],nums[r] = nums[r],nums[index]
            return index
        # 必须背下！
        def helper(nums,l,r,k):
            if l == r:
                return #nums[l]
            
            pos = partition(nums,l,r)
            if pos + 1 == k:
                return #nums[pos]
            elif pos + 1 < k:
                helper(nums,pos + 1,r,k)
            else:
                helper(nums,l,pos - 1,k)
                
        helper(nums,0,len(nums) - 1,k)
        
        if k <= 0: return
        k = min(k,len(nums))

        res = []
        for i in range(k):
            res.append(nums[i])

        #This sort can be accepted since it only sort the k elements
        res = sorted(res,reverse=True)
        return res

test = Solution()

test.topk([1,2,3,4,5,6,8,9,10,7], 3)