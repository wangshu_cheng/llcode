
Check out 296 find median from data stream


# '''
# Numbers keep coming, return the median of numbers at every time a new number added.

# Have you met this question in a real interview? Yes
# Clarification
# What's the definition of Median?
# - Median is the number that in the middle of a sorted array. If there are n numbers in a sorted 
# array A, the median is A[(n - 1) / 2]. For example, if A=[1,2,3], median is 2. If A=[1,19], median is 1.

# Example
# For numbers coming list: [1, 2, 3, 4, 5], return [1, 1, 2, 2, 3].

# For numbers coming list: [4, 5, 1, 3, 2, 6, 0], return [4, 4, 4, 3, 3, 3, 3].

# For numbers coming list: [2, 20, 100], return [2, 2, 20].


# '''

# import heapq
# class Solution:
#     """
#     @param nums: A list of integers.
#     @return: The median of numbers
#     """
#     def medianII(self, nums):
#         # write your code here
#         # define left max heap for new data < median, right min heap for new data > median
#         #if use python heapq, there is only one min heap,use nlargest for max value

#         #left min heap's size should be equal or 1 less than right max
#         #so if len(right heap) - len(left heap) > 1, we should push mid to left and pop one from right as mid

#         if not nums:
#             return []
#         res = []
#         lheap = [] # max heap
#         rheap = []
#         mid = nums[0]
#         res.append(mid)
#         for i in range(1,len(nums)):
#             if nums[i] <= mid:
#                 # change to negative to make a max heap
#                 heapq.heappush(lheap,-nums[i])
#             else:
#                 heapq.heappush(rheap,nums[i])
                
#             while len(lheap) > len(rheap):
#                 heapq.heappush(rheap,mid)
#                 mid = -heapq.heappop(lheap)
#             while len(rheap) - len(lheap) >= 2:
#                 heapq.heappush(lheap,-mid)
#                 mid = heapq.heappop(rheap)
            
#             res.append(mid)
#         return res
#             