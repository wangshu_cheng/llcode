'''
Given a matrix of lower alphabets and a dictionary. Find all words in the dictionary that can be found in the matrix. 
A word can start from any position in the matrix and go left/right/up/down to the adjacent position. 


Have you met this question in a real interview? Yes
Example
Given matrix:
[d o a f
 a g a i
 d c a n]
and dictionary:
{"dog", "dad", "dgdg", "can", "again"}

return {"dog", "dad", "can", "again"}

'''

class TrieNode:
    def __init__(self):
        self.hasWord = False
        self.children = {}
        self.s = ""
        
class TrieTree:
    def __init__(self):
        self.root = TrieNode()
        
    def addWord(self, word):
        # Write your code here
        now = self.root
        for c in word:
            if c not in now.children:
                now.children[c] = TrieNode()
            now = now.children[c]
        now.hasWord = True
        now.s = word
        
    # def find(self,s):
    #     now = self.root
    #     for i in s:
    #         if i not in now.children:
    #             return False
    #         now = now.children[i]
    #     return now.hasWord
        

class Solution:
    #search word in Trie tree and add to result
    #搜索条件是当前值是否在给定 trie node的children 里面
    def search(self,res,board,x,y,root):
        pos = [(1,0),(-1,0),(0,1),(0,-1)]
        n,m = len(board),len(board[0])
        if root.hasWord == True:
            if root.s not in res:
               res.append(root.s)
        #border check:
        if (x<0 or x>= n or y<0 or y>=m or board[x][y] == 0 or root == None):
            return
        
        if board[x][y] in root.children:
            for i in xrange(4):
                now = board[x][y]
                board[x][y] = 0 # set to 0 to mark as visited and reverse back later,
                self.search(res,board,x+pos[i][0],y+pos[i][1],root.children[now])
                board[x][y] = now
        
                
            
        
    # @param board, a list of lists of 1 length string
    # @param words: A list of string
    # @return: A list of string
    def wordSearchII(self, board, words):
        # write your code here
        # none check:
        
        #put dic to trie tree:
        tree = TrieTree()
        for word in words:
            tree.addWord(word)
        res = []
        
        n,m = len(board),len(board[0])
        for i in xrange(n):
            for j in xrange(len(board[i])):
                self.search(res,board,i,j,tree.root)
        return res
        
        
        
        
        