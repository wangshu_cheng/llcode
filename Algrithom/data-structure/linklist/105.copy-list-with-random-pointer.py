'''
A linked list is given such that each node contains an additional random pointer which could point to any node 
in the list or null 

O(1) space. (no hash table)

Return a deep copy of the list.

'''
# Definition for singly-linked list with a random pointer.
# class RandomListNode:
#     def __init__(self, x):
#         self.label = x
#         self.next = None
#         self.random = None
class Solution:
    # @param head: A RandomListNode
    # @return: A RandomListNode
    def copyRandomList(self, head):
        # write your code here
        #这里是一个技巧，不直接deep copy的原因是，如果直接deepcopy，那么里面的random 指针还是只想原有的node
        #没有意义，之后再给它附上新node的地址，很难操作，除非用hash表（题目不允许）
        #所以这里搞了一个新老node 链接在一起的 mix 链表，这样后面只需要把 老node的random的下一个元素（新元素）给
        #老node的下一个元素的random 指针就可以了。


        def duplicateNodes(head):
            #head 可以随便 reassign，不会影响outscope的head值
            while head:
                #create the new node firstly
                newNode = RandomListNode(head.label)
                newNode.next = head.next
                #这里把original list的random copy给新list并没有实际意义，只是为了后面判断random是否为空
                newNode.random = head.random

                #pointer the head to the new node
                head.next = newNode

                #move head to next original element
                if head.next:
                    head = head.next.next#move to next make sense node :)
                    
        def copyRandom(head):
            while head:
                if head.next.random:#第一个新元素
                    head.next.random = head.random.next #next is the new node.
                head = head.next.next
        
        def splitTwoList(head):
            if not head:
                return
            newHead = head.next
            while head:
                tmp = head.next
                head.next = tmp.next
                head = head.next
                if tmp.next:
                    tmp.next = tmp.next.next
            return newHead
                
        if not head:
            return
        duplicateNodes(head)
        copyRandom(head)
        return splitTwoList(head)
        
        
        