'''
Write a program to find the node at which the intersection of two singly linked lists begins.


For example, the following two linked lists:

A:          a1 → a2
                   ↘
                     c1 → c2 → c3
                   ↗            
B:     b1 → b2 → b3
begin to intersect at node c1.

Thinking:
1 Link A tail to B, make one linklist
2 Detect the loop and find interscection using one link list detect loop method.

'''
# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def getIntersectionNode(self, headA, headB):
        """
        :type head1, head1: ListNode
        :rtype: ListNode
        """
        if not headA or not headB:
            return 
        
        def detectCycle(self, head):
            # write your code here
            if not head or not head.next:
                return 
            
            slow,fast = head,head.next
        
            while fast != slow:
                if not fast or not fast.next:
                    #here means no cycle
                    return None
                fast = fast.next.next
                slow = slow.next
            
            # find the intersection point
            while head != slow.next:
                head = head.next
                slow = slow.next
            return head
            
        #Link A and B together, then check one link intersection
        #find tail of A
        tailA = headA
        while tailA and tailA.next:
            tailA = tailA.next
        #connect to B
        tailA.next = headB
        result = detectCycle(self, headA)
        #disconnect head A and B, we are not suppose to change original link list,
        tailA.next = None
        return result