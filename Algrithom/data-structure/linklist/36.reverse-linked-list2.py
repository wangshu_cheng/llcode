'''

 Reverse Linked List II
 -Reverse a linked list from position m to n.

'''
"""
Definition of ListNode

class ListNode(object):

    def __init__(self, val, next=None):
        self.val = val
        self.next = next
"""
class Solution:
    """
    @param head: The head of linked list
    @param m: start position
    @param n: end position
    """
    def reverseBetween(self, head, m, n):
        # write your code here
        if not head or m >= n:
            return head
        dumy = ListNode(0)
        dumy.next = head
        pre = dumy
        for i in range(m - 1):
            pre = pre.next
        head = pre.next
        newhead = pre
        #reverse n - m step
        for i in range(n - m + 1):
            tmp = head.next
            head.next = newhead
            newhead = head
            head = tmp
        # let m point to head
        pre.next.next = head
        # let pre point to new head
        pre.next = newhead
        return dumy.next






class Solution(object):
    def reverseBetween(self, head, m, n):
        """
        :type head: ListNode
        :type m: int
        :type n: int
        :rtype: ListNode
        """
        if m==n: return head
        p = head
        pre = None
        last = None
        start = None
        tmp = None
        for i in range(n):
            if i == m-1:
                pre = p
        end = p
        last = end.next
        start = pre.next
        newHead = pre
        
        cnt = n-m
        #while start and cnt > 0:
        for i in range(n - m + 1):
            tmp = start.next
            start.next = newHead
            newHead = start
            start = tmp
            cnt -= 1
        pre.next = newHead
        if tmp:
            tmp.next = last
        return head
        
                
            
        
        
        
        
