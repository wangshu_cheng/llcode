'''
Sort a linked list using insertion sort.

Have you met this question in a real interview? Yes
Example
Given 1->3->2->0->null, return 0->1->2->3->null.
'''
class Solution:
    """
    @param head: The first node of linked list.
    @return: The head of linked list.
    """ 
    def insertionSortList(self, head):
        # write your code here
        dummy = ListNode(0)

        while head:
            node = dummy

            while node.next and node.next.val < head.val:
                node = node.next

            tmp = head.next
            head.next = node.next
            node.next = head
            head = tmp
        
        return dummy.next