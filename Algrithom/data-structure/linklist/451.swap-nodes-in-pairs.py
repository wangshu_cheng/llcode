'''
Given a linked list, swap every two adjacent nodes and return its head.

Example
Given 1->2->3->4, you should return the list as 2->1->4->3.



'''

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    # @param head, a ListNode
    # @return a ListNode
    def swapPairs(self, head):
        # Write your code here
        if not head or not head.next:
            return head
            
        dumy = ListNode(0)
        
        #Don't forget below line, other wise, dumy means nothing
        dumy.next = head
        
        head = dumy
        
        #the one only matters is head pointer, the other pointer is facilite u to do sth
        
        # to convenient, define n1 and n2, otherwise, need use a lot .next.next ...
        while head.next and head.next.next:
            n1,n2 = head.next,head.next.next
            #前3步交换
            head.next = n2
            n1.next = n2.next
            n2.next = n1
            
            #最后一步跳跃
            head = n1
        
        return dumy.next

# Recursively    
def swapPairs(self, head):
    if head and head.next:
        tmp = head.next
        head.next = self.swapPairs(tmp.next)
        tmp.next = head
        return tmp
    return head