'''

Implement a function to check if a linked list is a palindrome.

'''


# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    # @param head, a ListNode
    # @return a boolean
    def isPalindrome(self, head):
        # Write your code here
        def reverse(head):
            newhead = None
            while head:
                tmp = head.next
                head.next = newhead
                newhead = head
                head = tmp
            return newhead
            
        def getMid(head):
            if not head or not head.next:
                return head
            slow,fast = head,head.next
            while fast and fast.next:
                slow = slow.next
                fast = fast.next.next
            return slow
            
        if not head or not head.next:
            return True
        
        mid = getMid(head)
        head2 = mid.next
        mid.next = None

        head2 = reverse(head2)
        
        while head and head2:
            if head.val != head2.val:
                return False
            head = head.next
            head2 = head2.next
            
        return True
            
        