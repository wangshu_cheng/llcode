'''
Merge sort

Sort a linked list in O(n log n) time using constant space complexity.

'''

"""
Definition of ListNode
class ListNode(object):

    def __init__(self, val, next=None):
        self.val = val
        self.next = next
"""
class Solution:
    """
    @param head: The first node of the linked list.
    @return: You should return the head of the sorted linked list,
                  using constant space complexity.
    """
    def sortList(self, head):
        # write your code here
        
        def findMid(head):
            slow,fast = head,head.next
            while fast and fast.next:
                slow = slow.next
                fast = fast.next.next
            return slow
        
        def merge(h1,h2):
            dumy = ListNode(0)
            head = dumy
        
            while h1 and h2:
                if h1.val < h2.val:
                    head.next = h1
                    h1 = h1.next
                else:
                    head.next = h2
                    h2 = h2.next
                head = head.next
        
            if h1:
                head.next = h1
            elif h2:
                head.next = h2
            return dumy.next
        
        
        def mergeSort(head):
            if not head or not head.next:
                return head
        
            mid = findMid(head)
            right = mergeSort(mid.next)
            mid.next = None
            left = mergeSort(head)
        
            return merge(left,right)
            
        return mergeSort(head)