"""
Definition of ListNode
class ListNode(object):

    def __init__(self, val, next=None):
        self.val = val
        self.next = next

Definition of TreeNode:
class TreeNode:
    def __init__(self, val):
        self.val = val
        self.left, self.right = None, None
"""

'''

Thinking:
由于linklist 遍历很慢，最小的遍历方法 就是从小到大一次，对应到树就是inorder；
由于是BST，可以根据长度来判断递归结束；不停的取长度中值，当 l < r 时递归结束（对应到树的none）
两种方法：
1 通过这个属性，我们可以根据链表的长度构造一个 inorder traversal
2 Find middle，作为root， 然后再两边下去


'''
# in order travers:

class Solution:
    """
    @param head: The first node of linked list.
    @return: a tree node
    """
    def sortedListToBST(self, head):
        # write your code here
        
        if not head:
            return None
        
        self.cur = head
            
        def countListLen(head):
            count = 0
            while head:
                head = head.next
                count += 1
            return count
        
        def helper(l,r):
            if l > r:
                return
            # node = TreeNode(0)
            mid = l + (r - l) / 2
            left = helper(l,mid - 1)
            node = TreeNode(self.head.val)
            self.head = self.head.next
            right = helper(mid + 1, r)

            node.left = left
            node.right = right
            return node
            
        n = countListLen(head)
        return helper(0,n -1)


# Find middle

class Solution:

    def findMiddle(self, head):

        # The pointer used to disconnect the left half from the mid node.
        prevPtr = None
        slowPtr = head
        fastPtr = head

        # Iterate until fastPr doesn't reach the end of the linked list.
        while fastPtr and fastPtr.next:
            prevPtr = slowPtr
            slowPtr = slowPtr.next
            fastPtr = fastPtr.next.next

        # Handling the case when slowPtr was equal to head.
        if prevPtr:
            prevPtr.next = None

        return slowPtr


    def sortedListToBST(self, head):
        """
        :type head: ListNode
        :rtype: TreeNode
        """

        # If the head doesn't exist, then the linked list is empty
        if not head:
            return None

        # Find the middle element for the list.
        mid = self.findMiddle(head)

        # The mid becomes the root of the BST.
        node = TreeNode(mid.val)

        # Base case when there is just one element in the linked list
        if head == mid:
            return node

        # Recursively form balanced BSTs using the left and right halves of the original list.
        node.left = self.sortedListToBST(head)
        node.right = self.sortedListToBST(mid.next)
        return node
