'''
Given a linked list, return the node where the cycle begins.

If there is no cycle, return null.

'''

"""
Definition of ListNode
class ListNode(object):

    def __init__(self, val, next=None):
        self.val = val
        self.next = next
"""
class Solution:
    """
    @param head: The first node of the linked list.
    @return: The node where the cycle begins. 
                if there is no cycle, return null
    """
    def detectCycle(self, head):
        # write your code here
        if not head or not head.next:
            return 
        
        slow,fast = head,head.next

        #先跑到相遇（套圈），如果不套圈，那么没有环

        while fast != slow:
            if not fast or not fast.next:
                #here means no cycle
                return None
            fast = fast.next.next
            slow = slow.next
        
        #can only remember
        #套圈后，head 和slow一起跑，相遇点就是 环开始点

        while head != slow.next:
            head = head.next
            slow = slow.next
        return head
            
            
        
