'''
Given a list, rotate the list to the right by k places, where k is non-negative.

Have you met this question in a real interview? Yes
Example
Given 1->2->3->4->5 and k = 2, return 4->5->1->2->3.


'''


# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    # @param head: the list
    # @param k: rotate to the right k places
    # @return: the list after rotation
    def rotateRight(self, head, k):
        # write your code here
        if not head or not head.next:
            return head
        
        #get length
        
        n = 0
        h1 = head
        while h1:
            h1 = h1.next
            n += 1
        
        k = k % n
        
        dumy = ListNode(0)
        dumy.next = head
        fast = dumy
        slow = dumy

        for i in range(k):
            fast = fast.next
        
        while fast.next:
            fast = fast.next
            slow = slow.next
        
        fast.next = dumy.next
        dumy.next = slow.next
        slow.next = None
        
        return dumy.next
        
            
        
