'''
You have two numbers represented by a linked list, where each node contains a single digit.
 The digits are stored in reverse order, such that the 1's digit is at the head of the list. 
 Write a function that adds the two numbers and returns the sum as a linked list.

Have you met this question in a real interview? Yes
Example
Tags
Related Problems
 Notes
Given 7->1->6 + 5->9->2. That is, 617 + 295.

Return 2->1->9. That is 912.

Given 3->1->5 and 5->9->2, return 8->0->8.

'''

# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    # @param l1: the first list
    # @param l2: the second list
    # @return: the sum list of l1 and l2 
    def addLists(self, l1, l2):
        # write your code here
        if not l2:
            return l1
        if not l1:
            return l2
        dumy = ListNode(0)
        cur = dumy
        over = 0
        while l1 and l2:
            value = (over + l1.val + l2.val) % 10
            over = (over + l1.val + l2.val) / 10
            node = ListNode(value)
            cur.next = node
            l1 = l1.next
            l2 = l2.next
            cur = cur.next
            
        while l1:
            value = (over + l1.val) % 10
            over = (over + l1.val) / 10
            node = ListNode(value)
            cur.next = node
            l1 = l1.next
            cur = cur.next
        while l2:
            value = (over + l2.val) % 10
            over = (over + l2.val) / 10
            node = ListNode(value)
            cur.next = node
            l2 = l2.next
            cur = cur.next
        if over > 0:
            node = ListNode(1)
            cur.next = node
        return dumy.next


#More precise:
class Solution(object):
    def addTwoNumbers(self, l1, l2):
        """
        :type l1: ListNode
        :type l2: ListNode
        :rtype: ListNode
        """
        res=ListNode(None)
        head=res
        val=0
        while l1 or l2 :
            val/=10 #0/10 still = 0;
            
            if l1:
                val+=l1.val
                l1=l1.next
            if l2:
                val+=l2.val
                l2=l2.next
            head.next= ListNode(val%10)
    
            head=head.next
    
        #if still has overflow value, create new nodes:
        if val/10==1:
            head.next=ListNode(1)
        return res.next

        