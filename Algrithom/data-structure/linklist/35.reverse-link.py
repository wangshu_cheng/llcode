class Solution:
    """
    @param head: The first node of the linked list.
    @return: You should return the head of the reversed linked list. 
                  Reverse it in-place.
    """
    def reverse(self, head):
        # write your code here
        
        if not head or not head.next:
            return head
        
        newHead = None
        #2个指针差一步
        while head:
            #前两步指针翻转
            tmp = head.next
            head.next = newHead
            #后两部指针向后跳跃
            newHead = head
            head = tmp
        return newHead