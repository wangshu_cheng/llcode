'''
Find the nth to last element of a singly linked list. 

The minimum number of nodes in list is n.
'''


"""
Definition of ListNode
class ListNode(object):

    def __init__(self, val, next=None):
        self.val = val
        self.next = next
"""
class Solution:
    """
    @param head: The first node of linked list.
    @param n: An integer.
    @return: Nth to last node of a singly linked list. 
    """
    def nthToLast(self, head, n):
        # write your code here
        slow,fast = head,head
        #move fast to nth, if not return None
        for i in range(n):
            if fast:
                fast = fast.next
            else:
                return None
                
        #now run together,when fast reach end, then slow is the nth.
        while fast:
            slow = slow.next
            fast = fast.next
        return slow