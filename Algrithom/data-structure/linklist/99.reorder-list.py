"""
Definition of ListNode
class ListNode(object):

    def __init__(self, val, next=None):
        self.val = val
        self.next = next
"""
class Solution:
    """
    @param head: The first node of the linked list.
    @return: nothing
    """
    def reorderList(self, head):
        # write your code here
        #1 find mid
        #2 reverse 2n half
        #3 merge 1 harf and 2 half
        def reverse(head):
            tail = None
            while head:
                temp = head.next
                head.next = tail
                tail = head
                head = temp
            return tail
            
        def findMid(head):
            slow,fast = head,head.next
            while fast and fast.next:
                fast = fast.next.next
                slow = slow.next
            return slow
            
        def merge(h1,h2):
            dumy = ListNode(0)
            idx = 0
            while h1 and h2:
                if idx %2 == 0: # even 
                    dumy.next = h1
                    h1 = h1.next
                else:
                    dumy.next = h2
                    h2 = h2.next
                dumy = dumy.next
                idx += 1
                
            if h1:
                dumy.next = h1
            else:
                dumy.next = h2
        
        
        #main:
        if not head or not head.next:
            return
        mid = findMid(head)
        tail = reverse(mid.next)
        mid.next = None
        merge(head,tail)
