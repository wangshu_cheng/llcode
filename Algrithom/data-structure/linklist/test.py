class RandomListNode:
    def __init__(self, x):
        self.label = x
        self.next = None
        self.random = None


head = RandomListNode(0)
e1 = RandomListNode(1)

head.next = e1


def deepcopy(head):
	dumy = RandomListNode(0)
	cur = dumy

	while head:
		newNode = RandomListNode(head.label)
		cur.next = newNode
		cur = cur.next
		head = head.next

	return dumy.next

	# head = head.next


newHead = deepcopy(head)

print newHead.label
print newHead.next.label


print head.label
print head.next.label