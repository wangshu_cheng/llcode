"""
Definition of ListNode
class ListNode(object):

    def __init__(self, val, next=None):
        self.val = val
        self.next = next
"""

class Solution:
    # @param head, a ListNode
    # @return a ListNode
    def removeDuplicates(self, head):
        # Write your code here
        if not head:
            return
        hashset = set()
        dumy = ListNode(0)
        dumy.next = head
        node = dumy

        while node and node.next:
            
            if node.next.val not in hashset:
                hashset.add(node.next.val) 
                node = node.next
            else:
                node.next = node.next.next
    
        return dumy.next
            
            
            
    
        