# Definition for singly-linked list.
# class ListNode:
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution:
    # @param head, a ListNode
    # @param val, an integer
    # @return a ListNode
    def removeElements(self, head, val):
        # Write your code here
        if not head:
            return head
        
        dumy = ListNode(0)
        dumy.next = head
        cur = dumy
        
        while cur and cur.next:
            if  cur.next.val == val:
                cur.next = cur.next.next
            else:
                cur = cur.next
        
        return dumy.next
                