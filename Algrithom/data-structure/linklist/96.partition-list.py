'''
Given a linked list and a value x, partition it such that all nodes less than x come before nodes greater than or equal to x.

You should preserve the original relative order of the nodes in each of the two partitions.

For example,
Given 1->4->3->2->5->2 and x = 3,
return 1->2->2->4->3->5.
开2个新的dumy head ，一个存小于X的nodes一个存大于x的nodes，然后拼在一起
和普通的reverse, rotate 不同，那些只是重新定义头尾而已

'''
# Definition for singly-linked list.
# class ListNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.next = None

class Solution(object):
    def partition(self, head, x):
        """
        :type head: ListNode
        :type x: int
        :rtype: ListNode
        """
        if not head or not head.next:
            return head
        leftdumy,rightdumy = ListNode(0),ListNode(0)
        left,right = leftdumy,rightdumy
        
        while head:
            if head.val < x:
                left.next = head
                left = head #same as left = left.next?? I think so
            else:
                right.next = head
                right = head
            head = head.next
        # right 的最后指向最后一个大值，如果right.next不把它设为None，那么他指向原来的list里面的下一个，答案就错了！
        right.next = None
        #注意！ 这里绝对不是 left.next = right.next,因为right是移动指针，已经移到底了
        left.next = rightdumy.next
        return leftdumy.next
        