'''
Given a sorted linked list, delete all nodes that have duplicate numbers, 
leaving only distinct numbers from the original list.
这题和112 的区别是他不留重复的值（不是只留一个）

这样的话，cur指针应该始终指向前一个，（不能让自己处在可能被删的境地）

For example,
Given 1->2->3->3->4->4->5, return 1->2->5.
Given 1->1->1->2->3, return 2->3.

'''

"""
Definition of ListNode
class ListNode(object):

    def __init__(self, val, next=None):
        self.val = val
        self.next = next
"""
class Solution:
    """
    @param head: A ListNode
    @return: A ListNode
    """
    def deleteDuplicates(self, head):
        # write your code here
        if not head:
            return
        dumy = ListNode(0)
        dumy.next = head
        #since the first can be delelted as well, so we need a dumy ahead of it
        cur = dumy
        while cur.next and cur.next.next:
            if cur.next.val == cur.next.next.val:
                #一路猛删
                val = cur.next.val
                while cur.next and cur.next.val == val:
                    cur.next = cur.next.next
            else:
                cur = cur.next
        return dumy.next
            
            