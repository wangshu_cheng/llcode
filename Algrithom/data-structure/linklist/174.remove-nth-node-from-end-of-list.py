'''
Given a linked list, remove the nth node from the end of list and return its head.
'''


"""
Definition of ListNode
class ListNode(object):

    def __init__(self, val, next=None):
        self.val = val
        self.next = next
"""
class Solution:
    """
    @param head: The first node of linked list.
    @param n: An integer.
    @return: The head of linked list.
    """

'''
idea, 找一个快指针从前往后走n步，然后和慢指针同时往后走，当快指针到头了，慢指针就到了倒数n的位置了

'''

    def removeNthFromEnd(self, head, n):
        # write your code here
        #boarder check
        if n <= 0:
            return None

        dumy = ListNode(None)
        dumy.next = head
        slow,fast = dumy,dumy.next
        
        for i in range(n):
            # if fast come to none within n step, then the n is too big or list is too short, return None
            if fast == None:
                return None
            fast = fast.next
            
        while fast: #no need check fast.next != None
            slow = slow.next
            fast = fast.next
        
        #remove nth element
        if slow and slow.next:
            slow.next = slow.next.next
        
        return dumy.next
            