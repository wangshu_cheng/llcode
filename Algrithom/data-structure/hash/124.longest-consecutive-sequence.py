'''
Given an unsorted array of integers, find the length of the longest consecutive elements sequence.

Have you met this question in a real interview? Yes
Clarification
Your algorithm should run in O(n) complexity.

Example
Given [100, 4, 200, 1, 3, 2],
The longest consecutive elements sequence is [1, 2, 3, 4]. Return its length: 4.

知识点：
updown 算最长连续seq 方法，似乎只能背了。

Thinking:
Sort and use 2 variable to get longest sequence,but it's nlogn

'''

class Solution:
    """
    @param num, a list of integer
    @return an integer
    """
    def longestConsecutive(self, num):
        # write your code here
        if not num:
            return 0
        
        n = len(num)
        
        myset = set()
        
        for i in range(n):
            myset.add(num[i])
            
        longest = 0
        for i in range(n):
            up = num[i] + 1
            while up in myset:
                myset.remove(up)
                up += 1
            down = num[i] - 1
            while down in myset:
                myset.remove(down)
                down -= 1
            
            longest = max(longest,up - down - 1)
        return longest
            
                
                
            
        