

class Solution(object):
    def isAnagram(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: bool
        """
        dic = {}
        
        for c in s:
            if c not in dic:
                dic[c] = 1
            else:
                dic[c] += 1
                
        for c in t:
            if c not in dic or dic[c] <= 0:
                return False
            else:
                dic[c] -= 1
                
        for k in dic:
            if dic[k] != 0:
                return False
        return True
            