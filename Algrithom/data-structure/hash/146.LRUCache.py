'''
Design and implement a data structure for Least Recently Used (LRU) cache. It should support the following 
operations: get and set.

get(key) - Get the value (will always be positive) of the key if the key exists in the cache, otherwise return -1.
set(key, value) - Set or insert the value if the key is not already present. When the cache reached its capacity,
 it should invalidate the least recently used item before inserting a new item.

Basic datastructure is hash, hash key is key,hash value is a link node, with key value data.;
But we also need to know the position of each data, so the data itself should be 
link list(don't think of array, or stack, this is big data)

Thinking:
The key to solve this problem is using a double linked list which enables us to quickly move nodes
快速存取=> Hash， 快速删除=〉 LinkList, 所以是link list + hash 的组合

make each data (key value pair) a link node, and put each link node to hash table.
LRU需要一个能够带有顺序，并且能快速插入删除的结构，那么只有链表； 但同时还要O（1）的读取，所以就是 hash + linklist
链表的作用就在于 判断谁是新的谁是旧的，remove old. 由于需要频繁插入，删除，所以链表是最好的选择。

hash[key] 存的是当前node前面的node。

'''

#LRU cache关键点在于，由于是cpu cache，所以空间非常有限；所以需要快速的把不常用的item 删除，所以才要 linklist


class LinkNode:
    def __init__(self,key = None,value = None, pre = None, next = None):
        self.pre = pre
        self.next = next
        self.key = key
        self.value = value

class LRUCache(object):

    def __init__(self, capacity):
        """
        :type capacity: int
        """
        self.capacity = capacity
        self.hash={}
        self.head = None
        self.end = None
        

    def get(self, key):
        """
        :type key: int
        :rtype: int
        """
        if key in self.hash:
            node = self.hash[key]
            #below 2 steps are put the node to the most front(we think it's the most popular)
            self.remove(node)
            self.setHead(node)
            return node.value
        return -1
    
    def remove(self,node):
        if node.pre:
            node.pre.next = node.next
        else:
            self.head = node.next
        
        if node.next:
            node.next.pre = node.pre
        else:
            self.end = node.pre
    def setHead(self,node):
        node.next = self.head
        node.pre = None
        if self.head:
            self.head.pre = node
        self.head = node
        #初始状况，当list为空的时候
        if self.end == None:
            self.end = self.head

    def put(self, key, value):
        """
        :type key: int
        :type value: int
        :rtype: void
        """
        #If the data in hash, then update it
        if key in self.hash:
            old = self.hash[key]
            old.value = value
            self.remove(old)
            self.setHead(old)
        else:
            new_node = LinkNode(key,value)
            #超过capacity了把尾巴（最不常用的）删掉
            if len(self.hash) >= self.capacity:
                del self.hash[self.end.key]
                self.remove(self.end)
                self.setHead(new_node)
            else:
                self.setHead(new_node)
            
            self.hash[key] = new_node
     
        


# Your LRUCache object will be instantiated and called as such:
# obj = LRUCache(capacity)
# param_1 = obj.get(key)
# obj.put(key,value)








# #blow use single link node:
# class LinkedNode:
    
#     def __init__(self, key=None, value=None, next=None):
#         self.key = key
#         self.value = value
#         self.next = next

# class LRUCache(object):

#     def __init__(self, capacity):
#         """
#         :type capacity: int
#         """
#         self.hash = {}
#         self.head = LinkedNode()
#         self.tail = self.head
#         self.capacity = capacity
        
#     def push_back(self, node):
#         self.hash[node.key] = self.tail
#         self.tail.next = node
#         self.tail = node
    
#     def pop_front(self):
#         del self.hash[self.head.next.key]
#         self.head.next = self.head.next.next
#         self.hash[self.head.next.key] = self.head
        
#     # change "prev->node->next...->tail"
#     # to "prev->next->...->tail->node"
#     def kick(self, prev):
#         node = prev.next
#         if node == self.tail:
#             return
#         prev.next = node.next
#         if node.next is not None:
#             self.hash[node.next.key] = prev
#             node.next = None
#         self.push_back(node)
#     def get(self, key):
#         """
#         :type key: int
#         :rtype: int
#         """
#         if key not in self.hash:
#             return -1
#         self.kick(self.hash[key])
#         return self.hash[key].next.value
        

#     def put(self, key, value):
#         """
#         :type key: int
#         :type value: int
#         :rtype: void
#         """
#         if key in self.hash:
#             self.kick(self.hash[key])
#             self.hash[key].next.value = value
#         else:
#             self.push_back(LinkedNode(key, value))
#             if len(self.hash) > self.capacity:
#                 self.pop_front()








# #WARNING!!! Below code can not pass:

# class LinkNode:
#     def __init__(self,key = None,value = None, pre = None, next = None):
#         self.pre = pre
#         self.next = next
#         self.key = key
#         self.value = value
        
# class LRUCache:
    
#     # @param capacity, an integer
#     def __init__(self, capacity):
#         #new data is in tail, oldest data is in head.next        
#         self.head = LinkNode(-1,-1)
#         #head,tail 本身并不放真实数据，head.next 指向第一个真实数据，tail.pre指向最后一个真实数据
#         self.tail = LinkNode(-1,-1)
#         self.hash = {}
#         self.capacity = capacity
#         self.tail.pre = self.head
#         self.head.next = self.tail

#     def removeOld(self):
#         #remove from hash
#         self.hash.pop(self.head.next.key, None)
#         #remove the link list
#         self.head.next = self.head.next.next
#         self.head.next.pre = self.head
    
#     def moveToTail(self,node):
#         node.pre = self.tail.pre
#         self.tail.pre = node
#         node.pre.next = node
#         node.next = self.tail
        

        
#     # @return an integer
#     def get(self, key):
#         if key not in self.hash:
#             return -1
#         result = self.hash[key].value
#         #if fetched, then it is the latest data, move to tail
#         current = self.hash[key]
#         #从当前位置脱离，并移到尾部
#         current.pre.next = current.next
#         current.next.pre = current.pre
#         self.moveToTail(current)
#         print "get, hash = ",self.hash

#         return result
        
#     # @param key, an integer
#     # @param value, an integer
#     # @return nothing
#     def set(self, key, value):
        
#         if key in self.hash:
#             self.hash[key].value = value
#             return
#         #这是LRU cache的精华：
#         if len(self.hash) == self.capacity:
#             self.removeOld()
        
#         #data 本身就是一个linknode，key就是key，value是key value Pair
#         newNode = LinkNode(key,value)
#         self.hash[key] = newNode
#         self.moveToTail(newNode)
#         print "set, hash = ",self.hash

test = LRUCache(2)
test.set(2,1)
test.set(1,1)
print test.get(2)
test.set(4,1)
print test.get(1)

