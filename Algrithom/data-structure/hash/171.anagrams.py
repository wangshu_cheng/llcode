'''
Given an array of strings, return all groups of strings that are anagrams.

 Notice

All inputs will be in lower-case

Have you met this question in a real interview? Yes
Example
Given ["lint", "intl", "inlt", "code"], return ["lint", "inlt", "intl"].

Given ["ab", "ba", "cd", "dc", "e"], return ["ab", "ba", "cd", "dc"].
'''


'''
Another easy solution is to use sorted word as hash key
'''

class Solution:
    # @param strs: A list of strings
    # @return: A list of strings
    def anagrams(self, strs):
        # write your code here
        
        def getHash(count):
            hash = 0
            a= 378551
            b = 63689
            
            for num in count:
                hash = hash * a + num
                a = a * b
            return hash
        # if use below hash function, will have collison.
        # def getHash(count):
        #     sum = 0
        #     for i in count:
        #         sum = sum * 33 + i
        #         sum = sum % 65535
        #     return sum
        
        if not strs:
            return []
            
        dic = {} 
        res = []
        
        for str in strs:
            count = [0 for i in range(26)]
            for i in str:
                count[ord(i) - ord('a')] += 1
            hashCode = getHash(count)
            
            if hashCode not in dic:
                dic[hashCode] = [str]
            else:
                dic[hashCode].append(str)

        for key in dic:
            if len(dic[key]) > 1:
                res += dic[key]

        return res
            
                
            
            