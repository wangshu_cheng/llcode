'''
Given a list of words and an integer k, return the top k frequent words in the list.
知识点：
1 sorted 函数第二个参数 compare function
'''

class Solution:
    # @param {string[]} words a list of string
    # @param {int} k an integer
    # @return {string[]} a list of string
    def topKFrequentWords(self, words, k):
        # Write your code here
        #use hash
        if not words or k <= 0:
            return []
        
        dic = {}
        arr = []
        for w in words:
            if w not in dic:
                dic[w] = 1
            else:
                dic[w] += 1
        
        for key in dic:
            arr.append((dic[key],key))
            
        def compare(a,b):
            # string can directly compare using python,but not + and -.
            if a[0] > b[0] or (a[0] == b[0] and a[1] < b[1]):
                return -1
            elif a[0] == b[0] and a[1] == b[1]:
                return 0
            else:
                return 1
            
            
        arr = sorted(arr,cmp=compare)
        
        res = []
        
        for i in range(k):
            res.append(arr[i][1])
            
        return res