'''Leetcode 49:
Group Anagrams  QuestionEditorial Solution  My Submissions
Total Accepted: 82541
Total Submissions: 287905
Difficulty: Medium
Given an array of strings, group anagrams together.

For example, given: ["eat", "tea", "tan", "ate", "nat", "bat"], 
Return:

[
  ["ate", "eat","tea"],
  ["nat","tan"],
  ["bat"]
]
'''

class Solution(object):
    def groupAnagrams(self, strs):
        """
        :type strs: List[str]
        :rtype: List[List[str]]
        """
        hash = {}
        res = []
        for s in strs:
            s_sorted = "".join(sorted(list(s)))
            if s_sorted not in hash:
                hash[s_sorted] = [s]
            else:
                hash[s_sorted].append(s)
            
        for key in hash:
            res.append(hash[key])
        
        return res

