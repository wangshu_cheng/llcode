'''
An animal shelter holds only dogs and cats, and operates on a strictly "first in, first out" 
basis. People must adopt either the "oldest" (based on arrival time) of all animals at the shelter,
 or they can select whether they would prefer a dog or a cat (and will receive the oldest animal 
 of that type). They cannot select which specific animal they would like. Create the data structures 
 to maintain this system and implement operations such as enqueue, dequeueAny, dequeueDog and dequeueCat.
'''

class AnimalShelter(object):

    def __init__(self):
        # do some intialize if necessary
        self.cats = []
        self.dogs = []
        self.tot = 0

    """
    @param {string} name
    @param {int} type, 1 if Animal is dog or 0
    @return nothing
    """
    def enqueue(self, name, type):
        # Write yout code here
        self.tot += 1
        if type == 1:
            self.dogs.append((name, self.tot))
        else:
            self.cats.append((name, self.tot))

    # return a string
    def dequeueAny(self):
        # Write your code here
        if len(self.dogs) == 0:
            return self.dequeueCat()
        elif len(self.cats) == 0:
            return self.dequeueDog()
        else:
            if self.dogs[0][1] < self.cats[0][1]:
                return self.dequeueDog()
            else:
                return self.dequeueCat()

    # return a string
    def dequeueDog(self):
        # Write your code here
        name = self.dogs[0][0]
        del self.dogs[0]
        return name

    # return a string
    def dequeueCat(self):
        # Write your code here
        name = self.cats[0][0]
        del self.cats[0]
        return name