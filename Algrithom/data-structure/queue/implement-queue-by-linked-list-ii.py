
class Dequeue(object):

    def __init__(self):
        # do some intialize if necessary
        self.first, self.last = None, None

    # @param {int} item an integer
    # @return nothing
    def push_front(self, item):
        # Write yout code here
        if self.first is None:
            self.first = Node(item)
            self.last = self.first
        else:
            tmp = Node(item)
            self.first.prev = tmp
            tmp.next = self.first
            self.first = tmp

    # @param {int} item an integer
    # @return nothing
    def push_back(self, item):
        # Write yout code here
        if self.last is None:
            self.first = Node(item)
            self.last = self.first
        else:
            tmp = Node(item)
            self.last.next = tmp
            tmp.prev = self.last
            self.last = tmp

    # @return an integer
    def pop_front(self):
        # Write your code here
        if self.first is not None:
            item = self.first.val
            self.first = self.first.next
            if self.first is not None:
                self.first.prev = None
            else:
                self.last = None
            return item

        return -12

    # @return an integer
    def pop_back(self):
        # Write your code here
        if self.last is not None:
            item = self.last.val
            self.last = self.last.prev
            if self.last is not None:
                self.last.next = None
            else:
                self.first = None
            return item

        return -12

class Node():

    def __init__(self, _val):
        self.next = self.prev = None
        self.val = _val