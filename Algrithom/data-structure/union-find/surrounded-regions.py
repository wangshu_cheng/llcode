'''
Given a 2D board containing 'X' and 'O', capture all regions surrounded by 'X'.

A region is captured by flipping all 'O''s into 'X''s in that surrounded region.

Have you met this question in a real interview? Yes
Example
X X X X
X O O X
X X O X
X O X X
After capture all regions surrounded by 'X', the board should be:

X X X X
X X X X
X X X X
X O X X

'''
class Solution:
    # @param {list[list[str]]} board a 2D board containing 'X' and 'O'
    # @return nothing 
    def bfs(self,board,x,y):
	    if not board:
	        return
	    if x == None or y == None:
	        return
	    n,m = len(board),len(board[0])
	    dir = [(1,0),(-1,0),(0,1),(0,-1)]
	    
	    if board[x][y] != 'O':
	        return
	    q=[(x,y)]
	    while q:
	        cur_x,cur_y = q.pop(0)
	        board[cur_x][cur_y] = 'P'
	        for i in range(4):
	            new_x,new_y = cur_x+dir[i][0],cur_y+dir[i][1]
	            if new_x <0 or new_x >=n or new_y<0 or new_y >=m:
	                continue
	            if board[new_x][new_y] == 'O':
	                q.append((new_x,new_y))
	                
    def surroundedRegions(self, board):
        # Write your code here
        if not board:
		    return
        n,m = len(board),len(board[0])
        for i in xrange(n):
            self.bfs(board,i,0)
            self.bfs(board,i,m-1)
        for j in xrange(m):
            self.bfs(board,0,j)
            self.bfs(board,n-1,j)
			
		# now we get all the 0 subset connected to boarder as "P"
        for i in xrange(n):
		  for j in xrange(m):
		     if board[i][j] == 'O':
		        board[i][j] = 'X'
		     elif board[i][j] == 'P':
		        board[i][j] = 'O'
		            

	   
	        
	    