'''
Given n nodes labeled from 0 to n - 1 and a list of undirected edges (each edge is a pair of nodes), write a function to check whether these edges make up a valid tree.

 Notice

You can assume that no duplicate edges will appear in edges. Since all edges are undirected, [0, 1] is the same as [1, 0] and thus will not appear together in edges.

Have you met this question in a real interview? Yes
Example
Given n = 5 and edges = [[0, 1], [0, 2], [0, 3], [1, 4]], return true.

Given n = 5 and edges = [[0, 1], [1, 2], [2, 3], [1, 3], [1, 4]], return false.

Thinking:
就是检查有没有环并且没有孤立点，有环的情况就是某个下一级试图访问已经访问过的点；
'''

class Solution:
    # @param {int} n an integer
    # @param {int[][]} edges a list of undirected edges
    # @return {boolean} true if it's a valid tree, or false

    #Method1: BFS:
    def validTree(self, n, edges):
        """
        :type n: int
        :type edges: List[List[int]]
        :rtype: bool
        """
        dic = {}
        for i in range(n):
            dic[i] = list()
        
        for e in edges:
            dic[e[0]].append(e[1])
            dic[e[1]].append(e[0])
        
        visited = [False]*n
        q = [0] # 0 is root
        while q:
            cur = q.pop(0)
            if visited[cur]:
                return False
            visited[cur] = True
            for nei in dic[cur]:
                if not visited[nei]:
                    q.append(nei)
        
        #检查有没有孤立点
        for b in visited:
            if not b:
                return False
        return True


    #Method2: DFS:
    def validTree(self, n, edges):
        """
        :type n: int
        :type edges: List[List[int]]
        :rtype: bool
        """
        #parent 用来解决无向图的问题，保证不访问同一个edge 里面已经访问过的点
        def helper(cur,parent,dic,visited):
            if visited[cur]:return False
            visited[cur] = True
            
            neighbours = dic[cur]
            
            for nei in neighbours:
                #nei 不能等于它的root的root，这就是解决undiret graph 带来的问题
                # 比如： 0->[1]
                #   1->[0,4]

                # 当我们走到  helper(1,0) 的时候；1 的neibougher 有0， 因为 1-0，0-1 无方向；
                # 但是0 已经走过了，不能再走了，所以加上 nei!= parent 

                if nei == parent:
                    print nei
                if nei!= parent and not helper(nei,cur,dic,visited):
                    return False
            return True       
        dic = {}
        for i in range(n):
            dic[i] = list()
        
        for e in edges:
            dic[e[0]].append(e[1])
            dic[e[1]].append(e[0])
        
        visited = [False]*n
        
        if not helper(0,-1,dic,visited):
            return False
        
        #检查有没有孤立点
        for b in visited: # if any one not visted, it's False
            if not b:
                return False
        return True


    #method3 UF:
    
    class UnionFind:
        def __init__(self,n):
            self.father = {}
            for i in range(n):
                self.father[i] = i
                
        def find(self,i):
            parent = self.father[i]
            while parent != self.father[parent]:
                parent = self.father[parent]
            temp = -1
            fa = i
            while (fa!=self.father[fa]):
                temp = self.father[fa]
                self.father[fa] = parent
                fa = temp
            return parent
            # if self.father[i] == i:
            #     return i
            # self.father[i] = self.find(self.father[i])
            # return self.father[i]
            
        def union(self,i,j):
            fa_i = self.father[i]
            fa_j = self.father[j]
            if fa_i != fa_j:
                self.father[fa_i] = fa_j
    
    def validTree(self, n, edges):
        # Write your code here
    # Use union find to check loop, if no loop ,it's tree
    # How to?
    # Step1: create set of all nodes
    # Step2: use edges as relationship to union
    # Step3: Before each step2, do check if the have same father(already in a union), if yes, has loop
        if n == 0:
            return False
        if edges == None or len(edges) != n-1:
            return False
            
        uf = Solution.UnionFind(n)
        
        for ed in edges:
            if uf.find(ed[0]) == uf.find(ed[1]):
                return False
            uf.union(ed[0],ed[1])
            
        return True
    