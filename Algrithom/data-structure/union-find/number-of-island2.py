'''
HARD, union find

Given a n,m which means the row and column of the 2D matrix and an array of pair A
( size k). Originally, the 2D matrix is all 0 which means there is only sea in the matrix. 
The list pair has k operator and each operator has two integer A[i].x, A[i].y means that
 you can change the grid matrix[A[i].x][A[i].y] from sea to island. Return how many island 
 are there in the matrix after each operator.


0 is represented as the sea, 1 is represented as the island. If two 1 is adjacent, 
we consider them in the same island. We only consider up/down/left/right adjacent.

Have you met this question in a real interview? Yes
Example
Given n = 3, m = 3, array of pair A = [(0,0),(0,1),(2,2),(2,1)].

return [1,1,2,2].

这个问题 看起来可以转化为 岛屿1的搜索问题，但是搜索 复杂度是O(N**2), 如果每一步都搜索的话，
那么复杂度就是 k*N**2 ，k 是A的长度，显然太慢，所以要用数据结构， union find
'''

# Definition for a point.
# class Point:
#     def __init__(self, a=0, b=0):
#         self.x = a
#         self.y = b
class Solution:
    # @param {int} n an integer
    # @param {int} m an integer
    # @param {Pint[]} operators an array of point
    # @return {int[]} an integer array
    class UnionFind:
            def __init__(self,n,m):
                self.father = dict()
                for i in range(n):
                    for j in range(m):
                        id = self.convertId(i,j,m)
                        self.father[id] = id
                        
            def convertId(self,i,j,m):
                return i*m + j
                
            def find(self,x):
                parent = self.father[x]
                while parent != self.father[parent]:
                    parent = self.father[parent]
                return parent
            
            def union(self,x,y):
                fa_x = self.find(x)
                fa_y = self.find(y)
                if fa_y != fa_x:
                    self.father[fa_x] = fa_y

    def numIslands2(self, n, m, operators):
        # Write your code here
        # 1 create a uf class
        # 2 for each opearation, we pre add count 1, than we try to do union with 
        # surrounding nodes, if has ajancent node, we union and reduce count 1
        
            
        #main logic:
        count = 0
        dr = [(0,1),(0,-1),(-1,0),(1,0)]
        uf = Solution.UnionFind(n,m)
        res = []
        grid = [[0 for i in range(m)] for j in range(n)]
        if not operators:
            return []
        for op in operators:
            count += 1
            x,y = op.x,op.y
            if grid[x][y] != 1:#if it's 1 we already handled before
                grid[x][y] = 1
                # pre add 1, if we found union in surrounding grid,we remove later
                # count += 1
                #find uions
                for i in range(4):
                    next_x,next_y = x + dr[i][0],y + dr[i][1]
                    if 0 <= next_x < n and 0 <= next_y < m and grid[next_x][next_y] == 1:
                        fa_now = uf.find(uf.convertId(x,y,m))
                        fa_next = uf.find(uf.convertId(next_x,next_y,m))
                        if fa_now != fa_next:
                            count -= 1
                            uf.union(fa_now,fa_next)
            res.append(count)
        return res       
                
                
                
                    