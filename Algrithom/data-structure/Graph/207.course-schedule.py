'''

There are a total of n courses you have to take, labeled from 0 to n-1.

Some courses may have prerequisites, for example to take course 0 you have to first take course 1, which is expressed as a pair: [0,1]

Given the total number of courses and a list of prerequisite pairs, is it possible for you to finish all courses?

Example 1:

Input: 2, [[1,0]] 
Output: true
Explanation: There are a total of 2 courses to take. 
             To take course 1 you should have finished course 0. So it is possible.
Example 2:

Input: 2, [[1,0],[0,1]]
Output: false
Explanation: There are a total of 2 courses to take. 
             To take course 1 you should have finished course 0, and to take course 0 you should
             also have finished course 1. So it is impossible.

Thinking:
This problem can be converted to finding if a graph contains a cycle.
'''

class Solution(object):

	#1 DFS:
    def canFinish(self, numCourses, prerequisites):
        """
        :type numCourses: int
        :type prerequisites: List[List[int]]
        :rtype: bool
        """
        if not numCourses or not prerequisites:
            return True
        #Convert the list of edges to Adjacency list:
        #use 2d matrix to present a graph, first dimention is node, 2nd is his neighour.
        graph = [[] for _ in range(numCourses)]
        visited = [0 for _ in range(numCourses)]
        for x,y in prerequisites:
            graph[y].append(x)
            
        def dfs(graph,node,visited):
            if visited[node] == -1:
                return False
            if  visited[node] == 1:# This is to remember this node and his neighor has not cycle.Save some space.
                return True
            
            visited[node] = -1
            
            for nei in graph[node]:
                if not dfs(graph,nei,visited):
                    return False
               
            #Very important!!!!! Set visit back 
            #normal method is to set visited[node] back to 0 here:
            visited[node] = 1 # This is to remember this node and his neighor has not cycle.Save some space.
            return True
        
        for node in range(numCourses):
            if not dfs(graph,node,visited):
                return False
        
        return True

      #2 BFS:
        def canFinish(self, numCourses, prerequisites):
        """
        :type numCourses: int
        :type prerequisites: List[List[int]]
        :rtype: bool
        """
        #BFS:
        if not prerequisites:
            return True
        
        LEN = len(prerequisites)
        pCounter = [0]*numCourses
        
        for i in range(LEN):
            pCounter[prerequisites[i][0]] += 1
        
        q = []
        
        # Find all root nodes
        # P count == 0 means this course not depend on any course, or 
        # Say he is not anyone's neighour, he is first level nodes.Root nodes.
        for i in range(numCourses):
            if pCounter[i] == 0:
                q.append(i)
                
        #number of courses that have no prerequisites
        numNoPre = len(q)
        
        while q:
            node = q.pop()
            for i in range(LEN):
                if prerequisites[i][1]==node:
                    pCounter[prerequisites[i][0]]-=1
                    if pCounter[prerequisites[i][0]]==0:
                            numNoPre+=1
                            q.append(prerequisites[i][0]);
        return numNoPre == numCourses;           
        