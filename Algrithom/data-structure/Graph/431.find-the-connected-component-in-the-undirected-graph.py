'''
Find the number connected component in the undirected graph. Each node in the graph contains a label and 
a list of its neighbors. (a connected component (or just component) of an undirected graph is a subgraph 
in which any two vertices are connected to each other by paths, and which is connected to no additional
 vertices in the supergraph.)


Clarification
Learn more about representation of graphs

Example
Given graph:

A------B  C
 \     |  | 
  \    |  |
   \   |  |
    \  |  |
      D   E
Return {A,B,D}, {C,E}. Since there are two connected component which is {A,B,D}, {C,E}


'''

# Definition for a undirected graph node
# class UndirectedGraphNode:
#     def __init__(self, x):
#         self.label = x
#         self.neighbors = []
class Solution:
    # @param {UndirectedGraphNode[]} nodes a array of undirected graph node
    # @return {int[][]} a connected set of a undirected graph
    
    # NOT WORKING!!!
    
    def connectedSet(self, nodes):
        # Write your code here
        #basic solution is BFS, no need union find
        visited = {}
        result = []
        
        def bfs(node,visited,result):
            q = [node]
            visited[node.label] = True
            row = list()
            
            while q:
                head = q.pop()
                row.append(head.label)
                
                for v in head.neighbors:
                    if visited[v.label] == False:
                        q.append(v)
                        visited[v.label] = True
            
            result.append(sorted(row))                    
            
        
        for node in nodes:
            visited[node.label] = False
        
        for node in nodes:
            if visited[node.label] == False:
                bfs(node,visited,result)
                
        return result
        