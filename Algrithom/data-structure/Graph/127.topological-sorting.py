'''
Given an directed graph, a topological order of the graph nodes is defined as follow:

For each directed edge A -> B in graph, A must before B in the order list.
The first node in the order can be any node in the graph with no nodes direct to it.
Find any topological order for the given graph.

You can assume that there is at least one topological order in the graph.

Clarification
Learn more about representation of graphs

Example
For graph as follow:

picture
   1
        4
0  2
        5
   3

The topological order can be:

[0, 1, 2, 3, 4, 5]
[0, 2, 3, 1, 5, 4]
...
'''
'''
Thinking:

The question is, we need find  ANY path by level, the level here is the 
nodes has no income pointer, means no node point to him.
So we can:
1 Find the first one which no one use him as neighbor, and get all his neighnors
for each neighbor, since he is traversaled, remove the reference counter by 1,
if it's 0, add to queue
'''

# Definition for a Directed graph node
# class DirectedGraphNode:
#     def __init__(self, x):
#         self.label = x
#         self.neighbors = []

class Solution:
    """
    @param graph: A list of Directed graph node
    @return: A list of integer
    """
    def topSort(self, graph):
        # write your code here
        
        
        if not graph:
            return []
        
        q = []
        dic = {}
        result = []
        
        #put every node's neighor to dic,  the head is not anyone's neighbor, so will not in dic now
        for node in graph:
            for neighbor in node.neighbors:
                if neighbor in dic:
                    dic[neighbor] += 1
                else:
                    dic[neighbor] = 1
                    
        # Find the head, start from it, push to queue, and pop queue, get result,using bfs
        # the head is not in dic now
        for node in graph:
            if node not in dic:
                q.append(node)
                
        while q:
            current = q.pop(0)
            result.append(current)
            for neighbor in current.neighbors:
                #count -1
                dic[neighbor] -= 1
                if dic[neighbor] == 0:
                    q.append(neighbor)
                    
        return result
        
                
                
                    
        
        