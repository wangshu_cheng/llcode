'''

Given a list of airline tickets represented by pairs of departure and arrival airports [from, to], reconstruct the itinerary in order. All of the tickets belong to a man who departs from JFK. Thus, the itinerary must begin with JFK.

Note:

If there are multiple valid itineraries, you should return the itinerary that has the smallest lexical order when read as a single string. For example, the itinerary ["JFK", "LGA"] has a smaller lexical order than ["JFK", "LGB"].
All airports are represented by three capital letters (IATA code).
You may assume all tickets form at least one valid itinerary.
Example 1:

Input: tickets = [["MUC", "LHR"], ["JFK", "MUC"], ["SFO", "SJC"], ["LHR", "SFO"]]
Output: ["JFK", "MUC", "LHR", "SFO", "SJC"]
Example 2:

Input: tickets = [["JFK","SFO"],["JFK","ATL"],["SFO","ATL"],["ATL","JFK"],["ATL","SFO"]]
Output: ["JFK","ATL","JFK","SFO","ATL","SFO"]
Explanation: Another possible reconstruction is ["JFK","SFO","ATL","JFK","ATL","SFO"]. But it is 

Thiking since we need return smallest lexical air port, so need use a heap for each eadge.

'''
import heapq
class Solution(object):
    
    def findItinerary(self, tickets):
        """
        :type tickets: List[List[str]]
        :rtype: List[str]
        """
        def dfs(s):
            h = self.hash.get(s,None)
            while h:
                dfs(heapq.heappop(h))
            self.res.insert(0,s)
        
        
        self.hash = {}
        self.res = []
        for ticket in tickets:
            if ticket[0] not in self.hash:
                h = list()
                self.hash[ticket[0]] = h
            heapq.heappush(self.hash[ticket[0]],ticket[1])
        dfs("JFK")
        return self.res