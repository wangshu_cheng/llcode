def print2Smallest(arr):
 
    # There should be atleast two elements
    arr_size = len(arr)
    if arr_size < 2:
        print "Invalid Input"
        return
 
    first = second = sys.maxint
    for i in range(0, arr_size):
 
        # If current element is smaller than first then
        # update both first and second
        if arr[i] < first:
            second = first
            first = arr[i]
 
        # If arr[i] is in between first and second then
        # update second
        elif (arr[i] < second and arr[i] != first):
            second = arr[i];
 
    if (second == sys.maxint):
        print "No second smallest element"
    else:
        print 'The smallest element is',first,'and' \
              ' second smallest element is',second

'''hash-function'''


int hashfunc(String key) {
int sum = 0;
for (int i = 0; i < key.length(); i++) {
sum = sum * 33 + (int)(key.charAt(i));
sum = sum % HASH_TABLE_SIZE;
}
return sum;
}

valid anagram:
class Solution(object):
    def isAnagram(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: bool
        """
        dic = {}
        
        for c in s:
            if c not in dic:
                dic[c] = 1
            else:
                dic[c] += 1
                
        for c in t:
            if c not in dic or dic[c] <= 0:
                return False
            else:
                dic[c] -= 1
                
        for k in dic:
            if dic[k] != 0:
                return False
        return True
            

group anagram
class Solution(object):
    def groupAnagrams(self, strs):
        """
        :type strs: List[str]
        :rtype: List[List[str]]
        """
        hash = {}
        res = []
        for s in strs:
            s_sorted = "".join(sorted(list(s)))
            if s_sorted not in hash:
                hash[s_sorted] = [s]
            else:
                hash[s_sorted].append(s)
            
        for key in hash:
            res.append(hash[key])
        
        return res


 def lowestCommonAncestor(self, root, A, B):
        def helper(root,A,B):
            #Base情况，当root栽倒None 或 A 或 B时，返回,因为再往下，不可能是父亲了， 而是儿子了 haha:) 
            if root == None or root == A or root == B:
                return root
            #两边同时往下栽
            left = helper(root.left,A,B)
            right = helper(root.right,A,B)
            #如果左儿子是A 同时右儿子又是B（或者左儿子是B，右儿子是A），那么答案就是root
            if left and right:
                return root
            #如果左儿子是A或B，右儿子还没有值（另外一个元素在更下面），那么答案就是左儿子
            if left:
                return left
            #同理
            if right:
                return right

        return helper(root,A,B)

lowestCommonAncestor
    #below code works for BST only
    def lowestCommonAncestor_bst(self, root, A, B):
        # write your code here
        if not root:
            return
        while root:
            if root.val >= A.val and root.val <=B.val:
                break
            if root.val < A.val:
                root = root.right
            if root.val > B.val:
                root = root.left
        return root

maxsubarray
    def maxSubArray(self, nums):
        # write your code here
        
        if not nums:
            return
        
        n = len(nums)
        lmax = [0 for i in range(n)]
        lmax[0] = nums[0]
        
        res = lmax[0]
        
        for i in range(1,n):
            lmax[i] = max(lmax[i - 1] + nums[i], nums[i])
            res = max(res,lmax[i])
            
        return res

mergeTwoLists

def mergeTwoLists(self, l1, l2):
        # write your code here
        dumy = ListNode(0)
        head = dumy
        
        while l1 and l2:
            if l1.val < l2.val:
                head.next = l1
                l1 = l1.next
            else:
                head.next = l2
                l2 = l2.next
            head = head.next
        
        if l1:
            head.next = l1
        
        if l2:
            head.next = l2
            
        return dumy.next

valid BST
 def isValidBST(self, root):
        # write your code here
        
        def helper(root):
            
            if not root:
                #[isvalid,max value of this subtree, min value of this tree]
                return [True,-sys.maxint,sys.maxint]
            
            left = helper(root.left)
            right = helper(root.right)
                
            isValid = left[0] and right[0]
            if (root.val <= left[1] or root.val >=right[2]):
                isValid = False
            
            return [isValid,max(right[1],root.val),min(left[2],root.val)]
        return helper(root)[0]
            
reorder list:

 def reorderList(self, head):
        # write your code here
        def findMid(head):
            slow,fast = head,head
            while fast and fast.next:
                fast = fast.next.next
                slow = slow.next
            return slow
            
        def reverse(head):
            newHead = None
            
            while head:
                tmp = head.next
                head.next = newHead
                newHead = head
                head = tmp
                
            return newHead
        
        #use odd even idx will be simple
        def merge(l1,l2):
            dumy = ListNode(0)
            idx = 0
            while l1 and l2:
                if idx % 2 == 0:
                    dumy.next = l1
                    l1 = l1.next
                else:
                    dumy.next = l2
                    l2 = l2.next
                dumy = dumy.next
                idx += 1
            
            if l1:
                dumy.next = l1
            else:
                dumy.next = l2
        
        if not head or not head.next:
            return head
        mid = findMid(head)
        tail = reverse(mid.next)
        mid.next = None
        merge(head,tail)

decode ways:
class Solution:
    # @param s, a string
    # @return an integer
    def numDecodings(self, s):
        if s=="" or s[0]=='0': return 0
        dp=[1,1]
        for i in range(2,len(s)+1):
            if 10 <=int(s[i-2:i]) <=26 and s[i-1]!='0':
                dp.append(dp[i-1]+dp[i-2])
            elif int(s[i-2:i])==10 or int(s[i-2:i])==20:
                dp.append(dp[i-2])
            elif s[i-1]!='0':
                dp.append(dp[i-1])
            else:
                return 0
        return dp[len(s)]

num of island
def numIslands(self, grid):
        # Write your code here

                        
        def removeIsland(grid,x,y,m,n):
            grid[x][y] = 0
            dr = [(0,1),(0,-1),(-1,0),(1,0)]
            for i in range(4):
                newX = x + dr[i][0]
                newY = y + dr[i][1]
                if 0 <= newX < m and 0 <= newY < n:
                    if grid[newX][newY] == 1:
                        removeIsland(grid,newX,newY,m,n)
    
        # def removeIsland(grid,x,y,m,n):
        #     q = [(x,y)]
        #     dr = [(1,0),(-1,0),(0,1),(0,-1)]
        #     while q:
        #         x1,y1 = q.pop(0)
        #         grid[x1][y1] = 0
        #         for i in range(4):
        #             newX = x1 + dr[i][0]
        #             newY = y1 + dr[i][1]
        #             if 0 <= newX < m and 0 <= newY < n and grid[newX][newY] == 1: 
        #                 q.append((newX,newY))
                
        
        if not grid:
            return 0
            
        m,n = len(grid),len(grid[0])
        
        if m + n == 0:
            return
        
        count = 0
        
        for i in range(m):
            for j in range(n):
                if grid[i][j] == 1:
                    removeIsland(grid,i,j,m,n)
                    count += 1
                    
        return count
