'''
Given a set of candidate numbers (C) and a target number (T), find all unique combinations in C where the 
candidate numbers sums to T.

The same repeated number may be chosen from C unlimited number of times.

For example, given candidate set 2,3,6,7 and target 7, 
A solution set is: 
[7] 
[2, 2, 3] 

All positives.

'''
class Solution:
    # @param candidates, a list of integers
    # @param target, integer
    # @return a list of lists of integers
    def combinationSum(self, candidates, target):
        # write your code here
        
        def helper(candidates,path,target,pos):
            if  target == 0:
                res.append(list(path))
                return
            for i in range(pos,len(candidates)):
                if candidates[i] > target:
                    break
                
                path.append(candidates[i])
                helper(candidates,path,target - candidates[i],i) # use i means can repeat number
                path.pop()
            
        if not candidates:
            return []
                
        res = []
        path = []
        
        helper(sorted(candidates),path,target,0)
        return res
            
                
            
            