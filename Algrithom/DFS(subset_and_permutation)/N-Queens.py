'''
The n-queens puzzle is the problem of placing n queens on an n×n chessboard such that no two queens attack each other.

Given an integer n, return all distinct solutions to the n-queens puzzle.

Each solution contains a distinct board configuration of the n-queens' placement, where 'Q' and '.' both indicate a queen and an empty space respectively.
'''
class Solution:
    """
    Get all distinct N-Queen solutions
    @param n: The number of queens
    @return: All distinct solutions
    """
    def solveNQueens(self, n):
        #write your code here
        #this is like permutation,the sequence matters, it can go back, so no need position.
        
        # The defination of each rows (the temp result is a single array, the index is the row number, and the value
        # is the column index, like if rows = [3,2,4] , means the row0 the queen is at col 3, row 1 queen at col 2 ,etc)
        
        #first thing write basic search(dfs). same as permutation
        def search(rows):
            if len(rows) == n:
                result.append(convert(rows))
            
            for i in xrange(n):
                #add new row (the position of Q in next row) check valid
                if not isValid(rows,i):
                    continue
                rows.append(i)
                search(rows) 
                rows.pop()
        
        #valid function
        def isValid(rows,row):
            for i in range(len(rows)):
                #check if Q in new row has conflict with existing Queens in same column:
                if rows[i] == row:
                    return False
                #check diagons:
                # if it's in diaangle tan (45) or tan(-45)
                # than the row index difference must be same as the col differece(45 degree triangle)
                #len(rows) is the new row index when we added a new row
                if abs( len(rows) - i ) == abs( row - rows[i] ):
                    return False
            return True
                
        
        #convert function:
        #convert the row single array to 2d array of Q and .
        def convert(rows):
            n = len(rows)
            result = []
            
            for j in range(n):
                tmp = ['.' for i in range(n)]
                for i in range(n):
                    if i == rows[j]:
                        tmp[i] = 'Q'
                result.append(''.join(tmp))
            return result
        
        #main function:
        if n<= 0:
            return []

        result = []
        rows = []
        search(rows)
        return result
        

    
    
                


