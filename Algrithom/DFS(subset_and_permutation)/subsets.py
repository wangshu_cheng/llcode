'''
Question:
Given a set of distinct integers, return all possible subsets.
Elements in a subset must be in non-descending order.
The solution set must not contain duplicate subsets.
Example
If S = [1,2,3], a solution is:
Time complexity: Cn1+Cn2+Cn3+....+Cnn = 2**n - 1

[
  [3],
  [1],
  [2],
  [1,2,3],
  [1,3],
  [2,3],
  [1,2],
  []
]

 #这个算法复杂度应该是2^n, 而不是n!，（2^n 比n！小）（是2^n见下面一亩三分地分析）, 因为每一步下去分叉都会小一个（不能往回走） 结果是 2**n , why??
一亩三分地：
http://www.1point3acres.com/bbs/forum.php?mod=viewthread&tid=117602&page=1#pid1703514
T(n) = nT(n-1) + kn + c; 
如果按这个公式来的话，确实是O(n!)没错。

但是事实上，注意循环中的第二句
recursion(i+1,end,comb,result,k-1);
中的第一个参数是i+1, 而不是i。也就是说，这f(n)中的n次递归调用并不都是f(n-1)，而是f(n-1), f(n-2), f(n-3), ... f(1), f(0). 
你可能直觉上觉得这两者差不多，比如n + n + n + ... + n (n 个 n) 是 O(n^2) 级别的，而 1 + 2 + 3 + .. + n 也是O(n^2)级别的。
但是这里不同，这里是递归调用。第一层递归中省下来的哪怕一次循环在后续的递归调用中都会被以至少指数等级放大。所以，就是这一点i和i+1的差别，
导致了O(n!)和O(2^n)的区别。

This is template code for all subset DFS problem.

口诀：插分插递弹 adarp

插入 path
分支
插入 element
递归 
弹出element

113_PathSum2

'''

'''
Clean version without comments
'''

def subsets(self, S):
    # write your code here
    def helper(S,path,pos):
        result.append(list(path))
        #如果是tree，这个循环就改为2个分支，left tree and right tree
        #pos + 1 相当于 树里面 把root.left,和 root.right 带入helper，这样才能往前走
        #同样道理，如果不加pos， 那么每次就又走回来了 

        #The number of recursive calls, T(n) satisfies the recurrence T(n) = T(n - 1) + T(n - 2) + ... + T(1) + T(0), which solves to T(n) = O(2^n). Since we spend O(n) time within a call, the time complexity is O(n2^n);

        for i in xrange(pos,len(S)):
            path.append(S[i])
            helper(S,path,i + 1) 
            path.pop()

    result = []
    path = []
    S = sorted(S)
    helper(S,path,0)
    return result

'''
Full version with comments:

'''


def subsets(self, S):
    # write your code here
    def helper(S,path,pos):

        '''
            Finish condition（结束符）
            like:
              1. pos = len(s)  # if must traversal whole array
              2. target = 0
              3. len(path) = len(nums) # path must be same length of orginal array,permute problem

              return

        '''

        '''
        Append temporary result to final result list
         Must append list(path) , not path!
        '''
        result.append(list(path))
        '''全范围搜索，如果是tree，则遍历所有子树，对于array来说，必须用position控制走向，防止死循环，
           树则没有这个问题，树没有环。113_pathSum2.py
        '''

        #关于pos：
        #如果是tree，这个循环就改为2个分支，left tree and right tree
        #pos + 1 相当于 树里面 把root.left,和 root.right 带入helper，这样才能往前走
        #同样道理，如果不加pos， 那么每次就又走回来了
        for i in xrange(pos,len(S)):
            '''
            Skip contition:（跳跃符）
            like:
              if S[i] in path:  #Prevent duplicated elements in path if need:（比如题目给的array带有重复元素）
                 continue / break

              if i > pos and S[i] == S[i-1]: # skip the duplcate element in orginal list at each path's head
                continue
            
              if S[i] > target:
                 break

              在path里去重
              if prev != -1（说明prev有值） and s[i] == prev: # 跳过重复的，和前面一样
                 continue

            '''


            '''
              Add data in path condition:
                1 选择你要的element组合加入path，不一定简单的加入元素
                 can be sth(like a subarray) related to the element, like:
                   path.append(S[pos:i])

            '''
            path.append(S[i])
            '''
               Result converge condition:（收敛）
               like:
                Position: - 很重要！
                   不能回头的情况加position，两种加法：
                    不能重复自己的（更不能回头）要pass i+1， 如： 1，2，3
                    可以重复自己（但不能回头） 要pass i， 如 1，1，2，2

                  if we can not travel back, use position to move fwd
                  In permutation case, we need travel back, so no need position
                Target:
                   each time we pass in a reduced target to converge


            '''
            helper(S,path,i + 1) #这里是i+1 不是pos + 1
            path.pop()

            '''
              Keep previous state if need:

              prev = S[i]
            '''
            
    result = []
    path = []
    
    '''
     Must sort , don't forget!!
    '''
    S = sorted(S)
    helper(S,path,0)
    return result


'''
Non-recursive:
 use bit , 2 for loop.
'''


