'''
Given a list of numbers, return all possible permutations.

Have you met this question in a real interview? Yes
Example
For nums = [1,2,3], the permutations are:

[
  [1,2,3],
  [1,3,2],
  [2,1,3],
  [2,3,1],
  [3,1,2],
  [3,2,1]
]
复杂度：  O(n*n!).

Thinking: 相当于搜索 N 高度， N-1 叉树（）， 这个算法复杂度应该是(N-1) ** N,比combination大


'''

class Solution:
    """
    @param nums: A list of Integers.
    @return: A list of permutations.
    """
    # recursive:
    def permute(self, nums):
        
        if not nums:
            return []

        def helper(result,nums,path):
            #permutation不是subset(combination)，它的答案一定是包括所有字符
            if len(path) == len(nums):
                result.append(list(path))
                return 

            for i in xrange(len(nums)):
                '''
                    Since I can go back, so I need to make sure there is no duplicate
                    In complicated question, we can write a function to check 
                    like isvalid (check N queen problem)
                '''
                '''
                答案里不能有重复的元素,也可以用hash ， 数组来实现判重复，不过空间大一点，不是in place
                e.g.
                if visited[i] == 1:
                        continue

                '''
                if nums[i] in path:
                    continue
  
                '''
                    no position means can go back,

                '''
                path.append(nums[i])
                helper(result,nums,path)
                path.pop()
                '''
                    如果前面用了 hash， 这里必须置回去
                    visited[i] = 0
                '''
                    
        result = []
        path = []
        helper(result,sorted(nums),path)
        return result
    
test = Solution()
print test.permute([1,2,3])