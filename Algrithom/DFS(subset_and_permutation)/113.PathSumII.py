'''
Question:
Given a binary tree, find all paths that sum of the nodes in the path equals to a given number target.

A valid path is from root node to any of the leaf nodes.

Thinking
注意，“find all path”，一定是用 subset 模板, append, recursive and pop.

Given a binary tree, and target = 5:

     1
    / \
   2   4
  / \
 2   3
return

[
  [1, 2, 2],
  [1, 4]
]


subset template: (插分插递弹)
/Users/jamescheng/Documents/llcode/subset_and_permutation(dfs)/subsets.py
'''
class Solution:
    # @param {TreeNode} root the root of binary tree
    # @param {int} target an integer
    # @return {int[][]} all valid paths
    def binaryTreePathSum(self, root, target):
        # Write your code here
        
        def dfs(node,path,target):
            if not node:
                return
            if node.left == None and node.right == None:#this is leaf node
                '''结束条件'''
                if target == node.val:
                    result.append(list(path)) #插
                return
            
            if node.left: #分
                path.append(node.left.val) #插                
                dfs(node.left,path,target - node.val) #递
                path.pop() #弹
                
            if node.right:
                path.append(node.right.val)
                dfs(node.right,path,target - node.val)
                path.pop()
            

        result = []
        
        if not root:
            return []
            
        dfs(root,[root.val],target)
        return result
            