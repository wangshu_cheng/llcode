'''
Given a list of numbers that may has duplicate numbers, return all possible subsets

 Notice

Each element in a subset must be in non-descending order.
The ordering between two subsets is free.
The solution set must not contain duplicate subsets.
Have you met this question in a real interview? Yes
Example
If S = [1,2,2], a solution is:

[
  [2],
  [1],
  [1,2,2],
  [2,2],
  [1,2],
  []
]

他一共忽略掉了：[2''],[1,2''] 这里2‘’表示第2个2 

'''


def subsets(self, S):
    # write your code here
    def helper(S,path,pos):

        ## can add finish condition here, like pos = len(s) - must finish traverls whole array, or what ever.

        result.append(list(path))
        for i in xrange(pos,len(S)):
            ## can add in logic to prevent duplicated elements in PATH (NOT skip leader number)  if need:
            ##   if S[i] in path:
            ##      continue

            # 如果以下条件满足，那么就是说明有重复元素，而且第一个还没有扫到
            # 就是 2''的情况（第二个2），如果第一个2 扫过了，那么i就应该等于pos (helper(S,path,i + 1))
            if i != pos and S[i] == S[i-1]:
                continue 
            path.append(S[i])
            helper(S,path,i + 1)
            path.pop()
            
            
    result = []
    path = []
    
    S = sorted(S)
    helper(S,path,0)
    return result