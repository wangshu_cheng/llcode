'''
Find all possible combinations of k numbers that add up to a number n, given that only numbers from 1 to 9 can be used and each combination should be a unique set of numbers.

Note:

All numbers will be positive integers.
The solution set must not contain duplicate combinations.
Example 1:

Input: k = 3, n = 7
Output: [[1,2,4]]
Example 2:

Input: k = 3, n = 9
Output: [[1,2,6], [1,3,5], [2,3,4]]
'''
class Solution(object):
    def combinationSum3(self, k, n):
        """
        :type k: int
        :type n: int
        :rtype: List[List[int]]
        """
        def helper(candidates,path,target,pos):
            if target == 0 and len(path) == k:
                result.append(list(path))
                return
            prev = -1
            for i in range(pos,len(candidates)):
                if candidates[i] > target or len(path) > k:
                    break
                if prev != -1 and candidates[i] == candidates[i-1]:
                    continue

                path.append(candidates[i])
                helper(candidates,path,target - candidates[i],i + 1)
                path.pop()
                prev = candidates[i]
                
        if k <= 0:
            return []
        
        result = []
        path = []
        
        candidates=[i for i in range(1,10)]
        
        helper(candidates,path,n,0)
        
        return result