'''
Given two integers n and k, return all possible combinations of k numbers out of 1 ... n.

Have you met this question in a real interview? Yes
Example
For example,
If n = 4 and k = 2, a solution is:
[[2,4],[3,4],[2,3],[1,2],[1,3],[1,4]]
'''
class Solution(object):
    def combine(self, n, k):
        """
        :type n: int
        :type k: int
        :rtype: List[List[int]]
        """
        def helper(candidates,path,pos):
            if len(path) == k:
                result.append(list(path))
                return

            for i in range(pos,len(candidates)):
                if len(path) > k:
                    break

                path.append(candidates[i])
                helper(candidates,path,i + 1)
                path.pop()
                
        if n <= 0:
            return []
        
        result = []
        path = []
        
        candidates=[i for i in range(1,n+1)]
        
        helper(candidates,path,0)
        
        return result