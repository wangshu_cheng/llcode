
'''

Given a string s, partition s such that every substring of the partition is a palindrome.

Return all possible palindrome partitioning of s.

Thinking, this is not usual substring case, 
in usual substring:
abcdef, 
substring must be conjancent, like:
a,b,c, bcd,def,  but not abe,cf. The time complexity is n**2

if need partition all the posibilities, then it's subset problem,
it's Cn1 + Cn2 + ....+Cnn , which is 2**n - 1


Idea, this is similar as subset, the difference is, 

in subset , the result is one number one element, but here, one subarray is one element,

e.g
 in subset, we get:
 [
 [1]
 [1,2,3]
  ..
 ]
Here we need:
[
 ['aaa','b'],
 ['aba','bb']
]

So what we need to push to each row list is a substring (subarray), instead of a element

'''


class Solution:
    # @param s, a string
    # @return a list of lists of string
    def partition(self, s):
        # write your code here
        
        def isPalind(s):
            start,end = 0, len(s) - 1
            while start < end:
                if s[start] != s[end]:
                    return False
                start += 1
                end -= 1
            return True
        
        def helper(s,sublist,pos):
            # we only need to push to a row list 
            # when the finish search the whole string.
            if pos == len(s):
                result.append(list(sublist))
                return

            for i in range(pos ,len(s)):
                #instead of directly push s[i] into sublist
                #we need to get substring, validate, then push
                prefix = s[pos:i + 1]
                if not isPalind(prefix):
                    continue
                sublist.append(prefix)
                helper(s,sublist,i + 1)
                sublist.pop()
                   
        if not s:
            return []
        
        result = []
        sublist = []
        
        helper(s,sublist,0)
        
        return result

# attachement, subset template:

def subsets(self, S):
    # write your code here
    def helper(S,path,pos):

        ## can add finish condition here, like pos = len(s) - must finish traverls whole array, or what ever.

        result.append(list(path))
        for i in xrange(pos,len(S)):
            ## can add in logic to prevent duplicated elements if need:
            ##   if S[i] in path:
            ##      continue

            path.append(S[i])
            helper(S,path,i + 1)
            path.pop()
            
            
    result = []
    path = []
    
    S = sorted(S)
    helper(S,path,0)
    return result