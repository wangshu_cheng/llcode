'''
Given n unique integers, number k (1<=k<=n) and target.

Find all possible k integers where their sum is target.
'''

class Solution:
    """
    @param A: An integer array.
    @param k: A positive integer (k <= length(A))
    @param target: Integer
    @return a list of lists of integer 
    """
    def kSumII(self, A, k, target):
        # write your code here
        
        def helper(A,k,myList,target,pos):
            #Stop condition:
            if target == 0 and k == 0:
                result.append(list(myList))
                return
             
            for i in range(pos,len(A)):
                # skip condition
                if A[i] > target:
                    break
                
                myList.append(A[i])
                #move,  reduce k and target to converge, must i + 1 because we can not duplicate
                #even ourself
                helper(A,k - 1,myList,target - A[i],i + 1)
                myList.pop()
                
        if not A:
            return []
        
        result = []
        myList = []
        A = sorted(A)
        helper(A,k,myList,target,0)
        return result
        