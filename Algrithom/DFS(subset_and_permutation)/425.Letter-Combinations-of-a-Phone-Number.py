'''
Given a digit string, return all possible letter combinations that the number could represent.

A mapping of digit to letters (just like on the telephone buttons) is given below.

hash = {}
hash[1] = []
hash[2] = ['a','b','c']
hash[3] = ['d','e','f']
hash[4] = ['g','h','i']
hash[5] = ['j','k','l']
hash[6] = ['m','n','o']
hash[7] = ['p','q','s']
hash[8] = ['t','u','v']
hash[9] = ['w','x','y','z']
hash[0] = []

Input:Digit string "23"
Output: ["ad", "ae", "af", "bd", "be", "bf", "cd", "ce", "cf"].

Thinking, this is Cn1 * Cn2* ... Cnn = N**N complexity

可以用基本排列组合公式，所不同的是，它是对于不同数组之间的，所以对于每一次递归，需要换一个数组。

'''


class Solution(object):
    def letterCombinations(self, digits):
        """
        :type digits: str
        :rtype: List[str]
        """
        
        dict = {'2':['a','b','c'],
                '3':['d','e','f'],
                '4':['g','h','i'],
                '5':['j','k','l'],
                '6':['m','n','o'],
                '7':['p','q','r','s'],
                '8':['t','u','v'],
                '9':['w','x','y','z']
                }
        if not digits:
            return []
        res = []
        def helper(digits,path):
            if len(path) == len(digits):
                res.append(path)
                return
            for letter in dict[digits[len(path)]]: #用len(path) 来换数组，或者直接指定一个变量，每次递归加1，也可以
                path += letter
                helper(digits,path)
                path = path[:-1]
                
        helper(digits,'')
        
        return res



#Version2  merge and combine
class solution(object):
    def letterCombinations(self, digits):
        chr = ["", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz"]
        res = []
        #遍历每个数字
        for i in range(0, len(digits)):
            num = int(digits[i])
            tmp = []
            for j in range(0, len(chr[num])):
                if len(res):
                    for k in range(0, len(res)):
                        tmp.append(res[k] + chr[num][j])
                else:
                    tmp.append(str(chr[num][j]))
            res = list(tmp)  #shallow or deep copy all ok
        return res

test = solution()
test.letterCombinations("234")

output:
tmp ['a', 'b', 'c']
tmp ['ad', 'bd', 'cd', 'ae', 'be', 'ce', 'af', 'bf', 'cf']
tmp ['adg', 'bdg', 'cdg', 'aeg', 'beg', 'ceg', 'afg', 'bfg', 'cfg', 'adh', 'bdh', 'cdh', 'aeh', 'beh', 'ceh', 'afh', 'bfh', 'cfh', 'adi', 'bdi', 'cdi', 'aei', 'bei', 'cei', 'afi', 'bfi', 'cfi']
