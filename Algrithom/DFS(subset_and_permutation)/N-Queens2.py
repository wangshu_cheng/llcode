'''
Follow up for N-Queens problem.

Now, instead outputting board configurations, return the total number of distinct solutions.
'''

class Solution:
    """
    Calculate the total number of distinct N-Queen solutions.
    @param n: The number of queens.
    @return: The total number of distinct solutions.
    """
    def totalNQueens(self, n):
        # write your code here
        def search(rows):
            if len(rows) == n:
                result.append(rows)
            
            for i in xrange(n):
                #add new row (the position of Q in next row) check valid
                if not isValid(rows,i):
                    continue
                rows.append(i)
                search(rows) 
                rows.pop()
        
        #valid function
        def isValid(rows,row):
            for i in range(len(rows)):
                #check if Q in new row has conflict with existing Queens in same column:
                if rows[i] == row:
                    return False
                #check diagons:
                # if it's in diaangle tan (45) or tan(-45)
                # than the row index difference must be same as the col differece(45 degree triangle)
                #len(rows) is the new row index when we added a new row
                if abs( len(rows) - i ) == abs( row - rows[i] ):
                    return False
            return True
        
        #main function:
        if n<= 0:
            return 0
        result = []
        rows = []
        search(rows)
        return len(result)