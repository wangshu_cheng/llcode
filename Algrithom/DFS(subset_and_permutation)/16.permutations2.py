'''
Given a list of numbers with duplicate number in it. Find all unique permutations.

Have you met this question in a real interview? Yes
Example
For numbers [1,2,2] the unique permutations are:

[
  [1,2,2],
  [2,1,2],
  [2,2,1]
]
'''

class Solution:
    """
    @param nums: A list of integers.
    @return: A list of unique permutations.
    """
    def permuteUnique(self, nums):
        # write your code here
        if not nums:return []
        def helper(nums,myList,visited):
            if len(myList) == len(nums):
                result.append(list(myList))
                return 

            for i in xrange(len(nums)):
                
                '''
                第一个条件：
                这里不能再用  if nums[i] in myList:， 因为我们是允许重复值的，（但不允许重复的index）所以只能用index
                来判断有没有visit过。

                第二个条件表示虽然该元素没有遍历过，但是他之前一个相同的元素也没有遍历过！那么还轮不到他！（必须先遍历前面那个，
                比如 1，2'，2''， 如果这个时候是1,那么就应该跳过2''）

                '''

                if visited[i] == 1 or (i>0 and nums[i] == nums[i-1] and visited[i - 1] == 0) :
                    continue
                visited[i] = 1
                myList.append(nums[i])
                helper(nums,myList,visited)
                myList.pop()
                visited[i] = 0 #所有递归前改过的都得改回来，相当于状态还原
                    
        result = []
        myList = []
        visited = [0 for i in range(len(nums))]
        helper(sorted(nums),myList,visited)
        return result