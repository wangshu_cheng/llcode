'''
Given a string containing only digits, restore it by returning all possible valid IP address combinations.

Example:

Input: "25525511135"
Output: ["255.255.11.135", "255.255.111.35"]
'''
class Solution(object):
    def restoreIpAddresses(self, s):
        """
        :type s: str
        :rtype: List[str]
        """
        #subset get all subset of s, and use isValid function to valid each subset, 
        
        def isvalid(s):
            if s[0] == '0':
                return s == '0' # eliminate case like "00","01"
            digit = int(s)
            return digit >=0 and digit <=255
        
        def helper(result,path,s,pos):
            if len(path) == 4: # cha
                if pos != len(s):
                    return
                sb = ".".join(path)
                result.append(sb)
                return
            for i in range(pos,len(s)):  #feng
                if i>=pos + 3:
                    break
                tmp = s[pos:i+1]
                if isvalid(tmp):
                    path.append(tmp)  # cha
                    helper(result,path,s,i+1) # di
                    path.pop() #tan
        
        result = []
        path = []
        if len(s) <4 or len(s)>12:
            return result
        helper(result,path,s,0)
        return result
                
                
            
            