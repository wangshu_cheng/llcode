'''
DFS 模板，

暴力解法是N factorial (根据排列组合公式)，所以算法上只能递归。

口诀：插分插递弹 adarp

插入 path
分支
插入 element
递归 
弹出element

'''

总结 各种不同的DFS的区别，比如能否回头，是否能重复用元素，去重复等

基础算法，纯暴力，N**N 复杂度，(permutation的基础)

def basic(s):
   res = []
   def helper(s,path):
     # 无终止条件
     res.append(list(path))
     for i in range(len(s)):
      # 可以回头
        path.append(s[i])
        helper(s,path)
        path.pop()

这样的话，假如输入是 【1，2，3】，那么输出是不限定长度的任何排列（无限结果），1,11,111,1111,11111.... 1,2,...

假如限定长度终止条件,比如 path 长度等于某个长度，或pos == len(s)：
   if len(path) = len(s):
        res.append(list(path))
        return

   if pos == len(s):  表示pos已经走到底了，也就是甩掉中间的subset


那么就成了，111,1,2,1,1,2,3.... 也就限定长度的任何排列， 复杂度 N**N

假如再限定不能重复，
       if s[i] in path:
          continue
那么就成了permutation， 复杂度 N!  (每个循环去掉一个重复值，所以N**N 变成了N!)

假如再限定不能回头，加入position(也就是没有顺序)，就成了subset 或 combination。
subset 不限定长度，可以是任意长度，而combination 限定固定长度。
所以subset的复杂度是
  #some one says: n!
  some one says: Cn0 + Cn1 + ...Cnn = 2**n - 1 ?? // stack over flow
而combination是 Cnk = n!/(k!(n-k)!)



   




def subsets(self, S):
    # write your code here
    def helper(S,path,pos):

        '''
        Finish condition
        like:
          1. pos = len(s)  
          2. target = 0
          3. len(path) = len(nums) # path must be same length of orginal array,permute problem

          return

        Append temporary result to final result list
         Must append list(path) , not path!
        '''
        result.append(list(path))
        '''全范围搜索，如果是tree，则遍历所有子树，对于array来说，用position可以控制走向-只能往前走，（如combination，subset）
        ，防止无限调用（树则没有这个问题，树没有环），如果可以回头如permutation，那么不用position，通过其他条件结束循环，如len(path) == len(nums)
        '''
        for i in xrange(pos,len(S)):
            '''
            Skip contition:
            like:
              if S[i] in path:  #Prevent duplicated elements in path if need:（比如题目给的array带有重复元素）
                 continue / break

              跳过想邻重复的元素
              if i > pos and S[i] == S[i-1]: # 
                continue
            
              if S[i] > target:
                 break

            '''


            '''
              Add data in path condition:
                1 选择你要的element组合加入path，不一定简单的加入元素
                 can be sth(like a subarray) related to the element, like:
                   path.append(S[pos:i])

            '''
            path.append(S[i])
            '''
               Result converge condition:（收敛）
               like:
                Position: - 很重要！
                   不能回头的情况加position，两种加法：
                    不能重复自己的（更不能回头）要pass i+1， 如： 1，2，3
                    可以重复自己（但不能回头） 要pass i， 如 1，1，2，2

                  if we can not travel back, use position to move fwd
                  In permutation case, we need travel back, so no need position
                Target:
                   each time we pass in a reduced target to converge


            '''
            helper(S,path,i + 1) #这里是i+1 不是pos + 1, 如果题目说同一个字符可以用多次，那么这里用i 不是i+1，参见combination sum
            path.pop()

            '''
              Keep previous state if need:

              prev = S[i]
            '''
            
    result = []
    path = []
    
    '''
     Sort 是让它有序，作用是当判断重复的时候有用
    '''
    S = sorted(S)
    helper(S,path,0)
    return result
