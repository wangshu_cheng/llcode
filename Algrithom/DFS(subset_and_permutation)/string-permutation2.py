'''
Given a string, find all permutations of it without duplicates.

Have you met this question in a real interview? Yes
Example
Given "abb", return ["abb", "bab", "bba"].

Given "aabb", return ["aabb", "abab", "baba", "bbaa", "abba", "baab"].
'''
class Solution:
    # @param {string} str a string
    # @return {string[]} all permutations
    def stringPermutation2(self, str):
        # Write your code here
        if not str:return []

        dict = {}

        def convertIdxToString(arr):
            result = ""
            for i in arr:
                result += dict[i]
            return result


        def helper(result,str,myList):
            if len(myList) == len(str):
                myString = convertIdxToString(myList)
                print "myString",myString
                if myString not in result:
                    result.append(myString)
                return 
            for i in xrange(len(str)):
                '''
                    Since I can go back, so I need to make sure there is no duplicate
                    In complicated question, we can write a function to check 
                    like isvalid (check N queen problem)
                    for this problem, since this is duplicated letter, so we need add index
                    to diff them.
                '''

                if i in myList:
                    continue
                '''
                    no position means can go back,

                '''
                myList.append(i)
                helper(result,str,myList)
                myList.pop()

        def moveToDict(str):
            for i in range(len(str)):
                dict[i] = str[i]

        moveToDict(str)


                    
        result = []
        myList = []
        helper(result,str,myList)
        return result
test = Solution()

print test.stringPermutation2("abb")
