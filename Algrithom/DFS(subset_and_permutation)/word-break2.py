'''
Given a string s and a dictionary of words dict, add spaces 
in s to construct a sentence where each word is a valid dictionary 
word. Return all such possible sentences. For example, given s = "catsanddog", dict = ["cat", "cats", "and", "sand", "dog"].
 A solution is ["cats and dog", "cat sand dog"].

Thinking:
We need get all the "split" possiblilty for the given string.

like "catsanddog", we need to split to cat sand dog or cats and dog
if we use index as the cut point, so it's  [2,6,9] or [3,6,9]
Generator + filter (mapper + reducer)

Generator: generate all the possiblility cut, it's subset problem, get all the subset from [0,1,2,...9], the results is hidden inside.
Filter: use is word and is possible to filter; 
then we can get the final result

The key point is the filter.

how to check is possible?

Is possible means starting from this index, you can not make a perfect cut. to determine a perfect cut, we need check from end to start.
If the last word perfect cut, then the nex to last ,, than....


'''

class Solution(object):
    def wordBreak(self, s, wordDict):
        """
        :type s: str
        :type wordDict: Set[str]
        :rtype: List[str]
        """
        def search(s,path,result,isPossible,pos):
            if not isPossible[pos]:
                return
            if pos == len(s):
                pre = 0
                solution = ""
                for i in range(len(path)):
                    solution += s[pre:path[i]+1] + " "
                    pre = path[i]+1
                solution = solution.strip()
                result.append(solution)
                return
            for i in range(pos,len(s)):
                if s[pos:i+1] not in wordDict:
                    continue
                path.append(i)
                search(s,path,result,isPossible,i+1)
                path.pop()
                
        isPossible = [False for i in range(len(s) + 1)]
        isPossible[len(s)] = True
        for i in range(len(s)-1,-1,-1):
                #here we can use max word len to optimize,see below code.
                #so we only need search range(i,i+maxLen)
            for j in range(i,len(s)):
                if s[i:j+1] in wordDict and isPossible[j+1]:
                    isPossible[i] = True
                    break
        res = []
        search(s,[],res,isPossible,0)
        return res


#########################
optimize code for is possible check
        maxLen = max([len(w) for w in dict])
        

        isPossible = [False for i in range(len(s) + 1)]
        isPossible[len(s)] = True
        for i in range(len(s)-1,-1,-1):
            for j in range(i,i+maxLen):
                if s[i:j+1] in dict and isPossible[j+1]:
                    isPossible[i] = True
                    break
                
        return isPossible[0]