'''
Given n pairs of parentheses, write a function to generate all combinations of well-formed parentheses.

For example, given n = 3, a solution set is:

[
  "((()))",
  "(()())",
  "(())()",
  "()(())",
  "()()()"
]

'''
class Solution(object):
    def generateParenthesis(self, n):
        """
        :type n: int
        :rtype: List[str]
        """
        s = ""
        def helper(left,right,s,result):
            #remove the invalid one, like "(()))("
            if left > right:
                return
            if left == 0 and right == 0:
                result.append(s)
                return
            
            if left > 0:
                helper(left-1,right,s+"(",result)
            if right > 0:
                helper(left,right-1,s+")",result)
        
        result = []
        helper(n,n,s,result)
        return result
            

                    
                
       