'''
Given a collection of candidate numbers (C) and a target number (T),
 find all unique combinations in C where the candidate numbers sums to T.

Each number in C may only be used once in the combination.
'''


class Solution:    
    """
    @param candidates: Given the candidate numbers
    @param target: Given the target number
    @return: All the combinations that sum to target
    """
    def combinationSum2(self, candidates, target): 
        # write your code here
        def helper(candidates,path,target,pos):
            if target == 0:
                result.append(list(path))
                return

            for i in range(pos,len(candidates)):
                if candidates[i] > target:
                    break
                # skip duplicated and adjancent item
                if i > pos and candidates[i] == candidates[i-1]:
                    continue

                path.append(candidates[i])
                helper(candidates,path,target - candidates[i],i+1)
                path.pop()
                
        if not candidates:
            return []
        
        result = []
        path = []
        
        candidates=sorted(candidates)
        
        helper(candidates,path,target,0)
        
        return result