'''
Given a 2D board and a word, find if the word exists in the grid.

The word can be constructed from letters of sequentially adjacent cell, where "adjacent" 
cells are those horizontally or vertically neighboring. The same letter cell may not be
 used more than once.

Have you met this question in a real interview? Yes
Example
Given board =

[
  "ABCE",
  "SFCS",
  "ADEE"
]
word = "ABCCED", -> returns true,
word = "SEE", -> returns true,
word = "ABCB", -> returns false.

'''

class Solution:
    # @param board, a list of lists of 1 length string
    # @param word, a string
    # @return a boolean
    def exist(self, board, word):
        # write your code here
        if not board or not board[0] or not word:
            return False
        m,n = len(board),len(board[0])

        def search(board,m,n,i,j,word,pos):
            if pos == len(word):
                return True
                
            if i >= m or i < 0 or j >= n or j < 0 or board[i][j] != word[pos]:
               return False
            
            dr = [[-1,0],[1,0],[0,1],[0,-1]]
            rst = False
            board[i][j] = '#'
            for k in range(4):
               rst = rst or search(board,m,n,i+dr[k][0],j+dr[k][1],word,pos+1)
            board[i][j] = word[pos]
            return rst
            
        res = False
        for i in range(m):
            for j in range(n):
                if board[i][j] == word[0]:# 这句有没有都一样；search里面还有判断
                    res = search(board,m,n,i,j,word,0)
                    if res == True:
                        return True
        return res

        