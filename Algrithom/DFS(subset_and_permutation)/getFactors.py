'''
Numbers can be regarded as product of its factors. For example,

8 = 2 x 2 x 2;
  = 2 x 4.
Write a function that takes an integer n and return all possible combinations of its factors.

Note:

You may assume that n is always positive.
Factors should be greater than 1 and less than n.
Example 1:

Input: 1
Output: []
Example 2:

Input: 37
Output:[]
Example 3:

Input: 12
Output:
[
  [2, 6],
  [2, 2, 3],
  [3, 4]
]
Example 4:

Input: 32
Output:
[
  [2, 16],
  [2, 2, 8],
  [2, 2, 2, 4],
  [2, 2, 2, 2, 2],
  [2, 4, 4],
  [4, 8]
]
'''
def getFactors(self, n):
    """
    :type n: int
    :rtype: List[List[int]]
    """
    
    '''
    Idea is, find the factor start from 2, we call it i, if i**2 >n, than it's too big,stop seaching;
    if i**2<=n, than i is candidate factor, can further search other factor by reassign n=n/i (if n%i=0)
    and new start factor as i.
    
    Can do it recursively or iteratively,
    For iterative, we need a stack to hold temp data(like BFS)
    
    '''

    def helper(path,n,pos):

        if n <= 1:
            if len(path) > 1:
                result.append(list(path))
            return
        for i in xrange(pos,n + 1):
            if n % i != 0:
                continue
            path.append(i)
            helper(path,n/i,i) 
            path.pop()


            
    result = []
    path = []

    
    n = 10
    helper(path,n,2)
    print result