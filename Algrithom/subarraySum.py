'''
Given an integer array, find a subarray where the sum of numbers is zero. 
Your code should return the index of the first number and the index of the last number.

Example
Given [-3, 1, 2, -3, 4], return [0, 2] or [1, 3].

Note
There is at least one subarray that it's sum equals to zero.


Thinking:
Pre sum question, the subarray with sum zero is 2 presum array whose difference is zero.
like: presum[0]  = 0
presum[3] = sum([-3,1,2]) = 0
so ans is (0,3-1) = (1,3)

presum[4] = -3
presum[1] = -3
so presum[4] - presum[1] = 0, so ans is (1 ,(4-1)) = (1,3)


store every seqence subarray sum (start from 0,end at i, inclusive) to dic, key is sum, 
value is index, if we find a bigger subarray(start from 0,end at j, inclusive), also 
has the same sum, then we know from i + 1 to j, the sum is 0.


'''
#Use presume, more general method:
def subarray(nums):
    #none check:
    if not nums:
        return []

    #init:
    dic = {}
    dic[0] = 0 # Don't forget!!
    presum = [0 for i in range(len(nums) + 1)]

    #main:
    #We only need to find 2 equal presum, to find equal, use dic:
    for i in range(1,len(nums) + 1):
        presum[i] = presum[i-1] + nums[i-1]
        
        if presum[i] in dic:
            return [dic[presum[i]],i - 1]
        dic[presum[i]] = i

    return []



#version 2 using a sum varible in stead of presum array 
#to save some space 
def subarraySum_v2( nums):
    # write your code here
    # get sum of each index, than put to dic 

    #None check
    if not nums:
        return []

    #init
    dic = {}
    dic[0] = -1
    sum = 0

    #main
    for i in range(len(nums)):
        sum += nums[i]
        if sum in dic:
            return [dic[sum]+1,i]
        dic[sum] = i

    #return
    return []
A = [-3, 1, 2, -3, 4]

print subarray(A)

