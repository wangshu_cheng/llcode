'''
Given a non negative integer number num. For every numbers i in the range 0 ≤ i ≤ num calculate the number of 1's in their binary representation and return them as an array.

Example:
For num = 5 you should return [0,1,1,2,1,2].

Follow up:

It is very easy to come up with a solution with run time O(n*sizeof(integer)). But can you do it in linear time O(n) /possibly in a single pass?
Space complexity should be O(n).
'''

#Simple solution:
class Solution(object):
    def countBits(self, num):
        """
        :type num: int
        :rtype: List[int]
        """
        
        def count_one(num):
            cnt = 0
            while num:
                if num & 1 == 1:
                    cnt += 1
                num = num >> 1
                
            return cnt
        
        data = [i for i in range(num+1)]
        res = []
        for element in data:
            res.append(count_one(element))
        return res

# advance:
2. Improved Solution

# For number 2(10), 4(100), 8(1000), 16(10000), ..., the number of 1's is 1.
# Any other number can be converted to be 2^m + x. For example, 9=8+1, 10=8+2.
# The number of 1's for any other number is 1 + # of 1's in x.


public int[] countBits(int num) {
    int[] result = new int[num+1];
 
    int p = 1; //p tracks the index for number x
    int pow = 1;
    for(int i=1; i<=num; i++){
        if(i==pow){
            result[i] = 1;
            pow <<= 1;
            p = 1;
        }else{
            result[i] = result[p]+1;
            p++;
        }
 
    }
 
    return result;
}