'''

Given a string array words, find the maximum value of length(word[i]) * length(word[j]) where the two words do not share common letters. You may assume that each word will contain only lower case letters. If no such two words exist, return 0.

Example 1:

Input: ["abcw","baz","foo","bar","xtfn","abcdef"]
Output: 16 
Explanation: The two words can be "abcw", "xtfn".
Example 2:

Input: ["a","ab","abc","d","cd","bcd","abcd"]
Output: 4 
Explanation: The two words can be "ab", "cd".
Example 3:

Input: ["a","aa","aaa","aaaa"]
Output: 0 
Explanation: No such pair of words.

'''

class Solution(object):
    def maxProduct(self, words):
        """
        :type words: List[str]
        :rtype: int
        """
        
        if not words:
            return 0
        
        arr = [0 for i in range(len(words))]
        for i in range(len(words)):
            for j in range(len(words[i])):
                c = ord(words[i][j])
                # What does this mean? It means let 1 left shift number of bits, the number is the difference between current letter and letter a.  So when they | together, each letter stand on a signle bit. Eg.
                # bc,  'b' - 'a' = 1, so 1 << 1 is 10; and c is 100, d is 1000, e is 10000, f is 100000.
                # so if word1 and word2 has no common letter, then word1 & word2 must = 0 !
                arr[i] |= (1 << (c - ord('a')))
        res = 0
        
        for i in range(len(words)):
            for j in range(i+1,len(words)):
                if arr[i] & arr[j] == 0:
                    res = max(res,len(words[i]) * len(words[j])  )
                    
        return res