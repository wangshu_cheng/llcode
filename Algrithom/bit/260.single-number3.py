'''
Given an array of numbers nums, in which exactly two elements appear only once and all the other elements appear exactly twice. Find the two elements that appear only once.

Example:

Input:  [1,2,1,3,2,5]
Output: [3,5]
Note:

The order of the result is not important. So in the above example, [5, 3] is also correct.
Your algorithm should run in linear runtime complexity. Could you implement it using only constant space complexity?

'''


class Solution(object):
    def singleNumber(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        
        '''
        Back ground: 
        1 What is negative number in binary?
        like 5: 
        5: 000...,0110
        -5: first reverse 5, so 1...., 1001, than add 1, so it's 111..,1010
        so number&-number equals the last "1" bit, you can try.
        like 3&-3==1, 2&-2==2
        
        Idea:
        First round, XOR from head to toe, get the XOR result of the 2 single number;
        2nd round, use number&-number equals the last "1" bit,get the last 1 bit,
        That means in this bit, one number is 1, the other number is 0; so can make 2 groups of the array,
        one is have this bit =1 , the other =0.in each group, it's all dual value + 1 single value, than xor to get 
        each single number of each group.
        
        '''
        #phase1: get the xor result of the 2 single numbers:
        p1=0
        for i in nums:
            p1^=i
        #get the last bit:
        p1&=-p1
        
        res=[0,0]
        #phase2
        for i in nums:
            if i&p1==0:
                res[0]^=i
            else:
                res[1]^=i
        return res
        
        
        
        