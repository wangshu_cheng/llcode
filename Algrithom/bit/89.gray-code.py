'''
The gray code is a binary numeral system where two successive values differ in only one bit.

Given a non-negative integer n representing the total number of bits in the code, print the sequence of gray code. A gray code sequence must begin with 0.

Example 1:

Input: 2
Output: [0,1,3,2]
Explanation:
00 - 0
01 - 1
11 - 3
10 - 2

For a given n, a gray code sequence may not be uniquely defined.
For example, [0,2,3,1] is also a valid gray code sequence.

00 - 0
10 - 2
11 - 3
01 - 1
Example 2:

Input: 0
Output: [0]
Explanation: We define the gray code sequence to begin with 0.
             A gray code sequence of n has size = 2n, which for n = 0 the size is 20 = 1.
             Therefore, for n = 0 the gray code sequence is [0].

'''
class Solution(object):
    def grayCode(self, n):
        """
        :type n: int
        :rtype: List[int]
        """
        if n == 0:
            return [0]
        
        res = self.grayCode(n-1)
        numToAdd = 1 << (n-1)
        
        for i in range(len(res) - 1,-1,-1):
            res.append(numToAdd + res[i])
        return res


class Solution(object):
    def grayCode(self, n):
        """
        :type n: int
        :rtype: List[int]
        """
        dic={}
        #value is the item, value is index
        start=0
        for i in xrange(2**n):#total n bit
            tmp=1
            while tmp<2**n:
                if start not in dic.values():
                    print start
                    dic[i]=start
                    break
                else:
                    #revert the bit change:
                    tmp2=tmp>>1
                    start^=tmp2
                start^=tmp
                tmp<<=1
            #last item:
            if start not in dic:
                dic[i]=start
    
        return dic.values()

        