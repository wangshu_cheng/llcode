'''
1 先create 个dump = ListNode(0), 然后dump.next = head, 没有坏处，最后返回dump.next
dumy 是不移动的，它是用来保留你的头指针。不能直接用head的原因是如果head改变了，你就没有head了
2 Link list don't accupy memory, so feel free to create more temparory head for you process.
3 Usually, we need a dumy and a cur (moving pointer) for a solution.
cur = dumy at beginning.

'''


'''
 reverse template:
就是一步一步，把所有的箭头都反过来指

从       A->B->C->null 到：
   null<-A<-B<-A

所以，每个node都要把指针反过来指向前面；
为了不lost next指针，next指针需要先存下来

 '''
 def reverse(head):
 	#set new head to none firstly
    newhead = None
    while head:
    	#save next pointer
        temp = head.next
        #re point next to new head
        head.next = newhead # for the head node, it will be the last so point to null
        #move new head forward
        newhead = head
        #move head forward
        head = temp
    return newhead

 #reverse double link:
     def reverse(head):
        Node temp = null;
        Node current = head;
 
        # /swap next and prev for all nodes of 
        #  doubly linked list 
        while current :
            temp = current.prev
            current.prev = current.next;
            current.next = temp;
            current = current.prev;
 
        # /* Before changing head, check for the cases like empty 
        #  list and list with only one node */
        if temp:
            head = temp.prev
        return head
  


'''
swap

'''
  def swapPairs(self, head):
        # Write your code here
        if not head or not head.next:
            return head
            
        dumy = ListNode(0)
        
        #Don't forget below line, other wise, dumy means nothing
        dumy.next = head
        
        head = dumy
        
        #the one only matters is head pointer, the other pointer is facilite u to do sth
        
        # to convenient, define n1 and n2, otherwise, need use a lot .next.next ...
        while head.next and head.next.next:
            n1,n2 = head.next,head.next.next
            
            head.next = n2
            n1.next = n2.next
            n2.next = n1
            head = n1
        
        return dumy.next


'''
Soft delete
'''
def deleteNode(self, node):
    # write your code here
    if not node or not node.next:
        return 
    
    #this is soft delete, just assign value of next node to this node, then delete next node
    next = node.next
    node.val = next.val
    node.next = next.next
    return

'''
找中值：
'''
def findMid(head):
    slow,fast = head,head.next
    while fast and fast.next:
        fast = fast.next.next
        slow = slow.next
    return slow

'''
merge 2 list one by one.
'''
def merge(h1,h2):
    dumy = ListNode(0)
    idx = 0
    while h1 and h2:
        if idx %2 == 0: # even 
            dumy.next = h1
            h1 = h1.next
        else:
            dumy.next = h2
            h2 = h2.next
        dumy = dumy.next
        idx += 1
        
    if h1:
        dumy.next = h1
    else:
        dumy.next = h2

'''
detect cycle
'''
def hasCycle(self, head):
    # write your code here
    if not head or not head.next:
        return False
    
    slow,fast = head,head.next


    while fast != slow:
        if not fast or not fast.next:
            #here means no cycle
            return False
        fast = fast.next.next
        slow = slow.next
    return True

'''
find intersection point

'''
def detectCycle(self, head):
    # write your code here
    if not head or not head.next:
        return 
    
    slow,fast = head,head.next

    #先跑到相遇（套圈），如果不套圈，那么没有环

    while fast != slow:#when meet, means has loop
        # reach end, still not meet,means no loop
        if not fast or not fast.next:
            #here means no cycle
            return None
        fast = fast.next.next
        slow = slow.next
    
    #can only remember
    #套圈后，head 和slow一起跑，相遇点就是 环开始点

    while head != slow.next:
        head = head.next
        slow = slow.next
    return head


#This is print  alternate nodes of the given Linked List, first from head to end, and then from end to head. If Linked List has even number of nodes, then skips the last node.
#if input is 1 2 3 4 5 6 , output is 1 3 5 5 3 1
void fun(struct node* start)
{
  if(start == NULL)
    return;
  printf("%d  ", start->data); 
  
  if(start->next != NULL )
    fun(start->next->next);
  printf("%d  ", start->data);
}

