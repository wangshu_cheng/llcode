'''
Binary search 通用模板

When?
● 你需要优化一个O(n)的暴力算法到更快的算法
● Sorted Array or Rotated Sorted Array

How?
● 找到满足某个条件的第一个或者是最后一个位置
'''


class Solution:
    # @param nums: The integer array
    # @param target: Target number to find
    # @return the first position of target in nums, position start from 0 
    def binarySearch(self, nums, target):
        if len(nums) == 0:
            return -1
            
        start, end = 0, len(nums) - 1
        while start + 1 < end:
            mid = (start + end) / 2
            '''
            甩除条件：
            一定是和 mid 有关的东西，不一定是以mid为index的array的值，也可以是任何与mid有关的表达式；
            '''
            if nums[mid] < target:
                start = mid
            # 大于和等于合为一类：
            else:
                end = mid
        #有时候先判断左边，有时候先判断右边，不确定。要看题意
        if nums[start] == target:
            return start
        if nums[end] == target:
            return end
        return -1