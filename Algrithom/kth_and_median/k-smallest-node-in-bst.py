''''
Thinking:

1 Inorder traversal is ascend traversal for bst.
So just get the k th value (use a counter)
'''
# Definition for a binary tree node.
# class TreeNode(object):
#     def __init__(self, x):
#         self.val = x
#         self.left = None
#         self.right = None

class Solution(object):
    def kthSmallest(self, root, k):
        """
        :type root: TreeNode
        :type k: int
        :rtype: int
        """
        # if you don't want use class varible, need use a referece type like array
        # you can not pass a primitive value varable as parameter, they will be overwrited again and again.
        res = [-2**31]
        count = [0]
        def helper(root):
            if not root:
                return
            helper(root.left)
            # q.append(root.val)
            count[0] += 1 
            if count[0] == k:
                res[0] = root.val
                return
            helper(root.right)
        helper(root)
        return res[0]