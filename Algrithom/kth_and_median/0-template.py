1 partition:

def partition(numbers,l,r):
    if l == r:
        return l
    '''
    if the question has given a pivot, 就像把比k小的放到比k大的左边之类的
    我们就不需要 把最后一个当成 pivot 了，下一行不需要
    '''
    pivot = numbers[r]
    index = l
    for i in range(l,r):
        if numbers[i] >= pivot:
            numbers[i],numbers[index] = numbers[index],numbers[i]
            index +=1
    '''
    同理，如果给定pivot，这一行不需要
    '''
    numbers[r],numbers[index] = numbers[index],numbers[r]
    return index
