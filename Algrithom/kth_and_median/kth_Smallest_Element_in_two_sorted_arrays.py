'''
Idea:

The easy solution is to use merge 2 array and get kth index, this need space of O(m+n) 
The better is 2 pointer, but time is O(k)

The best solution require O(lg(m) + lg(n)), we need use binary search

k here mean number of element, so index is k - 1

'''
def kthsmallest(A, B, k):
    if len(A) == 0:
        return B[k - 1]
    elif len(B) == 0:
        return A[k - 1]

    if k == 1:
    	return min(A[0],B[0])

    #this is a skill, to assign big value if currrent array is
    # less than k/2, in this case, we must drop the other array,
    # so can set the compare mid value to a big data.
    mida = A[k/2 - 1] if (k/2 -1) < len(A) else 2**31
    midb = B[k/2 - 1] if (k/2 -1) < len(B) else 2**31 # this is to say
    
    if mida < midb:
    	return kthsmallest(A[k/2 :],B,k - k/2)
    #if mida == midb, drop first k/2 A or B is same thing.
    else:
    	return kthsmallest(A,B[k/2:],k - k/2)


#test case:
A = [1,2,3,4,6,7,8]
B = [1,2,3,4,7,9,10]
k = 8

print kthsmallest(A,B,k)



