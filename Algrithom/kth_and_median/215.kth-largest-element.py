'''
Find K-th largest element in an array.

'''
class Solution:
    # @param k & A a integer and an array
    # @return ans a integer
    def kthLargestElement(self, k, numbers):
        
        def partition(numbers,l,r):
            if l == r:
                return l
        
            pivot = numbers[r]
            index = l
            for i in range(l,r):
                if numbers[i] >= pivot:
                    numbers[i],numbers[index] = numbers[index],numbers[i]
                    index += 1
            numbers[r],numbers[index] = numbers[index],numbers[r]
            return index
        
        
        def helper(numbers,l,r,k):
            if l == r:
                return numbers[l]
        
            pos = partition(numbers,l,r)
        
            if pos + 1 == k:
                return numbers[pos]
        
            elif pos + 1 <k:
                return helper(numbers, pos + 1, r,k)
            else:
                return helper(numbers,l,pos - 1, k)
        
        #main:
        if not numbers:
            return 0
        if k <= 0:
            return 0
    
        return helper(numbers,0,len(numbers) - 1, k)