'''
A city's skyline is the outer contour of the silhouette formed by all the buildings in that city when viewed from a distance.
 Now suppose you are given the locations and height of all the buildings as shown on a cityscape photo (Figure A),
  write a program to output the skyline

The geometric information of each building is represented by a triplet of integers [Li, Ri, Hi], where Li and Ri are
 the x coordinates of the left and right edge of the ith building, respectively, and Hi is its height. It is guaranteed 
 that 0 ≤ Li, Ri ≤ INT_MAX, 0 < Hi ≤ INT_MAX, and Ri - Li > 0. You may assume all buildings are perfect rectangles 
 grounded on an absolutely flat surface at height 0.

For instance, the dimensions of all buildings in Figure A are recorded as: [ [2 9 10], [3 7 15], [5 12 12], [15 20 10], [19 24 8] ] .

The output is a list of "key points" (red dots in Figure B) in the format of [ [x1,y1], [x2, y2], [x3, y3], ... ] 
that uniquely defines a skyline. A key point is the left endpoint of a horizontal line segment. Note that the last
 key point, where the rightmost building ends, is merely used to mark the termination of the skyline, and always has
  zero height. Also, the ground in between any two adjacent buildings should be considered part of the skyline contour.

For instance, the skyline in Figure B should be represented as:[ [2 10], [3 15], [7 12], [12 0], [15 10], [20 8], [24, 0] ].

This is a Python version of my modification of dong.wang.1694's brilliant C++ solution. It sweeps from left to right.
 But it doesn't only keep heights of "alive buildings" in the priority queue and it doesn't remove them as soon as 
 their building is left behind. Instead, (height, right) pairs are kept in the priority queue and they stay in there 
 as long as there's a larger height in there, not just until their building is left behind.

In each loop, we first check what has the smaller x-coordinate: adding the next building from the input, or removing 
the next building from the queue. In case of a tie, adding buildings wins, as that guarantees correctness (think about it :-).
 We then either add all input buildings starting at that x-coordinate or we remove all queued buildings ending at
  that x-coordinate or earlier (remember we keep buildings in the queue as long as they're "under the roof" of a
   larger actually alive building). And then, if the current maximum height in the queue differs from the last in 
   the skyline, we add it to the skyline
'''

#Method1: TTL because python has no heap.remove\


'''
Thinking:
The idea is:
1 to split all the building points to tow points, (start,height) and (end,height)
and process one by one. For start point, make the height negative so we can know current
is start or end point.
2 Use max heap to store current max height
3 Once the height changes, from low to high or from high to low, create a result data.

'''

import heapq
class Solution(object):
    def getSkyline(self, buildings):
        """
        :type buildings: List[List[int]]
        :rtype: List[List[int]]
        """
        res = []
        
        buildingPoints = []
        
        for b in buildings:
            buildingPoints.append([b[0],-b[2]])
            buildingPoints.append([b[1],b[2]])

        #first sort by x and then by height; (because when two building staring 
        # at same x, then we need sort them by height from high to low, means high one goes first
        # when starting point is same and low one goes first when ending point is same
        # because in starting point, height is negative, so eventually they are same,or asending order)
        buildingPoints.sort(key = lambda x : (x[0],x[1]))
        h = [] #max queue
        dic = {}
        
        heapq.heappush(h,0)
        preMax = 0
        
        for bp in buildingPoints:
            if bp[1] < 0:
                heapq.heappush(h,bp[1]) #push negative value, because we need max heap
                
            else:
                h.remove(-bp[1])
                heapq.heapify(h)
                
                #heapq.heappop(h)
            curHeight = -h[0]
            if curHeight != preMax: # height changes, consider make a data
                res.append([bp[0],curHeight])
                preMax = curHeight
        return res





#Method2: AC, but don't understand

from heapq import *

class Solution:
    def getSkyline(self, LRH):
        skyline = []
        i, n = 0, len(LRH)
        liveHR = []
        while i < n or liveHR:
            if not liveHR or i < n and LRH[i][0] <= -liveHR[0][1]:
                x = LRH[i][0]
                while i < n and LRH[i][0] == x:
                    heappush(liveHR, (-LRH[i][2], -LRH[i][1]))
                    i += 1
            else:
                x = -liveHR[0][1]
                while liveHR and -liveHR[0][1] <= x:
                    heappop(liveHR)
            height = len(liveHR) and -liveHR[0][0]
            if not skyline or height != skyline[-1][1]:
                skyline += [x, height],
        return skyline