'''
Given an interval list which are flying and landing time of the flight. 
How many airplanes are on the sky at most?

Notice
If landing and flying happens at the same time, we consider landing should happen at first.

Example
For interval list

[
  [1,10],
  [2,3],
  [5,8],
  [4,7]
]
Return 3
'''

"""
Definition of Interval.
class Interval(object):
    def __init__(self, start, end):
        self.start = start
        self.end = end
"""

from operator import itemgetter
class Solution:
    # @param airplanes, a list of Interval
    # @return an integer
    def countOfAirplanes(self, airplanes):
        # write your code here
        
        if not airplanes:
            return 0
            
        flightList = []
        
        for i in airplanes:
            flightList.append((i.start,"start"))
            flightList.append((i.end,"end"))
        
        #sort flightList by time then by 'start' or 'end':
        # just like SQL group by, that means if there is 2 item
        #with same time, then 'start' is first.
        #e.g after sorted, [(1,'start'),(2,'start'),(3,'end'),(4,'start'),(4,'end')]

        flightList = sorted(flightList,key = itemgetter(0,1))
        # print flightList
        
        count = 0
        max_airlines = 0
        
        for i in flightList:
            if i[1] == "start":
                count += 1
                
            elif i[1] == "end":
                count -= 1
            else:
                return "Wrong flag"
            max_airlines = max(max_airlines,count)
        return max_airlines
            
                