'''
扫描线的关键：
扫描线是解决起始结束时间混在一起的题目。

基本解法是：
第一步：
把起始时间和结束时间拆开来（同时加入起始和终止标志，通常是用tuple 标示，怎加一个信息量），作为同一个时间轴，然后排序，排序的方法
是按照 时间大小和类型排序； 也就是说先按时间排，时间相同情况再按标志排序。
第二步：
loop sorted tuple list， 如果标志为start ， count+1，否则-1， 求各个时刻count 最大值
'''

