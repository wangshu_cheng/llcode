def checkPowerOf2(self, n):
    # write your code here
    #method1: O(lg(31)) ~= O(N)
    #start from 1, continues left shift(times 2) if equals N, than N is power of 2
    res = 1
    for i in xrange(31):
        if res == n:
            return True
        res = res << 1
    return False
    
    #method2: O(1)
    # if n <= 0:
    #     return False
    
    # return (n & (n-1)) == 0