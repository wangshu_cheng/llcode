'''
Find any position of a target number in a sorted array. Return -1 if target does not exist.

Have you met this question in a real interview? Yes
Example
Given [1, 2, 2, 4, 5, 5].

For target = 2, return 1 or 2.

For target = 5, return 4 or 5.

For target = 6, return -1.


Thinking:
This is basic question.

'''


def findPosition(self, A, target):
    # Write your code here
    if not A : return -1
    
    LEN = len(A)
    start,end = 0,LEN-1
    while start + 1 < end:
        mid = start + (end - start) / 2
        if A[mid] < target:#target at left side
            start = mid
        else:
            end = mid
    if A[start] == target:
        return start
    if A[end] == target:
        return edn
    return -1                