'''
Questioin:
Suppose a sorted array is rotated at some pivot unknown to you beforehand.

(i.e., 0 1 2 4 5 6 7 might become 4 5 6 7 0 1 2).

Find the minimum element.

The array may contain duplicates.

Example
Given [4,4,5,6,7,0,1,2] return 0

Thinking:
This question is same as the previous one, the difference is, it contain duplicates.
When duplicate happens, then mid can probably == start,

'''

class Solution:
    """
    @param A : an integer ratated sorted array and duplicates are allowed
    @param target : an integer to be searched
    @return : a boolean
    """
    def search(self, A, target):
        # write your code here
        if not A:
          return False
        
        start,end = 0,len(A) - 1  
        
        while start + 1 < end:
            mid = start + (end - start) / 2
            if A[mid] == target:
                return True
            if  A[mid] > A[start]:#mid is in left half
                if A[start] <= target and target <= A[mid]:
                    end = mid
                else:
                    start = mid
            elif A[mid] < A[start]: # mid is in right half, start and mid won't be same since we use while start +1 <end
                if A[mid] <= target and target <= A[end]:
                    start = mid
                else:
                    end = mid
            #This line is to handle duplicate case, if no duplicate, combine with the 2nd else.
            #when  A[mid] == A[start], we don't know which part is in, so we move start to right by 1
            else:
                start += 1
        
        if A[start] == target:
            return True
        if A[end] == target:
            return True
        return False