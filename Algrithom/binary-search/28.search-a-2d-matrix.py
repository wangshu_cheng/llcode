'''
Question:
Write an efficient algorithm that searches for a value in an m x n matrix.
This matrix has the following properties:
Integers in each row are sorted from left to right.
The first integer of each row is greater than the last integer of the previous row.
Example
Consider the following matrix:

[
    [1, 3, 5, 7],
    [10, 11, 16, 20],
    [23, 30, 34, 50]
]
Given target = 3, return true.

Challenge
O(log(n) + log(m)) time

Thinking:
1 二维转一维模板：
首先把他想象成一维数组，我们使用全局的 index 0， m*n-1
但是全局index 无法从matrix里面寻址，必须转成局部index：
 公式：

  n = number of cols

  local_index_row = global_index / n
  local_index_col = global_index % n


Solution:

'''

class Solution:
    """
    @param matrix, a list of lists of integers
    @param target, an integer
    @return a boolean, indicate whether matrix contains target
    """
    def searchMatrix(self, matrix, target):
        # write your code here
        if not matrix:
            return False
        
        m,n = len(matrix),len(matrix[0])
        
        #now use binary search to get the global index  of target:
        start,end = 0, m*n - 1
        
        while start + 1 < end:
            mid = start + (end - start) / 2
            number = matrix[mid / n][mid % n]
            if number == target:
                return True
            elif number < target:
                start = mid
            else:
                end = mid
                
        if matrix[start / n][start % n] == target:
        	return True
        if matrix[end / n][end % n] == target:
        	return True
        return False