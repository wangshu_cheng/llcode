'''
Question:
There is an integer array which has the following features:

1 The numbers in adjacent positions are different.
2 A[0] < A[1] && A[A.length - 2] > A[A.length - 1].
We define a position P is a peek if:

A[P] > A[P-1] && A[P] > A[P+1]

Find a peak element in this array. Return the index of the peak.


If the array may contains multiple peeks, find any of them.

Example
Given [1, 2, 1, 3, 4, 5, 7, 6]

Return index 1 (which is number 2) or 6 (which is number 7)

Challenge
Time complexity O(logN)


Thinking:

1 If ask for logN, then must be binary search.
2 在这里， target变成了浮动的， 
为了找到peak的位置，当目前值（mid） 小于后一个值时，那么peak在后面，所以 start = mid
当目前值（mid） 小于前一个值时，那么peak在前面，所以 end = mid
当mid既不比前面的大，也不必后面的小，那么把他设为start 或 end都可以


'''

class Solution:
    #@param A: An integers list.
    #@return: return any of peek positions.
    def findPeak(self, A):
        # write your code here
        if not A:
            return 0
            
        start,end = 1,len(A) - 2
        
        while start + 1 < end:
            mid = start + (end - start) / 2
            if A[mid] < A[mid - 1]:
                end = mid
            elif A[mid] < A[mid + 1]:
                start = mid
            else:
                start = mid #or end = mid
        
        if A[start] >= A[end]:
            return start
        else:
            return end
