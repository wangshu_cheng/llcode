'''
Question:
Given a target number, a non-negative integer k and an integer array A sorted in ascending order,
 find the k closest numbers to target in A, sorted in ascending order by the difference between the 
 number and target. Otherwise, sorted in ascending order by number if the difference is same.

Example
Given A = [1, 2, 3], target = 2 and k = 3, return [2, 1, 3].
Given A = [1, 4, 6, 8], target = 3 and k = 3, return [4, 1, 6].

Thinking:

# 1. Find the first index that A[index] >= target
# 2. Set two pointers left = index - 1 and right = index
# 3. Compare A[left] and A[right] to decide which pointer should move

'''

class Solution:
    # @param {int[]} A an integer array
    # @param {int} target an integer
    # @param {int} k a non-negative integer
    # @return {int[]} an integer array
    def kClosestNumbers(self, A, target, k):
        # Algorithm:
        # 1. Find the first index that A[index] >= target
        # 2. Set two pointers left = index - 1 and right = index
        # 3. Compare A[left] and A[right] to decide which pointer should move
        
        index = self.firstIndex(A, target)
        #设置以 first index为基准的2个点，外扩；
        left, right = index - 1, index
        result = []
        for i in range(k):
            #边界情况1：如果左边已经到底了那么没法扩了，只能扩右边
            if left < 0:
                result.append(A[right])
                right += 1
            #边界情况2：如果右边已经到底了那么没法扩了，只能扩左边
            elif right == len(A):
                result.append(A[left])
                left -= 1
            #普通情况，两侧都没到边界，则选择离target近的值：
            else:
                if target - A[left] <= A[right] - target: 
                    result.append(A[left])
                    left -= 1
                else:
                    result.append(A[right])
                    right += 1
                    
        return result
        
    '''
    This function is used to find the fist position which is JUST bigger than target
    '''
    def firstIndex(self, A, target):
        start, end = 0, len(A) - 1
        while start + 1 < end:
            mid = (start + end) / 2
            if A[mid] < target:
                start = mid
            else:
                end = mid
        
        if A[start] >= target:
            return start
            
        if A[end] >= target:
            return end
            
        return len(A)
            
        