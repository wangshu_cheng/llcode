'''
Question:
Suppose a sorted array is rotated at some pivot unknown to you beforehand.

(i.e., 0 1 2 4 5 6 7 might become 4 5 6 7 0 1 2).

You are given a target value to search. If found in the array return its index, otherwise return -1.

You may assume no duplicate exists in the array.

Example
For [4, 5, 1, 2, 3] and target=1, return 2.

For [4, 5, 1, 2, 3] and target=0, return -1.

Challenge
O(logN) time


Thinking:
二分法的精髓在于根据某些特性，来判断target在哪一部分，从而都甩掉一半(target 不在的那一半)的数据，所以下一步就只需要处理剩下的一半；
所以数据一定要有一定的属性，
对于全排序数组，只要比较中数和target的大小，就很容易找到target不在的那一半，甩掉它；
对于rotate，情况，有一半是sorted的要么是mid 左边那一半，要么是mid右边那一半
如果 A[start] < A[mid]， 那么无疑，数组Mid 往左 的 半部分 是sorted的，
注意，这里只判断 A[mid] >= target 不行，因为，就算target <= A[mid],只要他不大于A[start],它还可能在右半边
所以必须要用 if A[start] <= target and target <= A[mid]:

右半边有序的情况同理。

'''

class Solution:
    """
    @param A : a list of integers
    @param target : an integer to be searched
    @return : an integer
    """
    def search(self, A, target):
        # write your code here
        if not A:
          return -1
        
        start,end = 0,len(A) - 1  
        
        while start + 1 < end:
            mid = start + (end - start) / 2
            if A[mid] == target:
                return mid
            if A[mid] > A[start]:#mid 在pivot左半边
                #这时候，可以看是否target 是否在start 和 mid 中介
                #由于start 到mid 是 sorted，所以可以这
                if A[start] <= target and target <= A[mid]:
                    end = mid
                else:
                    start = mid
            else: # A[mid] <= A[start] 那么mid 在右边，start在左边
                if A[mid] <= target and target <= A[end]:
                    start = mid
                else:
                    end = mid
        
        if A[start] == target:
            return start
        if A[end] == target:
            return end
        return -1
