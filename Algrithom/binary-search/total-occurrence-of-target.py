'''
Given a target number and an integer array sorted in ascending order. Find the total number of occurrences of target 
in the array.

Example
Given [1, 3, 3, 4, 5] and target = 3, return 2.

Given [2, 2, 3, 4, 6] and target = 4, return 1.

Given [1, 2, 3, 4, 5] and target = 6, return 0.

Thinking:
This is same as search rang question, binary search solution is to find the start end end position by 2 binary search.
'''

class Solution:
    # @param {int[]} A an integer array sorted in ascending order
    # @param {int} target an integer
    # @return {int} an integer
    def totalOccurrence(self, A, target):
        # Write your code here
        # 2 times of binary search to get the head and tail of the duplicated target
        
        if not A:
            return 0
        start,end = 0, len(A) - 1
        count = 0
        # 1 find head
        while start + 1 < end:
            mid = start + (end - start) / 2
            if A[mid] < target:
                start = mid
            #drop and thing >= target:
            else:
                end = mid

        if A[start] == target:
            head = start
        
        elif A[end] == target:
            head = end
        else:
            head = -1
            
        # 2 find tail
        start,end = 0, len(A) - 1
        count = 0
        # 1 find head
        while start + 1 < end:
            mid = start + (end - start) / 2
            #drop and thing <= target:
            if A[mid] <= target:
                start = mid
            else:
                end = mid

        if A[end] == target:
            tail = end
        
        elif A[start] == target:
            tail = start
        else:
            tail = -1
            
        if head >=0 and tail >=0:
            return tail -head + 1
        else:
            return 0
                    