'''
Question:
Given a big sorted array with positive integers sorted by ascending order. The array is so big so that you
 can not get the length of the whole array directly, and you can only access the kth number by ArrayReader.get(k) 
 (or ArrayReader->get(k) for C++). Find the first index of a target number. Your algorithm should be in O(log k),
  where k is the first index of the target number.

Return -1, if the number doesn't exist in the array.

If you accessed an inaccessible index (outside of the array), ArrayReader.get will return -1.

Example
Given [1, 3, 6, 9, 21, ...], and target = 3, return 1.

Given [1, 3, 6, 9, 21, ...], and target = 4, return -1.

Challenge
O(log k), k is the first index of the given target number.

Thinking:
1 For the access of the biggest data.
Since it's very big, we only need to get some where larger than the target index. so we can times 2 each time.
        #get the end index:
        index = 1
        while reader.get(index) != -1 and reader.get(index) < target:
            index = index * 2

Solution:

'''

"""
Definition of ArrayReader:
class ArrayReader:
    def get(self, index):
        # this would return the number on the given index
        # if there is no number on that index, return -1
"""
class Solution:
    # @param {ArrayReader} reader: An instance of ArrayReader 
    # @param {int} target an integer
    # @return {int} an integer
    def searchBigSortedArray(self, reader, target):
        # write your code here
        if not reader:
            return -1
        
        #get the end index:
        index = 1
        while reader.get(index) != -1 and reader.get(index) < target:
            index = index * 2
            
        start,end = 0,index
        while start + 1 < end:
            mid = start + (end - start) / 2
            if reader.get(mid) != -1 and reader.get(mid) < target:
                start = mid
            else:
                end = mid
            
        if reader.get(start) != -1 and reader.get(start) == target:
            return start
        if reader.get(end) != -1 and reader.get(end) == target:
            return end
        return -1