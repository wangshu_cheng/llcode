'''
Question:
Find the last position of a target number in a sorted array. Return -1 if target does not exist.

Example
Given [1, 2, 2, 4, 5, 5].

For target = 2, return 2.

For target = 5, return 5.

For target = 6, return -1.

Thinking:
For last position, we drop anything <= target.


'''

class Solution:
    # @param {int[]} A an integer array sorted in ascending order
    # @param {int} target an integer
    # @return {int} an integer
    def lastPosition(self, A, target):
        # Write your code here
        nums = A
        if not nums: return -1
        
        LEN = len(nums)
        start,end = 0,LEN-1
        while start + 1 < end:
            mid = start + (end - start) / 2
            # drop anything <= target.
            if nums[mid] <= target:
                start = mid
            else:
                end = mid
        
        if nums[end] == target:
            return end
        if nums[start] == target:
            return start
        return -1