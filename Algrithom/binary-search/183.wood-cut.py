'''
Question:
Given n pieces of wood with length L[i] (integer array). Cut them into small pieces to guarantee you could
 have equal or more than k pieces with the same length. What is the longest length you can get from the n
  pieces of wood? Given L & k, return the maximum length of the small pieces.

Notice
You couldn't cut wood into float length.

Example
For L=[232, 124, 456], k=7, return 114.

Thinking:

PLS BE NOTED, THIS IS A VERY GOOD EXAMPLE TO TELL YOU THE BINARY SEARCH CAN USE DATA INSTEAD OF 
INDEX FOR BINARY !!!

以最大的那块木头做2分法切割；看是否所有木条按照这种切法的 总值 大于k，如果小于k那么说明木条太长，再切一半。
如果大于k，那么加大木条长度
'''


class Solution:
    """
    @param L: Given n pieces of wood with length L[i]
    @param k: An integer
    return: The maximum length of the small pieces.
    """
    def woodCut(self, L, k):
        # write your code here
        # be noted, binary search not necessary use index
        # can use real number as well
        #Don't forget this, this is possible if all the length of the wood is smaller than the k.
        
        if sum(L) < k: # this condition includes the L is empty case.
            return 0
        
        longest = max(L)
        start,end = 1, longest
        while start + 1 < end:
            mid = start + (end - start) / 2
            num_of_pieces = sum([ i / mid for i in L ])
            if num_of_pieces < k: # too long
                end = mid
            else:
                start = mid
        #结果可能是start 或end， end要大于 start； 所以如果end满足要求，就先取end
        if sum([i / end for i in L]) >= k:
            return end
        return start


        