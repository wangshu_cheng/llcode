'''
Question:
Write an efficient algorithm that searches for a value in an m x n matrix, return the occurrence of it.

This matrix has the following properties:

Integers in each row are sorted from left to right.
Integers in each column are sorted from up to bottom.
No duplicate integers in each row or column.
Example
Consider the following matrix:

[
  [1, 3, 5, 7],
  [2, 4, 7, 8],
  [3, 5, 9, 10]
]
Given target = 3, return 2.

Challenge
O(m+n) time and O(1) extra space

Thinking:
和前一题的区别在于它是要找occurence，而不是是否存在；另外矩阵条件也变了，
由于矩阵条件改变，无法转成一维的 sorted 数组，所以无法用二分法；

这题和二分法无关， 而是一道矩阵 traversal 题目；

关键点在于，找到一个起始点，从起始点开始，无论往大走还是往小走，都只有一个方向，这样的目的是每走一步都可以甩掉一行或一列，所以大大优于
暴力搜索。

考察了上下左右4个corner，发现只有左下corner满足要求。

拿左下corner当起始点，如果和target比偏小，那么只有往右走，如果偏大，只有往上走。

Solution:

'''

def searchMatrix(self, matrix, target):
    # write your code here
    if not matrix:
        return 0
    rows,cols = len(matrix),len(matrix[0])
    #start point at bottom left, x,y
    x = rows - 1
    y = 0
    
    occurs = 0
    
    while x >= 0 and y < cols:
        if matrix[x][y] == target:
            occurs += 1
            #because no duplicate number in each row and col
            x -= 1
            y += 1
        elif matrix[x][y] > target:
            x -= 1
        else:
            y += 1
    return occurs
        