'''
Summary
First and last position of a target.
Find range of a target
Find close to target - after BS, compare abs diff to decide who is closer, start or end?
如果遇到有duplcicate 元素，往往是通过 start + 1 或 end - 1 方式求解。
wood cut 题目告诉我们 甩除 表达式只要和mid相关就行，不一定是 以mid作为index的array的元素。
'''

'''
Binary search 通用模板
'''


class Solution:
    # @param nums: The integer array
    # @param target: Target number to find
    # @return the first position of target in nums, position start from 0 
    def binarySearch(self, nums, target):
        if len(nums) == 0:
            return -1
            
        start, end = 0, len(nums) - 1
        while start + 1 < end:
            mid = (start + end) / 2
            '''
            甩除条件：
            一定是和 mid 有关的东西，不一定是以mid为index的array的值，也可以是任何与mid有关的表达式；
            '''
            if nums[mid] < target:
                start = mid
            # 大于和等于合为一类：
            else:
                end = mid
                
        if nums[start] == target:
            return start
        if nums[end] == target:
            return end
        return -1