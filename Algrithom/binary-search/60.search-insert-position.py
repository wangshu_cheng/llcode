'''
Question:
Given a sorted array and a target value, return the index if the target is found. If not, 
return the index where it would be if it were inserted in order(first position >= target).
You may assume NO duplicates in the array.
Example:
[1,3,5,6], 5 → 2

[1,3,5,6], 2 → 1

[1,3,5,6], 7 → 4

[1,3,5,6], 0 → 0

O(log(n)) time


Thinking:
1 When the question is about "sorted" array, then we can think use binary search
2 When ask for log(n) complexity, must be binary search.

Follow up:
如果题目换成了 如果target 不存在，找 last position < target？see solution2

Solution：
'''
class Solution:
    """
    @param A : a list of integers
    @param target : an integer to be inserted
    @return : an integer
    """
    def searchInsert(self, A, target):
        # write your code here
        if A == None: return
        if len(A) == 0:
            return 0

        #注意，binary search 只能查出 target 在给定数组范围内的情况
        #对于超出范围的，需要另外处理

        #第一个条件其实已经 在后面cover了
        if target < A[0]:
            return 0
            
        if target > A[len(A) - 1]:
            return len(A)
        
        start,end = 0, len(A)-1
        while start + 1 < end:
            mid = start + (end - start) / 2
            if A[mid] == target:
                return mid
            elif A[mid] < target:
                start = mid
            else:
                end = mid
        #Usually, the target will between start and end, but there is 2 corner case;
        #Corner 1 the target smaller than the first element of array,That's why here must use A[start] >= target.
        #根据模板，我们只需要A[START] == TARGET即可了
        if A[start] >= target:
            return start
        
        elif A[end] >= target:
            return end


'''

'''
class Solution2:
    """
    @param A : a list of integers
    @param target : an integer to be inserted
    @return : an integer
    """
    def searchInsert(self, A, target):
        # write your code here
        if A == None: return
        if len(A) == 0:
            return 0

        #if target supper small, no one smaller than him, then return 0
        if target < A[0]
           return 0
        
        start,end = 0, len(A)-1
        while start + 1 < end:
            mid = start + (end - start) / 2
            if A[mid] == target:
                return mid
            elif A[mid] < target:
                start = mid
            else:
                end = mid
        #find last poistion , we need start from end
        if A[end] == target:
            return end
        
        if A[end] < target:
            return end + 1
        if A[start] == target:
        	return start;

        return start + 1