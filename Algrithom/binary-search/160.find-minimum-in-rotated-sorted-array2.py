'''
Question:
Suppose a sorted array is rotated at some pivot unknown to you beforehand.

(i.e., 0 1 2 4 5 6 7 might become 4 5 6 7 0 1 2).

Find the minimum element.

The array may contain duplicates.

Example
Given [4,4,5,6,7,0,1,2] return 0


Thinking:
The difference with find minimum 1 question, is the array contains duplicated item,
This makes original code not work if the 'end' and the mid is same value,
So we move end close to pivot by 1 if mid == num[end]. To make the end even smaller.
最小值是一定是在右半边，不是左半边。

'''

class Solution:
    # @param num: a rotated sorted array
    # @return: the minimum number in the array
    def findMin(self, num):


 #   这道题目在面试中不会让写完整的程序
 #   只需要知道最坏情况下 [1,1,1....,1] 里有一个0
 #   这种情况使得时间复杂度必须是 O(n)
 #   因此写一个for循环就好了。
 #   如果你觉得，不是每个情况都是最坏情况，你想用二分法解决不是最坏情况的情况，那你就写一个二分吧。
 #   反正面试考的不是你在这个题上会不会用二分法。这个题的考点是你想不想得到最坏情况。

        # write your code here
        # # 1 greedy O(N)
        # if not num:
        #     return -1
        # n = len(num)
        # result = 2**32
        # for i in range(n):
        #     result = min(result,num[i])
            
        # return result
        
        
        #2 binary search, if he ask for lgN .
        
        if len(num) == 0:
            return 0
            
        start, end = 0, len(num) - 1

        while start + 1 < end:
            mid = (start + end) / 2
            if num[mid] < num[end]:
                end = mid
            #Key thing!     // if mid equals to end, that means it's fine to remove end
                # so the smallest element won't be removed
            # if direct use end = mid, than in case of 11110111, the smallest item will be skipped.
            elif num[mid] == num[end]:
                end -= 1
            else:
                start = mid
        return min(num[start],num[end])



        