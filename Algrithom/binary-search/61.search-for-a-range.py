'''
Question:
Given a sorted array of n integers, find the starting and ending position of a given target value.
If the target is not found in the array, return [-1, -1].

Example
Given [5, 7, 7, 8, 8, 10] and target value 8,
return [3, 4].

Thinking:
This is 2 question, one is to find the start position of target, the 2nd is to find the end position.
For start position,follow template, and we need to drop the element which >= target (set end = mid)
 for end position, vice verse.
'''

class Solution:
    """
    @param A : a list of integers
    @param target : an integer to be searched
    @return : a list of length 2, [index1, index2]
    """
    def searchRange(self, A, target):
       
        if not A:
            return [-1,-1]
            
        #get first position
        def getFirstPosition(A,target):
            start,end = 0,len(A) - 1
            while start + 1 < end:
                mid = (start + end) / 2
                if A[mid] < target:
                    start = mid
                #if A[mid] >= target, end = mid, since we can drop the elements which >= target 
                else:
                    end = mid
            if A[start] == target:
                return start
            if A[end] == target:
                return end
            return -1
        
        #get last postion
        def getLastPosition(A,target):
            start,end = 0,len(A) - 1
            while start + 1 < end:
                mid = (start + end) / 2
                #if A[mid] <= target, start = mid, since we can drop the elements which <= target
                if A[mid] <= target:
                    start = mid
                else:
                    end = mid
            if A[end] == target:
                return end
            if A[start] == target:
                return start
            return -1
        #done
        
        left,right = getFirstPosition(A,target),getLastPosition(A,target)
            
        return [left,right]