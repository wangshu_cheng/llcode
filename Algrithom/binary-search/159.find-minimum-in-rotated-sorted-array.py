'''
Question:
Suppose a sorted array is rotated at some pivot unknown to you beforehand.

(i.e., 0 1 2 4 5 6 7 might become 4 5 6 7 0 1 2).

Find the minimum element.

You may assume no duplicate exists in the array.

Example
Given [4, 5, 6, 7, 0, 1, 2] return 0

'''

class Solution:
    # @param num: a rotated sorted array
    # @return: the minimum number in the array
    def findMin(self, num):
        # write your code here
        if not num:
            return -1
            
        start, end = 0, len(num) - 1
        #choose last number as target
        #target = num[end]
        while start + 1 < end:
            mid = start + (end - start) / 2
            #if less than or equal last value, means mid is on the right side of the pivot
            if num[mid] <= num[end]:
                end = mid
            else:
                start = mid
        #如果我搞不清是 start 还是 end ， 题目要求找最小值，所以干脆取最小。
        #实际上，这两个值都有可能
        return min(num[start], num[end])