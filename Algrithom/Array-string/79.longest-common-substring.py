'''
 Longest Common Substring

 Description
 Notes
 Testcase
 Judge
Given two strings, find the longest common substring.

Return the length of it.

 Notice

The characters in substring should occur continuously in original string.
 This is different with subsequence.
'''

这题有两种解法，可以用指针解法，比较推荐，因为没用多余空间，或者序列型动归，
n*m空间。

class Solution:
    # @param A, B: Two string.
    # @return: the length of the longest common substring.
    def longestCommonSubstring(self, A, B):
        # write your code here
        
        # Method1 pointer , 单指针，探针
        
        if A == None or B == None:
            return 0
            
        max_len = 0
        for i in range(len(A)):
            #如果题目是prefix，那么第二个循环可以省掉，因为只能从后开始match，而不是从
            #string的任何位置

            for j in range(len(B)):
                p = 0
                while i + p < len(A) and j + p < len(B) and A[i + p] == B[j + p]:
                    p += 1
                max_len = max(max_len,p)
                
        return max_len
        
   
    #Method2 DP,
    #since brute force is exponecial , so can consider DP,
    #xue lie xing DP, use n + 1 space, each fn state means the state after each number.
   
    def longestCommonSubstring(self, A, B):    
        if not A or not B:
            return 0
        m,n = len(A),len(B)
        # A 第i点和B第j点结尾的2个子字串的最长可能字串； 结果就是  f[i][j]的最大值
        f = [[0 for j in range(n + 1)] for i in range(m + 1)]
        max_value = 0
        for i in range(1,m + 1):
            for j in range(1,n + 1):
                if A[i - 1] == B[j - 1]:
                 #这题和 subseq 题的区别就在于，状态是每一点的最大长度。
                #这也很好理解，因为subarray需要连续，一旦断了，需要重新计。
                    f[i][j] = f[i - 1][j - 1] + 1
                else:
                    f[i][j] = 0
                max_value = max(max_value,f[i][j])
                    
        return max_value            
        
                        
