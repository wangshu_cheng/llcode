# Given an integer, convert it to a roman numeral.

# Input is guaranteed to be within the range from 1 to 3999.
class Solution(object):
    def intToRoman(self, num):
        """
        :type num: int
        :rtype: str
        """
       #Defination: roman = {'M': 1000,'D': 500 ,'C': 100,'L': 50,'X': 10,'V': 5,'I': 1}

        M = ["", "M", "MM", "MMM"];#1000
        C = ["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"]
        X = ["", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"]
        I = ["", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"]
        return M[num/1000] + C[(num%1000)/100] + X[(num%100)/10] + I[num%10]