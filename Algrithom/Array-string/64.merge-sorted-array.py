'''
Given two sorted integer arrays A and B, merge B into A as one sorted array.
'''

class Solution:
    """
    @param A: sorted integer array A which has m elements, 
              but size of A is m+n
    @param B: sorted integer array B which has n elements
    @return: void
    """
    def mergeSortedArray(self, A, m, B, n):
        # write your code here
        i,j = m-1,n-1
        index = m + n - 1# last index of the big A array
        while i >= 0 and j >= 0:
            if A[i] > B[j]:
                A[index] = A[i]#move large number to end.
                i -= 1
            else:
                A[index] = B[j]
                j -= 1
            index -= 1
            
        
        # if j >= 0:
        #     for k in range(j+1):
        #         A[k] = B[k]
        
        while j >= 0:
            A[index] = B[j]
            index -= 1
            j -= 1
        
        #below looks no need, since if A still left, than they are smallest.
        # while i >= 0:
        #     A[index] = A[i]
        #     index -= 1
        #     i -= 1   