''' Medium
Implement a basic calculator to evaluate a simple expression string.

The expression string contains only non-negative integers, +, -, *, / operators and empty spaces . The integer division should truncate toward zero.

Example 1:

Input: "3+2*2"
Output: 7
Example 2:

Input: " 3/2 "
Output: 1
Example 3:

Input: " 3+5 / 2 "
Output: 5

'''
class Solution(object):
    def calculate(self, s):
        """
        :type s: str
        :rtype: int
        """
        #boarder:
        if len(s)==0:return 0
        
        num=0
        sign='+'
        stack=[]
        for idx,i in enumerate(s):
            if i.isdigit():
                num=num*10+int(i)
            if (i in ('+','-','*','/')) or idx==len(s)-1:
               if sign=='+':
                   stack.append(num)
               elif sign=='-':
                   stack.append(-num)
               elif sign=='*':
                   stack.append(int(stack.pop())*num)
               elif sign=='/':
                   top=int(stack.pop())
                   if top>=0:
                       stack.append(top/num)
                   else:
                       stack.append(-(abs(top)/num))
               sign=i
               num=0
        return sum(stack)
                   
        