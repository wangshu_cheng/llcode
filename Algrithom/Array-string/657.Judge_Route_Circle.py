class Solution(object):
    def judgeCircle(self, moves):
        """
        :type moves: str
        :rtype: bool
        """
        if not moves:
            return False
        
        end = [0,0]
        
        for step in moves:
            if step == 'U':
                end[1] += 1
            elif step == 'D':
                end[1] -=1
            elif step == 'L':
                end[0] -= 1
            elif step == 'R':
                end[0] += 1
        
        if end[0] == 0 and end[1] == 0:
            return True
        return False