'''
Given an integer array, find a subarray with sum closest to zero.
 Return the indexes of the first number and last number.
'''
class Solution:
    """
    @param nums: A list of integers
    @return: A list of integers includes the index of the first number 
             and the index of the last number
    """
    def subarraySumClosest(self, nums):
        # sort the presum get the smallest difference (so the subarray close to zero)
        # Presume defination:
        # the sum before current NUMBER of element
        #like [1,1,2,3], presum[0] always = 0, presum[3] = from num[0] add up to num[2], = 4
        
        if not nums:
            return []
        if len(nums) == 1:
            return [0,0]
        
        #get presum
        #presum define: [0,i], first element is the presum value, the 2nd is the ending index
        presum = [[0,i] for i in range(len(nums) + 1)]
        for i in xrange(1,len(nums) + 1):
            presum[i][0] = presum[i-1][0] + nums[i-1]
        #sort presum
        presum = sorted(presum) #it will sort based on first value, which is sum
        
        #loop presum to find the smallest gap
        smallest = 2**31
        res = []
        for i in range(1,len(presum)):
            if  presum[i][0] - presum[i-1][0] < smallest:
                smallest = presum[i][0] - presum[i-1][0]
                res = [presum[i][1]-1,presum[i-1][1]-1]
                res=sorted(res) # I don't know which index is higher, so need sort
                res[0] += 1 # Because the first element is the end of previous presum so need add 1
        return res
            
            
        
        
        