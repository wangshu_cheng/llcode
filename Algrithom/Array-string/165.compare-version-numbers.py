'''
Compare two version numbers version1 and version2.
If version1 > version2 return 1; if version1 < version2 return -1;otherwise return 0.

You may assume that the version strings are non-empty and contain only digits and the . character.
The . character does not represent a decimal point and is used to separate number sequences.
For instance, 2.5 is not "two and a half" or "half way to version three", it is the fifth second-level revision of the second first-level revision.

Example 1:

Input: version1 = "0.1", version2 = "1.1"
Output: -1

Example 2:

Input: version1 = "1.0.1", version2 = "1"
Output: 1
Example 3:

Input: version1 = "7.5.2.4", version2 = "7.5.3"
Output: -1
'''

class Solution(object):
    def compareVersion(self, version1, version2):
        """
        :type version1: str
        :type version2: str
        :rtype: int
        """
        versions1 = version1.split('.')
        versions2 = version2.split('.')
        
        #remove the 01 things
        versions1 = [int(x) for x in versions1]
        versions2 = [int(x) for x in versions2]
        
        i,j = 0,0
        
        while i < len(versions1) and j < len(versions2):
            if versions1[i] > versions2[j]:
                return 1
            elif versions1[i] < versions2[j]:
                return -1
            i += 1
            j += 1


        if len(versions1[i:]):
        	# 1.0.0 is same as 1 , 1.0.0.1.0 is bigger
            if sum(versions1[i:])== 0:
                return 0
            else: return 1
        if len(versions2[j:]):
            if sum(versions2[j:]) == 0:
                return 0
            else: return -1
        return 0
        
                
