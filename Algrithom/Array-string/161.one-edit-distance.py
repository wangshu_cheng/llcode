''' Medium
Given two strings s and t, determine if they are both one edit distance apart.

Note: 

There are 3 possiblities to satisify one edit distance apart:

Insert a character into s to get t
Delete a character from s to get t
Replace a character of s to get t
Example 1:

Input: s = "ab", t = "acb"
Output: true
Explanation: We can insert 'c' into s to get t.
Example 2:

Input: s = "cab", t = "ad"
Output: false
Explanation: We cannot get t from s by only one step.
Example 3:

Input: s = "1203", t = "1213"
Output: true
Explanation: We can replace '0' with '1' to get t.

'''
class Solution(object):
    def isOneEditDistance(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: bool
        """
        '''
        The question is about to find whether the 2 words can be made same at one single action.
        Like insert, or delete or replace,
        insert and delete are same thing, just move the pointer of longer words one letter more and compare:
        s[i:]==t[i+1:] ?
        replace is easy, just compare the remaining substring: s[i+1:]==t[i+1]?
        '''
        
        m = len(s)
        n = len(t)
        i,j = 0,0
        
        if abs(m-n)>1:
            return False
        
        #Store totally "move",like how many insert/delete/replace
        cnt = 0
        while i < m and j < n:
            if s[i] == t[j]:
                i+=1
                j+=1
                continue
            else:
                cnt += 1
                if cnt > 1:
                    return False
                if m>n:
                    i +=1
                elif m<n:
                    j +=1
                else:
                    i += 1
                    j += 1
            
        if i < m or j<n:
            cnt +=1 
        return cnt == 1
            
        