'''
Given an array nums, write a function to move all 0's to the end of it while maintaining 
the relative order of the non-zero elements.

For example, given nums = [0, 1, 0, 3, 12], after calling your function, 
nums should be [1, 3, 12, 0, 0].

Note:
You must do this in-place without making a copy of the array.
Minimize the total number of operations.
'''
'''
Thinking:
If found zero, try to find the next non zero and swap

'''

class Solution(object):
    def moveZeroes(self, nums):
        """
        :type nums: List[int]
        :rtype: void Do not return anything, modify nums in-place instead.
        """
        
        if not nums:
            return nums
        
        for i in range(len(nums)-1):#here we move to 
            if nums[i] == 0:
                j = i + 1
                # from i+i move right wards till find first non-zero item and swap.
                while j < len(nums) - 1 and nums[j] == 0:
                    j += 1
                nums[i],nums[j] = nums[j],nums[i]