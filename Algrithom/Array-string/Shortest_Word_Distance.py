'''
Given a list of words and two words word1 and word2, return the shortest distance between 
these two words in the list.

For example,
Assume that words = ["practice", "makes", "perfect", "coding", "makes"].

Given word1 = “coding”, word2 = “practice”, return 3.
Given word1 = "makes", word2 = "coding", return 1.

Note:
You may assume that word1 does not equal to word2, and word1 and word2 are both in the list.
'''

'''
Thinking:
一个方向traversal， 不停更新2个index，不停求最小值
'''

class Solution(object):
    def shortestDistance(self, words, word1, word2):
        """
        :type words: List[str]
        :type word1: str
        :type word2: str
        :rtype: int
        """
        
        i1 = -1
        i2 = -1
        res = 2**31
        for i in range(len(words)):
            if words[i] == word1:
                i1 = i
            elif words[i] == word2:
                i2 = i
            
            if i1 != -1 and i2 != -1:
                res = min(res,abs(i1 - i2))
        return res