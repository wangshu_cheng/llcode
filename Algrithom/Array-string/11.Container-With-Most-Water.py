'''
Given n non-negative integers a1, a2, ..., an, where each represents a point at coordinate (i, ai). n vertical lines are drawn such that the two endpoints of line i is at (i, ai) and (i, 0). Find two lines, which together with x-axis forms a container, such that the container contains the most water.
Note: You may not slant the container and n is at least 2.
'''
class Solution(object):
    def maxArea(self, height):
        """
        :type height: List[int]
        :rtype: int
        
      y ^
        |
        |     a2
        |     |  a3          an
        |  a1 |  |     a5    | 
        |  |  |  |  a4 |     |
        |  |  |  |  |  | ..  |
        --------------------------->
       0   1  2  3  4  5 ..  n     x
        
        It's not like
        https://leetcode.com/problems/trapping-rain-water/ 
        Which is to get the total water for all the bars problem,
        It is to find the any 2 bars which hold most water.
        The first and last bar is good cadidate because it's the widest one.
        """
        i, j = 0, len(height) - 1
        water = 0
        while i < j:
            water = max(water, (j - i) * min(height[i], height[j]))
            if height[i] < height[j]:
                i += 1
            else:
                j -= 1
        return water