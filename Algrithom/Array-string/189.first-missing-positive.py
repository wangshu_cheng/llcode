'''
Given an unsorted integer array, (should start from 1)find the first missing positive integer.

Example
Given [1,2,0] return 3,
and [3,4,-1,1] return 2.
[1,2,3,4]
首先该题目给出的数组是连续的，只是缺了一个数；
第二，他可以破坏原来的次序；
所以我们可以得出，A[i] 应该在 index A[i] - 1 上 也就是  A[i] should equals to A[A[i]-1]；
那么我们就交换，如果发现A[i] 不在 A[i]-1，就把它和 A[i]-1的数交换（有排序算法的意思）
那么最后，只有那个missing的不在位置上。

'''

#thinking: 
#put value i to index i-1, than the missing one is the one not match.
# e.g.  if we put 1,-1,3,4, than the missing is 2.

class Solution:
    # @param A, a list of integers
    # @return an integer
    def firstMissingPositive(self, A):
        # write your code here

        
        if not A:
            return 1
            
        for i in range(len(A)):
            while A[i] > 0 and A[i] <= len(A) and A[i] != i + 1: 
                #the value of the position which A[i] should be in:
                # tmp = A[A[i] - 1]
                # if tmp == A[i]:
                #     break
                # A[A[i] - 1] = A[i]
                # A[i] = tmp

                #如果一样就不换了， 不然死循环
                if A[A[i] - 1] == A[i]:
                   break
                A[A[i] - 1],A[i] = A[i],A[A[i] - 1]
                 
        for i in range(len(A)):
            if A[i] != i + 1:
                return i + 1
                
        return len(A) + 1
            