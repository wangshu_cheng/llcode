'''
Given an array nums of integers and an int k, partition the array (i.e move the elements in "nums") 
such that:

All elements < k are moved to the left
All elements >= k are moved to the right
Return the partitioning index, i.e the first index i nums[i] >= k.
'''


'''
1 quick sort method, lazy指针

'''
def partitionArray(self, nums, k):
    index = 0
    n = len(nums)
    l,r = 0,n
    for i in range(l,n):
        if nums[i] < k:
            nums[i],nums[index] = nums[index],nums[i]
            index += 1
    return index
        
'''
2 pointer method:
'''

def partitionArray(self, nums, k):
    # write your code here
    # you should partition the nums by k
    # and return the partition index as description
    if nums == None :
        return
    i,j = 0,len(nums)-1
    
    while i<=j:
        while i<=j and nums[i] < k:
            i += 1
        while i<=j and nums[j] >= k:
            j -= 1
        if i <= j:
            nums[i],nums[j] = nums[j],nums[i]
            i += 1
            j -= 1
    return i