'''
Given an integers array A.

Define B[i] = A[0] * ... * A[i-1] (bypass itself A[i]) * A[i+1] * ... * A[n-1], calculate B WITHOUT divide operation.

Have you met this question in a real interview? Yes
Example
For A = [1, 2, 3], return [6, 3, 2].
'''

class Solution:
    """
    @param A: Given an integers array A
    @return: An integer array B and B[i]= A[0] * ... * A[i-1] * A[i+1] * ... * A[n-1]
    """

    # 对每一个index， 弄两个数组，第一个存放 不包括 该 index 的 所有左边数组乘积； 
    # 对第2个存放 不包括 该 index 的 所有右边数组乘积； 

    def productExceptSelf(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        
        #1 build array1
        m=len(nums)
        array1=[1 for i in xrange(m)]
        
        # left half
        for i in xrange(1,m):
            array1[i]=array1[i-1]*nums[i-1]

        # right half 
        array2=[1 for i in xrange(m)]
        for i in xrange(m-2,-1,-1):
            array2[i] = array2[i+1]*nums[i+1]


        for i in range(m):
            array1[i]*=array2[i]
            
        
        '''  优化法，不需要第二个array，array2, 用一个variable 代替
        tmp=1
        # 然后再求 i 右半边的乘积
        for i in xrange(m-1,-1,-1):
            array1[i]*=tmp
            tmp*=nums[i]
        '''

        return array1


    # def productExcludeItself(self, A):
    #     # write your code here
        
    #     '''
    #     蒋委员长教育我们，用空间换时间。所以要做到O（n），可以造出两个阶乘的数组，然后再乘
    #     {              1,         a[0],    a[0]*a[1],    a[0]*a[1]*a[2],  },注意，要错一位
    #     { a[1]*a[2]*a[3],    a[2]*a[3],         a[3],                 1,  }
    #     中第二个可以直接用变量替代
    #     '''
    #     #1 build array1
    #     m=len(nums)
    #     array1=[1 for i in xrange(m)]
        
    #     # array1[i] 表示到 index i 左边所有的元素乘积，不包括nums[i],所以array1[0]一定是1
    #     for i in xrange(1,m):
    #         array1[i]=array1[i-1]*nums[i-1]

    #     tmp=1
    #     # 然后再求 i 右半边的乘积
    #     for i in xrange(m-1,-1,-1):
    #         array1[i]*=tmp
    #         tmp*=nums[i]
    #     return array1


