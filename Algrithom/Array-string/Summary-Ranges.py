
# Given a sorted integer array without duplicates, return the summary of its ranges.

# Example 1:
# Input: [0,1,2,4,5,7]
# Output: ["0->2","4->5","7"]
# Example 2:
# Input: [0,2,3,4,6,8,9]
# Output: ["0","2->4","6","8->9"]

class Solution(object):
    def summaryRanges(self, nums):
        """
        :type nums: List[int]
        :rtype: List[str]
        """
        if not nums:return []
        if len(nums)==1:return [str(nums[0])]
        res=[]
        #now the nums length >1
        nums = sorted(nums)
        start=nums[0]
        #this is to append a bigger value at end to handle the last value.
        nums+=[nums[-1]+2]
        for i in xrange(1,len(nums)):
            if nums[i]-nums[i-1]>1:
                if start!=nums[i-1]:
                    res.append(str(start)+"->"+str(nums[i-1]))
                else:
                    res.append(str(start))
                #dont forget the reset the start value.
                start=nums[i]
        return res