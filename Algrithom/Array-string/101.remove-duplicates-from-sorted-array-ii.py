'''
Follow up for "Remove Duplicates":
What if duplicates are allowed at most twice?

For example,
Given sorted array A = [1,1,1,2,2,3],

Your function should return length = 5, and A is now [1,1,2,2,3].
'''

class Solution:
    """
    @param A: a list of integers
    @return an integer
    """

    #method1 hash
    def removeDuplicates(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if not nums:return 0
        dic={}
        dic[nums[0]]=1
        lazy=1
        for i in xrange(1,len(nums)):
            if nums[i] not in dic:
                dic[nums[i]]=1
            else: dic[nums[i]]+=1
            if nums[i]!=nums[i-1] or dic[nums[i]]<=2:
                nums[lazy]=nums[i]
                lazy+=1
                
        return lazy

    #method2 flag
    def removeDuplicates(self, A):
        # write your code here
        
        if not A:
            return 0
        if len(A) == 1:
            return A
            
        lazy = 1
        flag = 0
        
        for i in range(1,len(A)):
            if A[i] == A[i - 1] and flag == 0:
                flag =1
                A[lazy] = A[i]
                lazy += 1
            elif flag == 1 and A[i] == A[i - 1]:
                continue
            elif  A[i] != A[i - 1]:
                A[lazy] = A[i]
                lazy += 1
                flag = 0
        return lazy
            
          