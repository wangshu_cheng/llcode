# Given a non-negative integer represented as a non-empty array of digits, plus one to the integer.

# You may assume the integer do not contain any leading zero, except the number 0 itself.

# The digits are stored such that the most significant digit is at the head of the list.

# Thinking: Convert to integer and convert back.

class Solution(object):
    def plusOne(self, digits):
        """
        :type digits: List[int]
        :rtype: List[int]
        """
        ret = []
        if not digits:
            return ret
            
        number = 0
        
        for i in digits:
            number = number * 10 + i
        
        number += 1
        
        while number > 0:
            mod = number % 10
            ret.insert(0,mod)
            number /= 10
            
        return ret