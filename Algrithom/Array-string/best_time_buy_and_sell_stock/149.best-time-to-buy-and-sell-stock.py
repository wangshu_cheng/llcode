
'''

This is basic question. only 1 transaction allowed.

Quesiton1, 
Say you have an array for which the ith element is the price of a given stock on day i.

If you were only permitted to complete at most one transaction (ie, buy one and sell one 
share of the stock), design an algorithm to find the maximum profit.

Analysis
only one transaction(buy and sale) of the whole period,
so solution is to loop and find the smallest, and 
'''

class Solution:
    """
    @param prices: Given an integer array
    @return: Maximum profit
    """
    def maxProfit(self, prices):
        # write your code here
        #remember the lowest price ever, and try max profile every day.
        #it's not realistic in real world.
        
        #init:
        if not prices:
            return 0
        
        profit = 0
        lowest = sys.maxint
        for i in prices:
            lowest = min(i,lowest)#当前最低价
            #当前可能最大利润
            current_max_profit = i - lowest
            profit = max(profit,current_max_profit)#global 最大profit
        #我只卖一次最大profit的那个，所以return profit即可
        return profit



            