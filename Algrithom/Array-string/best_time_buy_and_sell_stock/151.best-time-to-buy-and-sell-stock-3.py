'''
和上题不同的是你有限定交易次数，是2次，就是2次买卖。

Say you have an array for which the ith element is the price of a given stock on day i.

Design an algorithm to find the maximum profit. You may complete at most two transactions.
这个题目是按时间顺序做2次买卖，2买，2卖。第一次买卖结束后，做第二次买卖。就profit最大值。
Thinking:
需要找到一个点，在它之前的一次买卖和再它之后的一次买卖profit最大。
解法：
设计2个数组，一个存放到目前i点之前买卖的最大profit，一个存放目前点i之后买卖的岁大profit，然后求p1[i]+p2[i]的最大值


Anaysis:
由于可以买卖2次，所以从头到尾遍历找一次买卖的最大值
然后从尾到头找到一次买卖最大值。

'''

class Solution:
    """
    @param prices: Given an integer array
    @return: Maximum profit
    """
    def maxProfit(self, prices):
        # write your code here
        n = len(prices)
        if n <= 1:
            return 0
        p1 = [0] * n # 对于每个i，在i点之前买卖最大profit
        p2 = [0] * n # 对于每个i，在i点之后买卖最大profit
        
        minV = prices[0]
        for i in range(1,n):
        	#this is continously find lowest 
            minV = min(minV, prices[i]) 
            #this is buidling a 'global' max profit for each i 
            p1[i] = max(p1[i - 1], prices[i] - minV)
        

        # how to understand the reverse bigest profit?
        #就是 在 i 点上买进，将来卖出的最大profit
        maxV = prices[-1]
        for i in range(n-2, -1, -1):
        	#this is continously find highest price 
            maxV = max(maxV, prices[i])
            #this is buidling a 'global' max profit for each i from high to low            
            p2[i] = max(p2[i + 1], maxV - prices[i])
        
        res = 0
        for i in range(n):
            res = max(res, p1[i] + p2[i])
        return res