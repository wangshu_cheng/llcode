
'''

Compare with stock 1, this is unlimited transactions.

Question 2: 
Say you have an array for which the ith element is the price of a given stock on day i.

Design an algorithm to find the maximum profit. You may complete as many transactions 
as you like (ie, buy one and sell one share of the stock multiple times). However, 
you may not engage in multiple transactions at the same time (ie, you must sell the stock before you buy again).

Analysis
can buy and sale everyday, plus all the profit

'''
class Solution:
    """
    @param prices: Given an integer array
    @return: Maximum profit
    """
    def maxProfit(self, prices):
        # write your code here
        
        if not prices or len(prices) <= 1:
            return 0
            
        profit = 0
        for i in range(1, len(prices)):
            if prices[i] > prices[i-1]:
                #since we have unlimit transaction so，短线操作只要有差价就买卖，求profit总和。
                profit += prices[i] - prices[i-1]
        return profit