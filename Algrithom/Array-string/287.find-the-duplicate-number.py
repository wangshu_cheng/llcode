''' Medium
Given an array nums containing n + 1 integers where each integer is between 1 and n (inclusive), prove that at least one duplicate number must exist. Assume that there is only one duplicate number, find the duplicate one.

Example 1:

Input: [1,3,4,2,2]
Output: 2
Example 2:

Input: [3,1,3,4,2]
Output: 3
Note:

You must not modify the array (assume the array is read only).
You must use only constant, O(1) extra space.
Your runtime complexity should be less than O(n2).
There is only one duplicate number in the array, but it could be repeated more than once.
'''
class Solution(object):
    def findDuplicate(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        '''
        2分法：
        这道题的根本就是要利用 nums containing n + 1 integers where each integer is between 1 and n (inclusive)，
        也就是说index 和 值有联系关系。
        找到中间index mid， 如果值小于 index mid 的部分有序（没有重复值 比如总长度为10，那么mid = （0+9）/ 2  = 4；）
        那么重复值一定发生在值大于4的部分；       
        
        '''
        
        def count(nums,mid):
            cnt = 0
            for item in nums:
                if item <= mid:
                    cnt += 1
            return cnt
        
        l = 0
        r = len(nums) - 1
        
        while l + 1 < r:
            mid = l + (r-l)/2
            if count(nums,mid) <= mid:
                l = mid
            else:
                r = mid
        if count(nums,l) <= l:
            return r
        return l