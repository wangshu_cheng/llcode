'''

You are playing the following Bulls and Cows game with your friend: You write down a number and ask your friend to guess what the number is. Each time your friend makes a guess, you provide a hint that indicates how many digits in said guess match your secret number exactly in both digit and position (called "bulls") and how many digits match the secret number but locate in the wrong position (called "cows"). Your friend will use successive guesses and hints to eventually derive the secret number.

Write a function to return a hint according to the secret number and friend's guess, use A to indicate the bulls and B to indicate the cows. 

Please note that both secret number and friend's guess may contain duplicate digits.

Example 1:

Input: secret = "1807", guess = "7810"

Output: "1A3B"

Explanation: 1 bull and 3 cows. The bull is 8, the cows are 0, 1 and 7.
Example 2:

Input: secret = "1123", guess = "0111"

Output: "1A1B"

Explanation: The 1st 1 in friend's guess is a bull, the 2nd or 3rd 1 is a cow.
Note: You may assume that the secret number and your friend's guess only contain digits, and their lengths are always equal.

'''


class Solution(object):
    def getHint(self, secret, guess):
        """
        :type secret: str
        :type guess: str
        :rtype: str
        """
        if not secret or not guess:
            return
        
        shash = {}
        ghash = {}
        A = 0
        B = 0
        for i in range(len(secret)):
            if secret[i] == guess[i]:
                A += 1
            else:
                shash[secret[i]] = shash.get(secret[i],0) + 1
                ghash[guess[i]] = ghash.get(guess[i],0) + 1
                
        for key in shash:
            if key in ghash:
                #这里检查重复值；11102，34012，这种情况Cow 是1，34012，11102， cow 也是1.
                #所以cow是 该数字 ghash 和 shash 最小的count。
                B += min(ghash[key],shash[key])
        return str(A)+'A'+str(B)+'B'
        










        '''
        Put the secret into dictionay, key is position, value is digit
        check if both postion and value matches, add one bull
        else if  value exist in dic matches, add one cow
        for each cow or bull match, remove the key from dictionry
        repeat
        






        '''
        # VERY TRICKY, F*** U !
        #2 loop
        #first loop to get bulls
        #2nd loop to get cows, 
        # If the guess value is exist in the counter, add one cow
        # and if a cow already found, reduce it's currence 
        from collections import Counter
        c = Counter(secret)
        dic={}
        bulls=cows=0
        for i in xrange(len(secret)):
            dic[i]=secret[i]
            if guess[i] == secret[i]:
                bulls+=1
                c[secret[i]]-=1
        for j in xrange(len(secret)):
            if guess[j]!=secret[j]:
                if guess[j] in c and c[guess[j]]>0:
                    cows+=1
                    c[guess[j]]-=1
        return str(bulls)+'A'+str(cows)+'B'