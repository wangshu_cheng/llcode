'''
Given an array of integers, find out whether there are two distinct indices i and j in the array such that the absolute difference between nums[i] and nums[j] is at most t and the absolute difference between i and j is at most k.

Example 1:

Input: nums = [1,2,3,1], k = 3, t = 0
Output: true
Example 2:

Input: nums = [1,0,1,1], k = 1, t = 2
Output: true
Example 3:

Input: nums = [1,5,9,1,5,9], k = 2, t = 3
Output: false

'''

class Solution(object):
    def containsNearbyAlmostDuplicate(self, nums, k, t):
        """
        :type nums: List[int]
        :type k: int
        :type t: int
        :rtype: bool
        """
             
        #method1 brute force, time out
#         if not nums:
#             return False
#         if len(nums) < 2:
#             return False
#         for i in xrange(len(nums) - 1):
#             for j in xrange(i + 1,len(nums)):
#                 if abs(nums[i] - nums[j]) <= t and abs(i-j) <= k:
#                     return True
                
#         return False
    
        #method2:
        #bucket 方法，本质也是增加空间，空间换时间
        #用t+1的宽度做bucket 宽度，why t+1 ？ 因为t+1 - 1 = t; t+1宽度bucket 里面，头和尾部的元素差值正好是t。
        #这样，我们根据数值往bucket 里面放元素；如果一个bucket 能放两个，那么就满足条件了。
        
        if not nums or k == None or t == None or t<0:
            return False
        
        buckets = {}
        
        for i in range(len(nums)):
            bucket_num = nums[i] / (t + 1)
            if bucket_num in buckets:
                return True
            elif bucket_num-1 in buckets and abs(nums[i] - buckets[bucket_num - 1]) <= t:
                return True
            elif bucket_num+1 in buckets and abs(nums[i] - buckets[bucket_num + 1]) <= t:
                return True
            buckets[bucket_num] = nums[i]
            #remove the old key which index difference with current number is greater than k 
            if i>=k : del buckets[nums[i-k]/(t+1)]
                
        return False
            
            