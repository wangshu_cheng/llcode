'''
Given an array and a value, remove all occurrences of that value in place and return the new length.

The order of elements can be changed, and the elements after the new length don't matter.

Example
Given an array [0,4,4,0,0,2,4,4], value=4

return 4 and front four elements of the array is [0,0,0,2]

the question is about move all the target value to the end!
So when found target, swap to end.

'''

class Solution:


   #Layz 指针
    def removeElement(self, nums, val):
        """
        :type nums: List[int]
        :type val: int
        :rtype: int
        """
        if not nums:
            return 0
        
        l = 0
        n = len(nums)
        for i in range(n):
            if nums[i] != val:
                nums[l] = nums[i]
                l += 1
        return l


    """
    @param A: A list of integers
    @param elem: An integer
    @return: The new length after remove
    """
    def removeElement(self, A, elem):
        # write your code here
        if not A:
            return 0
        
        j = len(A) - 1
        for i in range(len(A)):
            while i <= j and A[i] == elem:
                A[i],A[j] = A[j],A[i]# or simplly assign A[i] = A[j],since we don't care the end elemnts,
                # A[i] = A[j]
                j -= 1
        return j + 1

            
        
        
                
        
            