* 65, median 题还需要再理解

技巧：
1 有时候原数组开头或结尾再加一个dumy的元素用于方便循环计算；

1 Presum - 用于有关 从头到某一元素的 subarray sum 问题

'''
1 presum 
'''
dic[0] = 0 # Don't forget!!

'''
presum defination:
the sum end at index + 1 或者说结束于第几个element
presum[i] means sum from nums[0] to nums[i-1] inclusive.

So, the subarray sum from index i to j (inclusive) is:

 presum[j+1] - presum[i]

'''
presum = [0 for i in range(len(nums) + 1)] # we need n+1 length, since we store a dumy 0.
for i in xrange(1,len(nums) + 1):
    presum[i] = presum[i-1] + nums[i-1]
'''
If we want to keep the index value some times, then we can define presum like:
presum define: [0,i], first element is the presum value, the 2nd is the ending index
'''
presum = [[0,i] for i in range(len(nums) + 1)]
for i in xrange(1,len(nums) + 1):
    presum[i][0] = presum[i-1][0] + nums[i-1]
    

2 Global max sum and local max sum
- 用于有关 中间某个 最大sum subarray 问题,local max是帮助 计算global max用的
最终还是要用 global max 

'''

2 Local max is used to 计算以某一点为终点的 “最大” subarray sum，注意，和presum 不同的是，这里的subarray是任意起始点，不需要从
0开始，如果每个元素都是positive，那么presum 和 lmax 相同

'''

    def maxSubArray(self, nums):
        # write your code here
        
        if not nums:
            return 0
        
        n = len(nums)
        
        lmax = [0 for i in range(n)]
        
        lmax[0] = nums[0]
        res = nums[0]
        for i in range(1,n):
            lmax[i] = max(lmax[i - 1] + nums[i],nums[i])
            res = max(res,lmax[i])
        return res


'''
如果题目求到目前index 为止的最大subarray（subarray不一定结束于目前位置），那么可以用global max：

Global max is used to calculate the largest subarray so far.

记忆：local max 一定包括当前num的值，而global max则不一定

'''
def maxSubArray(self, nums):
    # write your code here
    if not nums:
        return 0
    n = len(nums)
    gmax = [0 for i in range(n)]
    lmax = [0 for i in range(n)]
    
    gmax[0] = nums[0]
    lmax[0] = nums[0]
    
    for i in range(1,n):
        #local max must include this num[i]
        lmax[i] = max(lmax[i-1] + nums[i],nums[i])
        #global max no need include this num[i]
        
'''
这里，如果不用gmax 直接求lmax得最大值也是一样，还节约内存
'''
        gmax[i] = max(gmax[i-1],lmax[i]) 
    return gmax[n-1]

'''
3 去重：
单指针，当发现有不一样的元素的时候指针才移动
'''
def removeDuplicates(self, A):
    # write your code here
    if not A:
        return 0
        
    end = len(A) - 1
    l = 0
    for i in range(1,len(A)):
        if A[i] != A[i - 1]:
            l += 1
            A[l] = A[i]

    return l + 1
'''
4 完全去掉某一个元素
'''
def removeElement(self, A, elem):
    # write your code here
    if not A:
        return 0
    
    j = len(A) - 1
    for i in range(len(A)):
        while i <= j and A[i] == elem:
            A[i],A[j] = A[j],A[i]# or simplly assign A[i] = A[j],since we don't care the end elemnts,
            # A[i] = A[j]
            j -= 1
    return j + 1


'''
5 first missing:
'''
# @return an integer
def firstMissingPositive(self, A):
    # write your code here
    #thinking: 
    #put value i to index i-1, than the missing one is the one not match.
    # e.g.  if we put 1,-1,3,4, than the missing is 2.
    
    if not A:
        return 1
        
    for i in range(len(A)):
        while A[i] > 0 and A[i] <= len(A) and A[i] != i + 1: 
            #the value of the position which A[i] should be in:
            tmp = A[A[i] - 1]
            if tmp == A[i]:
                break
            A[A[i] - 1] = A[i]
            A[i] = tmp
             
    for i in range(len(A)):
        if A[i] != i + 1:
            return i + 1
            
    return len(A) + 1

'''
6
Use a array to record last index of each char,
763-partition-label

'''

7 2d -45 翻转
   #2 -45 flip:
        for i in xrange(n):
            for j in xrange(i):#here is to i not to N !!!!
               if i!=j:# i==j, don't need flip
                  matrix[i][j],matrix[j][i]=matrix[j][i],matrix[i][j]



Count() count how many char in a string

"aabbc".count('a')  => 2


'''
template 1: get all substrings:
'''
def substrings(line):
    if not line:
        return []
    res = []
    i,j = 0,0
    LEN = len(line)
    # # First loop, loop starting point of substring
    # for i in range(LEN):
    #     #2nd for each starting point, move end point till last
    #     while j < LEN + 1:#remember here is LEN + 1, since the [i:j] notation don't take the last element
    #         if (line[i:j]):
    #             res.append(line[i:j])
    #         j += 1
    #     j = i
    # return res

    for i in range(LEN):
        for j in range(i + 1,LEN + 1):
            res.append(line[i:j])
    return res


    #or while loop:
    for i in range(LEN):
        #2nd for each starting point, move end point till last
        while j < LEN + 1:#remember here is LEN + 1, since the [i:j] notation don't take the last element
            if (line[i:j]):
                res.append(line[i:j])
            j += 1
        j = i
    return res

print substrings("abcdef")
