'''
Given an array of n positive integers and a positive integer s, find the minimal length of a contiguous subarray of which the sum ≥ s. If there isn't one, return 0 instead.

Example: 

Input: s = 7, nums = [2,3,1,2,4,3]
Output: 2
Explanation: the subarray [4,3] has the minimal length under the problem constraint.
Follow up:
If you have figured out the O(n) solution, try coding another solution of which the time complexity is O(n log n). 
'''

class Solution(object):
    def minSubArrayLen(self, s, nums):
        """
        :type s: int
        :type nums: List[int]
        :rtype: int
        """
        '''
        2 pointer
        
        '''
        if not nums:return 0
        m=len(nums)
        res=m+1
        start=0
        total=0
        for i in xrange(m):
            total+=nums[i]
            while total>=s:
                res=min(res,i-start+1)
                total-=nums[start]
                start+=1

        return res if res<=m else 0

            
        