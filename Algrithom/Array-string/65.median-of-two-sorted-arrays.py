'''
There are two sorted arrays A and B of size m and n respectively. 
Find the median of the two sorted arrays.

Step1: check the TOTAL LENGTH of 2 array is odd or even, if odd, we need 
set k = len/2 + 1, else k = len/2

Divide and conque, dont forget return recursive

'''
class Solution:
    """
    @param A: An integer array.
    @param B: An integer array.
    @return: a double whose format is *.5 or *.0
    """
    def findMedianSortedArrays(self,A,B):
            # define kth largest first
        def kthsmallest(A, B, k):
            if len(A) == 0:
                return B[k - 1]
            elif len(B) == 0:
                return A[k - 1]
        
            if k == 1:
            	return min(A[0],B[0])
        
            #this is a skill, to assign big value if currrent array is
            # less than k/2, in this case, we must drop the other array's left half
            #用极限法思考，如果A是空，那么就是求B的median，就是把B的前半段抛掉
            #because we need totally k smallest, the 0 to k-1 smallest is not answer
            # so can set the compare mid value to a big data.
            mida = A[k/2 - 1] if (k/2 -1) < len(A) else 2**31
            midb = B[k/2 - 1] if (k/2 -1) < len(B) else 2**31 # this is to say
            
            if mida < midb:
            	return kthsmallest(A[k/2 :],B,k - k/2)
            #if mida == midb, drop first k/2 A or B is same thing.
            else:
            	return kthsmallest(A,B[k/2:],k - k/2)
    
        
        
        LEN = len(A) + len(B)
        if LEN % 2 == 1:
            return kthsmallest(A,B,LEN/2 + 1)
        else:
            return (kthsmallest(A,B,LEN/2) + kthsmallest(A,B,LEN/2 + 1)) / 2.0
