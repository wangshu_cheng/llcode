'''
Question:
For a given source string and a target string, you should output the first index(from 0)
 of target string in source string.
If target does not exist in source, just return -1.

Thinking:
这是让你实现 index of 的功能，相当于python： String.index(substring).
主要靠你基本的循环操作，这个题目就是 2 重循环，O（N2）复杂度即可；

考点：
注意循环边界值。


Solution:String
'''


#simple soluton:

def strStr(self, source, target):
    # write your code here
    if source == None or target == None:
        return -1

    for i in range(len(source) - len(target) + 1):
        if source[i:i + len(target)] == target:
            return i
    
    return -1


def strStr(self, source, target):
        # write your code here

        # method1
        
        i,j = 0,0
        if source == None or target == None:
             return -1
        m,n = len(source),len(target)
        if source == target:
            return 0
        
        if target == '':
            return 0
    
        for i in range(m - n + 1):
            for j in range(n):
                if source[i : i + j + 1] == target:
                    return i
        return -1
        
        
        #########method2
        
        i,j = 0,0
        if source == None or target == None:
             return -1
        m,n = len(source),len(target)
        if source == target:
            return 0
        
        if target == '':
            return 0
        
        while i < m - n + 1:
            while j < n:

                if source[i : i + j + 1] == target:
                    return i
           
                j += 1
            i += 1
            j = 0
        return -1
        
        #######
        
        # if source == None or target == None:
        #     return -1
        # m,n = len(source),len(target)
        # for i in range(m - n + 1):
        #     if source[i:i+n] == target:
        #         return i
        # return -1





# Below solution is correct, however it fails AMS interview because it's different than java solution.
# class Solution:
#     def strStr(self, source, target):
#         # write your code here
#         if  source == None or  target == None:
#             return -1

#         #there we must use len(target)+1 to make sure the inner loop at least run once
#         # 如果source = target的话，那么外层循环至少要走一次
#         for i in xrange(len(source)-len(target)+1):
#         	#同样道理，如果也要至少走一次
#             #so the j contains data, this is for the 2nd value = "" case.
#             for j in xrange(len(target)+1):
#                 if j == len(target):
#                     return i
#                 if source[i+j] != target[j]:
#                     break

#         return -1