'''
Longest Common Prefix

 Description
 Notes
 Testcase
 Judge
Given k strings, find the longest common prefix (LCP).
Example
For strings "ABCD", "ABEF" and "ACEF", the LCP is "A"
For strings "ABCDEFG", "ABCEFG" and "ABCEFA", the LCP is "ABC"

Tags 
'''
'''
这题和longest common substring 一样； 只是substring变成了prefix
也是用 单指针 方法。
prefix 就是从index 0 开始的substring

'''

方法一：前后两两相比求prefix，最后求到最小的common prefix
class Solution:
    # @param strs: A list of strings
    # @return: The longest common prefix
    def longestCommonPrefix(self, strs):
        # write your code here
        if not strs:
            return ""
            
        tmp = strs[0] #tmp means intersection,交集
        for i in range(1,len(strs)):
            l = 0
            while l < len(strs[i]) and l < len(tmp) and strs[i][l] == tmp[l]:
                l += 1
            if l == 0:
                return ""
            #用第一个和第二个string的交集去更新tmp，然后再比较
            tmp = tmp[:l]
        
        return tmp

# 自己相处的brute force 解发AC 了。

思路：
    选第一个字符串，然后把它的第一个char 做为第一个可能的prefix， loop 所有的字符串，如果它们都有
    这个char，那么prefex 加上这个char，一旦不满足，立即结束程序。

class Solution(object):

    def longestCommonPrefix(self, strs):
        """
        :type strs: List[str]
        :rtype: str
        """

        if not strs:
            return ''

        start = strs[0]
        prefix = ''
        for i in range(len(start)):
            j = 0
            while j < len(strs):
                if i >= len(strs[j]) or start[i] not in strs[j] \
                    or strs[j][i] != start[i]:
                    break
                j += 1
            if j == len(strs):
                prefix += start[i]
            else: #
                break

        return prefix
                    