''' Medium
You are playing the following Flip Game with your friend: Given a string that contains only these two characters: + and -, you and your friend take turns to flip two consecutive "++" into "--". The game ends when a person can no longer make a move and therefore the other person will be the winner.

Write a function to determine if the starting player can guarantee a win.

Example:

Input: s = "++++"
Output: true 
Explanation: The starting player can guarantee a win by flipping the middle "++" to become "+--+".
Follow up:
Derive your algorithm's runtime complexity.
'''

class Solution(object):
    def canWin(self, s):
        """
        :type s: str
        :rtype: bool
        """
        
        '''
        1 Imagine 2 players, one can move 1 step
        2 You are the 1st player, if 2nd player loose, you win
        3 If 2nd player never lose till the end(you can not move anymore), you lose.
        
        '''
        
        if len(s)<2:return False
        for i in xrange(len(s)-1):
            if s[i:i+2]=='++':
                t=s[:i]+'--'+s[i+2:]
                if self.canWin(t)==False:
                    return True
        return False
        