'''
Given a pattern and a string str, find if str follows the same pattern.

Here follow means a full match, such that there is a bijection between a letter in pattern and a non-empty word in str.

Example 1:

Input: pattern = "abba", str = "dog cat cat dog"
Output: true
Example 2:

Input:pattern = "abba", str = "dog cat cat fish"
Output: false
Example 3:

Input: pattern = "aaaa", str = "dog cat cat dog"
Output: false
Example 4:

Input: pattern = "abba", str = "dog dog dog dog"
Output: false
'''

class Solution(object):
    def wordPattern(self, pattern, str):
        """
        :type pattern: str
        :type str: str
        :rtype: bool
        """
        hash = {}
        
        l = str.split(" ")
        if len(l) != len(pattern):
            return False
        
        for i in range(len(pattern)):
            if pattern[i] not in hash:
            	#这里必须要 1对1 的mapping，不能多个pattern 对同一个word
                if l[i] not in hash.values():
                    hash[pattern[i]] = l[i]
                else:
                    return False
            else:
                if hash[pattern[i]] != l[i]:
                    return False
        return True