'''
Given an array of integers, find two non-overlapping subarrays
 which have the largest sum.The number in each subarray should be contiguous.
Return the largest sum.
'''

'''
Question, how to make sure there is no overlap of 2 global subarray?
'''

class Solution:
    """
    @param nums: A list of integers
    @return: An integer denotes the sum of max two non-overlapping subarrays
    """
    def maxTwoSubArrays(self, nums):
        # write your code here    
        if not nums:
            return
        
        n = len(nums)
        
        local_sum_left = nums[:]
    	global_sum_left = nums[:]
    	
        for i in range(1, n):
            local_sum_left[i] = max(nums[i], local_sum_left[i-1] + nums[i])
            global_sum_left[i] = max(local_sum_left[i], global_sum_left[i-1])
            
        local_sum_right = nums[:]
    	global_sum_right = nums[:]
    	
        for i in range(n-2, -1, -1):
            local_sum_right[i] = max(local_sum_right[i+1] + nums[i], nums[i])# i + 1  means previous value
            global_sum_right[i] = max(local_sum_right[i], global_sum_right[i+1])
            
        mx = -2**31
        for i in range(n - 1):
            mx = max(global_sum_left[i]+global_sum_right[i+1], mx)

        return mx