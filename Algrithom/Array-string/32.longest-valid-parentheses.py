'''
Given a string containing just the characters '(' and ')', find the length of the longest valid (well-formed) parentheses substring. 

 For "(()", the longest valid parentheses substring is "()", which has length = 2. 

 Another example is ")()())", where the longest valid parentheses substring is "()()", which has length = 4.
'''
'''
Thinking:
use stack to store index;
when '(' push, when stack empty and ')' also push.
The stack peak is used to calculate the total length of current match.
然后定义一个最大值变量打擂台即可；初始需要 搞一个 stack = [-1]

'''


class Solution(object):
    def longestValidParentheses(self, s):
        """
        :type s: str
        :rtype: int
        """
        res = 0
        # stack peak is used as the bottom to calculate the length of current matched parenthese
        # at very begining, we should store a init value.
        stack = [-1]
        for i in range(len(s)):
            if s[i] == '(':
                stack.append(i)
            else:
                stack.pop()
                if not stack:
                    # 必须要有托底的东西，否则无法算当前match 的长度
                    stack.append(i)
                else:
                    res = max(res, i - stack[-1])
        return res
        