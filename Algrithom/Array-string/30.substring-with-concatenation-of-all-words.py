'''
You are given a string, s, and a list of words, words, that are all of the same length. Find all starting indices of substring(s) in s that is a concatenation of each word in words exactly once and without any intervening characters.

Example 1:

Input:
  s = "barfoothefoobarman",
  words = ["foo","bar"]
Output: [0,9]
Explanation: Substrings starting at index 0 and 9 are "barfoor" and "foobar" respectively.
The output order does not matter, returning [9,0] is fine too.
Example 2:

Input:
  s = "wordgoodstudentgoodword",
  words = ["word","student"]
Output: []
'''

class Solution(object):
    def findSubstring(self, s, words):
        """
        :type s: str
        :type words: List[str]
        :rtype: List[int]
        """
        if not s or not words:
            return []
        res = []
        
        hash = {}
        for w in words:
            hash[w] = hash.get(w,0) + 1
        
        LEN = len(words[0])
        
        for j in range(LEN):
            currentMap = {}
            start = j
            count = 0
            for i in range(j,len(s)-LEN+1, LEN):
                sub = s[i:i+LEN]
                if sub in hash:
                    if sub in currentMap:
                        currentMap[sub] = currentMap[sub] + 1
                    else:
                        currentMap[sub] = 1
                        
                    count += 1
                
                    while currentMap[sub] > hash[sub]:
                        left = s[start:start+LEN]
                        currentMap[left] = currentMap[left] - 1
                        count -= 1
                        start = start + LEN

                    if count == len(words):
                        res.append(start)
                        left = s[start:start+LEN]
                        currentMap[left] = currentMap[left] -1
                        count -= 1
                        start = start + LEN
                else:
                    currentMap = {}
                    start = i + LEN
                    count = 0
            
        return res   