''' Easy
Write a function that takes a string as input and reverse only the vowels of a string.

Example 1:
Given s = "hello", return "holle".

Example 2:
Given s = "leetcode", return "leotcede".

Note:
The vowels does not include the letter "y".
'''
class Solution(object):
    def reverseVowels(self, s):
        """
        :type s: str
        :rtype: str
        """
        if not s:
            return s
       # s = s.lower()
        l = list(s)
        vowel = set()
        vowel.add('a')
        vowel.add('e')
        vowel.add('i')
        vowel.add('o')
        vowel.add('u')
        
        vowel.add('A')
        vowel.add('E')
        vowel.add('I')
        vowel.add('O')
        vowel.add('U')
        
        i = 0
        j=len(l)-1
        
        while i < j:
            if l[i] not in vowel:
                i += 1
                continue
            if l[j] not in vowel:
                j -= 1
                continue
            if l[i] in vowel and  l[j] in vowel:
                l[i],l[j] = l[j],l[i]
                i += 1
                j -= 1
                
        s = ''.join(l)    
            
        return s
        