'''
Given two arrays, write a function to compute their intersection.
Example
Given nums1 = [1, 2, 2, 1], nums2 = [2, 2], return [2].

Thinking：
intersection 就是交集。

比如[1,2,3,4,5] 和 [4,5,4,5] 就是[4,5]

#最简单的方法就是hashmap， 把第一个nums1 放入hash
第二个遍历，即可
'''

class Solution:
    # @param {int[]} nums1 an integer array
    # @param {int[]} nums2 an integer array
    # @return {int[]} an integer array
    def intersection(self, nums1, nums2):
        # Write your code here
        #find common item
        #1 hash map
        
        if not nums1 or not nums2:
            return []
            
        resHash = {}
        hash1 = {}
        res = []
        for i in range(len(nums1)):
            # hash1[nums1[i]] = hash1.get(nums1[i],0)
            if nums1[i] not in hash1:
                hash1[nums1[i]] = 1
        
        for j in range(len(nums2)):
            if nums2[j] in hash1:
                resHash[nums2[j]] = 1
        
        for key in resHash:
            res.append(key)
        return res
    
    '''
      method2  two pointer,
    '''
    def intersection(self, nums1, nums2):
        nums1 = sorted(nums1)
        nums2 = sorted(nums2)
        
        i,j  = 0,0
        
        resultHash = {}
        
        while i < len(nums1) and j < len(nums2):
            if nums1[i] == nums2[j]:
                if nums1[i] not in resultHash:
                    resultHash[nums1[i]] = 1
                i+=1
                j+=1
            elif nums1[i] < nums2[j]:
                i+=1
            else:
                j+=1
                
        res = []
        for key in resultHash:
            res.append(key)
        return res
        
    # method3 use binary Search to quick search one item in one arary in another.
    # Use binary search, we only need to sort the array need to be searched,like array1
    def intersection(self, nums1, nums2):
        
        def bsearch(target,nums):
            if not nums or target == None:
                return False
            
            start,end = 0,len(nums) - 1
            
            while start + 1 < end:
                mid = start + (end - start) / 2
                if nums[mid] == target:
                    return True
                elif nums[mid] < target:
                    start = mid
                else:
                    end = mid
                    
            if nums[start] == target:
                return True
            if nums[end] == target:
                return True
            return False
            
        
        
        if nums1 == None or nums2 == None:
            return []
        nums1 = sorted(nums1)
        
        resultHash = {}
        
        for i in range(len(nums2)):
            if nums2[i] in resultHash:
                continue
            elif bsearch(nums2[i],nums1):
                resultHash[nums2[i]] = 1
        
        res = []
        for key in resultHash:
            res.append(key)
            
        return res
            
            
        
        
    
    
    
            