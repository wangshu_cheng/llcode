'''
Given a non-empty array of integers, return the k most frequent elements.

For example,
Given [1,1,1,2,2,3] and k = 2, return [1,2].

Note: 
You may assume k is always valid, 1 ≤ k ≤ number of unique elements.
Your algorithm's time complexity must be better than O(n log n), where n is the array's size.
'''

import heapq
class Solution(object):
    def topKFrequent(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: List[int]
        """
        
        h = []
        
        hash = {}
        res = []
        for i in range(len(nums)):
            hash[nums[i]] = hash.get(nums[i],0) + 1
        
        #put to queue, use nums
        for key in hash:
            heapq.heappush(h,(-hash[key],key)) #because this is mini heap, so if you want max heap, use negative
            
        for i in range(k):
            res.append(heapq.heappop(h)[1])
        return res

     #Optimize, do not use len(nums) of heap?

     def topKFrequent(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: List[int]
        """
        
        h = []
        
        hash = {}
        res = []
        for i in range(len(nums)):
            hash[nums[i]] = hash.get(nums[i],0) + 1
        
        #put to queue, use nums
        for key in hash:
            if len(h)<k:
                heapq.heappush(h,(-hash[key],key))
            else:
                if hash[key] > h[-1][0]:
                    heapq.heappush(h,(-hash[key],key))

            
        for i in range(k):
            res.append(heapq.heappop(h)[1])
        return res