'''
Rotate an array of n elements to the right by k steps.

For example, with n = 7 and k = 3, the array [1,2,3,4,5,6,7] is rotated to [5,6,7,1,2,3,4].

Note:
Try to come up as many solutions as you can, there are at least 3 different ways
 to solve this problem.

[show hint]

Hint:
Could you do it in-place with O(1) extra space?
Related problem: Reverse Words in a String II

开始以为这个是partition sort题，后来发现不是。
Thinking: 
每个值都和k后面的值交换若干次，直到交换回原来的位置
从头开始一个一个往后跳步，每一步跳k，循环跳，直到跳到出发点为止；然后开始下一个出发点
注意不是两两交换，而是前面一个和后面一交换，这样每一个点都直接跳到它的目标点。
如果用brute force ，那么就是两个循环，内循环是交换排序。

'''


# 2.

#     1. Divide the array two parts: 1,2,3,4 and 5, 6
#     2. Reverse first part: 4,3,2,1,5,6
#     3. Reverse second part: 4,3,2,1,6,5
#     4. Reverse the whole array: 5,6,1,2,3,4



class Solution(object):
    def rotate(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: void Do not return anything, modify nums in-place instead.
        """


       #method 1 bubble (Not  AC)
        if not nums:
            return
        
        k = k % len(nums)
        
        for i in xrange(k):
            for j in xrange(len(nums)-1,0,-1):
                nums[j],nums[j-1] = nums[j-1],nums[j]


        #method 1.5 use extra space
        L = len(nums)
        
        a = [0 for i in range(L)]
        
        for i in xrange(L):
            a[(i+k)%L] = nums[i]
        
        for i in range(L):
            nums[i] = a[i]

        #method2 best, 三段式:
        def reverse(nums,start,end):
            while(start < end):
                nums[start],nums[end] = nums[end],nums[start]
                start +=1
                end -= 1
                
        if not nums:
            return
        
        k = k % len(nums)
        
        reverse(nums,0,len(nums) -k -1)
        reverse(nums,len(nums) -k, len(nums) -1)
        reverse(nums,0,len(nums) -1)


        #method3 Cyclic replacement

class Solution(object):
    def rotate(self, nums, k):
        """
        :type nums: List[int]
        :type k: int
        :rtype: void Do not return anything, modify nums in-place instead.
        """
        k = k % len(nums)
        pre = 0
        cur = 0
        count = 0
        for i in range(len(nums)):
            if count >= len(nums):
                break
            cur = i
            pre=nums[i]
            for j in range(len(nums)):
                next = (cur + k) % len(nums)
                nums[next],pre = pre,nums[next]
                cur = next
                count += 1
                if cur == i:
                    break


