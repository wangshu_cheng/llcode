'''
Given a string s, partition s such that every substring of the partition is a palindrome.

Return all possible palindrome partitioning of s.

Example
Given s = "aab", return:

[
  ["aa","b"],
  ["a","a","b"]
]
'''

class Solution:
    # @param s, a string
    # @return a list of lists of string
    def partition(self, s):
        # write your code here
        
        def isPalind(s):
            start,end = 0, len(s) - 1
            while start < end:
                if s[start] != s[end]:
                    return False
                start += 1
                end -= 1
            return True
        
        def helper(s,sublist,pos):
            if pos == len(s):
                result.append(list(sublist))
                return
            for i in range(pos ,len(s)):
                prefix = s[pos:i + 1]
                if not isPalind(prefix):
                    continue
                sublist.append(prefix)
                helper(s,sublist,i + 1)
                sublist.pop()
                   
        if not s:
            return []
        
        result = []
        sublist = []
        
        helper(s,sublist,0)
        
        return result
        