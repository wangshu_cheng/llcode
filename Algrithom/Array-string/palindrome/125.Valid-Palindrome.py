'''
Given a string, determine if it is a palindrome, considering only alphanumeric characters and ignoring cases.

Note: For the purpose of this problem, we define empty string as valid palindrome.

Example 1:

Input: "A man, a plan, a canal: Panama"
Output: true
Example 2:

Input: "race a car"
Output: false
'''
class Solution(object):
    def isPalindrome(self, s):
        """
        :type s: str
        :rtype: bool
        """
        newArr=[e.lower() for e in list(s) if e.isalnum() ]
        a=[]
        LEN = len(newArr)
        
        l, r = 0, LEN- 1
        
        while l < r:
            if newArr[l] != newArr[r]:
                return False
            l += 1
            r -= 1
        
        return True