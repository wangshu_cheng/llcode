''' HARD
Given a string S, you are allowed to convert it to a palindrome by adding characters in front of it. Find and return the shortest palindrome you can find by performing this transformation.

For example:

Given "aacecaaa", return "aaacecaaa".

Given "abcd", return "dcbabcd".
'''

class Solution(object):
    def shortestPalindrome(self, s):
        """
        :type s: str
        :rtype: str
        """
        
        j = 0
        
        for i in range(len(s)-1,-1,-1):#找到第一个使他不回文的位置
            if s[j] == s[i]:
                j += 1
        if j == len(s): #本身是回文
            return s
        
        suffix = s[j:] #后缀不能够匹配的字符串
        prefix = suffix[::-1]#前面补充prefix让他和suffix回文匹配

        #How to understand below line??
        mid = self.shortestPalindrome(s[0:j]) #递归调用找 [0,j]要最少可以补充多少个字符让他回文
        ans = prefix + mid + suffix
        return ans
        
        