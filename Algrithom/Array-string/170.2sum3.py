'''
Design and implement a TwoSum class. It should support the following operations: add and find.

and - Add the number to an internal data structure.
find - Find if there exists any pair of numbers which sum is equal to the value.

Example 1:

add(1); add(3); add(5);
find(4) -> true
find(7) -> false
Example 2:

add(3); add(1); add(2);
find(3) -> true
find(6) -> false
'''
class TwoSum(object):

    def __init__(self):
        """
        initialize your data structure here
        """
        self.arr=[]
        self.dic={}
        

    def add(self, number):
        """
        Add the number to an internal data structure.
        :rtype: nothing
        """
        self.arr.append(number)
        #the value is the occurenct of the number
        #dic.get() to get the value of the number, if not exit, return 0
        self.dic[number]=self.dic.get(number,0)+1
      

    def find(self, value):
        """
        Find if there exists any pair of numbers which sum is equal to the value.
        :type value: int
        :rtype: bool
        """
        for i in self.arr:
            if (value-i) in self.dic:
                if i!=value-i:return True
                
                elif i==value-i and self.dic[i] >1:
                    print i,self.dic[i]
                    return True

        return False
        

# Your TwoSum object will be instantiated and called as such:
# twoSum = TwoSum()
# twoSum.add(number)
# twoSum.find(value)