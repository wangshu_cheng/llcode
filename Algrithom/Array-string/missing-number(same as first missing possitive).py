# Given an array containing n distinct numbers taken from 0, 1, 2, ..., n, 
#find the one that is missing from the array.

# For example,
# Given nums = [0, 1, 3] return 2.

class Solution(object):
    def missingNumber(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        #This is o(nlog(n)) - because of sort, not good, try use binary search !
        # nums=sorted(nums)
        # if not nums:return 0
        # if nums[0]!=0:return 0
        # for i in xrange(1,len(nums)):
        #     if nums[i]!=nums[i-1]+1:return nums[i-1]+1
        # return nums[-1]+1
        
        # nums=sorted(nums) will modify array
        # l,r=0,len(nums)-1
        # while l<=r:# if only one item in array,so need check l==r
        #     mid=l+(r-l)/2
        #     if nums[mid]>mid:#means the missing idex in in left,if no missing the idx should == nums[idx]
        #         r=mid-1
        #     else:
        #         l=mid+1
        # return l
        
    #method 1: but it will modify the origin nums
    # 1 GE JIU GE WEI swap A[i] with A[A[i]] if A[i] != i
    # 2 after swap do one more scan, find the A[i] != i


class Solution(object):
    def firstMissingPositive(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if not nums:
            return 1
        
        for i in range(len(nums)):
            while nums[i]>0 and nums[i]<=len(nums) and nums[i]!=i+1:
                
                if nums[nums[i]-1] == nums[i]:
                    break;
                nums[nums[i]-1],nums[i] = nums[i],nums[nums[i]-1]
        
        for i in range(len(nums)):
            if nums[i] != i+1:
                return i+1
            
        return len(nums) + 1
        

 # below solution TTL !!!
    # def missingNumber(self, nums):
    #     """
    #     :type nums: List[int]
    #     :rtype: int
    #     """
    #     if not nums:
    #         return 0
    #     LEN = len(nums)
        
    #     for i in range(LEN):
    #         while i != nums[i] and nums[i] < LEN:
    #             tmp = nums[nums[i]]
    #             nums[nums[i]] = nums[i]
    #             nums[i] = tmp
                
    #     for j in range(LEN):
    #         if nums[j] != j:
    #             return j
    #     return LEN
        


# #method2 math
#     def missingNumber(self, nums):
#         """
#         :type nums: List[int]
#         :rtype: int
#         """
#         total = 0
#         for i in nums:
#             total += i
#         n = len(nums)
#         return n*(n+1)/2 - total
    
# #method3 bit,
#     def missingNumber(self, nums):
#         """
#         :type nums: List[int]
#         :rtype: int
#         """
#         missing = 0
#         for i in range(len(nums)):
#             missing ^= (i+1) ^ nums[i] # why i+1 ^ nums[i] here??
#         return missing
    

