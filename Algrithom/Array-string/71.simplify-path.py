'''
Given an absolute path for a file (Unix-style), simplify it.

For example,
path = "/home/", => "/home"
path = "/a/./b/../../c/", => "/c"

Corner Cases:

Did you consider the case where path = "/../"?
In this case, you should return "/".
Another corner case is the path might contain multiple slashes '/' together, such as "/home//foo/".
In this case, you should ignore redundant slashes and return "/home/foo".
'''

class Solution(object):
    def simplifyPath(self, path):
        """
        :type path: str
        :rtype: str
        """
        if not path:
            return ""
        paths = path.split('/')
        paths = [x for x in paths if x]
        res = []
        for p in paths:
            if p == '..':
                if len(res) > 0:
                    res.pop()
            elif p != '.' and p != '':
                res.append(p + '/')
        
        #Final path must start with "/"
        final_path = "/" + "".join(res)

        #Need remove last "/" if have
        if len(final_path) > 1 and final_path[-1] == '/':
            final_path = final_path[:-1]
        return final_path