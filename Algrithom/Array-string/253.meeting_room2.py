'''
Given an array of meeting time intervals consisting of start and end 
times [[s1,e1],[s2,e2],...] (si < ei), find the minimum number of conference
 rooms required.

For example,
Given [[0, 30],[5, 10],[15, 20]],
return 2.
'''
# Definition for an interval.

# class Interval(object):
#     def __init__(self, s=0, e=0):
#         self.start = s
#         self.end = e

class Solution(object):
    def minMeetingRooms(self, intervals):
        """
        :type intervals: List[Interval]
        :rtype: int
        """
        

        '''
        Method1: Heap:

        '''

        #heap的作用，存放目前来说，最快结束的会议的end time，这样，用下一个会议的start time 和它比，如果比他大，那么就说明不需要新的会议室，等它
        #结束就好了； 如果比它小，那么说明必须要用新的会议室；同时把自己的end time 放入heap，因为它将是下一个最快结束的会议时间。
        
        if not intervals:
            return 0
        
        intervals.sort(key=lambda x: x.start)
        
        h = []
        cnt = 1
        heapq.heappush(h,intervals[0].end)
        
        for i in range(1,len(intervals)):
            if intervals[i].start < h[0]:
                cnt += 1
            else:
                heapq.heappop(h)
            heapq.heappush(h,intervals[i].end)
        return cnt







  
        '''
        Method2 Greedy:
        Good question!
        The key thing is we need sort start and end in 2 lists separately;
        Then loop start, and for each start we check the end, to see if need more 
        room there is one room available)
        If start > end, need one room, if start < end, they is one room avaialbe.
        
        IMPORTANT!!
        *the start time is not the time to free a room, it just a time to know or to 
        check if there is a new room freed, by check the earliest end meetings.
        
        
        Quote from:https://leetcode.com/discuss/50793/my-python-solution-with-explanation
         # Very similar with what we do in real life. Whenever you want to start a meeting, 
         # you go and check if any empty room available (available > 0) and
         # if so take one of them ( available -=1 ). Otherwise,
         # you need to find a new room someplace else ( numRooms += 1 ).  
         # After you finish the meeting, the room becomes available again ( available += 1 ).
         

        '''
        starts=sorted(i.start for i in intervals)
        ends=sorted(i.end for i in intervals)
        
        e=0
        rooms=avails=0
        for s in starts:
            while ends[e]<=s :# first to collect all availabes;
                avails+=1
                e+=1
            
            if avails>0:avails-=1
            else:rooms+=1
        return rooms