'''The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of 
rows like this: (you may want to display this pattern in a fixed font for better legibility)

P   A   H   N
A P L S I I G
Y   I   R
And then read line by line: "PAHNAPLSIIGYIR"
Write the code that will take a string and make this conversion given a number of rows:

string convert(string text, int nRows);
convert("PAYPALISHIRING", 3) should return "PAHNAPLSIIGYIR".
'''

class Solution(object):
    def convert(self, s, numRows):
        """
        :type s: str
        :type numRows: int
        :rtype: str
        """
        '''
        Think:
        The question is about:
        1 Create n strings, each present 1 row
        2 In each string, append correct chars, the char movement is based on the zigzag
        '''
        #make s a list:
    
        l=list(s)
        #Create N strings, or a string array
        
        res=['' for x in range(numRows)]
        i=0
        while i<len(l): # i 控制一共走多少步
            #going down
            for idx in range(numRows): # idx 控制往下走步数
                if i<len(l):
                    res[idx]+=l[i]
                    i+=1
            #going back
            for idx1 in range(numRows-2,0,-1):# idx1 控制往回走步数
                if i<len(l):
                    res[idx1]+=l[i]
                    i+=1
        result = ''.join(res)
        return result