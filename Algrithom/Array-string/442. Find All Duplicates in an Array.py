# 442. Find All Duplicates in an Array
# Medium

# 1118

# 114

# Favorite

# Share
# Given an array of integers, 1 ≤ a[i] ≤ n (n = size of array), some elements appear twice and others appear once.

# Find all the elements that appear twice in this array.

# Could you do it without extra space and in O(n) runtime?

# Example:
# Input:
# [4,3,2,7,8,2,3,1]

# Output:
# [2,3]

# Thinking: 注意题目给出了i 的范围，所以知道一个萝卜一个坑， 可以把值和index关联

class Solution(object):
    def findDuplicates(self, nums):
        """
        :type nums: List[int]
        :rtype: List[int]
        """
        res = []
        if not nums:
            return []
        
        for i in nums:
            index = abs(i)-1
            if nums[index] < 0:
                res.append(abs(index+1))
            
            nums[index] = - nums[index]
        
        return res
        
        