'''
Given a collection of intervals, merge all overlapping intervals.

Have you met this question in a real interview? Yes
Example
Given intervals => merged intervals:

[                     [
  [1, 3],               [1, 6],
  [2, 6],      =>       [8, 10],
  [8, 10],              [15, 18]
  [15, 18]            ]
]

Thinking:
first, sort interval by start time.
then start from 1st, if current start small then previous end, then previous end = max(pre.end,cur.end)
continues compare, if no overlap, save to result.

'''
class Solution:
    # @param intervals, a list of Interval
    # @return a list of Interval
    def merge(self, intervals):
        # write your code here
        if not intervals:
            return []
            
        intervals = sorted(intervals,key=lambda x:x.start)
        res = []
        pre = intervals[0]
        for i in range(1,len(intervals)):
            cur = intervals[i]
            if cur.start <= pre.end:
                pre.end = max(pre.end,cur.end)
            else:
                res.append(pre)
                pre = cur
        res.append(pre)
        return res
                