''' EASY
The count-and-say sequence is the sequence of integers with the first five terms as following:

1.     1
2.     11
3.     21
4.     1211
5.     111221
1 is read off as "one 1" or 11.
11 is read off as "two 1s" or 21.
21 is read off as "one 2, then one 1" or 1211.
Given an integer n, generate the nth term of the count-and-say sequence.

Note: Each term of the sequence of integers will be represented as a string.

Example 1:

Input: 1
Output: "1"

Example 2:
Input: 4
Output: "1211"


'''

class Solution(object):
    def countAndSay(self, n):
        """
        :type n: int
        :rtype: str
        """
        '''
         1.     1
         2.     11
         3.     21
         4.     1211
         5.     111221 
         6.     312211
         7.     13112221
         8.     1113213211
         9.     31131211131221
         10.   13211311123113112211
         
        See the example, the next string is count and say of pre string
        '''
        #solution 1:

        if n <=0:
            return None
        res = '1'
        #because we start from 1,totally n-1 loop
        for i in range(n-1):
            tmp = ''
            count = 1
            for j in range(1,len(res)):
                if res[j] == res[j-1]:
                    count += 1
                else:
                    tmp += str(count)
                    tmp += res[j-1]
                    count = 1
            tmp += str(count)
            tmp += res[-1]
            res = tmp
        return res
        








        #solution 2
 class Solution(object):
    def countAndSay(self, n):
        def sayit(s):
            res=''
            count=1
            tmp=''
            say=''
            if s == '1':return '11'
            s+='*'
            for j in xrange(len(s)):
                if s[j]!=say:
                    if say!='':
                        tmp=str(count)+say
                    say=s[j]
                    count=1
                    res+=tmp
                else:
                    count+=1
                    tmp=str(count)+say
            return res
        
        s=str(1)
        for _ in xrange(n-1):
            s = sayit(s)
        return s