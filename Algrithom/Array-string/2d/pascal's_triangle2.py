class Solution(object):
    def getRow(self, rowIndex):
        """
        :type rowIndex: int
        :rtype: List[int]
        """
        '''
        Since we only need the k row, we don't need maintain a 2D array
        So can make a row add it self from base case
        '''
        #base case:
        row0=[1]
        row1=[1,1]
        if rowIndex==0:return row0
        if rowIndex==1:return row1
        #now handle k>=2 case:
        row=row1
        # for i in xrange(1,len(row))
        while len(row)<rowIndex+1:
            row=[row[i]+row[i-1] for i in xrange(1,len(row))]
            row=[1]+row+[1]
        return row