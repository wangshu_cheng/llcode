'''
Determine if a 9x9 Sudoku board is valid. Only the filled cells need to be validated according to the following rules:

Each row must contain the digits 1-9 without repetition.
Each column must contain the digits 1-9 without repetition.
Each of the 9 3x3 sub-boxes of the grid must contain the digits 1-9 without repetition.

The Sudoku board could be partially filled, where empty cells are filled with the character '.'.

Example 1:

Input:
[
  ["5","3",".",".","7",".",".",".","."],
  ["6",".",".","1","9","5",".",".","."],
  [".","9","8",".",".",".",".","6","."],
  ["8",".",".",".","6",".",".",".","3"],
  ["4",".",".","8",".","3",".",".","1"],
  ["7",".",".",".","2",".",".",".","6"],
  [".","6",".",".",".",".","2","8","."],
  [".",".",".","4","1","9",".",".","5"],
  [".",".",".",".","8",".",".","7","9"]
]
Output: true
Example 2:

Input:
[
  ["8","3",".",".","7",".",".",".","."],
  ["6",".",".","1","9","5",".",".","."],
  [".","9","8",".",".",".",".","6","."],
  ["8",".",".",".","6",".",".",".","3"],
  ["4",".",".","8",".","3",".",".","1"],
  ["7",".",".",".","2",".",".",".","6"],
  [".","6",".",".",".",".","2","8","."],
  [".",".",".","4","1","9",".",".","5"],
  [".",".",".",".","8",".",".","7","9"]
]
Output: false
Explanation: Same as Example 1, except with the 5 in the top left corner being 
    modified to 8. Since there are two 8's in the top left 3x3 sub-box, it is invalid.
Note:

A Sudoku board (partially filled) could be valid but is not necessarily solvable.
Only the filled cells need to be validated according to the mentioned rules.
The given board contain only digits 1-9 and the character '.'.
The given board size is always 9x9.

'''

class Solution(object):
    def isValidSudoku(self, board):
        """
        :type board: List[List[str]]
        :rtype: bool
        """
        '''
        Key thing:
        use k = i / 3 * 3 + j / 3 to present the subbox.!!!
        (i/3*3 is not equal to i !!, it can only be 0,3,6,given i from 0 to 8)
        and j/3 can only be 0,1,2, so total sub box can be 0,1,2,3,4,5,6,7,8, totally 9 !
        
        So create 3 hash table, each present row, column and sub-box which current node in .
        in each those 3 hash table, any number's count can be maximum 1.(no num can be show up twice)
        
        '''
        if not board:return False
        m,n=len(board),len(board[0])
        check_row=[[0 for i in range(9)] for j in range(9)]#three 2d array to check each row, col and sub box
        check_col=[[0 for i in range(9)] for j in range(9)]#three 2d array to check each row, col and sub box
        check_box=[[0 for i in range(9)] for j in range(9)]#three 2d array to check each row, col and sub box
        for i in xrange(m):
            for j in xrange(n):
                if board[i][j] != '.':
                    num=int(board[i][j])-1 # need -1 becasue the index of array is 0~8
                    k=i/3*3+j/3
                    #because if previously the same number of same row,col or box have exist, it is false
                    if check_row[i][num] or check_col[j][num] or check_box[k][num]:
                        return False
                    #assign value to all the checking 2d arrayes
    
                    check_row[i][num]=check_col[j][num]= check_box[k][num]=1
        return True
                