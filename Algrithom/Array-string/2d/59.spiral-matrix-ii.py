'''
Given a positive integer n, generate a square matrix filled with elements from 1 to n2 in spiral order.

Example:

Input: 3
Output:
[
 [ 1, 2, 3 ],
 [ 8, 9, 4 ],
 [ 7, 6, 5 ]
]
'''


class Solution(object):
    def generateMatrix(self, n):
        """
        :type n: int
        :rtype: List[List[int]]
        """
        result = [[0 for i in range(n)] for j in range(n)]
        total = n*n
        k=1
        top = 0
        bottom = n-1
        left=0
        right=n-1
        
        while k<=total:
            for i in range(left,right+1):
                result[top][i] = k
                k+=1
            top += 1
            
            for i in range(top,bottom+1):
                result[i][right] = k
                k+=1
            right -= 1
            
            for i in range(right,left-1,-1):
                result[bottom][i] = k
                k+=1
            bottom -= 1
            
            for i in range(bottom,top-1,-1):
                result[i][left] = k
                k+=1
            left +=1
        return result
        