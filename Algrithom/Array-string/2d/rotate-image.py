'''
You are given an n x n 2D matrix representing an image.

Rotate the image by 90 degrees (clockwise).

Note:

You have to rotate the image in-place, which means you have to modify the input 2D matrix directly. DO NOT allocate another 2D matrix and do the rotation.

Example 1:

Given input matrix = 
[
  [1,2,3],
  [4,5,6],
  [7,8,9]
],

rotate the input matrix in-place such that it becomes:
[
  [7,4,1],
  [8,5,2],
  [9,6,3]
]
Example 2:

Given input matrix =
[
  [ 5, 1, 9,11],
  [ 2, 4, 8,10],
  [13, 3, 6, 7],
  [15,14,12,16]
], 

rotate the input matrix in-place such that it becomes:
[
  [15,13, 2, 5],
  [14, 3, 4, 1],
  [12, 6, 8, 9],
  [16, 7,10,11]
]
'''
class Solution(object):
    def rotate(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: void Do not return anything, modify matrix in-place instead.
        """
        '''
        先上下翻转（不是180度转，只是最后一行变第一行etc）
        然后沿着－45度对角线翻转
        '''
        n=len(matrix)
        #1 for each row, reverse the row index, means row[n-1] become row[0]...etc
        
        for i in xrange(n/2):
            matrix[n-i-1],matrix[i]=matrix[i],matrix[n-i-1]
        #above line can be replaced by matrix.reverse();
        
        #2 -45 flip:
        for i in xrange(n):
            for j in xrange(i):#here is to i not to N !!!!
                # flip when j=0 - i-1;  When i==j, don't need flip
                matrix[i][j],matrix[j][i]=matrix[j][i],matrix[i][j]
        
            