# This not dp

#thinking: parscal's 特点：每一行的元素等于它的列树，比如第二行，它就有2个元素，第3行就是3个元素
#所以 可以用 for j in range(1,i) 

class Solution(object):
    def generate(self, numRows):
        """
        :type numRows: int
        :rtype: List[List[int]]
        """
        '''
        每一行除了第一和最后一个数字外，其他数字j都是上一行 j + j-1 的值
        '''
        m=numRows
        
        #boarder case:
        if m==0:return []
        if m==1:return [[1]]
        if m==2:return [[1],[1,1]]
        
        #rows means total 2d rows
        rows=[[] for i in xrange(m)]
        rows[0]=[1]
        rows[1]=[1,1]

        for i in xrange(2,m):
            rows[i]=[1]#first value is always 1
            #now add in middle values:
            for j in xrange(1,i):
                rows[i].append(rows[i-1][j]+rows[i-1][j-1])
            #add last value, which is also always 1
            rows[i].append(1)

        return rows