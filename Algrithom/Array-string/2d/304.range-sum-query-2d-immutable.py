'''
Given a 2D matrix matrix, find the sum of the elements inside the rectangle defined by its upper left corner (row1, col1) and lower right corner (row2, col2).

Range Sum Query 2D
The above rectangle (with the red border) is defined by (row1, col1) = (2, 1) and (row2, col2) = (4, 3), which contains sum = 8.

Example:
Given matrix = [
  [3, 0, 1, 4, 2],
  [5, 6, 3, 2, 1],
  [1, 2, 0, 1, 5],
  [4, 1, 0, 1, 7],
  [1, 0, 3, 0, 5]
]

sumRegion(2, 1, 4, 3) -> 8
sumRegion(1, 1, 2, 2) -> 11
sumRegion(1, 2, 2, 4) -> 12
Note:
You may assume that the matrix does not change.
There are many calls to sumRegion function.
You may assume that row1 ≤ row2 and col1 ≤ col2.

记住presum 要开N+1长度数组， 当前点记录前一点一直到最开始之和，不包括当前点。
'''
class NumMatrix(object):

    def __init__(self, matrix):
        """
        :type matrix: List[List[int]]
        """
        if not matrix or len(matrix[0]) == 0:
            return
        M = len(matrix)
        N = len(matrix[0])
        self.dp = [[0 for i in range(N+1)]  for j in range(M)] # in each row, dp[i][j] means 该点之前所有点之和
        
        
        for i in range(M):
            self.dp[i][0] = matrix[i][0]
            for j in range(1,N+1):
                # So for every row, i, the row sum is stored in dp[i][j] , dp[i][j] means the row sum till j
                self.dp[i][j] = self.dp[i][j-1] + matrix[i][j-1]
        

    def sumRegion(self, row1, col1, row2, col2):
        """
        :type row1: int
        :type col1: int
        :type row2: int
        :type col2: int
        :rtype: int
        """
        total = 0
        for row in range(row1,row2+1):
            total += self.dp[row][col2+1] - self.dp[row][col1]
        return total
        


# Your NumMatrix object will be instantiated and called as such:
# obj = NumMatrix(matrix)
# param_1 = obj.sumRegion(row1,col1,row2,col2)