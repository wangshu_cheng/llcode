'''
Given a m x n matrix, if an element is 0, set its entire row and column to 0. Do it in-place.

Example 1:

Input: 
[
  [1,1,1],
  [1,0,1],
  [1,1,1]
]
Output: 
[
  [1,0,1],
  [0,0,0],
  [1,0,1]
]
Example 2:

Input: 
[
  [0,1,2,0],
  [3,4,5,2],
  [1,3,1,5]
]
Output: 
[
  [0,0,0,0],
  [0,4,5,0],
  [0,3,1,0]
]
'''

class Solution(object):
    def setZeroes(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: void Do not return anything, modify matrix in-place instead.
        """
        #Key thing is firstly change the line and row to * or sth else, it to avoid cascade set 0
        
        m,n=len(matrix),len(matrix[0])
        for i in xrange(m):
            for j in xrange(n):
                if matrix[i][j]==0:
                    #set this row to 'x'
                    for k in xrange(n):
                        if matrix[i][k]!=0: matrix[i][k]='*'
                    #set this col to 'x'
                    for k in xrange(m):
                        if matrix[k][j]!=0: matrix[k][j]='*'
        #now change '*' to zero
        for i in xrange(m):
            for j in xrange(n):
                if matrix[i][j]=='*':
                    matrix[i][j]=0