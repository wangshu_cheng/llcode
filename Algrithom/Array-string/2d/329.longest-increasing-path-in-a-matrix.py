'''

Given an integer matrix, find the length of the longest increasing path.

From each cell, you can either move to four directions: left, right, up or down. You may NOT move diagonally or move outside of the boundary (i.e. wrap-around is not allowed).

Example 1:

Input: nums = 
[
  [9,9,4],
  [6,6,8],
  [2,1,1]
] 
Output: 4 
Explanation: The longest increasing path is [1, 2, 6, 9].
Example 2:

Input: nums = 
[
  [3,4,5],
  [3,2,6],
  [2,2,1]
] 
Output: 4 
Explanation: The longest increasing path is [3, 4, 5, 6]. Moving diagonally is not allowed.


Thinking: DFS/BFS  + cache

'''

class Solution(object):
    def longestIncreasingPath(self, matrix):
        """
        :type matrix: List[List[int]]
        :rtype: int
        """
        #DFS + cache, cache store the longest increaing path staring this point.
        
        def dfs(matrix,x,y,cache):
            if cache[x][y] != 0:
                return cache[x][y]
            
            n = len(matrix[0])
            m = len(matrix)
            drs = [(-1,0),(1,0),(0,-1),(0,1)]
            for dr in drs:
                x1 = x + dr[0]
                y1 = y + dr[1]
                if x1<m and x1>=0 and y1<n and y1>=0 and matrix[x1][y1] > matrix[x][y]:
                    cache[x][y] = max(cache[x][y],dfs(matrix,x1,y1,cache))
            #Plus 1, because from x,y to x1,y1 is 1 step
            cache[x][y] += 1
            return cache[x][y]
        
        res = 0
        cache = [[0 for i in range(len(matrix[0]))] for j in range(len(matrix))]
        for i in range(len(matrix)):
            for j in range(len(matrix[0])):
                res = max(res,dfs(matrix,i,j,cache))
        return res
                    
                