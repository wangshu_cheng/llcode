'''
Given a 2D binary matrix filled with 0's and 1's, find the largest square containing only 1's and return its area.

Example:

Input: 

1 0 1 0 0
1 0 1 1 1
1 1 1 1 1
1 0 0 1 0

Output: 4

'''

dp[i,j] 以i，j点为正方形右下角的最大边长


class Solution(object):
    def maximalSquare(self, matrix):
        """
        :type matrix: List[List[str]]
        :rtype: int
        
        """
        if len(matrix)==0: return 0
        
        M=len(matrix[0])#this is column,or x axis
        N=len(matrix)#this is row,or y axis
    
        dp={}
        max_size = 0
        for i in range(0,M):
            dp[(0,i)]=int(matrix[0][i])
            max_size = max(max_size,dp[(0,i)])
    
        for j in range(0,N):
            dp[(j,0)]=int(matrix[j][0])
            max_size = max(max_size,dp[(j,0)])
    
    
        for i in range(1,N):
            for j in range(1,M):
                if matrix[i][j]=='0':
                    dp[(i,j)] = 0;
                else:
                    #so hard to think!!
                    dp[(i,j)] = min(dp[(i-1,j)],dp[(i,j-1)],dp[(i-1,j-1)]) + 1
                    
                max_size = max(max_size,dp[(i,j)])
    
        return max_size*max_size