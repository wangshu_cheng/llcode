''' MEDIUM
Find the total area covered by two rectilinear rectangles in a 2D plane.

Each rectangle is defined by its bottom left corner and top right corner as shown in the figure.

Rectangle Area

Example:

Input: A = -3, B = 0, C = 3, D = 4, E = 0, F = -1, G = 9, H = 2
Output: 45

This problem can be converted as a overlap internal problem. On the x-axis, there are (A,C) and (E,G); on the y-axis, there are (F,H) and (B,D). If they do not have overlap, the total area is the sum of 2 rectangle areas. If they have overlap, the total area should minus the overlap area.
'''
class Solution(object):
    def computeArea(self, A, B, C, D, E, F, G, H):
        """
        :type A: int
        :type B: int
        :type C: int
        :type D: int
        :type E: int
        :type F: int
        :type G: int
        :type H: int
        :rtype: int
        """
        
        #1 check X -axis, if no overlap,then total area is sum of 2 rect 
        if C<E or G<A:
            return (G-E)*(H-F) + (C-A)*(D-B)
        
        #2 check Y -axis, if no overlap,then total area is sum of 2 rect 
        if D<F or H<B:
            return (G-E)*(H-F) + (C-A)*(D-B)
        left = max(A,E)
        right = min(G,C)
        top = min(D,H)
        bottom = max(B,F)
        
        return (G-E)*(H-F) + (C-A)*(D-B) - (right-left)*(top-bottom)
        
