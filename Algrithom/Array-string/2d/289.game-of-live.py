'''
Medium
According to the Wikipedia's article: "The Game of Life, also known simply as Life, is a cellular automaton devised by the British mathematician John Horton Conway in 1970."

Given a board with m by n cells, each cell has an initial state live (1) or dead (0). Each cell interacts with its eight neighbors (horizontal, vertical, diagonal) using the following four rules (taken from the above Wikipedia article):

Any live cell with fewer than two live neighbors dies, as if caused by under-population.
Any live cell with two or three live neighbors lives on to the next generation.
Any live cell with more than three live neighbors dies, as if by over-population..
Any dead cell with exactly three live neighbors becomes a live cell, as if by reproduction.
Write a function to compute the next state (after one update) of the board given its current state. The next state is created by applying the above rules simultaneously to every cell in the current state, where births and deaths occur simultaneously.

Example:

Input: 
[
  [0,1,0],
  [0,0,1],
  [1,1,1],
  [0,0,0]
]
Output: 
[
  [0,0,0],
  [1,0,1],
  [0,1,1],
  [0,1,0]
]
'''
class Solution(object):
    def gameOfLife(self, board):
        """
        :type board: List[List[int]]
        :rtype: void Do not return anything, modify board in-place instead.
        """
        if not board or not board[0]:
            return
        m = len(board)
        n = len(board[0])
        
        drs = [(0,1),(0,-1),(1,0),(-1,0),(1,1),(-1,-1),(1,-1),(-1,1)]
        
        for i in range(m):
            for j in range(n):
                cnt = 0
                for dr in drs:
                    x = i+dr[0]
                    y = j+dr[1]
                    if x>=0 and x<m and y>=0 and y<n and (board[x][y]&1) == 1:
                        cnt += 1
                #<2 next state is die
                if cnt <2:
                    board[i][j] &= 1
                # 2 or 3 next state is keep same board[i][j] <<1 means next step, next step or current step means no change
                if cnt ==2 or cnt == 3:
                    board[i][j] |= board[i][j] <<1 
                if cnt == 3:
                    board[i][j] |= 2
                if cnt > 3:
                    board[i][j] &=1
                        
        #board = [[board[i][j]>>1 for j in range(n)] for i in range(m)]
        for i in range(m):
            for j in range(n):
                board[i][j] = board[i][j] >> 1
                        