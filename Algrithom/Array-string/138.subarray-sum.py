'''
Given an integer array, find a subarray where the sum of numbers is zero. 
Your code should return the index of the first number and the index of the
 last number.

 Thinking:presum[i] --- i点之前(不包括i)的subarray sum
 presum = [0 for i in range(len(nums)+1)]
 然后找presum 相等的2个index，用dic
'''

class Solution:
    """
    @param nums: A list of integers
    @return: A list of integers includes the index of the first number 
             and the index of the last number
    """
    BEST: presum 
    O(n) + N space
    def subarraySum(self, nums):
        # write your code here
        dic = {}
        
        '''
        presum defination:
        the sum end at index + 1 或者说结束于第几个element

		'''
        presum = [0 for i in range(len(nums) + 1)] # we need n+1 length, since we store a dumy 0.
    
        '''
        presum dictionary
        '''
        dic[presum[0]] = 0 # Don't forget!!

        #main:
        #We only need to find 2 equal presum, to find equal, use dic:
        for i in range(1,len(nums) + 1):
            presum[i] = presum[i-1] + nums[i-1]
            
            if presum[i] in dic:
            	'''
            	 here is not    [dic[presum[i]] - 1,i - 1]
            	 这里第一个index其实是 dic[presum[i]] - 1 + 1， dic[presum[i]] - 1
            	 代表前一个presum结束的index，但是题目要求subarray的start index，那么就是 再+1
            	'''
                return [dic[presum[i]],i - 1]
            dic[presum[i]] = i
    
        return []

    '''
    Brute force - pointer:
    O(n2) + 1 space
    '''
    def subarraySum(self, nums): 
        # write your code here
        #1 scan, 1 pointer 2 loop O(n**2)
        
        if not nums:
            return []
            
        n = len(nums)
        for i in range(len(nums)):
            count = 0
            for j in range(i,len(nums)):
                count += nums[j]
                if count == 0:
                    return [i,j]
        return []

            
        
        
