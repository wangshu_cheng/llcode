'''
Given n points on a 2D plane, find the maximum number of points 
that lie on the same straight line.

thinking:
以某个点位起点，到该点的斜率相同的点都在一条线上。
所以，以某个点位起点，求所有其他的点到它的斜率，找出最大的相同斜率的值。
不需要回头，所以第二个循环从 i 开始，就像求所有的substring 一样。

'''

# Use gradient as key, same gradient number as value


class Solution:
    # @param {int[]} points an array of point
    # @return {int} an integer
    def maxPoints(self, points):
        # Write your code here
        len_points = len(points)
        if len_points <= 1:
            return len_points
        max_count = 0
        for i in range(0, len_points):
            p1 = points[i]
            gradients = {}
            infinite_count = 0
            duplicate_count = 0
            for j in range(i, len_points):
                p2 = points[j]
                dx = p2.x - p1.x
                dy = p2.y - p1.y
                if 0 == dx and 0 == dy:
                    duplicate_count += 1
                if 0 == dx:
                    infinite_count += 1
                else:
                    g = float(dy) / dx
                    gradients[g] = (gradients[g] + 1 if gradients.has_key(g) else 1)
            if infinite_count > max_count:
                max_count = infinite_count
            for k, v in gradients.items():
                v += duplicate_count#how much duplciated for the starting i point
                if v > max_count:
                    max_count = v
        return max_count