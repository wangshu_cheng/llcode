'''
Find the contiguous subarray within an array (containing at least one number) which has the largest product.
用local max做
'''
class Solution:
    # @param nums: an integer[]
    # @return: an integer
    def maxProduct(self, nums):
        # write your code here
        if not nums:
            return 0
        n = len(nums)
        lmin = [0 for i in range(n)]
        lmax = [0 for i in range(n)]
        
        lmin[0] = nums[0]
        lmax[0] = nums[0]

        res = nums[0]
        
        for i in range(1,n):
  
            if nums[i] > 0:
                lmax[i] = max(lmax[i-1] * nums[i],nums[i])
                lmin[i] = min(lmin[i-1] * nums[i],nums[i])
            else:
                lmax[i] = max(lmin[i-1] * nums[i],nums[i])
                lmin[i] = min(lmax[i-1] * nums[i],nums[i])

            res = max(res,lmax[i])
            
        return res

        # f, g = [], []
        # f.append(nums[0])
        # g.append(nums[0])
        # for i in xrange(1, len(nums)):
        #     f.append(max(f[i-1]*nums[i], g[i-1]*nums[i], nums[i]))
        #     g.append(min(f[i-1]*nums[i], g[i-1]*nums[i], nums[i]))
        # m = f[0]
        # for i in xrange(1, len(f)): m = max(m, f[i])
        # return m