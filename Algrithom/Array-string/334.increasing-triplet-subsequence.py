'''
Given an unsorted array return whether an increasing subsequence of length 3 exists or not in the array.

Formally the function should:
Return true if there exists i, j, k 
such that arr[i] < arr[j] < arr[k] given 0 ≤ i < j < k ≤ n-1 else return false.
Your algorithm should run in O(n) time complexity and O(1) space complexity.

Examples:
Given [1, 2, 3, 4, 5],
return true.

Given [5, 4, 3, 2, 1],
return false.
'''

class Solution(object):
    def increasingTriplet(self, nums):
        """
        :type nums: List[int]
        :rtype: bool
        """
        #解法一，比较好想些：
        #搞一个数组，存入每个元素是否之前有比他小的元素，true or false； 搞一个循环，从后往前扫，如果最大值大于当前值，而且当前值有前面小值，则ok
        
        if len(nums) < 3: return False
        
        has_pre_smaller = [False for i in range(len(nums))]
        
        small = nums[0]
        
        for i in range(1,len(nums)):
            if small < nums[i]:
                has_pre_smaller[i] = True
            else:
                small = min(small,nums[i])
        
        large = nums[-1]
        for j in range(len(nums)-2,-1,-1):
            if has_pre_smaller[j] == True:
                 if large > nums[j]:
                     return True
                 else:
                     large = max(large,nums[j])
        
        return False
          
          #method2, use 3 varible to record smallest so far, 2nd smallest so far, and current, if current > 2nd small, then True
          # smallest will be replaced if current < smallest, 2nd smallest will ONLY be replaced when current < 2nd small and 1st smal already been replaced.
        if len(nums) < 3: return False  
        
        s1 = nums[0]
        s2 = 2**32
        
        for i in range(1,len(nums)):
            if s1 >= nums[i]:
                s1 = nums[i]
            elif s2 >= nums[i]:
                s2 = nums[i]
            else:
                return True
        return False
            
        
        
    
      