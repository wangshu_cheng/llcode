'''
Given an array of integers, find the subarray with smallest sum.

Return the sum of the subarray.

 Notice

The subarray should contain one integer at least.

Have you met this question in a real interview? Yes
Example
For [1, -1, -2, 1], return -3.


'''

class Solution:
    """
    @param nums: a list of integers
    @return: A integer denote the sum of minimum subarray
    """
    def minSubArray(self, nums):
        # write your code here
        if not nums:
            return
        
        lmin = [0 for i in range(len(nums))]
        gmin = [0 for i in range(len(nums))]
        
        lmin[0] = gmin[0] = nums[0]
        
        for i in range(1,len(nums)):
            lmin[i] = min(lmin[i - 1] + nums[i],nums[i])
            gmin[i] = min(lmin[i],gmin[i - 1])
            
        return gmin[-1]

