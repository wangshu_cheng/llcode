 '''
 Remove Duplicates from Sorted Array

 Description
 Notes
 Testcase
 Judge
Given a sorted array, remove the duplicates in place such that each element appear only once and
 return the new length.

Do not allocate extra space for another array, you must do this in place with constant memory.

Have you met this question in a real interview? Yes
Example
Given input array A = [1,1,2],

Your function should return length = 2, and A is now [1,2].

Thinking:
用lazy 指针
一定要记住的技巧，单指针，当发现有不一样的元素的时候指针才移动，也就是说，每次移动都填入不同的值，
这样来去重。
'''

class Solution:
    """
    @param A: a list of integers
    @return an integer
    """
    def removeDuplicates(self, A):
        # write your code here
        if not A:
            return 0
            
        end = len(A) - 1
        lazy = 0
        for i in range(1,len(A)):
            if A[i] != A[i - 1]:
                lazy += 1
                A[lazy] = A[i]

        return lazy + 1