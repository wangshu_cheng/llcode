'''
Merge two given sorted integer array A and B into a new sorted integer array.

Have you met this question in a real interview? Yes
Example
A=[1,2,3,4]

B=[2,4,5,6]

return [1,2,2,3,4,4,5,6]


'''
class Solution:
    #@param A and B: sorted integer array A and B.
    #@return: A new sorted integer array
    def mergeSortedArray(self, A, B):
        # write your code here
        
        if not A:
            return B
        if not B:
            return A
        
        res = []
        
        p1,p2 = 0,0
        
        while p1 < len(A) and p2 < len(B):
            if A[p1] < B[p2]:
                res.append(A[p1])
                p1 += 1
            else:
                res.append(B[p2])
                p2 += 1
        
        if p1 < len(A):
            res += A[p1:]
        elif p2 < len(B):
            res += B[p2:]
        return res
            
        
        