class Solution(object):
    def selfDividingNumbers(self, left, right):
        """
        :type left: int
        :type right: int
        :rtype: List[int]
        """
        #thinking: for each num, get each digit and divide
        res = []

        
        def isValid(i):
            #get most right:
            cur = i
            while cur:
                if cur%10 ==0 or i%(cur%10) !=0:
                    return False
                cur/=10
            return True
        
        for i in range(left,right + 1):
            if isValid(i):
                res.append(i)
        return res