class Solution:
# @param {integer} n
# @return {integer}

'''
Firslty, assume all of them are prime;
2nd, mark all prime*prime+n*prime to non-prmie.
'''
    def countPrimes(self, n):
        if n < 3:
            return 0
        primes = [True] * n
        primes[0] = primes[1] = False
        for i in range(2, int(n ** 0.5) + 1):
            if primes[i]:
            	'''

It will mark out the numbers as none-prime:
4: 100 : 2 —> 4,6,8,10,12,14 … 100
which makes prime[4] =False, prime[6]=False, prime[8]=False…

similarly:
when i ==3:
9: 100 : 3 – > 9,12,15,18,21,24… 96, 99
when i == 4:
16: 100 :4 --> 16,20,24,28… 96,100
            	'''
                primes[i * i: n: i] = [False] * len(primes[i * i: n: i])
        return sum(primes)