'''
Given two integers representing the numerator and denominator of a fraction, return the fraction in string format.

If the fractional part is repeating, enclose the repeating part in parentheses.

Example 1:

Input: numerator = 1, denominator = 2
Output: "0.5"
Example 2:

Input: numerator = 2, denominator = 1
Output: "2"
Example 3:

Input: numerator = 2, denominator = 3
Output: "0.(6)"
'''
class Solution(object):
    def fractionToDecimal(self, numerator, denominator):
        """
        :type numerator: int
        :type denominator: int
        :rtype: str
        """
        if numerator == 0:
            return "0"
        
        fraction = ""
        
        #If either 分子或分母是负数，不同时 use ^
        if (numerator < 0) ^ (denominator < 0):
            fraction += '-'
        
        dividend = abs(numerator) #除数
        divisor =  abs(denominator)#被除数
        
        fraction += (str(dividend/divisor))
        remainder = dividend % divisor
        
        if remainder == 0:
            return str(fraction)
        fraction += "."
        
        hash = {}
        
        while remainder != 0:
            # remainder in hash means 开始循环了
            if remainder in hash:
                fraction = fraction[ :hash[remainder]] + "(" + fraction[hash[remainder] : ] # insert "(" in the position of hash[remainder]
                fraction += ")"
                break
            hash[remainder] = len(fraction)
            remainder *= 10
            fraction += str(remainder/divisor)
            remainder %= divisor
        return str(fraction)
                
        
        