'''
Given a 32-bit signed integer, reverse digits of an integer.

Example 1:

Input: 123
Output: 321
Example 2:

Input: -123
Output: -321
Example 3:

Input: 120
Output: 21
Note:
Assume we are dealing with an environment which could only store integers within the 32-bit signed integer range: [−231,  231 − 1]. For the purpose of this problem, assume that your function returns 0 when the reversed integer overflows.
'''

class Solution(object):
    def reverse(self, x):
        """
        :type x: int
        :rtype: int
        """
        t=x
        rev=0
        if x==0: 
        	return 0
        if t<0:
        	t=-t
        while(t>0):
            rev=rev*10+t%10
            t/=10
        if rev>2**31-1: 
        	return 0
        
        if x<0:
        	return -1*rev
        else:
        	return rev