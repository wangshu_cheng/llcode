''' Medium
A group of two or more people wants to meet and minimize the total travel distance. You are given a 2D grid of values 0 or 1, where each 1 marks the home of someone in the group. The distance is calculated using Manhattan Distance, where distance(p1, p2) = |p2.x - p1.x| + |p2.y - p1.y|.

Thinking:
If use normal bfs method (search every cell, then will be O(m2n2) time complex, so time out.

Why O(m2n2) ? because first, loop  each cell, O(mn), and for each cell you need search O(mn)
so O(mn*mn) = O(m2n2)

By using math, you can convert this question to:
This problem is converted to find the median value on x-axis and y-axis.



 )

'''

class Solution(object):
    def minTotalDistance(self, grid):
        """
        :type grid: List[List[int]]
        :rtype: int
        """
        m = len(grid)
        n = len(grid[0])
        
        cols = []
        rows = []
        
        for i in range(m):
            for j in range(n):
                #Add in people location
                if grid[i][j] == 1:
                    cols.append(j)
                    rows.append(i)
        
        total = 0
        #row is already in seq, so no need sort.
        for r in rows:
            total += abs(r - rows[len(rows)/2])
        cols = sorted(cols)
        for c in cols:
            total += abs(c - cols[len(cols)/2])
        return total