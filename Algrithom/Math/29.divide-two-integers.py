'''
Given two integers dividend and divisor, divide two integers without using multiplication, division and mod operator.

Return the quotient after dividing dividend by divisor.

The integer division should truncate toward zero.

Example 1:

Input: dividend = 10, divisor = 3
Output: 3
Example 2:

Input: dividend = 7, divisor = -3
Output: -2
Note:

Both dividend and divisor will be 32-bit signed integers.
The divisor will never be 0.
Assume we are dealing with an environment which could only store integers within the 32-bit signed integer range: [−231,  231 − 1]. For the purpose of this problem, assume that your function returns 231 − 1 when the division result overflows.

Thinking:
This problem can be solved based on the fact that any number can be converted to the format of the following:

num=a_0*2^0+a_1*2^1+a_2*2^2+...+a_n*2^n

'''

class Solution(object):
    def divide(self, dividend, divisor):
        """
        :type dividend: int
        :type divisor: int
        :rtype: int
        """
        
        '''
        Use bit maniplation,
        Division is just continously substraction,
        So we can use dividend to substract the divisor, if >0, than left shift divisor(we are not using multiplication, yeah..!) if still can substract, left shift divisor again, etc, till it can not, and get the remainder.We now have the base value of the result.
        Than use the remainder to repeatly do same thing , get the result as well, now use base result + this result, we get the final result
        
        '''
        #check sign:
        if divisor==0:return 2**31
        positive = (dividend<0) is (divisor<0) #表示符号相同
        dividend, divisor = abs(dividend), abs(divisor)
        
        res=0
        while dividend>=divisor:
        	#如果分子无法除以放大以后的分母了，那么把分子重新设回到原先的值
            tmp,i=divisor,1
            #把分母两部两部放大；加快速度
            while dividend>=tmp:
                dividend-=tmp
                res+=i
                i<<=1
                tmp<<=1
        if not positive:
            res = -res
            
        res = min(max(-2147483648, res), 2147483647)
        return res
            
            
