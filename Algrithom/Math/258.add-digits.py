'''
Given a non-negative integer num, repeatedly add all its digits until the result has only one digit.

Example:

Input: 38
Output: 2 
Explanation: The process is like: 3 + 8 = 11, 1 + 1 = 2. 
             Since 2 has only one digit, return it.
Follow up:
Could you do it without any loop/recursion in O(1) runtime?
'''
#Solution1:recursive:
class Solution(object):
    def addDigits(self, num):
        """
        :type num: int
        :rtype: int
        """
        if num==0:return 0
        total = 0
        s = str(num)
        for i in range(len(s)):
            total += int(s[i])
        if total < 10:
            return total
        else:
            #recrusive:
            return self.addDigits(total)
    
    

#Solution2:(don't understand):
class Solution(object):
    def addDigits(self, num):
        """
        :type num: int
        :rtype: int
        """
        if num==0:return 0
        return 1 + (num - 1) % 9;