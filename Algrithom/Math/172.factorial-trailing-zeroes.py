'''
Given an integer n, return the number of trailing zeroes in n!.

Example 1:

Input: 3
Output: 0
Explanation: 3! = 6, no trailing zero.
Example 2:

Input: 5
Output: 1
Explanation: 5! = 120, one trailing zero.
'''
class Solution(object):
    def trailingZeroes(self, n):
        """
        :type n: int
        :rtype: int
        """
        #just check how many 5 factor, since all 10 is by 5*2, and there is lots of 2 in n!, only few 5
        #so get 5 is sufficent.
        return 0 if n == 0 else n / 5 + self.trailingZeroes(n / 5)