'''
Given a string s, find the longest palindromic substring in s. You may assume that the maximum length of s is 1000.

Example 1:

Input: "babad"
Output: "bab"
Note: "aba" is also a valid answer.
Example 2:

Input: "cbbd"
Output: "bb"
'''

class Solution(object):
    def longestPalindrome(self, s):
        """
        :type s: str
        :rtype: str
        """
        n = len(s)
        fn = [[False for i in range(n)] for i in range(n)]
        fn[0][0] = True
        
        max_len = 0
        start = 0
        end = 0
        
        for i in range(n):
            for j in range(i):
            	#说明：如果s[j] == s[i] 并且内层fn[j+1][i-1]也为true
            	#那么相当于一个palindrom外面包了一层相等字符，那么当然也是palindrom
            	#注意i-j<=2，这个其实是初始条件
                if s[j] == s[i] and (fn[j+1][i-1] or i-j<=2):
                    fn[j][i] = True
                #收缩max len
                if fn[j][i] == True and max_len < i - j + 1:
                    max_len = i - j + 1
                    start = j
                    end = i
        return s[start:end+1]