'''A robot is located at the top-left corner of a m x n grid (marked 'Start' 
in the diagram below).

The robot can only move either down or right at any point in time.
 The robot is trying to reach the bottom-right corner of the grid 
 (marked 'Finish' in the diagram below).

How many possible unique paths are there?

 Notice

m and n will be at most 100.

Have you met this question in a real interview? Yes
Example

Above is a 3 x 7 grid. How many possible unique paths are there?
'''

class Solution:
    """
    @param n and m: positive integer(1 <= n , m <= 100)
    @return an integer
    """ 
    def uniquePaths(self, m, n):
    	#define f[n] - 到每一个格子(i,j)时候的可能路径

        #init value
        f=[[0 for i in range(n)] for i in range(m)]
        f[0][0] = 1
        # 第0行和第0列上面的格子，可能性都是1
        for i in range(m):
            f[i][0] = 1
        for i in range(n):
            f[0][i] = 1
        #function
        for i in range(1,m):
            for j in range(1,n):
            	#到每一个格子的可能性是它上面和左边的格子的可能性之和
                f[i][j] = f[i-1][j] + f[i][j-1]
        #答案就是终点的可能性哦
        return f[m-1][n-1]