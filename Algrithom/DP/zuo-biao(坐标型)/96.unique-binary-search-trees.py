'''
Given n, how many structurally unique BSTs (binary search trees) that store values 1...n?

Have you met this question in a real interview? Yes
Example
Given n = 3, there are a total of 5 unique BST's.

1           3    3       2      1
 \         /    /       / \      \
  3      2     1       1   3      2
 /      /       \                  \
2     1          2                  3
Thinking:

很好理解，一共i个node， 其中一个是root,那么children一共是 i-1 个，这些children有的在左边，有的在右边；
那么所有的可能就是left child的所有组合x right child的所有组合。比如左边2种，右边2种，那么就是4种。
BST是有序的，所以左右不可以互换。如果题目没有bst的要求，那么公式要乘以2
f[i] += （f[j] * f[i - 1 - j]） * 2  验证了 i=2,3 的情况。
而每一个i 则是所有这种组合的累加。

'''


class Solution:
    # @paramn n: An integer
    # @return: An integer
    def numTrees(self, n):
        # write your code here
        if n <= 1:
            return 1
            
        f = [0 for i in range(n + 1)]
        f[0] = 1
        f[1] = 1
        
        for i in range(2,n + 1):
            for j in range(i):
                f[i] += f[j] * f[i - 1 - j]
        
        return f[n]
