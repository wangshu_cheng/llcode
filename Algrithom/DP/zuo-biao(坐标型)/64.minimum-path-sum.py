'''
Given a m x n grid filled with non-negative numbers, 
find a path from top left to bottom right which minimizes the sum 
of all numbers along its path.

You can only move either down or right at any point in time.

1 every step have 2 choice, 2 **n
2  have 多项式级别 个数据，
So DP

'''

class Solution:
    """
    @param grid: a list of lists of integers.
    @return: An integer, minimizes the sum of all numbers along its path
    """
    def minPathSum(self, grid):
        # write your code here
        if not grid or not grid[0]:
            return 0
        m = len(grid)
        n = len(grid[0])
        #define:
        #state: f(i,j), the min path sum of current grid
        #init:
        f = [[0 for j in range(n)] for i in range(m)]
        f[0][0] = grid[0][0]
        for j in range(1,n):
            f[0][j] = f[0][j-1] + grid[0][j]
            
        for i in range(1,m):
            f[i][0] = f[i-1][0] + grid[i][0]
            
        #function:
        for i in range(1,m):
            for j in range(1,n):
                f[i][j] = min(f[i][j-1],f[i-1][j]) + grid[i][j]
                
        return f[m-1][n-1]