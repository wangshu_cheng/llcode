'''
The demons had captured the princess (P) and imprisoned her in the bottom-right corner of a dungeon. The dungeon consists of M x N rooms laid out in a 2D grid. Our valiant knight (K) was initially positioned in the top-left room and must fight his way through the dungeon to rescue the princess.

The knight has an initial health point represented by a positive integer. If at any point his health point drops to 0 or below, he dies immediately.

Some of the rooms are guarded by demons, so the knight loses health (negative integers) upon entering these rooms; other rooms are either empty (0's) or contain magic orbs that increase the knight's health (positive integers).

In order to reach the princess as quickly as possible, the knight decides to move only rightward or downward in each step.

 

Write a function to determine the knight's minimum initial health so that he is able to rescue the princess.

For example, given the dungeon below, the initial health of the knight must be at least 7 if he follows the optimal path RIGHT-> RIGHT -> DOWN -> DOWN.

-2 (K)  -3  3
-5  -10 1
10  30  -5 (P)
 

Note:

The knight's health has no upper bound.
Any room can contain threats or power-ups, even the first room the knight enters and the bottom-right room where the princess is imprisoned.
'''
class Solution(object):
    def calculateMinimumHP(self, dungeon):
        """
        :type dungeon: List[List[int]]
        :rtype: int
        """
        #get minimal path sum? same as path sum?
        #No, this question must reverse, from end to start.Why?
        #from start to end looks not easy to gurrantee each step total healthy >= 1
        
        if not dungeon:
            return 0
        
        m = len(dungeon)
        n = len(dungeon[0])
        
        dp = [[0 for i in range(n)] for j in range(m)]
        
        #init last point, the end point
        dp[m-1][n-1] = max(1-dungeon[m - 1][n - 1], 1)
        
        #init last row
        for j in range(n-2,-1,-1):
            # current dp of last row is right dp value - current dungeon value
            # why? 如果小人survive到了右边一格，那么在当前一格，他的血至少是下一格的血，加上他在当前格损耗掉的血
            # 和 1 比较的最大值，为什么要和1 比较？因为至少要是1，不然就死了。
            dp[m-1][j] = max(dp[m-1][j+1] - dungeon[m-1][j],1)
                             
        for i in range(m-2,-1,-1):
            dp[i][n-1] = max(dp[i+1][n-1] - dungeon[i][n-1],1)
            
        for i in range(m-2,-1,-1):
            for j in range(n-2,-1,-1):
                down = max(dp[i+1][j] - dungeon[i][j],1)
                right = max(dp[i][j+1] - dungeon[i][j],1)
                dp[i][j] = min(down,right)
                
        return dp[0][0]