'''
Given an array of non-negative integers, you are initially positioned at the
 first index of the array.

Each element in the array represents your maximum jump length at that position.

Your goal is to reach the last index in the minimum number of jumps.

Example
Given array A = [2,3,1,1,4]

The minimum number of jumps to reach the last index is 2. (Jump 1 step from 
index 0 to 1, then 3 steps to the last index.)

Thinking:

1 时间复杂度n**n
2 空间复杂度n
所以可以考虑DP


'''
class Solution:
    # @param A, a list of integers
    # @return an integer
    def jump(self, A):
        # write your code here

        #boarder check:
    	if not A or len(A) == 0:
    		return False 

    	n = len(A)
    	#init:设每一步都是无穷大，不可到达
    	f = [2**32 for i in range(n)]

        f[0] = 0
    	#到达每一步的最小sum，如果根本不可能到达该步，那么设为无穷大
    	#function：
    	for i in range(1,n):
	    	for j in range(i):
	    		'''
	    		查询所有小于i 的值，如果f[j]是无穷大那么就没有
	    		必要继续了，因为他通过它的路径不可能是sum最小值.
	    		所以，方程为：
	    		当 j 可达，而且j可以到达i, 那么f[i] = 最小的f[j]
	    		'''
	    		if f[j] < 2**32 and A[j] >= i - j:
	    			f[i] = min(f[i],f[j] + 1)#加1是因为从j到i要走一步
	    			

	    return f[n-1]