'''
Given an array of non-negative integers, you are initially positioned 
at the first index of the array.

Each element in the array represents your maximum jump length at that 
position.

Determine if you are able to reach the last index.

Example
A = [2,3,1,1,4], return true.
A = [3,2,1,0,4], return false.

Thinking:
1 Every step we have n posibility, so n**n is the 复杂度；
2 The data space is limited  to 多项式级别, there is duplicated traversal。

So DP.
DP解法关键是定义状态，和方程（状态之间联系），才有可能简化为多重循环。

'''

class Solution:
    # @param A, a list of integers
    # @return a boolean
    def canJump(self, A):

    	#boarder check:
    	if not A or len(A) == 0:
    		return False 

    	n = len(A)
    	#init:
    	f = [False for i in range(n)]

        f[0] = True
    	#到达每一步的可能，true or false
    	#function：
    	for i in range(1,n):
	    	for j in range(i):
	    		'''
	    		查询所有小于i 的值，如果f[j]本身就为false那么就没有
	    		必要继续了，因为他不可能到达f[i].
	    		所以，方程为：
	    		当 j 可达，而且j可以到达i, 那么f[i] = true
	    		'''
	    		if f[j] and A[j] >= i - j:
	    			f[i] = True
	    			break

	    return f[n-1]




