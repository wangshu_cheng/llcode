'''Given a triangle, find the minimum path sum from top to bottom. 
Each step you may move to adjacent numbers on the row below.

 Notice

Bonus point if you are able to do this using only O(n) extra space, 
where n is the total number of rows in the triangle.

Example
Given the following triangle:

[
     [2],
    [3,4],
   [6,5,7],
  [4,1,8,3]
]
The minimum path sum from top to bottom is 11 (i.e., 2 + 3 + 5 + 1 = 11).
'''
class Solution:
    """
    @param triangle: a list of lists of integers.
    @return: An integer, minimum path sum.
    """

    # Top down, can u do bottom up?
    def minimumTotal(self, triangle):
        # write your code here
        
        #none case:
        if not triangle:
            return 
        if not triangle[0]:
            return
        
        n = len(triangle)
        
        #define
        #f[i][j] 到达i,j点的最短路径长度

        #init: init 0 as a rectangle to simplify
        #初始点很重要因为他们是一切推导的基础
        f=[[0 for j in range(n)] for i in range(n)]
        
        f[0][0] = triangle[0][0]
        for i in xrange(1,n):
            f[i][0] = f[i-1][0] + triangle[i][0]
            f[i][i] = f[i-1][i-1] + triangle[i][-1]#here is same as triangle[i][i]
            #but you can not use f[i-1][-1] because we defined as rectangle

        #function:
        for i in xrange(1,n):
            for j in xrange(1,i):#每一行最多i个点，不是n个点。避开两条边，因为已经有值了
            	#当前最短路径是上一级两个可能点的最小值 加上当前点的值
                f[i][j] = min(f[i-1][j], f[i-1][j-1]) + triangle[i][j]

        result = 2**31 #can choose f[n-1][0], same thing
        #把最后一行取最小值得到结果
        for i in xrange(0,n):
            result = min(result,f[n-1][i])
        return result



#2 buttom up
    def minimumTotal(self, triangle):
        if not triangle:return
        m=len(triangle)
        res=[[i for i in row] for row in triangle]#hard copy triangle
        for i in xrange(m-2,-1,-1):
            for j in xrange(len(triangle[i])):#start from 2nd line from bottom
                res[i][j]=min(res[i+1][j],res[i+1][j+1]) + triangle[i][j]
        #so the min path of the whole 2d array is the min value of the last line's min path value
        return res[0][0]