'''
You are climbing a stair case. It takes n steps to reach to the top.

Each time you can either climb 1 or 2 steps. In how many distinct ways 
can you climb to the top?

Have you met this question in a real interview? Yes
Example
Given an example n=3 , 1+1+1=2+1=1+2=3

return 3


Thinking:
1 由于每一步都有2种可能，如果全部遍历，那么就是2**n 种可能；所以基础算法复杂度为 2**n；
2 阶梯的个数是 N 个(而不是2**n个)，所以，有重复数据；

根据1 和 2， 可以判定可以用 DP

'''
class Solution:
    """
    @param n: An integer
    @return: An integer
    """

    '''
    method1
    '''
    def climbStairs(self, n):
        # write your code here
        # don't forget, at first, you are at ground, which is not in the n stairs
        # so let's define states as below:
        # groud is f[0],
        # stairs is from f[1] to f[n], so we need array n+1
        
        #边界值
        if n<=1:
            return 1

        #define: f(n) 每一及阶梯的可能路径数
        #起始值
        f = [0 for i in range(n+1)]
        f[0] = 1 #注意这里是1， 可以用小值测试
        f[1] = 1
        #function
        for i in range(2,n+1):
            f[i] =  f[(i-1)]+ f[(i-2)] 

        #结果，到最后一级阶梯的可能路径数
        return f[n]
        

    '''
    Can u further optimize the storage??
    
    Thinking: From the function, we found, we 
    only need 1 step and 2 step before, 2 data, so:
    define now, last and evenlast,
    now = last + evenlast
    even last = last
    last = now
    further reduce memory to O(1):


    '''
    def climbStairs(self, n):
        if n<=1:
            return 1
        last,evenLast=1,1
        
        for i in range(2,n+1):
            now = last + evenLast
            evenLast = last
            last = now
            
        return now