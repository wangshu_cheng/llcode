'''Follow up for "Unique Paths":
Now consider if some obstacles are added to the grids. How many unique paths 
would there be? An obstacle and empty space is marked as 1 and 0 respectively 
in the grid.
For example,
There is one obstacle in the middle of a 3x3 grid as illustrated below.
[
  [0,0,0],
  [0,1,0],
  [0,0,0]
]
The total number of unique paths is 2.
'''
class Solution:
    """
    @param obstacleGrid: An list of lists of integers
    @return: An integer
    """
    def uniquePathsWithObstacles(self, obstacleGrid):
        # write your code here
        if not obstacleGrid:
            return
        if not obstacleGrid[0]:
            return
        m,n = len(obstacleGrid),len(obstacleGrid[0])
        
        f=[[0 for i in range(n)] for i in range(m)]

        #f[i,j]状态，表示可能的path数
        
        f[0][0] = 1
        
        for i in range(m):
            if obstacleGrid[i][0] != 1:
                f[i][0] = 1
        	#如果i = 0行中 有一个格子是obstacle，那么后面都是，还用0表示可能性
            else:
                break
        for i in range(n):
            if obstacleGrid[0][i] != 1:
                f[0][i] = 1
            else:
                break
        
        for i in range(1,m):
            for j in range(1,n):
                if obstacleGrid[i][j] != 1:
                    f[i][j] = f[i-1][j] + f[i][j-1]
                else:
                    f[i][j] = 0
        
        return f[m-1][n-1]