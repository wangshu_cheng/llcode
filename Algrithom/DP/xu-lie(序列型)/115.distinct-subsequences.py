'''
Given a string S and a string T, count the number of distinct subsequences of S which equals T.

A subsequence of a string is a new string which is formed from the original string by deleting some (can be none) of the characters without disturbing the relative positions of the remaining characters. (ie, "ACE" is a subsequence of "ABCDE" while "AEC" is not).

Example 1:

Input: S = "rabbbit", T = "rabbit"
Output: 3
Explanation:

As shown below, there are 3 ways you can generate "rabbit" from S.
(The caret symbol ^ means the chosen letters)

rabbbit
^^^^ ^^
rabbbit
^^ ^^^^
rabbbit
^^^ ^^^
Example 2:

Input: S = "babgbag", T = "bag"
Output: 5
Explanation:

As shown below, there are 5 ways you can generate "bag" from S.
(The caret symbol ^ means the chosen letters)

babgbag
^^ ^
babgbag
^^    ^
babgbag
^    ^^
babgbag
  ^  ^^
babgbag
    ^^^

'''
class Solution(object):
    def numDistinct(self, s, t):
        """
        :type s: str
        :type t: str
        :rtype: int
        """
        #状态函数， f[i][j],表示一刀切在i,j点之前； i，j点之前的2个subseq 相等的解法数，不包括i，j点
        f = [[0 for _ in range(len(t)+1)] for _ in range(len(s)+1)]
        
        for i in range(len(s)):
            f[i][0] = 1
        
        for i in range(len(s)):
            for j in range(len(t)):
                if s[i] == t[j]:
                    #解释一下： 如果 s[i]==t[j]，那么就是说 i，j点之前的总解法依然有价值；
                    #因为两个子串都加上一个相等的字符，解法总数只会增加，不会减少；
                    #如果 s[i]！=t[j] 那么之前的f[i][j] 就无效了；要记住，最终的解法一定是S的字串
                    #能包括所有的T，差一个都不行！也就是说就算 f[i][j]是无穷大，f[i][J+1] 也可能是0.
                    f[i+1][j+1] = f[i][j+1] + f[i][j]
                else:

                    f[i+1][j+1] = f[i][j+1]
                    
        return f[-1][-1]
        