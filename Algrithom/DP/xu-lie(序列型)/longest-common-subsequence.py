'''
Given two strings, find the longest common subsequence (LCS).

Your code should return the length of LCS.

Have you met this question in a real interview? Yes
Clarification
What's the definition of Longest Common Subsequence?

https://en.wikipedia.org/wiki/Longest_common_subsequence_problem
http://baike.baidu.com/view/2020307.htm
Example
For "ABCD" and "EDCA", the LCS is "A" (or "D", "C"), return 1.

For "ABCD" and "EACB", the LCS is "AC", return 2.

Thinking:
1 时间： 2**n 取出所有sub sequence的可能性是Cn1+Cn2+...+Cnn = 2**n-1
2 空间: n**2
3 切几刀的问题，（不是小人跳）是序列动规

这题的关键就是2个序列一定能 通过 斜着比较（i和j不同）或正着比较（i，j相同）得到答案

'''

class Solution:
    """
    @param A, B: Two strings.
    @return: The length of longest common subsequence of A and B.
    """
    def longestCommonSubsequence(self, A, B):
        # write your code here
        
        n,m = len(A),len(B)
        '''
        记住，序列型动规，我们需要开n+1空间，该空间的i表示切到 Array[i-1] 元素之后，
        当前数据值是Array[i-1]

        '''
        # 所有f[0][j] 和 f[i][0]都是零。
        f = [[0 for i in range(m+1)] for i in range(n+1)]
                   
        for i in xrange(1,n+1):
            for j in xrange(1,m+1): 
                #这题和 substring 题的区别就在于，状态是到每一点为止的最大长度。
                #这也很好理解，因为序列不需要连续嘛。断了不需要重新计数。
                f[i][j] = max(f[i-1][j],f[i][j-1])
                if A[i-1] == B[j-1]:
                    f[i][j] = max(f[i][j],f[i-1][j-1] + 1)
                    
        return f[n][m]