'''Given a string s and a dictionary of words dict, determine if s 
can be break into a space-separated sequence of one or more dictionary words.

Example
Given s = "lintcode", dict = ["lint", "code"].

Return true because "lintcode" can be break as "lint code".

!! word break is NOT substring problem, because you need consider all substrings.
Thinking:
1 Search: “break ” means all the possible combination (actually no length restriction), it's a subset problem.
So use subset template to get all subset, and check dic.

2 DP:
1 2**n，时间无限
2 空间有限
1&2 => DP
3 每一个index的切分都和前一个index切分无关 =>序列动归

序列动归值

'''


    DP solution:

    def wordBreak(self, s, dict):
        # write your code here
        # sequnce dp, we need add a pre state before head, and the total dp lenght is n+1
        #we need cut at after a char, so the last case is cut at n (not n-1)
        #first cut is before 0, we can set a f[0] = true, empty can be cut

        if not s:
            return True
        if not dict:
            return False
            
        n = len(s)
        #f[i]表示切在i点之前的位置，看能否break 成功；为什么呢？因为我们要f[0]存初始值；f[0]表示一定能切成功，
        #因为0之前什么都没有；
        f = [False for i in range(n+1)]
        f[0] = True
        
        maxLength = max([len(w) for w in dict])
        for i in range(1,n+1):
            #this is a optimization:
            #since no word is longer than maxLength

            '''
             Basic solution:
             问题：s[j:i] 可能很大，大于一个单词可能的长度，再查dict 没意义；
             所以可能的优化就是只查长度小于单词可能长度
             的字串，其他的缺省认为不满足
             	
            '''
             for j in range(i):
             	if f[j] == True and s[j:i] in dict:
             		f[i] = True
             		break

             '''
             优化版本：
             只以i 为中心往前面扫 maxLength(如果i 小于 maxLength
             那么就扫到i)
             '''
             for j in range(1,min(i,maxLength)+1):
                if f[i-j] == True and s[i-j:i] in dict:
                    f[i] = True
                    break
        return f[n]
        