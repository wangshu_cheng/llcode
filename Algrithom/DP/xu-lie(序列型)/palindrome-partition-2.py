'''
 Palindrome Partitioning II

Given a string s, cut s into some substrings such that every substring is a palindrome.

Return the minimum cuts needed for a palindrome partitioning of s.

Example
Given s = "aab",

Return 1 since the palindrome partitioning ["aa", "b"] could be produced using 1 cut.

Thinking:
切一共有n+1个切法，第一种切法，前面字符串为0，后面为n，最后一张，前面为n，最后为0.
每切一刀，都要满足后面和前面都是palindrome。

这个为序列型动归因为每一个状态 f[i]不是第i个的状态，而是前i个状态（到这个i之前，能切最小刀数）。
比如：
abcde，和abcdefg一样，都是只能切0刀。 所以f[0]~f[6]都一样都是0 。这样解释可以吗？？

'''

class Solution:
    # @param s, a string
    # @return an integer
    def minCut(self, s):
        #write your code here
        if not s:
            return 0
        if len(s)<=1:
            return 0
        
        #init:fn 表示 i 之前的所有点的最小切刀数
        n=len(s)
        f=[0 for i in range(len(s)+1) ]
        for i in range(n+1):
            f[i] = i - 1#这样已经是取最大值了，如果f[i] = 2**31也是一样
            #但是f[0] 必须是-1
        
        isPalindrome = self.getPalindrome(s)
        for i in range(1,len(s)+1):
            for j in range(i):
            	#用二维数组表示状态，不是一维！，因为对于每一个j（n种取值） i都有n的空间（n种取值）。
                if isPalindrome[j][i-1]:
                    f[i] = min(f[i],f[j]+1)
        return f[n]
    #这个函数用的是区间型动归
    def getPalindrome(self,s):
        n = len(s)
        isPalindrome =[ [False for i in range(n)] for j in range(n)]
        for i in range(n):
            isPalindrome[i][i] = True
        
        for i in range(n-1):
            isPalindrome[i][i+1] = s[i] == s[i+1]
        
        for length in range(2,n):
            for start in range(n-length):
                isPalindrome[start][start+length] = isPalindrome[start+1][start + length-1] and s[start] == s[start+length]
        return isPalindrome