'''
Given a sequence of integers, find the longest increasing subsequence (LIS).

You code should return the length of the LIS.

What's the definition of longest increasing subsequence?

Note:
This subsequence is not necessarily contiguous, or unique.

For `[4, 2, 4, 5, 3, 7]`, the LIS is `[2, 4, 5, 7]`, return `4`

Analysis:
1 每一步都要遍历n步，所以复杂度是n**2时间无限
2 空间复杂度是n，空间有限
找最大或最小；

所以考虑DP

'''

class Solution:
    """
    @param nums: The integer array
    @return: The length of LIS (longest increasing subsequence)
    """
    def longestIncreasingSubsequence(self, nums):
        if not nums:
        	return 0
        if len(nums) == 1:
            return 1

        #init:
        n = len(nums)
        max_value = 1
        f = [1 for i in range(n)]
        #define  f【i】 到第i个位置（注意绝对不是前i个位置）为止,包括 nums【i】的最长上升序列长度，答案是所有f[i]的最大值，
        # 答案不是f[n-1]的原因可能是，答案不一定包括 nums[-1]
	    for i in range(1,n):
	    	for j in range(i):
	    		#如果nums[j] 大于 nums[i]则不考虑
	    		if nums[i] > nums[j]:
        			f[i] = max(f[i],f[j]+1) #f[i] 打擂台
        			max_value = max(max_value,f[i])
		return max_value



