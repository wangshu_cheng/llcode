''' EASY
There are a row of n houses, each house can be painted with one of the three colors: red, blue or green. The cost of painting each house with a certain color is different. You have to paint all the houses such that no two adjacent houses have the same color.

The cost of painting each house with a certain color is represented by a n x 3 cost matrix. For example, costs[0][0] is the cost of painting house 0 with color red; costs[1][2] is the cost of painting house 1 with color green, and so on... Find the minimum cost to paint all houses.

Note:
All costs are positive integers.

Example:

Input: [[17,2,17],[16,16,5],[14,3,19]]
Output: 10
Explanation: Paint house 0 into blue, paint house 1 into green, paint house 2 into blue. 
             Minimum cost: 2 + 5 + 3 = 10.
'''

# This solution can be simplified by using 3 variables instead of matrix

class Solution(object):
    def minCost(self, costs):
        """
        :type costs: List[List[int]]
        :rtype: int
        """
        
        if not costs or not costs[0]:
            return 0
        n = len(costs)
        dp=[[0 for i in range(3)] for j in range(n)]
        
        dp[0][0] = costs[0][0]
        dp[0][1] = costs[0][1]
        dp[0][2] = costs[0][2]
        
        for i in range(1,n):
            dp[i][0] = min(dp[i-1][1], dp[i-1][2]) + costs[i][0]
            dp[i][1] = min(dp[i-1][0], dp[i-1][2]) + costs[i][1]
            dp[i][2] = min(dp[i-1][0], dp[i-1][1]) + costs[i][2]
        
        return min(dp[n-1][0],dp[n-1][1],dp[n-1][2])

# simplified:

class Solution(object):
    def minCost(self, costs):
        """
        :type costs: List[List[int]]
        :rtype: int
        """
        '''
        use 3 variable to present current min value if use this color
        r - means the current min value if use red; r=costs[i][0]+min(previous_b,previous_g) #prevous_b means when previous color is blue, the previous min value, etc.
        g - means the current min value if use green,etc
        b - means the current min value if use blue,etc
        '''
        #boarder check:
        if not costs:return 0
        r=g=b=0
        m=len(costs)#house number
        for i in xrange(m):
            pre_r,pre_g,pre_b=r,g,b
            r=costs[i][0]+min(pre_g,pre_b)
            g=costs[i][1]+min(pre_r,pre_b)
            b=costs[i][2]+min(pre_g,pre_r)
        return min(r,g,b)