'''
Given three strings: s1, s2, s3, determine whether s3 is formed by the interleaving of s1 and s2.

Have you met this question in a real interview? Yes
Example
For s1 = "aabcc", s2 = "dbbca"

When s3 = "aadbbcbcac", return true.
When s3 = "aadbbbaccc", return false.


Thinking:
1 时间：这是排列组合问题，不可能是多项式复杂度；（不是2重循环能解决的）
   强硬一点只能递归解决。
2 空间：只要求满足
'''

class Solution:
    """
    @params s1, s2, s3: Three strings as description.
    @return: return True if s3 is formed by the interleaving of
             s1 and s2 or False if not.
    @hint: you can use [[True] * m for i in range (n)] to allocate a n*m matrix.
    """
    def isInterleave(self, s1, s2, s3):
        # write your code here
        
        n,m,k = len(s1),len(s2),len(s3)
        if n+m !=k:
            return False
        
        f = [[True for i in range(m+1)] for j in range(n+1)]
        
        #init
        #this is init the senario when the S3 only construct from S1:
        for i in range(1,n+1):
            f[i][0] = f[i-1][0] and s3[i-1] == s1[i-1]
            
        #only from S2:
        for j in range(1,m+1):
            f[0][j] = f[0][j-1] and s3[j-1] == s2[j-1]
            
            
        for i in range(1,n+1):
            for j in range(1,m+1):
            	#分2种情况，1是S3当前字符（S3[i+j-1]）因为i,j表示第i个和第j个，那么他们对应的S3就是S3[i-1 + j-1 + 1],
            	#也就是S3[i+j+1],试想一下，S1[0] 和 S2[0]组成的是S3[1]
                f[i][j] = (f[i-1][j] and s1[i-1] == s3[i+j-1]) or (f[i][j-1] and s2[j-1] == s3[i+j-1])
        return f[n][m]





