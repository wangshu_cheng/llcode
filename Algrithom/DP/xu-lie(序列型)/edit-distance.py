'''
Given two words word1 and word2, find the minimum number of steps required to convert word1 to word2. 
(each operation is counted as 1 step.)

You have the following 3 operations permitted on a word:

Insert a character
Delete a character
Replace a character
Have you met this question in a real interview? Yes
Example
Given word1 = "mart" and word2 = "karma", return 3.

1 暴力复杂度：4**n
2 空间复杂度：n**2
so DP

'''

class Solution: 
    # @param word1 & word2: Two string.
    # @return: The minimum number of steps.
    def minDistance(self, word1, word2):
        # write your code here
        n,m = len(word1),len(word2)

        #定义function： word1第i个字符以前和word2第j个字符以前完成转换的步数，结果就是f[n][m]
        f = [[0 for i in range(m+1)] for i in range(n+1)]
        #注意，两条边要初始化为i，不是0， 因为从一个空串到一个i长度的字符串
        #需要i步插入
        for i in range(n+1):
            f[i][0] = i
        for i in range(m+1):
            f[0][i] = i
            
        for i in xrange(1,n+1):
            for j in xrange(1,m+1):
            	#如果当前 字符（Word1[i-1]和 word2[j-1]）相等，那么
            	#不增加任何操作， f[i][j] = f[i-1][j-1]
                if word1[i-1] == word2[j-1]:
                    f[i][j] = f[i-1][j-1]
                else:
	            	#如果当前 字符（Word1[i-1]和 word2[j-1]）不相等，有2种情况：
                	# min(f[i][j-1],f[i-1][j]) 表示delete当前A串或B串中不同的字符的情况
                	# f[i-1][j-1] 表示replace掉当前不同的情况
                	# 以上2中情况取最小值，然后 加 1,因为delete和replace都是一步操作.
                    f[i][j] = 1+ min(f[i-1][j-1],min(f[i][j-1],f[i-1][j]))
                
                    
        return f[n][m]