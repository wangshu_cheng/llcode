'''
There is a fence with n posts, each post can be painted with one of the k colors.

You have to paint all the posts such that no more than two adjacent fence posts have the same color.

Return the total number of ways you can paint the fence.

Note:
n and k are non-negative integers.

Example:

Input: n = 3, k = 2
Output: 6
Explanation: Take c1 as color 1, c2 as color 2. All possible ways are:

            post1  post2  post3      
 -----      -----  -----  -----       
   1         c1     c1     c2 
   2         c1     c2     c1 
   3         c1     c2     c2 
   4         c2     c1     c1  
   5         c2     c1     c2
   6         c2     c2     c1

'''
class Solution(object):
    def numWays(self, n, k):
        """
        :type n: int
        :type k: int
        :rtype: int
        """
        
        if not n:
            return 0
        if n == 1:
            return k
        if n == 2:
            return k*k
        
        
        dp = [0 for i in range(n+1)]
        
        dp[0] = 0
        dp[1] = k
        dp[2] = k*k # 2 post, each one have k option.
        
        for i in range(3,n+1):
            # Assuming there are 3 posts, 
            #  1.if the first one and the second one has the same color, then the third one has k-1 options. The first and second together has k options. 
            #   2.If the first and the second have different color, the total is k * (k-1), then the third one has k options. Therefore, f(3) = #(k-1)*k + k*(k-1)*k = (k-1)(k+k*k)
            # 
            dp[i] = (k-1)*(dp[i-1] + dp[i-2])
            
        return dp[-1]
            