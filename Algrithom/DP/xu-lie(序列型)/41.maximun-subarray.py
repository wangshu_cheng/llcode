'''
It's a DP question

Given an array of integers, find a contiguous subarray 
which has the largest sum.
'''

class Solution:
    """
    @param nums: A list of integers
    @return: An integer denote the sum of maximum subarray
    """

'''

Local max is used to calculate each subarray sum
Global max is used to calculate the largest subarray so far.

'''

    def maxSubArray(self, nums):
        # write your code here
        if not nums:
            return 0
        n = len(nums)
        gmax = [0 for i in range(n)]
        lmax = [0 for i in range(n)]
        
        gmax[0] = nums[0]
        lmax[0] = nums[0]
        
        for i in range(1,n):
            lmax[i] = max(lmax[i-1] + nums[i],nums[i])
            gmax[i] = max(gmax[i-1],lmax[i])
            
        return gmax[n-1]
            