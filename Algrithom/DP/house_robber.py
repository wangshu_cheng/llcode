'''
You are a professional robber planning to rob houses along a street. 
Each house has a certain amount of money stashed, the only constraint 
stopping you from robbing each of them is that adjacent houses have security
 system connected and it will automatically contact the police if two adjacent
  houses were broken into on the same night.

Given a list of non-negative integers representing the amount of money of each house,
 determine the maximum amount of money you can rob tonight without alerting the police.

'''

class Solution(object):
    def rob(self, nums):
        """
        :type nums: List[int]
        :rtype: int
        """
        if not nums:
            return 0
        if len(nums) <= 1:
            return nums[0]
        
        LEN = len(nums)
        f = [0 for i in range(LEN)]
        f[0] = nums[0]
        f[1] = max(nums[1],nums[0])
        
        if len(nums) == 2:
            return max(f[0],f[1])
        
        for i in range(2,LEN):
            f[i] = max((f[i-2] + nums[i]),f[i-1])
        return f[-1]