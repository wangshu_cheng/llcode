'''
Given a positive integer n, find the least number of perfect square numbers (for example, 1, 4, 9, 16, ...) which sum to n.

Example 1:

Input: n = 12
Output: 3 
Explanation: 12 = 4 + 4 + 4.
Example 2:

Input: n = 13
Output: 2
Explanation: 13 = 4 + 9.
'''

class Solution(object):
    def numSquares(self, n):
        """
        :type n: int
        :rtype: int
     
        This is modified BFS for graph,
        the origin algrithim is in :
        
        https://en.wikipedia.org/wiki/Graph_traversal#Breadth-first_search
        
        The tricky part is the level, need a aditinoaly loop to get the level value.
     
        """
     
        #DP:
        
#         if n == 0: return 0
#         if n == 1: return 1
        
#         m = int(n**0.5)
        
#         f = [2**32 for i in range(n+1)]
        
#         for i in range(1,n+1):
#             for j in range(1,m+1):
#                 if j*j == i:
#                     f[i] = 1
#                 elif i > j*j:
#                     f[i] = min(f[i],f[i-j*j]+1)
                    
#         return f[n]
            
    
        # BFS
        edges = [i*i for i in range(1,int(n**0.5)+1)]
        visited = [False] * (n+1)
        level=0 # this is the answer, how many "steps" to reach the final value
        q=[]
        q.append(0)
        while q:
            level+=1
            for i in range(len(q)):
                tmp = q.pop(0)
                for edge in edges:
                    #the adjacent vertex is just the source vertex + the edge
                    path = tmp+edge
                    if path==n:return level
                    if path > n: break
                    if visited[path]:
                        continue
                    visited[path]=True
                    q.append(path)
        