'''
You are given a m x n 2D grid initialized with these three possible values.

-1 - A wall or an obstacle.
0 - A gate.
INF - Infinity means an empty room. We use the value 231 - 1 = 2147483647 to represent INF 
as you may assume that the distance to a gate is less than 2147483647.
Fill each empty room with the distance to its nearest gate. 
If it is impossible to reach a gate, it should be filled with INF.

For example, given the 2D grid:
INF  -1  0  INF
INF INF INF  -1
INF  -1 INF  -1
  0  -1 INF INF
After running your function, the 2D grid should be:
  3  -1   0   1
  2   2   1  -1
  1  -1   2  -1
  0  -1   3   4
  '''


class Solution(object):
	#
	def wallsAndGates_DFS(self, rooms):
	    """
	    :type rooms: List[List[int]]
	    :rtype: void Do not return anything, modify rooms in-place instead.
	    """
	    if not rooms or not rooms[0]:
	        return
	    def search(rooms,m,n,x,y,v):
	        if x<0 or x >=m or y < 0 or y >= n or rooms[x][y] < v:
	            return
	        
	        rooms[x][y] = v
	        dr = [(-1,0),(1,0),(0,1),(0,-1)]
	        for i in range(4):
	            search(rooms,m,n,x+dr[i][0],y+dr[i][1],v + 1)
	        
	    m,n = len(rooms),len(rooms[0])
	    for i in range(m):
	        for j in range(n):
	            if rooms[i][j] == 0:
	                search(rooms,m,n,i,j,0)
	def wallsAndGates_BFS(self, rooms):
        """
        :type rooms: List[List[int]]
        :rtype: void Do not return anything, modify rooms in-place instead.
        """
        if not rooms or not rooms[0]:
            return
        m,n = len(rooms),len(rooms[0])
        dr = [(-1,0),(1,0),(0,1),(0,-1)]
        for i in range(m):
            for j in range(len(rooms[i])):
                if rooms[i][j] == 0:
                    q = [[i,j,0]]
                    while q:
                        cur = q.pop(0)
                        x,y,v= cur[0],cur[1],cur[2]
                        rooms[x][y] = v
                        for k in range(4):
                            x1 = x + dr[k][0]
                            y1 = y + dr[k][1]
                            if 0 <= x1 < m and 0 <= y1 < n and rooms[x1][y1] > v + 1:
                                q.append([x1,y1,v+1])
      