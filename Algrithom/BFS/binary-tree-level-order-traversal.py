'''
This is BFS template!!

'''
class Solution:
    """
    @param root: The root of binary tree.
    @return: Level order in a list of lists of integers
    """
    def levelOrder(self, root):
        # write your code here
        if not root:
            return []
        result = []
        
        q = [root]
        
        while q: # Bye add in the for loop below, now the while loop becomes loop row, it only excuted for each row.
            '''
            define level here if need
            '''
            level = list()
            '''
            loop current level, can use while loop as well:
            '''
            for i in range(len(q)):#the for loop is the loop to loop in each row.
                '''
                loop current level, don't forget here is pop(0) not pop, because it's queue, not stack!!
                '''
                node = q.pop(0)
                level.append(node.val)
                if node.left: 
                    q.append(node.left)
                if node.right:
                    q.append(node.right)
            '''
            if we need reverse traveral, from buttom to up, just insert to front like:
                result = [level] + result
                or:
                result.insert(0,level)
            '''
            result.append(level)
        return result