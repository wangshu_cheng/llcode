'''
Given a boolean 2D matrix, find the number of islands.

Notice

0 is represented as the sea, 1 is represented as the island. If two 1 is adjacent, 
we consider them in the same island. We only consider up/down/left/right adjacent.

Example
Given graph:

[
  [1, 1, 0, 0, 0],
  [0, 1, 0, 0, 1],
  [0, 0, 0, 1, 1],
  [0, 0, 0, 0, 0],
  [0, 0, 0, 0, 1]
]
return 3.

搜索题，如果按照搜索树来理解就是 4 叉树，树的高度应该是2n，我觉得

暴力的想法就是每一点都宽搜，开hash表 记录下每个走过的点；
宽搜一次完成后，然后再去找新的没有过的并且是1的点，然后再宽搜；
如果题目允许修改原来的grid，那么比较投机的办法就是把遍历过的点置为0，
来解决visited 问题。这题DFS，BFS差别不大，都是 n*m个遍历点，所以实际复杂度都是 O(n*m)

'''
# DFS search：
class Solution:
    # @param {boolean[][]} grid a boolean 2D matrix
    # @return {int} an integer
    def numIslands(self, grid):
        # Write your code here
        def removeIsland(grid,x,y,n,m):
            grid[x][y] = 0
            dr = [(0,1),(0,-1),(-1,0),(1,0)]
            # dr = [(1,0),(0,1),(0,-1),(-1,0)]
            for i in range(4):
                newX = x + dr[i][0]
                newY = y + dr[i][1]
                if 0 <= newX < n and 0 <= newY < m:
                    if grid[newX][newY] == 1:
                        removeIsland(grid,newX,newY,n,m)

        def removeIslandBFS(grid,x,y,n,m):
            q = [[x,y]]
            dr = [(0,1),(0,-1),(-1,0),(1,0)]
            while q:
               cur =  q.pop(0)
               grid[cur[0]][cur[1]] = 0
               for i in range(4):
                   x1 = cur[0] + dr[i][0]
                   y1 = cur[1] + dr[i][1]
                   if ( -1< x1 < n and -1 < y1 < m and grid[x1][y1] == 1 ):
                       q.append([x1,y1])

        if not grid:
            return 0
    
        n,m = len(grid),len(grid[0])
    
        if n+m==0:
            return 0
        
        count = 0
        for i in range(n):
            for j in range(m):
                if grid[i][j] == 1:
                    removeIsland(grid,i,j,n,m)
                    count+=1
        return count