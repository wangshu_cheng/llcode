'''
Clone an undirected graph. Each node in the graph contains a label and a list of its neighbors.

How we serialize an undirected graph:

Nodes are labeled uniquely.

We use # as a separator for each node, and , as a separator for node label and each neighbor of the node.

As an example, consider the serialized graph {0,1,2#1,2#2,2}.

The graph has a total of three nodes, and therefore contains three parts as separated by #.

First node is labeled as 0. Connect node 0 to both nodes 1 and 2.
Second node is labeled as 1. Connect node 1 to node 2.
Third node is labeled as 2. Connect node 2 to node 2 (itself), thus forming a self-cycle.
'''

# Definition for a undirected graph node
# class UndirectedGraphNode:
#     def __init__(self, x):
#         self.label = x
#         self.neighbors = []


'''
关键点： dictionary key 用 老node， value用新node
'''

class Solution:
    # @param node, a undirected graph node
    # @return a undirected graph node

    def cloneGraph(self, node):
                
        # write your code here
        if not node:
            return
        
        q = [node]
        dic = {}        
        #step 1 travels all the nodes and create new nodes (at this time,each node's 
        #neighbor is still emtpy array)
        #step 2 for each node, put already created new nodes to the neighbor array.
        
        #becasue graph can have circle, so we need use dic to prevent circle
        
        #step1 normal BFS traversal:
        while q:
            current = q.pop(0)
            '''
            去重，防环，勿忘！！
            '''
            if current in dic:
                continue
            newnode = UndirectedGraphNode(current.label)
            '''
            关键点： dictionary key 用 老node， value用新node
            '''
            dic[current] = newnode # the neighour field of new node we will fill later
            for i in current.neighbors:
                q.append(i)
                        
        #step2:
        # Now , we get all the nodes created, but their neighor are all empty, so we need loop 
        # all the orignal nodes 
        for key in dic:
            for nei in key.neighbors:
                dic[key].neighbors.append(dic[nei])# dic[key] is new node, dic[nei] is also new nodes
                
        return dic[node]