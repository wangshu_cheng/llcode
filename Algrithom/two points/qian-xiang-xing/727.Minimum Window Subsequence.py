'''
Given strings S and T, find the minimum (contiguous) substring W of S, so that T 
is a subsequence of W.

If there is no such window in S that covers all characters in T, 
return the empty string "". If there are multiple such minimum-length windows, 
return the one with the left-most starting index.

Example 1:
Input: 
S = "abcdebdde", T = "bde"
Output: "bcde"
Explanation: 
"bcde" is the answer because it occurs before "bdde" which has the same length.
"deb" is not a smaller window because the elements of T in the window must occur in order.
Note:

All the strings in the input will only contain lowercase letters.
The length of S will be in the range [1, 20000].
The length of T will be in the range [1, 100].


'''


DP：



'''
does this make sense??
target list先存入hashmap<String, Integer>  前面是字符串，后面是该字符串出现次数，初始一律是1
然后用two pointers扫 available tag list。
遇到在hashmap里的就更新map，次数减一。直到找完，记录length，start，end
找完后，就移动头指针，遇到在hashmap里的，次数要加1，移动中，如果也找完了（命中次数和map的size相等）
，就对比length，如果length短，再次记录。


'''

双指针：
Time out for larget input, but test locally looks good

class Solution(object):
    def minWindow(self, S, T):
        """
        :type S: str
        :type T: str
        :rtype: str
        """
        res = ""
        def isSeq(s,t):
            i,j = 0,0
            while i < len(s) and j < len(t):
                if s[i] != t[j]:
                    i += 1
                else:
                    i += 1
                    j += 1
                    if j == len(t):
                        return i
            return -1
        
        minLen = 20001
        
        for i in range(len(S) - len(T) + 1):
            while i < len(S) and S[i] != T[0]:
                i += 1
            end = min(i+minLen,len(S))
            l = isSeq(S[i:end],T)
            
            if (l != -1 and l < minLen):
                minLen = l
                res = S[i:i+l]
                
        return res