''''
159. Longest Substring with At Most Two Distinct Characters
DescriptionHintsSubmissionsDiscussSolution
Given a string s , find the length of the longest substring t  that contains at most 2 distinct characters.

Example 1:

Input: "eceba"
Output: 3
Explanation: t is "ece" which its length is 3.
Example 2:

Input: "ccaabbb"
Output: 5
Explanation: t is "aabbb" which its length is 5.
'''
class Solution(object):
    def lengthOfLongestSubstringTwoDistinct(self, s):
        """
        :type s: str
        :rtype: int
        """
        maxLen = 0
        hash = {}
        k = 2
        '''
        i is the start pointer,end is back pointer
        '''
        j = 0
        for i in range(len(s)):
            while j < len(s):
                if s[j] not in hash:
                    if len(hash) == k:
                        break
                    hash[s[j]] = 1
                else:
                    hash[s[j]] += 1
                j += 1
                
            maxLen = max(maxLen, j - i)
            #clear hash for each start point
            if s[i] in hash:
                if hash[s[i]] > 1:
                    hash[s[i]] -= 1
                else:
                    hash.pop(s[i],None)
        
        return maxLen