'''
 Longest Substring Without Repeating Characters

 Description
 Notes
 Testcase
 Judge
Given a string, find the length of the longest substring without repeating characters.

Example
For example, the longest substring without repeating letters for "abcabcbb" is "abc", which the length is 3.

For "bbbbb" the longest substring is "b", with the length of 1.
'''
class Solution:
    # @param s: a string
    # @return: an integer
    #2 pointer  method1 - 通用方法：
    # i is start, j is end
    
    '''
    Thinking:
    右指针一直往后移动，直到出现重复字符。
    这个时候移动左指针到重复字符第一次出现的位置（不是右指针的位置！！）
    
    如： abcdb j指向第二个b，那么i应该指向第一个b后面一个位置，并把前面的hash item清空

    '''
    #2 pointer   method2 jiu zhang solution
    #left is start, i is end
    def lengthOfLongestSubstring(self, s):
        # write your code here
        dic = {}
        left = 0
        res = 0
        i = 0
        for i in range(len(s)):
            if s[i] in dic:
                while left < i and s[i] != s[left]:
                    #一路向东清除hash table，右移指针
                    dic.pop(s[left],None)
                    left += 1
                left += 1
            else:
                dic[s[i]] = 1
                res = max(res,i - left + 1)
                
        return res

    def lengthOfLongestSubstring(self, s):
        # write your code here
        if not s: return 0
        n = len(s)
        dic = {}
        i,j = 0,0
        res = 0
        
        while j < n:
            if s[j] not in dic:
                dic[s[j]] = j
                j += 1
            else:
                res = max(res,j - i)
                i = j = dic[s[j]] + 1 #这里有点不够优化，因为dic清空了，所以j要回到i + 1
                dic = {}
                # break
        if j == n:
            res = max(res,j - i)
            
        return res


    '''
    通用方法：
    '''
    def lengthOfLongestSubstring(self, s):
        i,j = 0,0
        ans = 0
        dic = {}
        for i in range(len(s)):
            while i <= j < len(s):
                if s[j] not in dic:
                    dic[s[j]] = 1
                    j += 1
                else:
                    break
            ans = max(ans, j - i)
            dic.pop(s[i],None) # here is pop s[i] not s[j], it still has lots of duplicate steps
        return ans


    '''
    method2: hash table, 该方法并不通用；
    Explaination:
    维护一个全局的hash 表，记录每一个字母出现的index；
    同时定义一个变量 left， 记录subarray的开头，这个开头也就是重复字母第一次出现的位置加1；
    再解释一下，就是说，当subarray从左往右加元素的时候，一旦发现了重复元素，那么左边界left就
    改为第一次出现该重复元素的位置加1.
    比如：
    最初，没有重复元素，left始终是0
    abc  ，left 是 0
    abca, a 重复了，第一次出现a是 index 0 ，那么left 边界就是 1
    abcc, c 重复了， 第一次出现c 是index 2， 那么left 边界就是 3

    '''

    def lengthOfLongestSubstring(self, s):
        ans = 0
        left = 0
        dic = {}
        for i in range(len(s)):
            if s[i] in dic and dic[s[i]] >= left:
                left = dic[s[i]] + 1
            dic[s[i]] = i
            ans = max(ans, i - left + 1)
        return ans