'''
Given a string s, find the length of the longest substring T 
that contains at most k distinct characters.
'''

class Solution:
    # @param s : A string
    # @return : An integer

    # Self method, 
    '''
        Self method, 
        1 the first loop, move the first pointer(right border of window) to end, it don't need jump, so feel free to use for loop.
        2 When we found dic is larger then k, then we start to move left boarde, remove keys, until we the dic len is smaller then k, get max.
    '''


    def lengthOfLongestSubstringKDistinct(self, s, k):
        # write your code here
        maxLen = 0
        hash = {}
        '''
        i is the start pointer,end is back pointer
        '''
        j = 0
        for i in range(len(s)):
            while j < len(s):
                if s[j] not in hash:
                    if len(hash) == k:
                        break
                    hash[s[j]] = 1
                else:
                    hash[s[j]] += 1
                j += 1
                
            maxLen = max(maxLen, j - i)
            #clear hash for each start point
            if s[i] in hash:
                if hash[s[i]] > 1:
                    hash[s[i]] -= 1
                else:
                    hash.pop(s[i],None)
        
        return maxLen



    def lengthOfLongestSubstringKDistinct(s, k):
        # write your code here
        res = 0
        n = len(s)
        i,j = 0,0
        dic = {}
        for j in range(n):
            if s[j] not in dic:
                dic[s[j]] = 1
            else:
                dic[s[j]] += 1
            if len(dic) <= k:
                res = max(res,j - i + 1)
            else:
                while len(dic) > k:
                    if dic[s[i]] > 1:
                        dic[s[i]] -= 1
                    elif dic[s[i]] == 1:
                        dic.pop(s[i],None)
                    i += 1
                res = max(res,j - i + 1)
        return res

    s = "onykowalsrolughyufhxigqkwalvfo"
    k = 4

    print lengthOfLongestSubstringKDistinct(s,k)

