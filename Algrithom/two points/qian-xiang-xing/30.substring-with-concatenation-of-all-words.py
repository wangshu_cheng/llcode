'''
HARD

You are given a string, S, and a list of words, L, that are all of the same length. 
Find all starting indices of substring(s) in S that is a concatenation of each word in 
L exactly once and without any intervening characters. For example, given: S: "barfoothefoobarman" 
L: ["foo", "bar"] You should return the indices: [0,9]. (order does not matter).

Thinking:
其实就是找满足条件的substring.
Generator: substring 生成器（第二个指针用跳跃指针代替for 循环是最优解）；
Filter：每一步都是字典里的word

前向型双指针；一个指针往前推，作为窗口的头；另一指针作为窗口的尾，技巧在于：
1 前指针只能推到 len(s) - n*m
2 后指针是跳跃式的，每次跳一个单词长度（由题中条件-每个单词等长可以发现希望你利用这个条件）,这样可以减少循环次数；
 后指针移动的时候必须满足限定条件-必须是给定的word组合（用dic判断）

'''

public class Solution {
    public ArrayList<Integer> findSubstring(String S, String[] L) {
        ArrayList<Integer> result = new ArrayList<Integer>();
        HashMap<String, Integer> toFind = new HashMap<String, Integer>();
        HashMap<String, Integer> found = new HashMap<String, Integer>();
        int m = L.length, n = L[0].length();
        for (int i = 0; i < m; i ++){
            if (!toFind.containsKey(L[i])){
                toFind.put(L[i], 1);
            }
            else{
                toFind.put(L[i], toFind.get(L[i]) + 1);
            }
        }
        for (int i = 0; i <= S.length() - n * m; i ++){
            found.clear();
            int j;
            for (j = 0; j < m; j ++){
                int k = i + j * n;
                String stub = S.substring(k, k + n);
                if (!toFind.containsKey(stub)) break;
                if(!found.containsKey(stub)){
                    found.put(stub, 1);
                }
                else{
                    found.put(stub, found.get(stub) + 1);
                }
                if (found.get(stub) > toFind.get(stub)) break;
            }
            if (j == m) result.add(i);
        }
        return result;
    }
}