'''
Given a string source and a string target, find the minimum window in source which will 
contain all the characters in target.

 (Notice
  If there is no such window in source that covers all characters in target, 
  return the emtpy string "".If there are multiple such windows, you are guaranteed that there will always
 be only one unique minimum window in source.)

Clarification
Should the characters in minimum window has the same order in target? - Not necessary.

Example
For source = "ADOBECODEBANC", target = "ABC", the minimum window is "BANC"

'''

def minWindow(self, source, target):
    # write your code here
    
    if not source or not target:
        return ""
        
    def validate(shash,thash):
        for key in thash:
            if thash[key] > shash.get(key,0):
                return False
        return True
    
    LEN = len(source)
    
    thash = {}
    shash = {}
    
    res = [2**32,""]
    
    for s in target:
        if s not in thash:
            thash[s] = 1
        else:
            thash[s] += 1
    
    j = 0
    for i in range(LEN):#get subwindow, tail include
        if source[i] not in shash:
            shash[source[i]] = 1
        else:
            shash[source[i]] += 1
        #挤掉水分
        while j <= i and validate(shash,thash):
            if i-j + 1 < res[0]:
                res[0] = i-j + 1
                res[1] = source[j:i+1]
            shash[source[j]] -= 1
            j += 1
    return res[1]


