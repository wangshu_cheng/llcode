'''
Minimum Size Subarray Sum

Given an array of n positive integers and a positive integer s, find the minimal length of a subarray 
of which the sum ≥ s. If there isn't one, return -1 instead.

Example
Given the array [2,3,1,2,4,3] and s = 7, the subarray [4,3] has the minimal length under the problem constraint.
'''
class Solution:
     # @param nums: a list of integers
     # @param s: an integer
     # @return: an integer representing the minimum size of subarray

    def minimumSize(self, nums, s):
        # write your code here
        if not nums:
            return -1
        total = 0
        res = 2**31
        j = 0
        for i in range(len(nums)):
            total += nums[i]
            while j<=i and total >= s:
                res = min(res,i-j+1)
                total -= nums[j]
                j+=1
            
        if res == 2**31:
            res = -1
        return res
            


    def minimumSize(self, nums, s):
        # write your code here
        i,j = 0,0
        totalSum = 0
        ans = 2**31
        for i in range(len(nums)):
            while j < len(nums):
                #keep moving j if not meet conditon
                if totalSum < s:
                    totalSum += nums[j]
                    j += 1
                else:
                    break
            if totalSum >= s:
                ans = min(ans,j - i)
            #before starting another search with new i, remove the head number
            totalSum -= nums[i]
        
        if ans == 2**31:
            return -1
        return ans
        
            
                