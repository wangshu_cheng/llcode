'''
Given an array nums of n integers, find two integers in nums such that the sum is 
closest to a given number, target.

Return the difference between the sum of the two integers and the target.

Have you met this question in a real interview? Yes
Example
Given array nums = [-1, 2, 1, -4], and target = 4.

The minimum difference is 1. (4 - (2 + 1) = 1).
'''
class Solution:
    # @param {int[]} nums an integer array
    # @param {int} target an integer
    # @return {int} the difference between the sum and the target
    def twoSumCloset(self, nums, target):
        # Write your code here
        
        if not nums:
            return 0
            
        nums = sorted(nums)
        
        left,right = 0,len(nums) - 1
        res = 2**31
        while left < right:
            if nums[left] + nums[right] <= target:
                res = min(res, target - (nums[left] + nums[right]))
                left += 1
            else:
                res = min(res, (nums[left] + nums[right]) - target)
                right -= 1
                
        return res