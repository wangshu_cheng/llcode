'''
Given an array S of n integers, find three integers in S such that the sum 
is closest to a given number, target. Return the sum of the three integers.

'''

class Solution:
    """
    @param numbers: Give an array numbers of n integer
    @param target : An integer
    @return : return the sum of the three integers, the sum closest target.
    """

    def threeSumClosest(self, numbers, target):
        # write your code here
        if not numbers or len(numbers) < 3:
            return -1
        numbers = sorted(numbers)
        res = 2**31
        for i in range(len(numbers) - 2):
            first = numbers[i]
            j,k = i+1,len(numbers) - 1
            while j < k:
                total = first + numbers[j] + numbers[k]
                if abs(total - target) < abs(res - target):
                    res = total
                if total < target:
                   j += 1
                else:
                    k -= 1
        return res

    def threeSumClosest(self, numbers, target):
        # write your code here
        if not numbers or len(numbers) < 3:
            return -1
        
        numbers = sorted(numbers)
        n = len(numbers)
        
        closest = 2**31
        for i in range(n-2):
            #就是从小到大依次取3个数，i，最左，最右
            l,r = i+1,n-1
            while l<r:
                sums =  numbers[l] + numbers[r] + numbers[i]
                if sums > target:
                    r -= 1
                elif sums == target:
                    return target
                else:
                    l += 1
                #打擂台
                closest = sums if abs(sums - target) < abs(closest - target) else closest
        return closest
                
            