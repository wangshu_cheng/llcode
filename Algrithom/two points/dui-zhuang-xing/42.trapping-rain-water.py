'''
Given n non-negative integers representing an elevation map where the width of each bar is 1, 
compute how much water it is able to trap after raining.

Trapping Rain Water

Example
Given [0,1,0,2,1,0,1,3,2,1,2,1], return 6.
'''

class Solution:
    # @param heights: a list of integers
    # @return: a integer
    def trapRainWater(self, heights):
        if not heights or len(heights) == 0:
            return 0
        #初始化两个指针
        left,right = 0,len(heights) - 1
        smaller = 0
        area = 0
        #while 循环
        while left < right:
            #满足某条件
            if heights[left] < heights[right]:
                #do sth.
                #关于蓄水的问题，记住一定是洼地才有水
                #如何才能判定是洼地呢：
                # 条件1：眼前的柱子比远处的柱子要低
                # 条件2: 下一根柱子比眼前的要低
                smaller = heights[left]
                while left < right and heights[left] <= smaller:
                    area += smaller - heights[left]
                    left += 1
            else:
                smaller = heights[right]
                while left < right and heights[right] <= smaller:
                    area += smaller - heights[right]
                    right -= 1
        return area
   

    # def trapRainWater(self, heights):
    #     # write your code here
    #     if not heights:return 0
    #     water = 0
    #     m = len(heights)
    #     max_left,max_right = heights[0],heights[m-1]
    #     left,right = 0,m-1
    #     while left <= right:
    #         if heights[left] <= heights[right]:
    #             if heights[left] < max_left:
    #                 water += (max_left-heights[left])
    #             else:
    #                 max_left = max(max_left,heights[left])
    #             left += 1
    #         else:
    #             if heights[right] < max_right:
    #                 water += (max_right-heights[right])
    #             else:
    #                 max_right = max(max_right,heights[right])
    #             right -= 1
    #     return water
