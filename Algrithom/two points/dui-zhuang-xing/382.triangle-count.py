'''
Given an array of integers, how many three numbers can be found in the array, so that we can
 build an triangle whose three edges length is the three numbers that we find?

Have you met this question in a real interview? Yes
Example
Given array S = [3,4,6,7], return 3. They are:

[3,4,6]
[3,6,7]
[4,6,7]
Given array S = [4,4,4,4], return 4. They are:

[4(1),4(2),4(3)]
[4(1),4(2),4(4)]
[4(1),4(3),4(4)]
[4(2),4(3),4(4)]
'''

class Solution:
    # @param S: a list of integers
    # @return: a integer

其实 method1 和2 一样
#method1
    def triangleCount(self, S):
        # write your code here
        if not S:
            return 0
        count = 0
        S = sorted(S)
        for i in range(len(S)-1,1,-1):
            large = S[i]
            j,k = 0,i - 1
            while j < k:
                if S[j] + S[k] > large:
                    count += k - j
                    k -= 1
                else:
                    j += 1
        return count

#method2
    def triangleCount(self, S):
        # write your code here
        if not S:return 0
        
        nums = sorted(S)
        res = 0
        
        for i in xrange(2,len(S)):
            left,right = 0,i-1
            thisSide = nums[i]
            while left < right:
                if nums[left] + nums[right] > thisSide:
                    res += right - left
                    right -= 1
                else:
                    left += 1
        return res
                    
