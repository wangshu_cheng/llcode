'''
Given an array of integers, find how many pairs in the array such that their sum 
is bigger than target
Example
Given numbers = [2, 7, 11, 15], target = 24. Return 1. (11 + 15 is the only pair)
'''






class Solution:
    # @param nums, an array of integer
    # @param target, an integer
    # @return an integer
    def twoSum2(self, nums, target):
        # Write your code here
        if not nums:return 0
        nums = sorted(nums)
        n = len(nums)
        left,right = 0,n-1
        res = 0
        while left < right:
            if nums[left] + nums[right] > target:
            # 如果left 和 右的和大于target，那么从left 到 right中间所有的数和right的pair都大于target
            #所以一共有 right  - left个 pair
               res += right - left
            #right 减1，再试探新的组合
               right -= 1
            else:
               left += 1
        return res
        