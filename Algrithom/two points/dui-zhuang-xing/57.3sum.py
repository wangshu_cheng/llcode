'''
Given an array S of n integers, are there elements a, b, c in S 
such that a + b + c = 0? Find all unique triplets in the array 
which gives the sum of zero.
'''
'''
The question is ask about the value not index, so feel free to sort :)
'''

class Solution(object):
    '''
        题意：求数列中三个数之和为0的三元组有多少个，需去重
        暴力枚举三个数复杂度为O(N^3)
        先考虑2Sum的做法，假设升序数列a，对于一组解ai,aj, 另一组解ak,al 
        必然满足 i<k j>l 或 i>k j<l, 因此我们可以用两个指针，初始时指向数列两端
        指向数之和大于目标值时，右指针向左移使得总和减小，反之左指针向右移
        由此可以用O(N)的复杂度解决2Sum问题，3Sum则枚举第一个数O(N^2)
        使用有序数列的好处是，在枚举和移动指针时值相等的数可以跳过，省去去重部分
    '''
    def threeSum(self, nums):

        if not nums or len(nums) < 3:
            return []

        nums.sort()
        res = []
        length = len(nums)    -5 -4 1 3 

        # 注意 结尾是 length - 2，不是 length，可能丢分
        for i in range(0, length - 2):
            #注意如果重复的就跳过，不然会有重复答案
            if i > 0 and nums[i] == nums[i - 1]:
                continue

            target = nums[i] * -1
            left, right = i + 1, length - 1
            while left < right:
                if nums[left] + nums[right] == target:
                    res.append([nums[i], nums[left], nums[right]])
                    #必须要让指针走起来，否则死循环；
                    #这里2个指针要同时向内移动，因为任何一个指针都不可能和其他的配对了。
                    right -= 1
                    left += 1
                    #注意这里也要跳过重复的值
                    while left < right and nums[left] == nums[left - 1]:
                        left += 1
                    while left < right and nums[right] == nums[right + 1]:
                        right -= 1
                elif nums[left] + nums[right] > target:
                    right -= 1
                else:
                    left += 1
        return res

    #method2 use set
class Solution(object):
    def threeSum(self, nums):
        """
        :type nums: List[int]
        :rtype: List[List[int]]
        """
        if not nums or len(nums) < 3:
            return []
            
        res = set()
        nums = sorted(nums)
        for i in range(len(nums) - 2):
            target = -nums[i]
            j,k = i+1,len(nums) - 1
            while j < k:
                if nums[j] + nums[k] == target:
                    res.add((nums[i],nums[j],nums[k]))
                    k -= 1
                    j += 1
                elif nums[j] + nums[k] > target:
                    k -= 1
                else:
                    j += 1
        return list(res)