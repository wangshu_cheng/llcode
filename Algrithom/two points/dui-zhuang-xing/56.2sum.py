'''
Given an array of integers, find two numbers such that they add up to a specific target number.

The function twoSum should return indices of the two numbers such that they add up to the target,
 where index1 must be less than index2. Please note that your returned answers (both index1 and index2) 
 are NOT zero-based.
'''

class Solution:
    """
    @param numbers : An array of Integer
    @param target : target = numbers[index1] + numbers[index2]
    @return : [index1 + 1, index2 + 1] (index1 < index2)
    """

    '''
    如果题目要求要index，那么几乎只能用hash，不能用 sorted array + 双指针
    *(虽然也可以用 带index的 array （array of tuple (value,index） ) 解决
     但是那样用了额外空间，还不如hash 方法)
    '''

    def twoSum(self, numbers, target):
        # write your code here
     	if not numbers or len(numbers) < 2:
     		return
            
        # it's not sorted, so can only hash table
        hash = {}
        res = []
        for i in range(len(numbers)):
            if target - numbers[i] in hash:
                res.append(i+1)
                res.append(hash[target - numbers[i]]+1)
                return sorted(res)
            if numbers[i] not in hash:
                hash[numbers[i]] = i
        return res
            
     '''
     如果题目要求不能有额外空间；而且又不要求return index， 那么可以用sorted array + 双指针方法，

     *(虽然也可以用 带index的 array （array of tuple (value,index） ) 解决
     但是那样用了额外空间，还不如hash 方法)
     '''
     
     def towSum2(self,numbers,target):
     	if not numbers or len(numbers) < 2:
     		return
     	numbers = sorted(numbers)

     	l,r = 0, len(numbers) - 1
     	res = [0,0] #first index, last index

     	while l < r :
     		total = numbers[l] + numbers[r]
     		if total == target:
     			res = [l,r]
     			break
     		elif total < target:
     			l += 1
     		else:
     			r -= 1

     	return res

               
                
