两个指针
a. 对撞型 (2 sum 类 和 partition 类)
b. 前向型 (窗口类， 快慢类）
c. 两个数组，两个指针 (并行)如 longest common string


对撞型指针题目
（
*陈老师：这一类通过对撞型指针优化算法，根本上其实要证明就是不用扫描多余状态
）
2 Sum 类 （通过判断条件优化算法）
• 3 Sum Closest
• 4 Sum
• 3 Sum
• k sum
• Two sum II
• Triangle Count
• Trapping Rain Water
• Container With Most Water

Partition 类
• Partition-array
• Sort Colors
• Partition Array by Odd and Even
• Sort Letters by Case
• Valid Palindrome
• quick sort/ quick select/ nuts bolts problem

前向型指针题目
窗口类：
➢ Remove Nth Node From End of List
➢ minimum-size-subarray-sum
➢ Minimum Window Substring
➢ Longest Substring with At Most K Distinct Characters
➢ Longest Substring Without Repeating Characters
快慢类（用于link list）：
➢ Find the Middle of Linked List
➢ Linked List Cycle I, II

两个数组，两个指针题目
longest common string



Two sum 和灌水题目思路：

Two sum:

if (A[i] + A[j] > sum):
	j -= 1
	do sth
elif (A[i] + A[j] < sum):
	i += 1
	do sth
else:
	so sth
	i += 1 or j -= 1

灌水：
if (A[i] > A[j]):
	j -= 1
	do sth
elif (A[i] < A[j]):
	i += 1
	do sth
else:
	so sth
	i += 1 or j -= 1

总的来说，这一类通过对撞型指针优化算法，根本上其实要证明就是不用扫描多余状态

可以总结为一个模板：
if (A[i] 和 A[j] 满足某个条件):
	j -= 1
	do sth
elif (A[i] 和 A[j] 不满足某个条件):
	i += 1
	do sth
else:
	do sth
	i += 1 or j -= 1


