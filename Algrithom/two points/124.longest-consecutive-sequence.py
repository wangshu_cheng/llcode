'''
Given an unsorted array of integers, find the length of the longest consecutive elements sequence.

注意，这题是求最长的连续element 序列，就是值要连续，坐标不要求连续

Your algorithm should run in O(n) complexity.

Example
Given [100, 4, 200, 1, 3, 2],
The longest consecutive elements sequence is [1, 2, 3, 4]. Return its length: 4.

知识点：

这题用的是类似双指针方法，2个浮动指针左右搜
updown 算最长连续seq 方法，似乎只能背了。

'''

class Solution:
    """
    @param num, a list of integer
    @return an integer
    """
    def longestConsecutive(self, num):
        # write your code here
        # Firstly we need to store everyting to hash
        # Then we can : Easy solution is to sort the array then get the consecutive elements
        # 2nd solution is hard to think which is, for each i, we check up and down
        # and max result is up - down -1
        
        if not num:
            return 0
        
        dic = {}
        for i in num:
            dic[i] = 1
        # if you know how to remove key, you can do my_dict.pop("key", None)
        longest = 0
        for e in num:
            up = e + 1
            while up in dic and dic[up] > 0:
                dic[up] = 0
                up += 1
            down = e - 1
            while down in dic and dic[down] > 0:
                dic[down] = 0
                down -= 1
            longest = max(longest,up - down - 1)
        return longest