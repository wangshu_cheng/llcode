'''
An increasing continuous subsequence:

Can be from right to left or from left to right.
Indices of the integers in the subsequence should be continuous.

下标连续的递增或递减序列，continous subsequence 就是subarray

凡是连续的子序列，或者说subarray，都可 O（n） 做，不需要DP，DP往往是O(n2)

idea: search from left - > right, right - > left respectfully

'''

class Solution:
    # @param {int[]} A an array of Integer
    # @return {int}  an integer
    def longestIncreasingContinuousSubsequence(self, A):
        # Write your code here
        if not A:
            return 0
        #don't forget this.
        if len(A) == 1:
            return 1
        
        max_len = 0
        tmp = 0
        for i in range(1,len(A)):
            if A[i] >= A[i - 1]:
                tmp += 1
                max_len = max(max_len,tmp)
            else:
                tmp = 0
        # max_len = max_len + 1
        tmp = 0
        for j in range(len(A)-1,-1,-1):
            if A[j-1] >= A[j]:
                tmp += 1
                max_len = max(max_len,tmp)
            else:
                tmp = 0
        
        
        return max_len + 1
