'''

Given a sequence of integers, find the longest increasing subsequence (LIS).

You code should return the length of the LIS.

Have you met this question in a real interview? Yes
Clarification
What's the definition of longest increasing subsequence?

The longest increasing subsequence problem is to find a subsequence of a given sequence in which the subsequence's elements are in sorted order, lowest to highest, and in which the subsequence is as long as possible. This subsequence is not necessarily contiguous, or unique.

https://en.wikipedia.org/wiki/Longest_increasing_subsequence

Example
For `[5, 4, 1, 2, 3]`, the LIS is [1, 2, 3], return `3`

For `[4, 2, 4, 5, 3, 7]`, the LIS is `[2, 4, 5, 7]`, return `4`

Thinking:
这题有奇技淫巧： 通过一个数组，来存放从小到大的序列。然后得到它的长度即可。

方法：先把这个数组都填满 maxint， 然后逐一遍历题目给的数组； 把第一个比目标元素大的位置填上目标元素，比如：

utility array: [-1,max,max,max...]
target array:[5,1,2,4]
然后，几轮比较下来， untility array:

[-1,5,max,max...]
[-1,1,max,max..]
[-1,1,2,max..]
[-1,1,2,4..]
最后求出最后一个不是max的index 就是答案，3.



设计一个数组，先填满很大的数；


'''

class Solution:
    """
    @param nums: The integer array
    @return: The length of LIS (longest increasing subsequence)
    """


    '''
    1 binary search:

    '''
    import sys
    def longestIncreasingSubsequence(self, nums):
        
        def binarySearch(nums,k):
            start, end = 0, len(nums) - 1
            while start + 1 < end:
                mid = start + (end - start) / 2
                if nums[mid] > k:
                    end = mid
                else:
                    start = mid
            if nums[start] > k:
                return start
            if nums[end] > k:
                return end
            
        
        # write your code here
        if not nums:
            return 0
            
        n = len(nums)
        minLast = [sys.maxint for i in range(n + 1)]
        #这里加一个-1 方便计算。
        minLast[0] = -1
        
        for i in range(n):
            index = binarySearch(minLast,nums[i])
            minLast[index] = nums[i]
        
        for i in range(n ,0,-1):
            if minLast[i] != sys.maxint:
                return i
        return 0




    '''
    1 DP Solution, 
    function: loop i , for each i , loop j to i, count how many nums[j] < nums[i]
    '''

    def longestIncreasingSubsequence(self, nums):
        if not nums:
            return 0
        if len(nums) == 1:
            return 1
        
        n = len(nums)
        #init
        f = [1 for i in xrange(n)]
        max_value = 0
        for i in xrange(1,n):
            for j in xrange(i):
                #function
                if nums[j] <= nums[i]:
                    f[i] = max(f[i],f[j] + 1)
                    max_value = max(max_value,f[i])
        return max_value
