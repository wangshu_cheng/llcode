 '''
Compare with major1, the difference is it's need 1/3 occurence instead of 1/2
 '''

'''
Given an array of integers, the majority number is the number that occurs more than 1/3 of the size of the array.

Find it.
'''

 def majorityNumber(self, nums):
        candidate1, count1 = None, 0
        candidate2, count2 = None, 0
        for num in nums:
            if candidate1 == num:
                count1 += 1
            elif candidate2 == num:
                count2 += 1
            elif count1 == 0:
                count1 += 1
                candidate1 = num
            elif count2 == 0:
                count2 += 1
                candidate2 = num
            else:
                count1 -= 1
                count2 -= 1
    
        count1, count2 = 0, 0
        for num in nums:
            if candidate1 == num:
                count1 += 1
            elif candidate2 == num:
                count2 += 1
    
        return candidate1 if count1 > count2 else candidate2