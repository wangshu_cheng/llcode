'''
Given an array of integers, the majority number is the number that occurs more than half of the size of the array. 
Find it.
Thinking: there is only 1 element which is majority number, so there is 2 kind of elements:
1 Is majority
2 Is NOT majority

We use count to count the occurance of each element, if count > 0 at last, then it is major.

'''
class Solution:
    """
    @param nums: A list of integers
    @return: The majority number
    """
    def majorityNumber(self, nums):
        # write your code here
        count = 0
        candidate = -1
        for i in range(len(nums)):
            if count == 0:
                # if current candidate count ==0, we will change to another candidate
                candidate = nums[i]
                count += 1
            elif candidate == nums[i]:
                count += 1
            else:
                count -= 1
        return candidate
