'''
There are N gas stations along a circular route, where the amount of gas at station i is gas[i].

You have a car with an unlimited gas tank and it costs cost[i] of gas to travel from station i to
 its next station (i+1). You begin the journey with an empty tank at one of the gas stations.

Return the starting gas station's index if you can travel around the circuit once, otherwise return -1.

Example
Given 4 gas stations with gas[i]=[1,1,3,1], and the cost[i]=[2,2,1,1]. The starting gas station's index is 2.

Thinking:

从头到尾扫一遍，看看什么时候汽油箱 为负，那么就从那里重新开始，只要找到一次总邮箱不为负的，那个index 就是 起点，否则返回 -1

'''

class Solution:
    # @param gas, a list of integers
    # @param cost, a list of integers
    # @return an integer
    def canCompleteCircuit(self, gas, cost):
        # write your code here
        if not gas or not cost:
            return -1
        
        total = 0
        sum = 0
        index = -1
        for i in range(len(gas)):
            sum += gas[i] - cost[i] #保证每一次都能走到下一站
            total += gas[i] - cost[i]#保证能走一个圈
            
            if sum < 0:
                sum = 0
                index = i
                
        return index + 1 if total >= 0 else -1





